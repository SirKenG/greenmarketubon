"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*
 *
 * More info at [www.dropzonejs.com](http://www.dropzonejs.com)
 *
 * Copyright (c) 2012, Matias Meno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

// The Emitter class provides the ability to call `.on()` on Dropzone to listen
// to events.
// It is strongly based on component's emitter class, and I removed the
// functionality because of the dependency hell with different frameworks.
var Emitter = function () {
  function Emitter() {
    _classCallCheck(this, Emitter);
  }

  _createClass(Emitter, [{
    key: "on",

    // Add an event listener for given event
    value: function on(event, fn) {
      this._callbacks = this._callbacks || {};
      // Create namespace for this event
      if (!this._callbacks[event]) {
        this._callbacks[event] = [];
      }
      this._callbacks[event].push(fn);
      return this;
    }
  }, {
    key: "emit",
    value: function emit(event) {
      this._callbacks = this._callbacks || {};
      var callbacks = this._callbacks[event];

      if (callbacks) {
        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
          args[_key - 1] = arguments[_key];
        }

        for (var _iterator = callbacks, _isArray = true, _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
          var _ref;

          if (_isArray) {
            if (_i >= _iterator.length) break;
            _ref = _iterator[_i++];
          } else {
            _i = _iterator.next();
            if (_i.done) break;
            _ref = _i.value;
          }

          var callback = _ref;

          callback.apply(this, args);
        }
      }

      return this;
    }

    // Remove event listener for given event. If fn is not provided, all event
    // listeners for that event will be removed. If neither is provided, all
    // event listeners will be removed.

  }, {
    key: "off",
    value: function off(event, fn) {
      if (!this._callbacks || arguments.length === 0) {
        this._callbacks = {};
        return this;
      }

      // specific event
      var callbacks = this._callbacks[event];
      if (!callbacks) {
        return this;
      }

      // remove all handlers
      if (arguments.length === 1) {
        delete this._callbacks[event];
        return this;
      }

      // remove specific handler
      for (var i = 0; i < callbacks.length; i++) {
        var callback = callbacks[i];
        if (callback === fn) {
          callbacks.splice(i, 1);
          break;
        }
      }

      return this;
    }
  }]);

  return Emitter;
}();

var Dropzone = function (_Emitter) {
  _inherits(Dropzone, _Emitter);

  _createClass(Dropzone, null, [{
    key: "initClass",
    value: function initClass() {

      // Exposing the emitter class, mainly for tests
      this.prototype.Emitter = Emitter;

      /*
       This is a list of all available events you can register on a dropzone object.
        You can register an event handler like this:
        dropzone.on("dragEnter", function() { });
        */
      this.prototype.events = ["drop", "dragstart", "dragend", "dragenter", "dragover", "dragleave", "addedfile", "addedfiles", "removedfile", "thumbnail", "error", "errormultiple", "processing", "processingmultiple", "uploadprogress", "totaluploadprogress", "sending", "sendingmultiple", "success", "successmultiple", "canceled", "canceledmultiple", "complete", "completemultiple", "reset", "maxfilesexceeded", "maxfilesreached", "queuecomplete"];

      this.prototype.defaultOptions = {
        /**
         * Has to be specified on elements other than form (or when the form
         * doesn't have an `action` attribute). You can also
         * provide a function that will be called with `files` and
         * must return the url (since `v3.12.0`)
         */
        url: null,

        /**
         * Can be changed to `"put"` if necessary. You can also provide a function
         * that will be called with `files` and must return the method (since `v3.12.0`).
         */
        method: "post",

        /**
         * Will be set on the XHRequest.
         */
        withCredentials: false,

        /**
         * The timeout for the XHR requests in milliseconds (since `v4.4.0`).
         */
        timeout: 30000,

        /**
         * How many file uploads to process in parallel (See the
         * Enqueuing file uploads* documentation section for more info)
         */
        parallelUploads: 2,

        /**
         * Whether to send multiple files in one request. If
         * this it set to true, then the fallback file input element will
         * have the `multiple` attribute as well. This option will
         * also trigger additional events (like `processingmultiple`). See the events
         * documentation section for more information.
         */
        uploadMultiple: false,

        /**
         * Whether you want files to be uploaded in chunks to your server. This can't be
         * used in combination with `uploadMultiple`.
         *
         * See [chunksUploaded](#config-chunksUploaded) for the callback to finalise an upload.
         */
        chunking: false,

        /**
         * If `chunking` is enabled, this defines whether **every** file should be chunked,
         * even if the file size is below chunkSize. This means, that the additional chunk
         * form data will be submitted and the `chunksUploaded` callback will be invoked.
         */
        forceChunking: false,

        /**
         * If `chunking` is `true`, then this defines the chunk size in bytes.
         */
        chunkSize: 2000000,

        /**
         * If `true`, the individual chunks of a file are being uploaded simultaneously.
         */
        parallelChunkUploads: false,

        /**
         * Whether a chunk should be retried if it fails.
         */
        retryChunks: false,

        /**
         * If `retryChunks` is true, how many times should it be retried.
         */
        retryChunksLimit: 3,

        /**
         * If not `null` defines how many files this Dropzone handles. If it exceeds,
         * the event `maxfilesexceeded` will be called. The dropzone element gets the
         * class `dz-max-files-reached` accordingly so you can provide visual feedback.
         */
        maxFilesize: 256,

        /**
         * The name of the file param that gets transferred.
         * **NOTE**: If you have the option  `uploadMultiple` set to `true`, then
         * Dropzone will append `[]` to the name.
         */
        paramName: "file",

        /**
         * Whether thumbnails for images should be generated
         */
        createImageThumbnails: true,

        /**
         * In MB. When the filename exceeds this limit, the thumbnail will not be generated.
         */
        maxThumbnailFilesize: 10,

        /**
         * If `null`, the ratio of the image will be used to calculate it.
         */
        thumbnailWidth: 120,

        /**
         * The same as `thumbnailWidth`. If both are null, images will not be resized.
         */
        thumbnailHeight: 120,

        /**
         * How the images should be scaled down in case both, `thumbnailWidth` and `thumbnailHeight` are provided.
         * Can be either `contain` or `crop`.
         */
        thumbnailMethod: 'crop',

        /**
         * If set, images will be resized to these dimensions before being **uploaded**.
         * If only one, `resizeWidth` **or** `resizeHeight` is provided, the original aspect
         * ratio of the file will be preserved.
         *
         * The `options.transformFile` function uses these options, so if the `transformFile` function
         * is overridden, these options don't do anything.
         */
        resizeWidth: null,

        /**
         * See `resizeWidth`.
         */
        resizeHeight: null,

        /**
         * The mime type of the resized image (before it gets uploaded to the server).
         * If `null` the original mime type will be used. To force jpeg, for example, use `image/jpeg`.
         * See `resizeWidth` for more information.
         */
        resizeMimeType: null,

        /**
         * The quality of the resized images. See `resizeWidth`.
         */
        resizeQuality: 0.8,

        /**
         * How the images should be scaled down in case both, `resizeWidth` and `resizeHeight` are provided.
         * Can be either `contain` or `crop`.
         */
        resizeMethod: 'contain',

        /**
         * The base that is used to calculate the filesize. You can change this to
         * 1024 if you would rather display kibibytes, mebibytes, etc...
         * 1024 is technically incorrect, because `1024 bytes` are `1 kibibyte` not `1 kilobyte`.
         * You can change this to `1024` if you don't care about validity.
         */
        filesizeBase: 1000,

        /**
         * Can be used to limit the maximum number of files that will be handled by this Dropzone
         */
        maxFiles: null,

        /**
         * An optional object to send additional headers to the server. Eg:
         * `{ "My-Awesome-Header": "header value" }`
         */
        headers: null,

        /**
         * If `true`, the dropzone element itself will be clickable, if `false`
         * nothing will be clickable.
         *
         * You can also pass an HTML element, a CSS selector (for multiple elements)
         * or an array of those. In that case, all of those elements will trigger an
         * upload when clicked.
         */
        clickable: true,

        /**
         * Whether hidden files in directories should be ignored.
         */
        ignoreHiddenFiles: true,

        /**
         * The default implementation of `accept` checks the file's mime type or
         * extension against this list. This is a comma separated list of mime
         * types or file extensions.
         *
         * Eg.: `image/*,application/pdf,.psd`
         *
         * If the Dropzone is `clickable` this option will also be used as
         * [`accept`](https://developer.mozilla.org/en-US/docs/HTML/Element/input#attr-accept)
         * parameter on the hidden file input as well.
         */
        acceptedFiles: null,

        /**
         * **Deprecated!**
         * Use acceptedFiles instead.
         */
        acceptedMimeTypes: null,

        /**
         * If false, files will be added to the queue but the queue will not be
         * processed automatically.
         * This can be useful if you need some additional user input before sending
         * files (or if you want want all files sent at once).
         * If you're ready to send the file simply call `myDropzone.processQueue()`.
         *
         * See the [enqueuing file uploads](#enqueuing-file-uploads) documentation
         * section for more information.
         */
        autoProcessQueue: true,

        /**
         * If false, files added to the dropzone will not be queued by default.
         * You'll have to call `enqueueFile(file)` manually.
         */
        autoQueue: true,

        /**
         * If `true`, this will add a link to every file preview to remove or cancel (if
         * already uploading) the file. The `dictCancelUpload`, `dictCancelUploadConfirmation`
         * and `dictRemoveFile` options are used for the wording.
         */
        addRemoveLinks: false,

        /**
         * Defines where to display the file previews – if `null` the
         * Dropzone element itself is used. Can be a plain `HTMLElement` or a CSS
         * selector. The element should have the `dropzone-previews` class so
         * the previews are displayed properly.
         */
        previewsContainer: null,

        /**
         * This is the element the hidden input field (which is used when clicking on the
         * dropzone to trigger file selection) will be appended to. This might
         * be important in case you use frameworks to switch the content of your page.
         */
        hiddenInputContainer: "body",

        /**
         * If null, no capture type will be specified
         * If camera, mobile devices will skip the file selection and choose camera
         * If microphone, mobile devices will skip the file selection and choose the microphone
         * If camcorder, mobile devices will skip the file selection and choose the camera in video mode
         * On apple devices multiple must be set to false.  AcceptedFiles may need to
         * be set to an appropriate mime type (e.g. "image/*", "audio/*", or "video/*").
         */
        capture: null,

        /**
         * **Deprecated**. Use `renameFile` instead.
         */
        renameFilename: null,

        /**
         * A function that is invoked before the file is uploaded to the server and renames the file.
         * This function gets the `File` as argument and can use the `file.name`. The actual name of the
         * file that gets used during the upload can be accessed through `file.upload.filename`.
         */
        renameFile: null,

        /**
         * If `true` the fallback will be forced. This is very useful to test your server
         * implementations first and make sure that everything works as
         * expected without dropzone if you experience problems, and to test
         * how your fallbacks will look.
         */
        forceFallback: false,

        /**
         * The text used before any files are dropped.
         */
        dictDefaultMessage: "Drop files here to upload",

        /**
         * The text that replaces the default message text it the browser is not supported.
         */
        dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",

        /**
         * The text that will be added before the fallback form.
         * If you provide a  fallback element yourself, or if this option is `null` this will
         * be ignored.
         */
        dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",

        /**
         * If the filesize is too big.
         * `{{filesize}}` and `{{maxFilesize}}` will be replaced with the respective configuration values.
         */
        dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",

        /**
         * If the file doesn't match the file type.
         */
        dictInvalidFileType: "You can't upload files of this type.",

        /**
         * If the server response was invalid.
         * `{{statusCode}}` will be replaced with the servers status code.
         */
        dictResponseError: "Server responded with {{statusCode}} code.",

        /**
         * If `addRemoveLinks` is true, the text to be used for the cancel upload link.
         */
        dictCancelUpload: "Cancel upload",

        /**
         * If `addRemoveLinks` is true, the text to be used for confirmation when cancelling upload.
         */
        dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",

        /**
         * If `addRemoveLinks` is true, the text to be used to remove a file.
         */
        dictRemoveFile: "Remove file",

        /**
         * If this is not null, then the user will be prompted before removing a file.
         */
        dictRemoveFileConfirmation: null,

        /**
         * Displayed if `maxFiles` is st and exceeded.
         * The string `{{maxFiles}}` will be replaced by the configuration value.
         */
        dictMaxFilesExceeded: "You can not upload any more files.",

        /**
         * Allows you to translate the different units. Starting with `tb` for terabytes and going down to
         * `b` for bytes.
         */
        dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },

        /**
         * Called when dropzone initialized
         * You can add event listeners here
         */
        init: function init() {},


        /**
         * Can be an **object** of additional parameters to transfer to the server, **or** a `Function`
         * that gets invoked with the `files`, `xhr` and, if it's a chunked upload, `chunk` arguments. In case
         * of a function, this needs to return a map.
         *
         * The default implementation does nothing for normal uploads, but adds relevant information for
         * chunked uploads.
         *
         * This is the same as adding hidden input fields in the form element.
         */
        params: function params(files, xhr, chunk) {
          if (chunk) {
            return {
              dzuuid: chunk.file.upload.uuid,
              dzchunkindex: chunk.index,
              dztotalfilesize: chunk.file.size,
              dzchunksize: this.options.chunkSize,
              dztotalchunkcount: chunk.file.upload.totalChunkCount,
              dzchunkbyteoffset: chunk.index * this.options.chunkSize
            };
          }
        },


        /**
         * A function that gets a [file](https://developer.mozilla.org/en-US/docs/DOM/File)
         * and a `done` function as parameters.
         *
         * If the done function is invoked without arguments, the file is "accepted" and will
         * be processed. If you pass an error message, the file is rejected, and the error
         * message will be displayed.
         * This function will not be called if the file is too big or doesn't match the mime types.
         */
        accept: function accept(file, done) {
          return done();
        },


        /**
         * The callback that will be invoked when all chunks have been uploaded for a file.
         * It gets the file for which the chunks have been uploaded as the first parameter,
         * and the `done` function as second. `done()` needs to be invoked when everything
         * needed to finish the upload process is done.
         */
        chunksUploaded: function chunksUploaded(file, done) {
          done();
        },

        /**
         * Gets called when the browser is not supported.
         * The default implementation shows the fallback input field and adds
         * a text.
         */
        fallback: function fallback() {
          // This code should pass in IE7... :(
          var messageElement = void 0;
          this.element.className = this.element.className + " dz-browser-not-supported";

          for (var _iterator2 = this.element.getElementsByTagName("div"), _isArray2 = true, _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator]();;) {
            var _ref2;

            if (_isArray2) {
              if (_i2 >= _iterator2.length) break;
              _ref2 = _iterator2[_i2++];
            } else {
              _i2 = _iterator2.next();
              if (_i2.done) break;
              _ref2 = _i2.value;
            }

            var child = _ref2;

            if (/(^| )dz-message($| )/.test(child.className)) {
              messageElement = child;
              child.className = "dz-message"; // Removes the 'dz-default' class
              break;
            }
          }
          if (!messageElement) {
            messageElement = Dropzone.createElement("<div class=\"dz-message\"><span></span></div>");
            this.element.appendChild(messageElement);
          }

          var span = messageElement.getElementsByTagName("span")[0];
          if (span) {
            if (span.textContent != null) {
              span.textContent = this.options.dictFallbackMessage;
            } else if (span.innerText != null) {
              span.innerText = this.options.dictFallbackMessage;
            }
          }

          return this.element.appendChild(this.getFallbackForm());
        },


        /**
         * Gets called to calculate the thumbnail dimensions.
         *
         * It gets `file`, `width` and `height` (both may be `null`) as parameters and must return an object containing:
         *
         *  - `srcWidth` & `srcHeight` (required)
         *  - `trgWidth` & `trgHeight` (required)
         *  - `srcX` & `srcY` (optional, default `0`)
         *  - `trgX` & `trgY` (optional, default `0`)
         *
         * Those values are going to be used by `ctx.drawImage()`.
         */
        resize: function resize(file, width, height, resizeMethod) {
          var info = {
            srcX: 0,
            srcY: 0,
            srcWidth: file.width,
            srcHeight: file.height
          };

          var srcRatio = file.width / file.height;

          // Automatically calculate dimensions if not specified
          if (width == null && height == null) {
            width = info.srcWidth;
            height = info.srcHeight;
          } else if (width == null) {
            width = height * srcRatio;
          } else if (height == null) {
            height = width / srcRatio;
          }

          // Make sure images aren't upscaled
          width = Math.min(width, info.srcWidth);
          height = Math.min(height, info.srcHeight);

          var trgRatio = width / height;

          if (info.srcWidth > width || info.srcHeight > height) {
            // Image is bigger and needs rescaling
            if (resizeMethod === 'crop') {
              if (srcRatio > trgRatio) {
                info.srcHeight = file.height;
                info.srcWidth = info.srcHeight * trgRatio;
              } else {
                info.srcWidth = file.width;
                info.srcHeight = info.srcWidth / trgRatio;
              }
            } else if (resizeMethod === 'contain') {
              // Method 'contain'
              if (srcRatio > trgRatio) {
                height = width / srcRatio;
              } else {
                width = height * srcRatio;
              }
            } else {
              throw new Error("Unknown resizeMethod '" + resizeMethod + "'");
            }
          }

          info.srcX = (file.width - info.srcWidth) / 2;
          info.srcY = (file.height - info.srcHeight) / 2;

          info.trgWidth = width;
          info.trgHeight = height;

          return info;
        },


        /**
         * Can be used to transform the file (for example, resize an image if necessary).
         *
         * The default implementation uses `resizeWidth` and `resizeHeight` (if provided) and resizes
         * images according to those dimensions.
         *
         * Gets the `file` as the first parameter, and a `done()` function as the second, that needs
         * to be invoked with the file when the transformation is done.
         */
        transformFile: function transformFile(file, done) {
          if ((this.options.resizeWidth || this.options.resizeHeight) && file.type.match(/image.*/)) {
            return this.resizeImage(file, this.options.resizeWidth, this.options.resizeHeight, this.options.resizeMethod, done);
          } else {
            return done(file);
          }
        },


        /**
         * A string that contains the template used for each dropped
         * file. Change it to fulfill your needs but make sure to properly
         * provide all elements.
         *
         * If you want to use an actual HTML element instead of providing a String
         * as a config option, you could create a div with the id `tpl`,
         * put the template inside it and provide the element like this:
         *
         *     document
         *       .querySelector('#tpl')
         *       .innerHTML
         *
         */
        previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-image\"><img data-dz-thumbnail /></div>\n  <div class=\"dz-details\">\n    <div class=\"dz-size\"><span data-dz-size></span></div>\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n  </div>\n  <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n  <div class=\"dz-success-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Check</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <path d=\"M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" stroke-opacity=\"0.198794158\" stroke=\"#747474\" fill-opacity=\"0.816519475\" fill=\"#FFFFFF\" sketch:type=\"MSShapeGroup\"></path>\n      </g>\n    </svg>\n  </div>\n  <div class=\"dz-error-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Error</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <g id=\"Check-+-Oval-2\" sketch:type=\"MSLayerGroup\" stroke=\"#747474\" stroke-opacity=\"0.198794158\" fill=\"#FFFFFF\" fill-opacity=\"0.816519475\">\n          <path d=\"M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" sketch:type=\"MSShapeGroup\"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>",

        // END OPTIONS
        // (Required by the dropzone documentation parser)


        /*
         Those functions register themselves to the events on init and handle all
         the user interface specific stuff. Overwriting them won't break the upload
         but can break the way it's displayed.
         You can overwrite them if you don't like the default behavior. If you just
         want to add an additional event handler, register it on the dropzone object
         and don't overwrite those options.
         */

        // Those are self explanatory and simply concern the DragnDrop.
        drop: function drop(e) {
          return this.element.classList.remove("dz-drag-hover");
        },
        dragstart: function dragstart(e) {},
        dragend: function dragend(e) {
          return this.element.classList.remove("dz-drag-hover");
        },
        dragenter: function dragenter(e) {
          return this.element.classList.add("dz-drag-hover");
        },
        dragover: function dragover(e) {
          return this.element.classList.add("dz-drag-hover");
        },
        dragleave: function dragleave(e) {
          return this.element.classList.remove("dz-drag-hover");
        },
        paste: function paste(e) {},


        // Called whenever there are no files left in the dropzone anymore, and the
        // dropzone should be displayed as if in the initial state.
        reset: function reset() {
          return this.element.classList.remove("dz-started");
        },


        // Called when a file is added to the queue
        // Receives `file`
        addedfile: function addedfile(file) {
          var _this2 = this;

          if (this.element === this.previewsContainer) {
            this.element.classList.add("dz-started");
          }

          if (this.previewsContainer) {
            file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
            file.previewTemplate = file.previewElement; // Backwards compatibility

            this.previewsContainer.appendChild(file.previewElement);
            for (var _iterator3 = file.previewElement.querySelectorAll("[data-dz-name]"), _isArray3 = true, _i3 = 0, _iterator3 = _isArray3 ? _iterator3 : _iterator3[Symbol.iterator]();;) {
              var _ref3;

              if (_isArray3) {
                if (_i3 >= _iterator3.length) break;
                _ref3 = _iterator3[_i3++];
              } else {
                _i3 = _iterator3.next();
                if (_i3.done) break;
                _ref3 = _i3.value;
              }

              var node = _ref3;

              node.textContent = file.name;
            }
            for (var _iterator4 = file.previewElement.querySelectorAll("[data-dz-size]"), _isArray4 = true, _i4 = 0, _iterator4 = _isArray4 ? _iterator4 : _iterator4[Symbol.iterator]();;) {
              if (_isArray4) {
                if (_i4 >= _iterator4.length) break;
                node = _iterator4[_i4++];
              } else {
                _i4 = _iterator4.next();
                if (_i4.done) break;
                node = _i4.value;
              }

              node.innerHTML = this.filesize(file.size);
            }

            if (this.options.addRemoveLinks) {
              file._removeLink = Dropzone.createElement("<a class=\"dz-remove\" href=\"javascript:undefined;\" data-dz-remove>" + this.options.dictRemoveFile + "</a>");
              file.previewElement.appendChild(file._removeLink);
            }

            var removeFileEvent = function removeFileEvent(e) {
              e.preventDefault();
              e.stopPropagation();
              if (file.status === Dropzone.UPLOADING) {
                return Dropzone.confirm(_this2.options.dictCancelUploadConfirmation, function () {
                  return _this2.removeFile(file);
                });
              } else {
                if (_this2.options.dictRemoveFileConfirmation) {
                  return Dropzone.confirm(_this2.options.dictRemoveFileConfirmation, function () {
                    return _this2.removeFile(file);
                  });
                } else {
                  return _this2.removeFile(file);
                }
              }
            };

            for (var _iterator5 = file.previewElement.querySelectorAll("[data-dz-remove]"), _isArray5 = true, _i5 = 0, _iterator5 = _isArray5 ? _iterator5 : _iterator5[Symbol.iterator]();;) {
              var _ref4;

              if (_isArray5) {
                if (_i5 >= _iterator5.length) break;
                _ref4 = _iterator5[_i5++];
              } else {
                _i5 = _iterator5.next();
                if (_i5.done) break;
                _ref4 = _i5.value;
              }

              var removeLink = _ref4;

              removeLink.addEventListener("click", removeFileEvent);
            }
          }
        },


        // Called whenever a file is removed.
        removedfile: function removedfile(file) {
          if (file.previewElement != null && file.previewElement.parentNode != null) {
            file.previewElement.parentNode.removeChild(file.previewElement);
          }
          return this._updateMaxFilesReachedClass();
        },


        // Called when a thumbnail has been generated
        // Receives `file` and `dataUrl`
        thumbnail: function thumbnail(file, dataUrl) {
          if (file.previewElement) {
            file.previewElement.classList.remove("dz-file-preview");
            for (var _iterator6 = file.previewElement.querySelectorAll("[data-dz-thumbnail]"), _isArray6 = true, _i6 = 0, _iterator6 = _isArray6 ? _iterator6 : _iterator6[Symbol.iterator]();;) {
              var _ref5;

              if (_isArray6) {
                if (_i6 >= _iterator6.length) break;
                _ref5 = _iterator6[_i6++];
              } else {
                _i6 = _iterator6.next();
                if (_i6.done) break;
                _ref5 = _i6.value;
              }

              var thumbnailElement = _ref5;

              thumbnailElement.alt = file.name;
              thumbnailElement.src = dataUrl;
            }

            return setTimeout(function () {
              return file.previewElement.classList.add("dz-image-preview");
            }, 1);
          }
        },


        // Called whenever an error occurs
        // Receives `file` and `message`
        error: function error(file, message) {
          if (file.previewElement) {
            file.previewElement.classList.add("dz-error");
            if (typeof message !== "String" && message.error) {
              message = message.error;
            }
            for (var _iterator7 = file.previewElement.querySelectorAll("[data-dz-errormessage]"), _isArray7 = true, _i7 = 0, _iterator7 = _isArray7 ? _iterator7 : _iterator7[Symbol.iterator]();;) {
              var _ref6;

              if (_isArray7) {
                if (_i7 >= _iterator7.length) break;
                _ref6 = _iterator7[_i7++];
              } else {
                _i7 = _iterator7.next();
                if (_i7.done) break;
                _ref6 = _i7.value;
              }

              var node = _ref6;

              node.textContent = message;
            }
          }
        },
        errormultiple: function errormultiple() {},


        // Called when a file gets processed. Since there is a cue, not all added
        // files are processed immediately.
        // Receives `file`
        processing: function processing(file) {
          if (file.previewElement) {
            file.previewElement.classList.add("dz-processing");
            if (file._removeLink) {
              return file._removeLink.textContent = this.options.dictCancelUpload;
            }
          }
        },
        processingmultiple: function processingmultiple() {},


        // Called whenever the upload progress gets updated.
        // Receives `file`, `progress` (percentage 0-100) and `bytesSent`.
        // To get the total number of bytes of the file, use `file.size`
        uploadprogress: function uploadprogress(file, progress, bytesSent) {
          if (file.previewElement) {
            for (var _iterator8 = file.previewElement.querySelectorAll("[data-dz-uploadprogress]"), _isArray8 = true, _i8 = 0, _iterator8 = _isArray8 ? _iterator8 : _iterator8[Symbol.iterator]();;) {
              var _ref7;

              if (_isArray8) {
                if (_i8 >= _iterator8.length) break;
                _ref7 = _iterator8[_i8++];
              } else {
                _i8 = _iterator8.next();
                if (_i8.done) break;
                _ref7 = _i8.value;
              }

              var node = _ref7;

              node.nodeName === 'PROGRESS' ? node.value = progress : node.style.width = progress + "%";
            }
          }
        },


        // Called whenever the total upload progress gets updated.
        // Called with totalUploadProgress (0-100), totalBytes and totalBytesSent
        totaluploadprogress: function totaluploadprogress() {},


        // Called just before the file is sent. Gets the `xhr` object as second
        // parameter, so you can modify it (for example to add a CSRF token) and a
        // `formData` object to add additional information.
        sending: function sending() {},
        sendingmultiple: function sendingmultiple() {},


        // When the complete upload is finished and successful
        // Receives `file`
        success: function success(file) {
          if (file.previewElement) {
            return file.previewElement.classList.add("dz-success");
          }
        },
        successmultiple: function successmultiple() {},


        // When the upload is canceled.
        canceled: function canceled(file) {
          return this.emit("error", file, "Upload canceled.");
        },
        canceledmultiple: function canceledmultiple() {},


        // When the upload is finished, either with success or an error.
        // Receives `file`
        complete: function complete(file) {
          if (file._removeLink) {
            file._removeLink.textContent = this.options.dictRemoveFile;
          }
          if (file.previewElement) {
            return file.previewElement.classList.add("dz-complete");
          }
        },
        completemultiple: function completemultiple() {},
        maxfilesexceeded: function maxfilesexceeded() {},
        maxfilesreached: function maxfilesreached() {},
        queuecomplete: function queuecomplete() {},
        addedfiles: function addedfiles() {}
      };

      this.prototype._thumbnailQueue = [];
      this.prototype._processingThumbnail = false;
    }

    // global utility

  }, {
    key: "extend",
    value: function extend(target) {
      for (var _len2 = arguments.length, objects = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        objects[_key2 - 1] = arguments[_key2];
      }

      for (var _iterator9 = objects, _isArray9 = true, _i9 = 0, _iterator9 = _isArray9 ? _iterator9 : _iterator9[Symbol.iterator]();;) {
        var _ref8;

        if (_isArray9) {
          if (_i9 >= _iterator9.length) break;
          _ref8 = _iterator9[_i9++];
        } else {
          _i9 = _iterator9.next();
          if (_i9.done) break;
          _ref8 = _i9.value;
        }

        var object = _ref8;

        for (var key in object) {
          var val = object[key];
          target[key] = val;
        }
      }
      return target;
    }
  }]);

  function Dropzone(el, options) {
    _classCallCheck(this, Dropzone);

    var _this = _possibleConstructorReturn(this, (Dropzone.__proto__ || Object.getPrototypeOf(Dropzone)).call(this));

    var fallback = void 0,
        left = void 0;
    _this.element = el;
    // For backwards compatibility since the version was in the prototype previously
    _this.version = Dropzone.version;

    _this.defaultOptions.previewTemplate = _this.defaultOptions.previewTemplate.replace(/\n*/g, "");

    _this.clickableElements = [];
    _this.listeners = [];
    _this.files = []; // All files

    if (typeof _this.element === "string") {
      _this.element = document.querySelector(_this.element);
    }

    // Not checking if instance of HTMLElement or Element since IE9 is extremely weird.
    if (!_this.element || _this.element.nodeType == null) {
      throw new Error("Invalid dropzone element.");
    }

    if (_this.element.dropzone) {
      throw new Error("Dropzone already attached.");
    }

    // Now add this dropzone to the instances.
    Dropzone.instances.push(_this);

    // Put the dropzone inside the element itself.
    _this.element.dropzone = _this;

    var elementOptions = (left = Dropzone.optionsForElement(_this.element)) != null ? left : {};

    _this.options = Dropzone.extend({}, _this.defaultOptions, elementOptions, options != null ? options : {});

    // If the browser failed, just call the fallback and leave
    if (_this.options.forceFallback || !Dropzone.isBrowserSupported()) {
      var _ret;

      return _ret = _this.options.fallback.call(_this), _possibleConstructorReturn(_this, _ret);
    }

    // @options.url = @element.getAttribute "action" unless @options.url?
    if (_this.options.url == null) {
      _this.options.url = _this.element.getAttribute("action");
    }

    if (!_this.options.url) {
      throw new Error("No URL provided.");
    }

    if (_this.options.acceptedFiles && _this.options.acceptedMimeTypes) {
      throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");
    }

    if (_this.options.uploadMultiple && _this.options.chunking) {
      throw new Error('You cannot set both: uploadMultiple and chunking.');
    }

    // Backwards compatibility
    if (_this.options.acceptedMimeTypes) {
      _this.options.acceptedFiles = _this.options.acceptedMimeTypes;
      delete _this.options.acceptedMimeTypes;
    }

    // Backwards compatibility
    if (_this.options.renameFilename != null) {
      _this.options.renameFile = function (file) {
        return _this.options.renameFilename.call(_this, file.name, file);
      };
    }

    _this.options.method = _this.options.method.toUpperCase();

    if ((fallback = _this.getExistingFallback()) && fallback.parentNode) {
      // Remove the fallback
      fallback.parentNode.removeChild(fallback);
    }

    // Display previews in the previewsContainer element or the Dropzone element unless explicitly set to false
    if (_this.options.previewsContainer !== false) {
      if (_this.options.previewsContainer) {
        _this.previewsContainer = Dropzone.getElement(_this.options.previewsContainer, "previewsContainer");
      } else {
        _this.previewsContainer = _this.element;
      }
    }

    if (_this.options.clickable) {
      if (_this.options.clickable === true) {
        _this.clickableElements = [_this.element];
      } else {
        _this.clickableElements = Dropzone.getElements(_this.options.clickable, "clickable");
      }
    }

    _this.init();
    return _this;
  }

  // Returns all files that have been accepted


  _createClass(Dropzone, [{
    key: "getAcceptedFiles",
    value: function getAcceptedFiles() {
      return this.files.filter(function (file) {
        return file.accepted;
      }).map(function (file) {
        return file;
      });
    }

    // Returns all files that have been rejected
    // Not sure when that's going to be useful, but added for completeness.

  }, {
    key: "getRejectedFiles",
    value: function getRejectedFiles() {
      return this.files.filter(function (file) {
        return !file.accepted;
      }).map(function (file) {
        return file;
      });
    }
  }, {
    key: "getFilesWithStatus",
    value: function getFilesWithStatus(status) {
      return this.files.filter(function (file) {
        return file.status === status;
      }).map(function (file) {
        return file;
      });
    }

    // Returns all files that are in the queue

  }, {
    key: "getQueuedFiles",
    value: function getQueuedFiles() {
      return this.getFilesWithStatus(Dropzone.QUEUED);
    }
  }, {
    key: "getUploadingFiles",
    value: function getUploadingFiles() {
      return this.getFilesWithStatus(Dropzone.UPLOADING);
    }
  }, {
    key: "getAddedFiles",
    value: function getAddedFiles() {
      return this.getFilesWithStatus(Dropzone.ADDED);
    }

    // Files that are either queued or uploading

  }, {
    key: "getActiveFiles",
    value: function getActiveFiles() {
      return this.files.filter(function (file) {
        return file.status === Dropzone.UPLOADING || file.status === Dropzone.QUEUED;
      }).map(function (file) {
        return file;
      });
    }

    // The function that gets called when Dropzone is initialized. You
    // can (and should) setup event listeners inside this function.

  }, {
    key: "init",
    value: function init() {
      var _this3 = this;

      // In case it isn't set already
      if (this.element.tagName === "form") {
        this.element.setAttribute("enctype", "multipart/form-data");
      }

      if (this.element.classList.contains("dropzone") && !this.element.querySelector(".dz-message")) {
        this.element.appendChild(Dropzone.createElement("<div class=\"dz-default dz-message\"><span>" + this.options.dictDefaultMessage + "</span></div>"));
      }

      if (this.clickableElements.length) {
        var setupHiddenFileInput = function setupHiddenFileInput() {
          if (_this3.hiddenFileInput) {
            _this3.hiddenFileInput.parentNode.removeChild(_this3.hiddenFileInput);
          }
          _this3.hiddenFileInput = document.createElement("input");
          _this3.hiddenFileInput.setAttribute("type", "file");
          if (_this3.options.maxFiles === null || _this3.options.maxFiles > 1) {
            _this3.hiddenFileInput.setAttribute("multiple", "multiple");
          }
          _this3.hiddenFileInput.className = "dz-hidden-input";

          if (_this3.options.acceptedFiles !== null) {
            _this3.hiddenFileInput.setAttribute("accept", _this3.options.acceptedFiles);
          }
          if (_this3.options.capture !== null) {
            _this3.hiddenFileInput.setAttribute("capture", _this3.options.capture);
          }

          // Not setting `display="none"` because some browsers don't accept clicks
          // on elements that aren't displayed.
          _this3.hiddenFileInput.style.visibility = "hidden";
          _this3.hiddenFileInput.style.position = "absolute";
          _this3.hiddenFileInput.style.top = "0";
          _this3.hiddenFileInput.style.left = "0";
          _this3.hiddenFileInput.style.height = "0";
          _this3.hiddenFileInput.style.width = "0";
          document.querySelector(_this3.options.hiddenInputContainer).appendChild(_this3.hiddenFileInput);
          return _this3.hiddenFileInput.addEventListener("change", function () {
            var files = _this3.hiddenFileInput.files;

            if (files.length) {
              for (var _iterator10 = files, _isArray10 = true, _i10 = 0, _iterator10 = _isArray10 ? _iterator10 : _iterator10[Symbol.iterator]();;) {
                var _ref9;

                if (_isArray10) {
                  if (_i10 >= _iterator10.length) break;
                  _ref9 = _iterator10[_i10++];
                } else {
                  _i10 = _iterator10.next();
                  if (_i10.done) break;
                  _ref9 = _i10.value;
                }

                var file = _ref9;

                _this3.addFile(file);
              }
            }
            _this3.emit("addedfiles", files);
            return setupHiddenFileInput();
          });
        };
        setupHiddenFileInput();
      }

      this.URL = window.URL !== null ? window.URL : window.webkitURL;

      // Setup all event listeners on the Dropzone object itself.
      // They're not in @setupEventListeners() because they shouldn't be removed
      // again when the dropzone gets disabled.
      for (var _iterator11 = this.events, _isArray11 = true, _i11 = 0, _iterator11 = _isArray11 ? _iterator11 : _iterator11[Symbol.iterator]();;) {
        var _ref10;

        if (_isArray11) {
          if (_i11 >= _iterator11.length) break;
          _ref10 = _iterator11[_i11++];
        } else {
          _i11 = _iterator11.next();
          if (_i11.done) break;
          _ref10 = _i11.value;
        }

        var eventName = _ref10;

        this.on(eventName, this.options[eventName]);
      }

      this.on("uploadprogress", function () {
        return _this3.updateTotalUploadProgress();
      });

      this.on("removedfile", function () {
        return _this3.updateTotalUploadProgress();
      });

      this.on("canceled", function (file) {
        return _this3.emit("complete", file);
      });

      // Emit a `queuecomplete` event if all files finished uploading.
      this.on("complete", function (file) {
        if (_this3.getAddedFiles().length === 0 && _this3.getUploadingFiles().length === 0 && _this3.getQueuedFiles().length === 0) {
          // This needs to be deferred so that `queuecomplete` really triggers after `complete`
          return setTimeout(function () {
            return _this3.emit("queuecomplete");
          }, 0);
        }
      });

      var noPropagation = function noPropagation(e) {
        e.stopPropagation();
        if (e.preventDefault) {
          return e.preventDefault();
        } else {
          return e.returnValue = false;
        }
      };

      // Create the listeners
      this.listeners = [{
        element: this.element,
        events: {
          "dragstart": function dragstart(e) {
            return _this3.emit("dragstart", e);
          },
          "dragenter": function dragenter(e) {
            noPropagation(e);
            return _this3.emit("dragenter", e);
          },
          "dragover": function dragover(e) {
            // Makes it possible to drag files from chrome's download bar
            // http://stackoverflow.com/questions/19526430/drag-and-drop-file-uploads-from-chrome-downloads-bar
            // Try is required to prevent bug in Internet Explorer 11 (SCRIPT65535 exception)
            var efct = void 0;
            try {
              efct = e.dataTransfer.effectAllowed;
            } catch (error) {}
            e.dataTransfer.dropEffect = 'move' === efct || 'linkMove' === efct ? 'move' : 'copy';

            noPropagation(e);
            return _this3.emit("dragover", e);
          },
          "dragleave": function dragleave(e) {
            return _this3.emit("dragleave", e);
          },
          "drop": function drop(e) {
            noPropagation(e);
            return _this3.drop(e);
          },
          "dragend": function dragend(e) {
            return _this3.emit("dragend", e);
          }

          // This is disabled right now, because the browsers don't implement it properly.
          // "paste": (e) =>
          //   noPropagation e
          //   @paste e
        } }];

      this.clickableElements.forEach(function (clickableElement) {
        return _this3.listeners.push({
          element: clickableElement,
          events: {
            "click": function click(evt) {
              // Only the actual dropzone or the message element should trigger file selection
              if (clickableElement !== _this3.element || evt.target === _this3.element || Dropzone.elementInside(evt.target, _this3.element.querySelector(".dz-message"))) {
                _this3.hiddenFileInput.click(); // Forward the click
              }
              return true;
            }
          }
        });
      });

      this.enable();

      return this.options.init.call(this);
    }

    // Not fully tested yet

  }, {
    key: "destroy",
    value: function destroy() {
      this.disable();
      this.removeAllFiles(true);
      if (this.hiddenFileInput != null ? this.hiddenFileInput.parentNode : undefined) {
        this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput);
        this.hiddenFileInput = null;
      }
      delete this.element.dropzone;
      return Dropzone.instances.splice(Dropzone.instances.indexOf(this), 1);
    }
  }, {
    key: "updateTotalUploadProgress",
    value: function updateTotalUploadProgress() {
      var totalUploadProgress = void 0;
      var totalBytesSent = 0;
      var totalBytes = 0;

      var activeFiles = this.getActiveFiles();

      if (activeFiles.length) {
        for (var _iterator12 = this.getActiveFiles(), _isArray12 = true, _i12 = 0, _iterator12 = _isArray12 ? _iterator12 : _iterator12[Symbol.iterator]();;) {
          var _ref11;

          if (_isArray12) {
            if (_i12 >= _iterator12.length) break;
            _ref11 = _iterator12[_i12++];
          } else {
            _i12 = _iterator12.next();
            if (_i12.done) break;
            _ref11 = _i12.value;
          }

          var file = _ref11;

          totalBytesSent += file.upload.bytesSent;
          totalBytes += file.upload.total;
        }
        totalUploadProgress = 100 * totalBytesSent / totalBytes;
      } else {
        totalUploadProgress = 100;
      }

      return this.emit("totaluploadprogress", totalUploadProgress, totalBytes, totalBytesSent);
    }

    // @options.paramName can be a function taking one parameter rather than a string.
    // A parameter name for a file is obtained simply by calling this with an index number.

  }, {
    key: "_getParamName",
    value: function _getParamName(n) {
      if (typeof this.options.paramName === "function") {
        return this.options.paramName(n);
      } else {
        return "" + this.options.paramName + (this.options.uploadMultiple ? "[" + n + "]" : "");
      }
    }

    // If @options.renameFile is a function,
    // the function will be used to rename the file.name before appending it to the formData

  }, {
    key: "_renameFile",
    value: function _renameFile(file) {
      if (typeof this.options.renameFile !== "function") {
        return file.name;
      }
      return this.options.renameFile(file);
    }

    // Returns a form that can be used as fallback if the browser does not support DragnDrop
    //
    // If the dropzone is already a form, only the input field and button are returned. Otherwise a complete form element is provided.
    // This code has to pass in IE7 :(

  }, {
    key: "getFallbackForm",
    value: function getFallbackForm() {
      var existingFallback = void 0,
          form = void 0;
      if (existingFallback = this.getExistingFallback()) {
        return existingFallback;
      }

      var fieldsString = "<div class=\"dz-fallback\">";
      if (this.options.dictFallbackText) {
        fieldsString += "<p>" + this.options.dictFallbackText + "</p>";
      }
      fieldsString += "<input type=\"file\" name=\"" + this._getParamName(0) + "\" " + (this.options.uploadMultiple ? 'multiple="multiple"' : undefined) + " /><input type=\"submit\" value=\"Upload!\"></div>";

      var fields = Dropzone.createElement(fieldsString);
      if (this.element.tagName !== "FORM") {
        form = Dropzone.createElement("<form action=\"" + this.options.url + "\" enctype=\"multipart/form-data\" method=\"" + this.options.method + "\"></form>");
        form.appendChild(fields);
      } else {
        // Make sure that the enctype and method attributes are set properly
        this.element.setAttribute("enctype", "multipart/form-data");
        this.element.setAttribute("method", this.options.method);
      }
      return form != null ? form : fields;
    }

    // Returns the fallback elements if they exist already
    //
    // This code has to pass in IE7 :(

  }, {
    key: "getExistingFallback",
    value: function getExistingFallback() {
      var getFallback = function getFallback(elements) {
        for (var _iterator13 = elements, _isArray13 = true, _i13 = 0, _iterator13 = _isArray13 ? _iterator13 : _iterator13[Symbol.iterator]();;) {
          var _ref12;

          if (_isArray13) {
            if (_i13 >= _iterator13.length) break;
            _ref12 = _iterator13[_i13++];
          } else {
            _i13 = _iterator13.next();
            if (_i13.done) break;
            _ref12 = _i13.value;
          }

          var el = _ref12;

          if (/(^| )fallback($| )/.test(el.className)) {
            return el;
          }
        }
      };

      var _arr = ["div", "form"];
      for (var _i14 = 0; _i14 < _arr.length; _i14++) {
        var tagName = _arr[_i14];
        var fallback;
        if (fallback = getFallback(this.element.getElementsByTagName(tagName))) {
          return fallback;
        }
      }
    }

    // Activates all listeners stored in @listeners

  }, {
    key: "setupEventListeners",
    value: function setupEventListeners() {
      return this.listeners.map(function (elementListeners) {
        return function () {
          var result = [];
          for (var event in elementListeners.events) {
            var listener = elementListeners.events[event];
            result.push(elementListeners.element.addEventListener(event, listener, false));
          }
          return result;
        }();
      });
    }

    // Deactivates all listeners stored in @listeners

  }, {
    key: "removeEventListeners",
    value: function removeEventListeners() {
      return this.listeners.map(function (elementListeners) {
        return function () {
          var result = [];
          for (var event in elementListeners.events) {
            var listener = elementListeners.events[event];
            result.push(elementListeners.element.removeEventListener(event, listener, false));
          }
          return result;
        }();
      });
    }

    // Removes all event listeners and cancels all files in the queue or being processed.

  }, {
    key: "disable",
    value: function disable() {
      var _this4 = this;

      this.clickableElements.forEach(function (element) {
        return element.classList.remove("dz-clickable");
      });
      this.removeEventListeners();

      return this.files.map(function (file) {
        return _this4.cancelUpload(file);
      });
    }
  }, {
    key: "enable",
    value: function enable() {
      this.clickableElements.forEach(function (element) {
        return element.classList.add("dz-clickable");
      });
      return this.setupEventListeners();
    }

    // Returns a nicely formatted filesize

  }, {
    key: "filesize",
    value: function filesize(size) {
      var selectedSize = 0;
      var selectedUnit = "b";

      if (size > 0) {
        var units = ['tb', 'gb', 'mb', 'kb', 'b'];

        for (var i = 0; i < units.length; i++) {
          var unit = units[i];
          var cutoff = Math.pow(this.options.filesizeBase, 4 - i) / 10;

          if (size >= cutoff) {
            selectedSize = size / Math.pow(this.options.filesizeBase, 4 - i);
            selectedUnit = unit;
            break;
          }
        }

        selectedSize = Math.round(10 * selectedSize) / 10; // Cutting of digits
      }

      return "<strong>" + selectedSize + "</strong> " + this.options.dictFileSizeUnits[selectedUnit];
    }

    // Adds or removes the `dz-max-files-reached` class from the form.

  }, {
    key: "_updateMaxFilesReachedClass",
    value: function _updateMaxFilesReachedClass() {
      if (this.options.maxFiles != null && this.getAcceptedFiles().length >= this.options.maxFiles) {
        if (this.getAcceptedFiles().length === this.options.maxFiles) {
          this.emit('maxfilesreached', this.files);
        }
        return this.element.classList.add("dz-max-files-reached");
      } else {
        return this.element.classList.remove("dz-max-files-reached");
      }
    }
  }, {
    key: "drop",
    value: function drop(e) {
      if (!e.dataTransfer) {
        return;
      }
      this.emit("drop", e);

      var files = e.dataTransfer.files;

      this.emit("addedfiles", files);

      // Even if it's a folder, files.length will contain the folders.
      if (files.length) {
        var items = e.dataTransfer.items;

        if (items && items.length && items[0].webkitGetAsEntry != null) {
          // The browser supports dropping of folders, so handle items instead of files
          this._addFilesFromItems(items);
        } else {
          this.handleFiles(files);
        }
      }
    }
  }, {
    key: "paste",
    value: function paste(e) {
      if (__guard__(e != null ? e.clipboardData : undefined, function (x) {
        return x.items;
      }) == null) {
        return;
      }

      this.emit("paste", e);
      var items = e.clipboardData.items;


      if (items.length) {
        return this._addFilesFromItems(items);
      }
    }
  }, {
    key: "handleFiles",
    value: function handleFiles(files) {
      var _this5 = this;

      return files.map(function (file) {
        return _this5.addFile(file);
      });
    }

    // When a folder is dropped (or files are pasted), items must be handled
    // instead of files.

  }, {
    key: "_addFilesFromItems",
    value: function _addFilesFromItems(items) {
      var _this6 = this;

      return function () {
        var result = [];
        for (var _iterator14 = items, _isArray14 = true, _i15 = 0, _iterator14 = _isArray14 ? _iterator14 : _iterator14[Symbol.iterator]();;) {
          var _ref13;

          if (_isArray14) {
            if (_i15 >= _iterator14.length) break;
            _ref13 = _iterator14[_i15++];
          } else {
            _i15 = _iterator14.next();
            if (_i15.done) break;
            _ref13 = _i15.value;
          }

          var item = _ref13;

          var entry;
          if (item.webkitGetAsEntry != null && (entry = item.webkitGetAsEntry())) {
            if (entry.isFile) {
              result.push(_this6.addFile(item.getAsFile()));
            } else if (entry.isDirectory) {
              // Append all files from that directory to files
              result.push(_this6._addFilesFromDirectory(entry, entry.name));
            } else {
              result.push(undefined);
            }
          } else if (item.getAsFile != null) {
            if (item.kind == null || item.kind === "file") {
              result.push(_this6.addFile(item.getAsFile()));
            } else {
              result.push(undefined);
            }
          } else {
            result.push(undefined);
          }
        }
        return result;
      }();
    }

    // Goes through the directory, and adds each file it finds recursively

  }, {
    key: "_addFilesFromDirectory",
    value: function _addFilesFromDirectory(directory, path) {
      var _this7 = this;

      var dirReader = directory.createReader();

      var errorHandler = function errorHandler(error) {
        return __guardMethod__(console, 'log', function (o) {
          return o.log(error);
        });
      };

      var readEntries = function readEntries() {
        return dirReader.readEntries(function (entries) {
          if (entries.length > 0) {
            for (var _iterator15 = entries, _isArray15 = true, _i16 = 0, _iterator15 = _isArray15 ? _iterator15 : _iterator15[Symbol.iterator]();;) {
              var _ref14;

              if (_isArray15) {
                if (_i16 >= _iterator15.length) break;
                _ref14 = _iterator15[_i16++];
              } else {
                _i16 = _iterator15.next();
                if (_i16.done) break;
                _ref14 = _i16.value;
              }

              var entry = _ref14;

              if (entry.isFile) {
                entry.file(function (file) {
                  if (_this7.options.ignoreHiddenFiles && file.name.substring(0, 1) === '.') {
                    return;
                  }
                  file.fullPath = path + "/" + file.name;
                  return _this7.addFile(file);
                });
              } else if (entry.isDirectory) {
                _this7._addFilesFromDirectory(entry, path + "/" + entry.name);
              }
            }

            // Recursively call readEntries() again, since browser only handle
            // the first 100 entries.
            // See: https://developer.mozilla.org/en-US/docs/Web/API/DirectoryReader#readEntries
            readEntries();
          }
          return null;
        }, errorHandler);
      };

      return readEntries();
    }

    // If `done()` is called without argument the file is accepted
    // If you call it with an error message, the file is rejected
    // (This allows for asynchronous validation)
    //
    // This function checks the filesize, and if the file.type passes the
    // `acceptedFiles` check.

  }, {
    key: "accept",
    value: function accept(file, done) {
      if (file.size > this.options.maxFilesize * 1024 * 1024) {
        return done(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(file.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize));
      } else if (!Dropzone.isValidFile(file, this.options.acceptedFiles)) {
        return done(this.options.dictInvalidFileType);
      } else if (this.options.maxFiles != null && this.getAcceptedFiles().length >= this.options.maxFiles) {
        done(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}", this.options.maxFiles));
        return this.emit("maxfilesexceeded", file);
      } else {
        return this.options.accept.call(this, file, done);
      }
    }
  }, {
    key: "addFile",
    value: function addFile(file) {
      var _this8 = this;

      file.upload = {
        uuid: Dropzone.uuidv4(),
        progress: 0,
        // Setting the total upload size to file.size for the beginning
        // It's actual different than the size to be transmitted.
        total: file.size,
        bytesSent: 0,
        filename: this._renameFile(file),
        chunked: this.options.chunking && (this.options.forceChunking || file.size > this.options.chunkSize),
        totalChunkCount: Math.ceil(file.size / this.options.chunkSize)
      };
      this.files.push(file);

      file.status = Dropzone.ADDED;

      this.emit("addedfile", file);

      this._enqueueThumbnail(file);

      return this.accept(file, function (error) {
        if (error) {
          file.accepted = false;
          _this8._errorProcessing([file], error); // Will set the file.status
        } else {
          file.accepted = true;
          if (_this8.options.autoQueue) {
            _this8.enqueueFile(file);
          } // Will set .accepted = true
        }
        return _this8._updateMaxFilesReachedClass();
      });
    }

    // Wrapper for enqueueFile

  }, {
    key: "enqueueFiles",
    value: function enqueueFiles(files) {
      for (var _iterator16 = files, _isArray16 = true, _i17 = 0, _iterator16 = _isArray16 ? _iterator16 : _iterator16[Symbol.iterator]();;) {
        var _ref15;

        if (_isArray16) {
          if (_i17 >= _iterator16.length) break;
          _ref15 = _iterator16[_i17++];
        } else {
          _i17 = _iterator16.next();
          if (_i17.done) break;
          _ref15 = _i17.value;
        }

        var file = _ref15;

        this.enqueueFile(file);
      }
      return null;
    }
  }, {
    key: "enqueueFile",
    value: function enqueueFile(file) {
      var _this9 = this;

      if (file.status === Dropzone.ADDED && file.accepted === true) {
        file.status = Dropzone.QUEUED;
        if (this.options.autoProcessQueue) {
          return setTimeout(function () {
            return _this9.processQueue();
          }, 0); // Deferring the call
        }
      } else {
        throw new Error("This file can't be queued because it has already been processed or was rejected.");
      }
    }
  }, {
    key: "_enqueueThumbnail",
    value: function _enqueueThumbnail(file) {
      var _this10 = this;

      if (this.options.createImageThumbnails && file.type.match(/image.*/) && file.size <= this.options.maxThumbnailFilesize * 1024 * 1024) {
        this._thumbnailQueue.push(file);
        return setTimeout(function () {
          return _this10._processThumbnailQueue();
        }, 0); // Deferring the call
      }
    }
  }, {
    key: "_processThumbnailQueue",
    value: function _processThumbnailQueue() {
      var _this11 = this;

      if (this._processingThumbnail || this._thumbnailQueue.length === 0) {
        return;
      }

      this._processingThumbnail = true;
      var file = this._thumbnailQueue.shift();
      return this.createThumbnail(file, this.options.thumbnailWidth, this.options.thumbnailHeight, this.options.thumbnailMethod, true, function (dataUrl) {
        _this11.emit("thumbnail", file, dataUrl);
        _this11._processingThumbnail = false;
        return _this11._processThumbnailQueue();
      });
    }

    // Can be called by the user to remove a file

  }, {
    key: "removeFile",
    value: function removeFile(file) {
      if (file.status === Dropzone.UPLOADING) {
        this.cancelUpload(file);
      }
      this.files = without(this.files, file);

      this.emit("removedfile", file);
      if (this.files.length === 0) {
        return this.emit("reset");
      }
    }

    // Removes all files that aren't currently processed from the list

  }, {
    key: "removeAllFiles",
    value: function removeAllFiles(cancelIfNecessary) {
      // Create a copy of files since removeFile() changes the @files array.
      if (cancelIfNecessary == null) {
        cancelIfNecessary = false;
      }
      for (var _iterator17 = this.files.slice(), _isArray17 = true, _i18 = 0, _iterator17 = _isArray17 ? _iterator17 : _iterator17[Symbol.iterator]();;) {
        var _ref16;

        if (_isArray17) {
          if (_i18 >= _iterator17.length) break;
          _ref16 = _iterator17[_i18++];
        } else {
          _i18 = _iterator17.next();
          if (_i18.done) break;
          _ref16 = _i18.value;
        }

        var file = _ref16;

        if (file.status !== Dropzone.UPLOADING || cancelIfNecessary) {
          this.removeFile(file);
        }
      }
      return null;
    }

    // Resizes an image before it gets sent to the server. This function is the default behavior of
    // `options.transformFile` if `resizeWidth` or `resizeHeight` are set. The callback is invoked with
    // the resized blob.

  }, {
    key: "resizeImage",
    value: function resizeImage(file, width, height, resizeMethod, callback) {
      var _this12 = this;

      return this.createThumbnail(file, width, height, resizeMethod, false, function (dataUrl, canvas) {
        if (canvas === null) {
          // The image has not been resized
          return callback(file);
        } else {
          var resizeMimeType = _this12.options.resizeMimeType;

          if (resizeMimeType == null) {
            resizeMimeType = file.type;
          }
          var resizedDataURL = canvas.toDataURL(resizeMimeType, _this12.options.resizeQuality);
          if (resizeMimeType === 'image/jpeg' || resizeMimeType === 'image/jpg') {
            // Now add the original EXIF information
            resizedDataURL = ExifRestore.restore(file.dataURL, resizedDataURL);
          }
          return callback(Dropzone.dataURItoBlob(resizedDataURL));
        }
      });
    }
  }, {
    key: "createThumbnail",
    value: function createThumbnail(file, width, height, resizeMethod, fixOrientation, callback) {
      var _this13 = this;

      var fileReader = new FileReader();

      fileReader.onload = function () {

        file.dataURL = fileReader.result;

        // Don't bother creating a thumbnail for SVG images since they're vector
        if (file.type === "image/svg+xml") {
          if (callback != null) {
            callback(fileReader.result);
          }
          return;
        }

        return _this13.createThumbnailFromUrl(file, width, height, resizeMethod, fixOrientation, callback);
      };

      return fileReader.readAsDataURL(file);
    }
  }, {
    key: "createThumbnailFromUrl",
    value: function createThumbnailFromUrl(file, width, height, resizeMethod, fixOrientation, callback, crossOrigin) {
      var _this14 = this;

      // Not using `new Image` here because of a bug in latest Chrome versions.
      // See https://github.com/enyo/dropzone/pull/226
      var img = document.createElement("img");

      if (crossOrigin) {
        img.crossOrigin = crossOrigin;
      }

      img.onload = function () {
        var loadExif = function loadExif(callback) {
          return callback(1);
        };
        if (typeof EXIF !== 'undefined' && EXIF !== null && fixOrientation) {
          loadExif = function loadExif(callback) {
            return EXIF.getData(img, function () {
              return callback(EXIF.getTag(this, 'Orientation'));
            });
          };
        }

        return loadExif(function (orientation) {
          file.width = img.width;
          file.height = img.height;

          var resizeInfo = _this14.options.resize.call(_this14, file, width, height, resizeMethod);

          var canvas = document.createElement("canvas");
          var ctx = canvas.getContext("2d");

          canvas.width = resizeInfo.trgWidth;
          canvas.height = resizeInfo.trgHeight;

          if (orientation > 4) {
            canvas.width = resizeInfo.trgHeight;
            canvas.height = resizeInfo.trgWidth;
          }

          switch (orientation) {
            case 2:
              // horizontal flip
              ctx.translate(canvas.width, 0);
              ctx.scale(-1, 1);
              break;
            case 3:
              // 180° rotate left
              ctx.translate(canvas.width, canvas.height);
              ctx.rotate(Math.PI);
              break;
            case 4:
              // vertical flip
              ctx.translate(0, canvas.height);
              ctx.scale(1, -1);
              break;
            case 5:
              // vertical flip + 90 rotate right
              ctx.rotate(0.5 * Math.PI);
              ctx.scale(1, -1);
              break;
            case 6:
              // 90° rotate right
              ctx.rotate(0.5 * Math.PI);
              ctx.translate(0, -canvas.height);
              break;
            case 7:
              // horizontal flip + 90 rotate right
              ctx.rotate(0.5 * Math.PI);
              ctx.translate(canvas.width, -canvas.height);
              ctx.scale(-1, 1);
              break;
            case 8:
              // 90° rotate left
              ctx.rotate(-0.5 * Math.PI);
              ctx.translate(-canvas.width, 0);
              break;
          }

          // This is a bugfix for iOS' scaling bug.
          drawImageIOSFix(ctx, img, resizeInfo.srcX != null ? resizeInfo.srcX : 0, resizeInfo.srcY != null ? resizeInfo.srcY : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, resizeInfo.trgX != null ? resizeInfo.trgX : 0, resizeInfo.trgY != null ? resizeInfo.trgY : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);

          var thumbnail = canvas.toDataURL("image/png");

          if (callback != null) {
            return callback(thumbnail, canvas);
          }
        });
      };

      if (callback != null) {
        img.onerror = callback;
      }

      return img.src = file.dataURL;
    }

    // Goes through the queue and processes files if there aren't too many already.

  }, {
    key: "processQueue",
    value: function processQueue() {
      var parallelUploads = this.options.parallelUploads;

      var processingLength = this.getUploadingFiles().length;
      var i = processingLength;

      // There are already at least as many files uploading than should be
      if (processingLength >= parallelUploads) {
        return;
      }

      var queuedFiles = this.getQueuedFiles();

      if (!(queuedFiles.length > 0)) {
        return;
      }

      if (this.options.uploadMultiple) {
        // The files should be uploaded in one request
        return this.processFiles(queuedFiles.slice(0, parallelUploads - processingLength));
      } else {
        while (i < parallelUploads) {
          if (!queuedFiles.length) {
            return;
          } // Nothing left to process
          this.processFile(queuedFiles.shift());
          i++;
        }
      }
    }

    // Wrapper for `processFiles`

  }, {
    key: "processFile",
    value: function processFile(file) {
      return this.processFiles([file]);
    }

    // Loads the file, then calls finishedLoading()

  }, {
    key: "processFiles",
    value: function processFiles(files) {
      for (var _iterator18 = files, _isArray18 = true, _i19 = 0, _iterator18 = _isArray18 ? _iterator18 : _iterator18[Symbol.iterator]();;) {
        var _ref17;

        if (_isArray18) {
          if (_i19 >= _iterator18.length) break;
          _ref17 = _iterator18[_i19++];
        } else {
          _i19 = _iterator18.next();
          if (_i19.done) break;
          _ref17 = _i19.value;
        }

        var file = _ref17;

        file.processing = true; // Backwards compatibility
        file.status = Dropzone.UPLOADING;

        this.emit("processing", file);
      }

      if (this.options.uploadMultiple) {
        this.emit("processingmultiple", files);
      }

      return this.uploadFiles(files);
    }
  }, {
    key: "_getFilesWithXhr",
    value: function _getFilesWithXhr(xhr) {
      var files = void 0;
      return files = this.files.filter(function (file) {
        return file.xhr === xhr;
      }).map(function (file) {
        return file;
      });
    }

    // Cancels the file upload and sets the status to CANCELED
    // **if** the file is actually being uploaded.
    // If it's still in the queue, the file is being removed from it and the status
    // set to CANCELED.

  }, {
    key: "cancelUpload",
    value: function cancelUpload(file) {
      if (file.status === Dropzone.UPLOADING) {
        var groupedFiles = this._getFilesWithXhr(file.xhr);
        for (var _iterator19 = groupedFiles, _isArray19 = true, _i20 = 0, _iterator19 = _isArray19 ? _iterator19 : _iterator19[Symbol.iterator]();;) {
          var _ref18;

          if (_isArray19) {
            if (_i20 >= _iterator19.length) break;
            _ref18 = _iterator19[_i20++];
          } else {
            _i20 = _iterator19.next();
            if (_i20.done) break;
            _ref18 = _i20.value;
          }

          var groupedFile = _ref18;

          groupedFile.status = Dropzone.CANCELED;
        }
        if (typeof file.xhr !== 'undefined') {
          file.xhr.abort();
        }
        for (var _iterator20 = groupedFiles, _isArray20 = true, _i21 = 0, _iterator20 = _isArray20 ? _iterator20 : _iterator20[Symbol.iterator]();;) {
          var _ref19;

          if (_isArray20) {
            if (_i21 >= _iterator20.length) break;
            _ref19 = _iterator20[_i21++];
          } else {
            _i21 = _iterator20.next();
            if (_i21.done) break;
            _ref19 = _i21.value;
          }

          var _groupedFile = _ref19;

          this.emit("canceled", _groupedFile);
        }
        if (this.options.uploadMultiple) {
          this.emit("canceledmultiple", groupedFiles);
        }
      } else if (file.status === Dropzone.ADDED || file.status === Dropzone.QUEUED) {
        file.status = Dropzone.CANCELED;
        this.emit("canceled", file);
        if (this.options.uploadMultiple) {
          this.emit("canceledmultiple", [file]);
        }
      }

      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    }
  }, {
    key: "resolveOption",
    value: function resolveOption(option) {
      if (typeof option === 'function') {
        for (var _len3 = arguments.length, args = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
          args[_key3 - 1] = arguments[_key3];
        }

        return option.apply(this, args);
      }
      return option;
    }
  }, {
    key: "uploadFile",
    value: function uploadFile(file) {
      return this.uploadFiles([file]);
    }
  }, {
    key: "uploadFiles",
    value: function uploadFiles(files) {
      var _this15 = this;

      this._transformFiles(files, function (transformedFiles) {
        if (files[0].upload.chunked) {
          // This file should be sent in chunks!

          // If the chunking option is set, we **know** that there can only be **one** file, since
          // uploadMultiple is not allowed with this option.
          var file = files[0];
          var transformedFile = transformedFiles[0];
          var startedChunkCount = 0;

          file.upload.chunks = [];

          var handleNextChunk = function handleNextChunk() {
            var chunkIndex = 0;

            // Find the next item in file.upload.chunks that is not defined yet.
            while (file.upload.chunks[chunkIndex] !== undefined) {
              chunkIndex++;
            }

            // This means, that all chunks have already been started.
            if (chunkIndex >= file.upload.totalChunkCount) return;

            startedChunkCount++;

            var start = chunkIndex * _this15.options.chunkSize;
            var end = Math.min(start + _this15.options.chunkSize, file.size);

            var dataBlock = {
              name: _this15._getParamName(0),
              data: transformedFile.webkitSlice ? transformedFile.webkitSlice(start, end) : transformedFile.slice(start, end),
              filename: file.upload.filename,
              chunkIndex: chunkIndex
            };

            file.upload.chunks[chunkIndex] = {
              file: file,
              index: chunkIndex,
              dataBlock: dataBlock, // In case we want to retry.
              status: Dropzone.UPLOADING,
              progress: 0,
              retries: 0 // The number of times this block has been retried.
            };

            _this15._uploadData(files, [dataBlock]);
          };

          file.upload.finishedChunkUpload = function (chunk) {
            var allFinished = true;
            chunk.status = Dropzone.SUCCESS;

            // Clear the data from the chunk
            chunk.dataBlock = null;

            for (var i = 0; i < file.upload.totalChunkCount; i++) {
              if (file.upload.chunks[i] === undefined) {
                return handleNextChunk();
              }
              if (file.upload.chunks[i].status !== Dropzone.SUCCESS) {
                allFinished = false;
              }
            }

            if (allFinished) {
              _this15.options.chunksUploaded(file, function () {
                _this15._finished(files, '', null);
              });
            }
          };

          if (_this15.options.parallelChunkUploads) {
            for (var i = 0; i < file.upload.totalChunkCount; i++) {
              handleNextChunk();
            }
          } else {
            handleNextChunk();
          }
        } else {
          var dataBlocks = [];
          for (var _i22 = 0; _i22 < files.length; _i22++) {
            dataBlocks[_i22] = {
              name: _this15._getParamName(_i22),
              data: transformedFiles[_i22],
              filename: files[_i22].upload.filename
            };
          }
          _this15._uploadData(files, dataBlocks);
        }
      });
    }

    /// Returns the right chunk for given file and xhr

  }, {
    key: "_getChunk",
    value: function _getChunk(file, xhr) {
      for (var i = 0; i < file.upload.totalChunkCount; i++) {
        if (file.upload.chunks[i] !== undefined && file.upload.chunks[i].xhr === xhr) {
          return file.upload.chunks[i];
        }
      }
    }

    // This function actually uploads the file(s) to the server.
    // If dataBlocks contains the actual data to upload (meaning, that this could either be transformed
    // files, or individual chunks for chunked upload).

  }, {
    key: "_uploadData",
    value: function _uploadData(files, dataBlocks) {
      var _this16 = this;

      var xhr = new XMLHttpRequest();

      // Put the xhr object in the file objects to be able to reference it later.
      for (var _iterator21 = files, _isArray21 = true, _i23 = 0, _iterator21 = _isArray21 ? _iterator21 : _iterator21[Symbol.iterator]();;) {
        var _ref20;

        if (_isArray21) {
          if (_i23 >= _iterator21.length) break;
          _ref20 = _iterator21[_i23++];
        } else {
          _i23 = _iterator21.next();
          if (_i23.done) break;
          _ref20 = _i23.value;
        }

        var file = _ref20;

        file.xhr = xhr;
      }
      if (files[0].upload.chunked) {
        // Put the xhr object in the right chunk object, so it can be associated later, and found with _getChunk
        files[0].upload.chunks[dataBlocks[0].chunkIndex].xhr = xhr;
      }

      var method = this.resolveOption(this.options.method, files);
      var url = this.resolveOption(this.options.url, files);
      xhr.open(method, url, true);

      // Setting the timeout after open because of IE11 issue: https://gitlab.com/meno/dropzone/issues/8
      xhr.timeout = this.resolveOption(this.options.timeout, files);

      // Has to be after `.open()`. See https://github.com/enyo/dropzone/issues/179
      xhr.withCredentials = !!this.options.withCredentials;

      xhr.onload = function (e) {
        _this16._finishedUploading(files, xhr, e);
      };

      xhr.onerror = function () {
        _this16._handleUploadError(files, xhr);
      };

      // Some browsers do not have the .upload property
      var progressObj = xhr.upload != null ? xhr.upload : xhr;
      progressObj.onprogress = function (e) {
        return _this16._updateFilesUploadProgress(files, xhr, e);
      };

      var headers = {
        "Accept": "application/json",
        "Cache-Control": "no-cache",
        "X-Requested-With": "XMLHttpRequest"
      };

      if (this.options.headers) {
        Dropzone.extend(headers, this.options.headers);
      }

      for (var headerName in headers) {
        var headerValue = headers[headerName];
        if (headerValue) {
          xhr.setRequestHeader(headerName, headerValue);
        }
      }

      var formData = new FormData();

      // Adding all @options parameters
      if (this.options.params) {
        var additionalParams = this.options.params;
        if (typeof additionalParams === 'function') {
          additionalParams = additionalParams.call(this, files, xhr, files[0].upload.chunked ? this._getChunk(files[0], xhr) : null);
        }

        for (var key in additionalParams) {
          var value = additionalParams[key];
          formData.append(key, value);
        }
      }

      // Let the user add additional data if necessary
      for (var _iterator22 = files, _isArray22 = true, _i24 = 0, _iterator22 = _isArray22 ? _iterator22 : _iterator22[Symbol.iterator]();;) {
        var _ref21;

        if (_isArray22) {
          if (_i24 >= _iterator22.length) break;
          _ref21 = _iterator22[_i24++];
        } else {
          _i24 = _iterator22.next();
          if (_i24.done) break;
          _ref21 = _i24.value;
        }

        var _file = _ref21;

        this.emit("sending", _file, xhr, formData);
      }
      if (this.options.uploadMultiple) {
        this.emit("sendingmultiple", files, xhr, formData);
      }

      this._addFormElementData(formData);

      // Finally add the files
      // Has to be last because some servers (eg: S3) expect the file to be the last parameter
      for (var i = 0; i < dataBlocks.length; i++) {
        var dataBlock = dataBlocks[i];
        formData.append(dataBlock.name, dataBlock.data, dataBlock.filename);
      }

      this.submitRequest(xhr, formData, files);
    }

    // Transforms all files with this.options.transformFile and invokes done with the transformed files when done.

  }, {
    key: "_transformFiles",
    value: function _transformFiles(files, done) {
      var _this17 = this;

      var transformedFiles = [];
      // Clumsy way of handling asynchronous calls, until I get to add a proper Future library.
      var doneCounter = 0;

      var _loop = function _loop(i) {
        _this17.options.transformFile.call(_this17, files[i], function (transformedFile) {
          transformedFiles[i] = transformedFile;
          if (++doneCounter === files.length) {
            done(transformedFiles);
          }
        });
      };

      for (var i = 0; i < files.length; i++) {
        _loop(i);
      }
    }

    // Takes care of adding other input elements of the form to the AJAX request

  }, {
    key: "_addFormElementData",
    value: function _addFormElementData(formData) {
      // Take care of other input elements
      if (this.element.tagName === "FORM") {
        for (var _iterator23 = this.element.querySelectorAll("input, textarea, select, button"), _isArray23 = true, _i25 = 0, _iterator23 = _isArray23 ? _iterator23 : _iterator23[Symbol.iterator]();;) {
          var _ref22;

          if (_isArray23) {
            if (_i25 >= _iterator23.length) break;
            _ref22 = _iterator23[_i25++];
          } else {
            _i25 = _iterator23.next();
            if (_i25.done) break;
            _ref22 = _i25.value;
          }

          var input = _ref22;

          var inputName = input.getAttribute("name");
          var inputType = input.getAttribute("type");
          if (inputType) inputType = inputType.toLowerCase();

          // If the input doesn't have a name, we can't use it.
          if (typeof inputName === 'undefined' || inputName === null) continue;

          if (input.tagName === "SELECT" && input.hasAttribute("multiple")) {
            // Possibly multiple values
            for (var _iterator24 = input.options, _isArray24 = true, _i26 = 0, _iterator24 = _isArray24 ? _iterator24 : _iterator24[Symbol.iterator]();;) {
              var _ref23;

              if (_isArray24) {
                if (_i26 >= _iterator24.length) break;
                _ref23 = _iterator24[_i26++];
              } else {
                _i26 = _iterator24.next();
                if (_i26.done) break;
                _ref23 = _i26.value;
              }

              var option = _ref23;

              if (option.selected) {
                formData.append(inputName, option.value);
              }
            }
          } else if (!inputType || inputType !== "checkbox" && inputType !== "radio" || input.checked) {
            formData.append(inputName, input.value);
          }
        }
      }
    }

    // Invoked when there is new progress information about given files.
    // If e is not provided, it is assumed that the upload is finished.

  }, {
    key: "_updateFilesUploadProgress",
    value: function _updateFilesUploadProgress(files, xhr, e) {
      var progress = void 0;
      if (typeof e !== 'undefined') {
        progress = 100 * e.loaded / e.total;

        if (files[0].upload.chunked) {
          var file = files[0];
          // Since this is a chunked upload, we need to update the appropriate chunk progress.
          var chunk = this._getChunk(file, xhr);
          chunk.progress = progress;
          chunk.total = e.total;
          chunk.bytesSent = e.loaded;
          var fileProgress = 0,
              fileTotal = void 0,
              fileBytesSent = void 0;
          file.upload.progress = 0;
          file.upload.total = 0;
          file.upload.bytesSent = 0;
          for (var i = 0; i < file.upload.totalChunkCount; i++) {
            if (file.upload.chunks[i] !== undefined && file.upload.chunks[i].progress !== undefined) {
              file.upload.progress += file.upload.chunks[i].progress;
              file.upload.total += file.upload.chunks[i].total;
              file.upload.bytesSent += file.upload.chunks[i].bytesSent;
            }
          }
          file.upload.progress = file.upload.progress / file.upload.totalChunkCount;
        } else {
          for (var _iterator25 = files, _isArray25 = true, _i27 = 0, _iterator25 = _isArray25 ? _iterator25 : _iterator25[Symbol.iterator]();;) {
            var _ref24;

            if (_isArray25) {
              if (_i27 >= _iterator25.length) break;
              _ref24 = _iterator25[_i27++];
            } else {
              _i27 = _iterator25.next();
              if (_i27.done) break;
              _ref24 = _i27.value;
            }

            var _file2 = _ref24;

            _file2.upload.progress = progress;
            _file2.upload.total = e.total;
            _file2.upload.bytesSent = e.loaded;
          }
        }
        for (var _iterator26 = files, _isArray26 = true, _i28 = 0, _iterator26 = _isArray26 ? _iterator26 : _iterator26[Symbol.iterator]();;) {
          var _ref25;

          if (_isArray26) {
            if (_i28 >= _iterator26.length) break;
            _ref25 = _iterator26[_i28++];
          } else {
            _i28 = _iterator26.next();
            if (_i28.done) break;
            _ref25 = _i28.value;
          }

          var _file3 = _ref25;

          this.emit("uploadprogress", _file3, _file3.upload.progress, _file3.upload.bytesSent);
        }
      } else {
        // Called when the file finished uploading

        var allFilesFinished = true;

        progress = 100;

        for (var _iterator27 = files, _isArray27 = true, _i29 = 0, _iterator27 = _isArray27 ? _iterator27 : _iterator27[Symbol.iterator]();;) {
          var _ref26;

          if (_isArray27) {
            if (_i29 >= _iterator27.length) break;
            _ref26 = _iterator27[_i29++];
          } else {
            _i29 = _iterator27.next();
            if (_i29.done) break;
            _ref26 = _i29.value;
          }

          var _file4 = _ref26;

          if (_file4.upload.progress !== 100 || _file4.upload.bytesSent !== _file4.upload.total) {
            allFilesFinished = false;
          }
          _file4.upload.progress = progress;
          _file4.upload.bytesSent = _file4.upload.total;
        }

        // Nothing to do, all files already at 100%
        if (allFilesFinished) {
          return;
        }

        for (var _iterator28 = files, _isArray28 = true, _i30 = 0, _iterator28 = _isArray28 ? _iterator28 : _iterator28[Symbol.iterator]();;) {
          var _ref27;

          if (_isArray28) {
            if (_i30 >= _iterator28.length) break;
            _ref27 = _iterator28[_i30++];
          } else {
            _i30 = _iterator28.next();
            if (_i30.done) break;
            _ref27 = _i30.value;
          }

          var _file5 = _ref27;

          this.emit("uploadprogress", _file5, progress, _file5.upload.bytesSent);
        }
      }
    }
  }, {
    key: "_finishedUploading",
    value: function _finishedUploading(files, xhr, e) {
      var response = void 0;

      if (files[0].status === Dropzone.CANCELED) {
        return;
      }

      if (xhr.readyState !== 4) {
        return;
      }

      if (xhr.responseType !== 'arraybuffer' && xhr.responseType !== 'blob') {
        response = xhr.responseText;

        if (xhr.getResponseHeader("content-type") && ~xhr.getResponseHeader("content-type").indexOf("application/json")) {
          try {
            response = JSON.parse(response);
          } catch (error) {
            e = error;
            response = "Invalid JSON response from server.";
          }
        }
      }

      this._updateFilesUploadProgress(files);

      if (!(200 <= xhr.status && xhr.status < 300)) {
        this._handleUploadError(files, xhr, response);
      } else {
        if (files[0].upload.chunked) {
          files[0].upload.finishedChunkUpload(this._getChunk(files[0], xhr));
        } else {
          this._finished(files, response, e);
        }
      }
    }
  }, {
    key: "_handleUploadError",
    value: function _handleUploadError(files, xhr, response) {
      if (files[0].status === Dropzone.CANCELED) {
        return;
      }

      if (files[0].upload.chunked && this.options.retryChunks) {
        var chunk = this._getChunk(files[0], xhr);
        if (chunk.retries++ < this.options.retryChunksLimit) {
          this._uploadData(files, [chunk.dataBlock]);
          return;
        } else {
          console.warn('Retried this chunk too often. Giving up.');
        }
      }

      for (var _iterator29 = files, _isArray29 = true, _i31 = 0, _iterator29 = _isArray29 ? _iterator29 : _iterator29[Symbol.iterator]();;) {
        var _ref28;

        if (_isArray29) {
          if (_i31 >= _iterator29.length) break;
          _ref28 = _iterator29[_i31++];
        } else {
          _i31 = _iterator29.next();
          if (_i31.done) break;
          _ref28 = _i31.value;
        }

        var file = _ref28;

        this._errorProcessing(files, response || this.options.dictResponseError.replace("{{statusCode}}", xhr.status), xhr);
      }
    }
  }, {
    key: "submitRequest",
    value: function submitRequest(xhr, formData, files) {
      xhr.send(formData);
    }

    // Called internally when processing is finished.
    // Individual callbacks have to be called in the appropriate sections.

  }, {
    key: "_finished",
    value: function _finished(files, responseText, e) {
      for (var _iterator30 = files, _isArray30 = true, _i32 = 0, _iterator30 = _isArray30 ? _iterator30 : _iterator30[Symbol.iterator]();;) {
        var _ref29;

        if (_isArray30) {
          if (_i32 >= _iterator30.length) break;
          _ref29 = _iterator30[_i32++];
        } else {
          _i32 = _iterator30.next();
          if (_i32.done) break;
          _ref29 = _i32.value;
        }

        var file = _ref29;

        file.status = Dropzone.SUCCESS;
        this.emit("success", file, responseText, e);
        this.emit("complete", file);
      }
      if (this.options.uploadMultiple) {
        this.emit("successmultiple", files, responseText, e);
        this.emit("completemultiple", files);
      }

      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    }

    // Called internally when processing is finished.
    // Individual callbacks have to be called in the appropriate sections.

  }, {
    key: "_errorProcessing",
    value: function _errorProcessing(files, message, xhr) {
      for (var _iterator31 = files, _isArray31 = true, _i33 = 0, _iterator31 = _isArray31 ? _iterator31 : _iterator31[Symbol.iterator]();;) {
        var _ref30;

        if (_isArray31) {
          if (_i33 >= _iterator31.length) break;
          _ref30 = _iterator31[_i33++];
        } else {
          _i33 = _iterator31.next();
          if (_i33.done) break;
          _ref30 = _i33.value;
        }

        var file = _ref30;

        file.status = Dropzone.ERROR;
        this.emit("error", file, message, xhr);
        this.emit("complete", file);
      }
      if (this.options.uploadMultiple) {
        this.emit("errormultiple", files, message, xhr);
        this.emit("completemultiple", files);
      }

      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    }
  }], [{
    key: "uuidv4",
    value: function uuidv4() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c === 'x' ? r : r & 0x3 | 0x8;
        return v.toString(16);
      });
    }
  }]);

  return Dropzone;
}(Emitter);

Dropzone.initClass();

Dropzone.version = "5.2.0";

// This is a map of options for your different dropzones. Add configurations
// to this object for your different dropzone elemens.
//
// Example:
//
//     Dropzone.options.myDropzoneElementId = { maxFilesize: 1 };
//
// To disable autoDiscover for a specific element, you can set `false` as an option:
//
//     Dropzone.options.myDisabledElementId = false;
//
// And in html:
//
//     <form action="/upload" id="my-dropzone-element-id" class="dropzone"></form>
Dropzone.options = {};

// Returns the options for an element or undefined if none available.
Dropzone.optionsForElement = function (element) {
  // Get the `Dropzone.options.elementId` for this element if it exists
  if (element.getAttribute("id")) {
    return Dropzone.options[camelize(element.getAttribute("id"))];
  } else {
    return undefined;
  }
};

// Holds a list of all dropzone instances
Dropzone.instances = [];

// Returns the dropzone for given element if any
Dropzone.forElement = function (element) {
  if (typeof element === "string") {
    element = document.querySelector(element);
  }
  if ((element != null ? element.dropzone : undefined) == null) {
    throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");
  }
  return element.dropzone;
};

// Set to false if you don't want Dropzone to automatically find and attach to .dropzone elements.
Dropzone.autoDiscover = true;

// Looks for all .dropzone elements and creates a dropzone for them
Dropzone.discover = function () {
  var dropzones = void 0;
  if (document.querySelectorAll) {
    dropzones = document.querySelectorAll(".dropzone");
  } else {
    dropzones = [];
    // IE :(
    var checkElements = function checkElements(elements) {
      return function () {
        var result = [];
        for (var _iterator32 = elements, _isArray32 = true, _i34 = 0, _iterator32 = _isArray32 ? _iterator32 : _iterator32[Symbol.iterator]();;) {
          var _ref31;

          if (_isArray32) {
            if (_i34 >= _iterator32.length) break;
            _ref31 = _iterator32[_i34++];
          } else {
            _i34 = _iterator32.next();
            if (_i34.done) break;
            _ref31 = _i34.value;
          }

          var el = _ref31;

          if (/(^| )dropzone($| )/.test(el.className)) {
            result.push(dropzones.push(el));
          } else {
            result.push(undefined);
          }
        }
        return result;
      }();
    };
    checkElements(document.getElementsByTagName("div"));
    checkElements(document.getElementsByTagName("form"));
  }

  return function () {
    var result = [];
    for (var _iterator33 = dropzones, _isArray33 = true, _i35 = 0, _iterator33 = _isArray33 ? _iterator33 : _iterator33[Symbol.iterator]();;) {
      var _ref32;

      if (_isArray33) {
        if (_i35 >= _iterator33.length) break;
        _ref32 = _iterator33[_i35++];
      } else {
        _i35 = _iterator33.next();
        if (_i35.done) break;
        _ref32 = _i35.value;
      }

      var dropzone = _ref32;

      // Create a dropzone unless auto discover has been disabled for specific element
      if (Dropzone.optionsForElement(dropzone) !== false) {
        result.push(new Dropzone(dropzone));
      } else {
        result.push(undefined);
      }
    }
    return result;
  }();
};

// Since the whole Drag'n'Drop API is pretty new, some browsers implement it,
// but not correctly.
// So I created a blacklist of userAgents. Yes, yes. Browser sniffing, I know.
// But what to do when browsers *theoretically* support an API, but crash
// when using it.
//
// This is a list of regular expressions tested against navigator.userAgent
//
// ** It should only be used on browser that *do* support the API, but
// incorrectly **
//
Dropzone.blacklistedBrowsers = [
// The mac os and windows phone version of opera 12 seems to have a problem with the File drag'n'drop API.
/opera.*(Macintosh|Windows Phone).*version\/12/i];

// Checks if the browser is supported
Dropzone.isBrowserSupported = function () {
  var capableBrowser = true;

  if (window.File && window.FileReader && window.FileList && window.Blob && window.FormData && document.querySelector) {
    if (!("classList" in document.createElement("a"))) {
      capableBrowser = false;
    } else {
      // The browser supports the API, but may be blacklisted.
      for (var _iterator34 = Dropzone.blacklistedBrowsers, _isArray34 = true, _i36 = 0, _iterator34 = _isArray34 ? _iterator34 : _iterator34[Symbol.iterator]();;) {
        var _ref33;

        if (_isArray34) {
          if (_i36 >= _iterator34.length) break;
          _ref33 = _iterator34[_i36++];
        } else {
          _i36 = _iterator34.next();
          if (_i36.done) break;
          _ref33 = _i36.value;
        }

        var regex = _ref33;

        if (regex.test(navigator.userAgent)) {
          capableBrowser = false;
          continue;
        }
      }
    }
  } else {
    capableBrowser = false;
  }

  return capableBrowser;
};

Dropzone.dataURItoBlob = function (dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);
  for (var i = 0, end = byteString.length, asc = 0 <= end; asc ? i <= end : i >= end; asc ? i++ : i--) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob
  return new Blob([ab], { type: mimeString });
};

// Returns an array without the rejected item
var without = function without(list, rejectedItem) {
  return list.filter(function (item) {
    return item !== rejectedItem;
  }).map(function (item) {
    return item;
  });
};

// abc-def_ghi -> abcDefGhi
var camelize = function camelize(str) {
  return str.replace(/[\-_](\w)/g, function (match) {
    return match.charAt(1).toUpperCase();
  });
};

// Creates an element from string
Dropzone.createElement = function (string) {
  var div = document.createElement("div");
  div.innerHTML = string;
  return div.childNodes[0];
};

// Tests if given element is inside (or simply is) the container
Dropzone.elementInside = function (element, container) {
  if (element === container) {
    return true;
  } // Coffeescript doesn't support do/while loops
  while (element = element.parentNode) {
    if (element === container) {
      return true;
    }
  }
  return false;
};

Dropzone.getElement = function (el, name) {
  var element = void 0;
  if (typeof el === "string") {
    element = document.querySelector(el);
  } else if (el.nodeType != null) {
    element = el;
  }
  if (element == null) {
    throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector or a plain HTML element.");
  }
  return element;
};

Dropzone.getElements = function (els, name) {
  var el = void 0,
      elements = void 0;
  if (els instanceof Array) {
    elements = [];
    try {
      for (var _iterator35 = els, _isArray35 = true, _i37 = 0, _iterator35 = _isArray35 ? _iterator35 : _iterator35[Symbol.iterator]();;) {
        if (_isArray35) {
          if (_i37 >= _iterator35.length) break;
          el = _iterator35[_i37++];
        } else {
          _i37 = _iterator35.next();
          if (_i37.done) break;
          el = _i37.value;
        }

        elements.push(this.getElement(el, name));
      }
    } catch (e) {
      elements = null;
    }
  } else if (typeof els === "string") {
    elements = [];
    for (var _iterator36 = document.querySelectorAll(els), _isArray36 = true, _i38 = 0, _iterator36 = _isArray36 ? _iterator36 : _iterator36[Symbol.iterator]();;) {
      if (_isArray36) {
        if (_i38 >= _iterator36.length) break;
        el = _iterator36[_i38++];
      } else {
        _i38 = _iterator36.next();
        if (_i38.done) break;
        el = _i38.value;
      }

      elements.push(el);
    }
  } else if (els.nodeType != null) {
    elements = [els];
  }

  if (elements == null || !elements.length) {
    throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");
  }

  return elements;
};

// Asks the user the question and calls accepted or rejected accordingly
//
// The default implementation just uses `window.confirm` and then calls the
// appropriate callback.
Dropzone.confirm = function (question, accepted, rejected) {
  if (window.confirm(question)) {
    return accepted();
  } else if (rejected != null) {
    return rejected();
  }
};

// Validates the mime type like this:
//
// https://developer.mozilla.org/en-US/docs/HTML/Element/input#attr-accept
Dropzone.isValidFile = function (file, acceptedFiles) {
  if (!acceptedFiles) {
    return true;
  } // If there are no accepted mime types, it's OK
  acceptedFiles = acceptedFiles.split(",");

  var mimeType = file.type;
  var baseMimeType = mimeType.replace(/\/.*$/, "");

  for (var _iterator37 = acceptedFiles, _isArray37 = true, _i39 = 0, _iterator37 = _isArray37 ? _iterator37 : _iterator37[Symbol.iterator]();;) {
    var _ref34;

    if (_isArray37) {
      if (_i39 >= _iterator37.length) break;
      _ref34 = _iterator37[_i39++];
    } else {
      _i39 = _iterator37.next();
      if (_i39.done) break;
      _ref34 = _i39.value;
    }

    var validType = _ref34;

    validType = validType.trim();
    if (validType.charAt(0) === ".") {
      if (file.name.toLowerCase().indexOf(validType.toLowerCase(), file.name.length - validType.length) !== -1) {
        return true;
      }
    } else if (/\/\*$/.test(validType)) {
      // This is something like a image/* mime type
      if (baseMimeType === validType.replace(/\/.*$/, "")) {
        return true;
      }
    } else {
      if (mimeType === validType) {
        return true;
      }
    }
  }

  return false;
};

// Augment jQuery
if (typeof jQuery !== 'undefined' && jQuery !== null) {
  jQuery.fn.dropzone = function (options) {
    return this.each(function () {
      return new Dropzone(this, options);
    });
  };
}

if (typeof module !== 'undefined' && module !== null) {
  module.exports = Dropzone;
} else {
  window.Dropzone = Dropzone;
}

// Dropzone file status codes
Dropzone.ADDED = "added";

Dropzone.QUEUED = "queued";
// For backwards compatibility. Now, if a file is accepted, it's either queued
// or uploading.
Dropzone.ACCEPTED = Dropzone.QUEUED;

Dropzone.UPLOADING = "uploading";
Dropzone.PROCESSING = Dropzone.UPLOADING; // alias

Dropzone.CANCELED = "canceled";
Dropzone.ERROR = "error";
Dropzone.SUCCESS = "success";

/*

 Bugfix for iOS 6 and 7
 Source: http://stackoverflow.com/questions/11929099/html5-canvas-drawimage-ratio-bug-ios
 based on the work of https://github.com/stomita/ios-imagefile-megapixel

 */

// Detecting vertical squash in loaded image.
// Fixes a bug which squash image vertically while drawing into canvas for some images.
// This is a bug in iOS6 devices. This function from https://github.com/stomita/ios-imagefile-megapixel
var detectVerticalSquash = function detectVerticalSquash(img) {
  var iw = img.naturalWidth;
  var ih = img.naturalHeight;
  var canvas = document.createElement("canvas");
  canvas.width = 1;
  canvas.height = ih;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);

  var _ctx$getImageData = ctx.getImageData(1, 0, 1, ih),
      data = _ctx$getImageData.data;

  // search image edge pixel position in case it is squashed vertically.


  var sy = 0;
  var ey = ih;
  var py = ih;
  while (py > sy) {
    var alpha = data[(py - 1) * 4 + 3];

    if (alpha === 0) {
      ey = py;
    } else {
      sy = py;
    }

    py = ey + sy >> 1;
  }
  var ratio = py / ih;

  if (ratio === 0) {
    return 1;
  } else {
    return ratio;
  }
};

// A replacement for context.drawImage
// (args are for source and destination).
var drawImageIOSFix = function drawImageIOSFix(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
  var vertSquashRatio = detectVerticalSquash(img);
  return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
};

// Based on MinifyJpeg
// Source: http://www.perry.cz/files/ExifRestorer.js
// http://elicon.blog57.fc2.com/blog-entry-206.html

var ExifRestore = function () {
  function ExifRestore() {
    _classCallCheck(this, ExifRestore);
  }

  _createClass(ExifRestore, null, [{
    key: "initClass",
    value: function initClass() {
      this.KEY_STR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    }
  }, {
    key: "encode64",
    value: function encode64(input) {
      var output = '';
      var chr1 = undefined;
      var chr2 = undefined;
      var chr3 = '';
      var enc1 = undefined;
      var enc2 = undefined;
      var enc3 = undefined;
      var enc4 = '';
      var i = 0;
      while (true) {
        chr1 = input[i++];
        chr2 = input[i++];
        chr3 = input[i++];
        enc1 = chr1 >> 2;
        enc2 = (chr1 & 3) << 4 | chr2 >> 4;
        enc3 = (chr2 & 15) << 2 | chr3 >> 6;
        enc4 = chr3 & 63;
        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
          enc4 = 64;
        }
        output = output + this.KEY_STR.charAt(enc1) + this.KEY_STR.charAt(enc2) + this.KEY_STR.charAt(enc3) + this.KEY_STR.charAt(enc4);
        chr1 = chr2 = chr3 = '';
        enc1 = enc2 = enc3 = enc4 = '';
        if (!(i < input.length)) {
          break;
        }
      }
      return output;
    }
  }, {
    key: "restore",
    value: function restore(origFileBase64, resizedFileBase64) {
      if (!origFileBase64.match('data:image/jpeg;base64,')) {
        return resizedFileBase64;
      }
      var rawImage = this.decode64(origFileBase64.replace('data:image/jpeg;base64,', ''));
      var segments = this.slice2Segments(rawImage);
      var image = this.exifManipulation(resizedFileBase64, segments);
      return "data:image/jpeg;base64," + this.encode64(image);
    }
  }, {
    key: "exifManipulation",
    value: function exifManipulation(resizedFileBase64, segments) {
      var exifArray = this.getExifArray(segments);
      var newImageArray = this.insertExif(resizedFileBase64, exifArray);
      var aBuffer = new Uint8Array(newImageArray);
      return aBuffer;
    }
  }, {
    key: "getExifArray",
    value: function getExifArray(segments) {
      var seg = undefined;
      var x = 0;
      while (x < segments.length) {
        seg = segments[x];
        if (seg[0] === 255 & seg[1] === 225) {
          return seg;
        }
        x++;
      }
      return [];
    }
  }, {
    key: "insertExif",
    value: function insertExif(resizedFileBase64, exifArray) {
      var imageData = resizedFileBase64.replace('data:image/jpeg;base64,', '');
      var buf = this.decode64(imageData);
      var separatePoint = buf.indexOf(255, 3);
      var mae = buf.slice(0, separatePoint);
      var ato = buf.slice(separatePoint);
      var array = mae;
      array = array.concat(exifArray);
      array = array.concat(ato);
      return array;
    }
  }, {
    key: "slice2Segments",
    value: function slice2Segments(rawImageArray) {
      var head = 0;
      var segments = [];
      while (true) {
        var length;
        if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 218) {
          break;
        }
        if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 216) {
          head += 2;
        } else {
          length = rawImageArray[head + 2] * 256 + rawImageArray[head + 3];
          var endPoint = head + length + 2;
          var seg = rawImageArray.slice(head, endPoint);
          segments.push(seg);
          head = endPoint;
        }
        if (head > rawImageArray.length) {
          break;
        }
      }
      return segments;
    }
  }, {
    key: "decode64",
    value: function decode64(input) {
      var output = '';
      var chr1 = undefined;
      var chr2 = undefined;
      var chr3 = '';
      var enc1 = undefined;
      var enc2 = undefined;
      var enc3 = undefined;
      var enc4 = '';
      var i = 0;
      var buf = [];
      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(input)) {
        console.warn('There were invalid base64 characters in the input text.\nValid base64 characters are A-Z, a-z, 0-9, \'+\', \'/\',and \'=\'\nExpect errors in decoding.');
      }
      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');
      while (true) {
        enc1 = this.KEY_STR.indexOf(input.charAt(i++));
        enc2 = this.KEY_STR.indexOf(input.charAt(i++));
        enc3 = this.KEY_STR.indexOf(input.charAt(i++));
        enc4 = this.KEY_STR.indexOf(input.charAt(i++));
        chr1 = enc1 << 2 | enc2 >> 4;
        chr2 = (enc2 & 15) << 4 | enc3 >> 2;
        chr3 = (enc3 & 3) << 6 | enc4;
        buf.push(chr1);
        if (enc3 !== 64) {
          buf.push(chr2);
        }
        if (enc4 !== 64) {
          buf.push(chr3);
        }
        chr1 = chr2 = chr3 = '';
        enc1 = enc2 = enc3 = enc4 = '';
        if (!(i < input.length)) {
          break;
        }
      }
      return buf;
    }
  }]);

  return ExifRestore;
}();

ExifRestore.initClass();

/*
 * contentloaded.js
 *
 * Author: Diego Perini (diego.perini at gmail.com)
 * Summary: cross-browser wrapper for DOMContentLoaded
 * Updated: 20101020
 * License: MIT
 * Version: 1.2
 *
 * URL:
 * http://javascript.nwbox.com/ContentLoaded/
 * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
 */

// @win window reference
// @fn function reference
var contentLoaded = function contentLoaded(win, fn) {
  var done = false;
  var top = true;
  var doc = win.document;
  var root = doc.documentElement;
  var add = doc.addEventListener ? "addEventListener" : "attachEvent";
  var rem = doc.addEventListener ? "removeEventListener" : "detachEvent";
  var pre = doc.addEventListener ? "" : "on";
  var init = function init(e) {
    if (e.type === "readystatechange" && doc.readyState !== "complete") {
      return;
    }
    (e.type === "load" ? win : doc)[rem](pre + e.type, init, false);
    if (!done && (done = true)) {
      return fn.call(win, e.type || e);
    }
  };

  var poll = function poll() {
    try {
      root.doScroll("left");
    } catch (e) {
      setTimeout(poll, 50);
      return;
    }
    return init("poll");
  };

  if (doc.readyState !== "complete") {
    if (doc.createEventObject && root.doScroll) {
      try {
        top = !win.frameElement;
      } catch (error) {}
      if (top) {
        poll();
      }
    }
    doc[add](pre + "DOMContentLoaded", init, false);
    doc[add](pre + "readystatechange", init, false);
    return win[add](pre + "load", init, false);
  }
};

// As a single function to be able to write tests.
Dropzone._autoDiscoverFunction = function () {
  if (Dropzone.autoDiscover) {
    return Dropzone.discover();
  }
};
contentLoaded(window, Dropzone._autoDiscoverFunction);

function __guard__(value, transform) {
  return typeof value !== 'undefined' && value !== null ? transform(value) : undefined;
}
function __guardMethod__(obj, methodName, transform) {
  if (typeof obj !== 'undefined' && obj !== null && typeof obj[methodName] === 'function') {
    return transform(obj, methodName);
  } else {
    return undefined;
  }
}

/*!
* sweetalert2 v7.18.0
* Released under the MIT License.
*/
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):e.Sweetalert2=t()}(this,function(){"use strict";var e="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},t=function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")},n=function(){function e(e,t){for(var n=0;n<t.length;n++){var o=t[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}}return function(t,n,o){return n&&e(t.prototype,n),o&&e(t,o),t}}(),o=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var o in n)Object.prototype.hasOwnProperty.call(n,o)&&(e[o]=n[o])}return e},i=function e(t,n,o){null===t&&(t=Function.prototype);var i=Object.getOwnPropertyDescriptor(t,n);if(void 0===i){var r=Object.getPrototypeOf(t);return null===r?void 0:e(r,n,o)}if("value"in i)return i.value;var a=i.get;return void 0!==a?a.call(o):void 0},r=function(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,enumerable:!1,writable:!0,configurable:!0}}),t&&(Object.setPrototypeOf?Object.setPrototypeOf(e,t):e.__proto__=t)},a=function(e,t){if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!t||"object"!=typeof t&&"function"!=typeof t?e:t},s=function(e,t){if(Array.isArray(e))return e;if(Symbol.iterator in Object(e))return function(e,t){var n=[],o=!0,i=!1,r=void 0;try{for(var a,s=e[Symbol.iterator]();!(o=(a=s.next()).done)&&(n.push(a.value),!t||n.length!==t);o=!0);}catch(e){i=!0,r=e}finally{try{!o&&s.return&&s.return()}finally{if(i)throw r}}return n}(e,t);throw new TypeError("Invalid attempt to destructure non-iterable instance")},u="SweetAlert2:",l=function(e){var t=[];return e instanceof Map?e.forEach(function(e,n){t.push([n,e])}):Object.keys(e).forEach(function(n){t.push([n,e[n]])}),t},c=function(e){console.warn(u+" "+e)},d=function(e){console.error(u+" "+e)},p=[],f=function(e){-1===p.indexOf(e)&&(p.push(e),c(e))},m=function(e){return"function"==typeof e?e():e},h=function(t){return"object"===(void 0===t?"undefined":e(t))&&"function"==typeof t.then},v=Object.freeze({cancel:"cancel",backdrop:"overlay",close:"close",esc:"esc",timer:"timer"}),b=function(e){var t={};for(var n in e)t[e[n]]="swal2-"+e[n];return t},g=b(["container","shown","iosfix","popup","modal","no-backdrop","toast","toast-shown","fade","show","hide","noanimation","close","title","header","content","actions","confirm","cancel","footer","icon","icon-text","image","input","has-input","file","range","select","radio","checkbox","textarea","inputerror","validationerror","progresssteps","activeprogressstep","progresscircle","progressline","loading","styled","top","top-start","top-end","top-left","top-right","center","center-start","center-end","center-left","center-right","bottom","bottom-start","bottom-end","bottom-left","bottom-right","grow-row","grow-column","grow-fullscreen"]),y=b(["success","warning","info","question","error"]),w={previousActiveElement:null,previousBodyPadding:null},C=function(e,t){return!!e.classList&&e.classList.contains(t)},x=function(e){if(e.focus(),"file"!==e.type){var t=e.value;e.value="",e.value=t}},k=function(e,t,n){e&&t&&("string"==typeof t&&(t=t.split(/\s+/).filter(Boolean)),t.forEach(function(t){e.forEach?e.forEach(function(e){n?e.classList.add(t):e.classList.remove(t)}):n?e.classList.add(t):e.classList.remove(t)}))},B=function(e,t){k(e,t,!0)},A=function(e,t){k(e,t,!1)},E=function(e,t){for(var n=0;n<e.childNodes.length;n++)if(C(e.childNodes[n],t))return e.childNodes[n]},S=function(e){e.style.opacity="",e.style.display=e.id===g.content?"block":"flex"},P=function(e){e.style.opacity="",e.style.display="none"},O=function(e){for(;e.firstChild;)e.removeChild(e.firstChild)},L=function(e){return e&&(e.offsetWidth||e.offsetHeight||e.getClientRects().length)},T=function(e,t){e.style.removeProperty?e.style.removeProperty(t):e.style.removeAttribute(t)},j=function(){return document.body.querySelector("."+g.container)},_=function(e){var t=j();return t?t.querySelector("."+e):null},V=function(){return _(g.popup)},q=function(){return V().querySelectorAll("."+g.icon)},I=function(){return _(g.title)},R=function(){return _(g.content)},D=function(){return _(g.image)},N=function(){return _(g.progresssteps)},H=function(){return _(g.validationerror)},M=function(){return _(g.confirm)},W=function(){return _(g.cancel)},z=function(){return _(g.actions)},U=function(){return _(g.footer)},K=function(){return _(g.close)},F=function(){var e=Array.prototype.slice.call(V().querySelectorAll('[tabindex]:not([tabindex="-1"]):not([tabindex="0"])')).sort(function(e,t){return(e=parseInt(e.getAttribute("tabindex")))>(t=parseInt(t.getAttribute("tabindex")))?1:e<t?-1:0}),t=Array.prototype.slice.call(V().querySelectorAll('a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable], audio[controls], video[controls]'));return function(e){for(var t=[],n=0;n<e.length;n++)-1===t.indexOf(e[n])&&t.push(e[n]);return t}(e.concat(t))},Z=function(){return!document.body.classList.contains(g["toast-shown"])},Q=function(){return"undefined"==typeof window||"undefined"==typeof document},Y=('\n <div aria-labelledby="'+g.title+'" aria-describedby="'+g.content+'" class="'+g.popup+'" tabindex="-1">\n   <div class="'+g.header+'">\n     <ul class="'+g.progresssteps+'"></ul>\n     <div class="'+g.icon+" "+y.error+'">\n       <span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>\n     </div>\n     <div class="'+g.icon+" "+y.question+'">\n       <span class="'+g["icon-text"]+'">?</span>\n      </div>\n     <div class="'+g.icon+" "+y.warning+'">\n       <span class="'+g["icon-text"]+'">!</span>\n      </div>\n     <div class="'+g.icon+" "+y.info+'">\n       <span class="'+g["icon-text"]+'">i</span>\n      </div>\n     <div class="'+g.icon+" "+y.success+'">\n       <div class="swal2-success-circular-line-left"></div>\n       <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>\n       <div class="swal2-success-ring"></div> <div class="swal2-success-fix"></div>\n       <div class="swal2-success-circular-line-right"></div>\n     </div>\n     <img class="'+g.image+'" />\n     <h2 class="'+g.title+'" id="'+g.title+'"></h2>\n     <button type="button" class="'+g.close+'">×</button>\n   </div>\n   <div class="'+g.content+'">\n     <div id="'+g.content+'"></div>\n     <input class="'+g.input+'" />\n     <input type="file" class="'+g.file+'" />\n     <div class="'+g.range+'">\n       <input type="range" />\n       <output></output>\n     </div>\n     <select class="'+g.select+'"></select>\n     <div class="'+g.radio+'"></div>\n     <label for="'+g.checkbox+'" class="'+g.checkbox+'">\n       <input type="checkbox" />\n     </label>\n     <textarea class="'+g.textarea+'"></textarea>\n     <div class="'+g.validationerror+'" id="'+g.validationerror+'"></div>\n   </div>\n   <div class="'+g.actions+'">\n     <button type="button" class="'+g.confirm+'">OK</button>\n     <button type="button" class="'+g.cancel+'">Cancel</button>\n   </div>\n   <div class="'+g.footer+'">\n   </div>\n </div>\n').replace(/(^|\n)\s*/g,""),$=function(e){var t=j();if(t&&(t.parentNode.removeChild(t),A([document.documentElement,document.body],[g["no-backdrop"],g["has-input"],g["toast-shown"]])),!Q()){var n=document.createElement("div");n.className=g.container,n.innerHTML=Y,("string"==typeof e.target?document.querySelector(e.target):e.target).appendChild(n);var o=V(),i=R(),r=E(i,g.input),a=E(i,g.file),s=i.querySelector("."+g.range+" input"),u=i.querySelector("."+g.range+" output"),l=E(i,g.select),c=i.querySelector("."+g.checkbox+" input"),p=E(i,g.textarea);o.setAttribute("role",e.toast?"alert":"dialog"),o.setAttribute("aria-live",e.toast?"polite":"assertive"),e.toast||o.setAttribute("aria-modal","true");var f=function(){he.isVisible()&&he.resetValidationError()};return r.oninput=f,a.onchange=f,l.onchange=f,c.onchange=f,p.oninput=f,s.oninput=function(){f(),u.value=s.value},s.onchange=function(){f(),s.nextSibling.value=s.value},o}d("SweetAlert2 requires document to initialize")},J=function(t,n){if(!t)return P(n);if("object"===(void 0===t?"undefined":e(t)))if(n.innerHTML="",0 in t)for(var o=0;o in t;o++)n.appendChild(t[o].cloneNode(!0));else n.appendChild(t.cloneNode(!0));else t&&(n.innerHTML=t);S(n)},X=function(){if(Q())return!1;var e=document.createElement("div"),t={WebkitAnimation:"webkitAnimationEnd",OAnimation:"oAnimationEnd oanimationend",animation:"animationend"};for(var n in t)if(t.hasOwnProperty(n)&&void 0!==e.style[n])return t[n];return!1}(),G=function(){null===w.previousBodyPadding&&document.body.scrollHeight>window.innerHeight&&(w.previousBodyPadding=document.body.style.paddingRight,document.body.style.paddingRight=function(){if("ontouchstart"in window||navigator.msMaxTouchPoints)return 0;var e=document.createElement("div");e.style.width="50px",e.style.height="50px",e.style.overflow="scroll",document.body.appendChild(e);var t=e.offsetWidth-e.clientWidth;return document.body.removeChild(e),t}()+"px")},ee={title:"",titleText:"",text:"",html:"",footer:"",type:null,toast:!1,customClass:"",target:"body",backdrop:!0,animation:!0,allowOutsideClick:!0,allowEscapeKey:!0,allowEnterKey:!0,showConfirmButton:!0,showCancelButton:!1,preConfirm:null,confirmButtonText:"OK",confirmButtonAriaLabel:"",confirmButtonColor:null,confirmButtonClass:null,cancelButtonText:"Cancel",cancelButtonAriaLabel:"",cancelButtonColor:null,cancelButtonClass:null,buttonsStyling:!0,reverseButtons:!1,focusConfirm:!0,focusCancel:!1,showCloseButton:!1,closeButtonAriaLabel:"Close this dialog",showLoaderOnConfirm:!1,imageUrl:null,imageWidth:null,imageHeight:null,imageAlt:"",imageClass:null,timer:null,width:null,padding:null,background:null,input:null,inputPlaceholder:"",inputValue:"",inputOptions:{},inputAutoTrim:!0,inputClass:null,inputAttributes:{},inputValidator:null,grow:!1,position:"center",progressSteps:[],currentProgressStep:null,progressStepsDistance:null,onBeforeOpen:null,onAfterClose:null,onOpen:null,onClose:null,useRejections:!1,expectRejections:!1},te=["useRejections","expectRejections"],ne=function(e){return ee.hasOwnProperty(e)||"extraParams"===e},oe=function(e){return-1!==te.indexOf(e)},ie=function(e){for(var t in e)ne(t)||c('Unknown parameter "'+t+'"'),oe(t)&&f('The parameter "'+t+'" is deprecated and will be removed in the next major release.')},re={popupParams:o({},ee)},ae=function(e,t){var n=j(),o=V();if(o){null!==e&&"function"==typeof e&&e(o),A(o,g.show),B(o,g.hide),clearTimeout(o.timeout),document.body.classList.contains(g["toast-shown"])||(!function(){if(w.previousActiveElement&&w.previousActiveElement.focus){var e=window.scrollX,t=window.scrollY;w.previousActiveElement.focus(),void 0!==e&&void 0!==t&&window.scrollTo(e,t)}}(),window.onkeydown=re.previousWindowKeyDown,re.windowOnkeydownOverridden=!1);var i=function(){n.parentNode&&n.parentNode.removeChild(n),A([document.documentElement,document.body],[g.shown,g["no-backdrop"],g["has-input"],g["toast-shown"]]),Z()&&(null!==w.previousBodyPadding&&(document.body.style.paddingRight=w.previousBodyPadding,w.previousBodyPadding=null),function(){if(C(document.body,g.iosfix)){var e=parseInt(document.body.style.top,10);A(document.body,g.iosfix),document.body.style.top="",document.body.scrollTop=-1*e}}()),null!==t&&"function"==typeof t&&setTimeout(function(){t()})};X&&!C(o,g.noanimation)?o.addEventListener(X,function e(){o.removeEventListener(X,e),C(o,g.hide)&&i()}):i()}};var se=[],ue=function(){var e=V();e||he(""),e=V();var t=z(),n=M(),o=W();S(t),S(n),B([e,t],g.loading),n.disabled=!0,o.disabled=!0,e.setAttribute("data-loading",!0),e.setAttribute("aria-busy",!0),e.focus()};var le=Object.freeze({isValidParameter:ne,isDeprecatedParameter:oe,argsToParams:function(t){var n={};switch(e(t[0])){case"string":["title","html","type"].forEach(function(e,o){void 0!==t[o]&&(n[e]=t[o])});break;case"object":o(n,t[0]);break;default:return d('Unexpected type of argument! Expected "string" or "object", got '+e(t[0])),!1}return n},adaptInputValidator:function(e){return function(t,n){return e.call(this,t,n).then(function(){},function(e){return e})}},close:ae,closePopup:ae,closeModal:ae,closeToast:ae,isVisible:function(){return!!V()},clickConfirm:function(){return M().click()},clickCancel:function(){return W().click()},getTitle:I,getContent:R,getImage:D,getButtonsWrapper:function(){return f("swal.getButtonsWrapper() is deprecated and will be removed in the next major release, use swal.getActions() instead"),_(g.actions)},getActions:z,getConfirmButton:M,getCancelButton:W,getFooter:U,isLoading:function(){return V().hasAttribute("data-loading")},mixin:function(e){return function(s){function u(){return t(this,u),a(this,(u.__proto__||Object.getPrototypeOf(u)).apply(this,arguments))}return r(u,this),n(u,[{key:"_main",value:function(t){return i(u.prototype.__proto__||Object.getPrototypeOf(u.prototype),"_main",this).call(this,o({},e,t))}}]),u}()},queue:function(e){var t=this;se=e;var n=function(){se=[],document.body.removeAttribute("data-swal2-queue-step")},o=[];return new Promise(function(e,i){!function i(r,a){r<se.length?(document.body.setAttribute("data-swal2-queue-step",r),t(se[r]).then(function(t){void 0!==t.value?(o.push(t.value),i(r+1,a)):(n(),e({dismiss:t.dismiss}))})):(n(),e({value:o}))}(0)})},getQueueStep:function(){return document.body.getAttribute("data-swal2-queue-step")},insertQueueStep:function(e,t){return t&&t<se.length?se.splice(t,0,e):se.push(e)},deleteQueueStep:function(e){void 0!==se[e]&&se.splice(e,1)},setDefaults:function(t){if(!t||"object"!==(void 0===t?"undefined":e(t)))return d("the argument for setDefaults() is required and has to be a object");for(var n in ie(t),t)ne(n)&&(re.popupParams[n]=t[n])},resetDefaults:function(){re.popupParams=o({},ee)},showLoading:ue,enableLoading:ue,fire:function(){for(var e=arguments.length,t=Array(e),n=0;n<e;n++)t[n]=arguments[n];return new(Function.prototype.bind.apply(this,[null].concat(t)))}});function ce(){var e=this._domCache;this.params.showConfirmButton||(P(e.confirmButton),this.params.showCancelButton||P(e.actions)),A([e.popup,e.actions],g.loading),e.popup.removeAttribute("aria-busy"),e.popup.removeAttribute("data-loading"),e.confirmButton.disabled=!1,e.cancelButton.disabled=!1}var de={email:function(e){return/^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/.test(e)?Promise.resolve():Promise.reject("Invalid email address")},url:function(e){return/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/.test(e)?Promise.resolve():Promise.reject("Invalid URL")}};var pe=function(e,t,n){var o=j(),i=V();null!==t&&"function"==typeof t&&t(i),e?(B(i,g.show),B(o,g.fade),A(i,g.hide)):A(i,g.fade),S(i),o.style.overflowY="hidden",X&&!C(i,g.noanimation)?i.addEventListener(X,function e(){i.removeEventListener(X,e),o.style.overflowY="auto"}):o.style.overflowY="auto",B([document.documentElement,document.body,o],g.shown),Z()&&(G(),function(){if(/iPad|iPhone|iPod/.test(navigator.userAgent)&&!window.MSStream&&!C(document.body,g.iosfix)){var e=document.body.scrollTop;document.body.style.top=-1*e+"px",B(document.body,g.iosfix)}}()),w.previousActiveElement=document.activeElement,null!==n&&"function"==typeof n&&setTimeout(function(){n(i)})};var fe=Object.freeze({hideLoading:ce,disableLoading:ce,getInput:function(e){var t=this._domCache;if(!(e=e||this.params.input))return null;switch(e){case"select":case"textarea":case"file":return E(t.content,g[e]);case"checkbox":return t.popup.querySelector("."+g.checkbox+" input");case"radio":return t.popup.querySelector("."+g.radio+" input:checked")||t.popup.querySelector("."+g.radio+" input:first-child");case"range":return t.popup.querySelector("."+g.range+" input");default:return E(t.content,g.input)}},enableButtons:function(){this._domCache.confirmButton.disabled=!1,this._domCache.cancelButton.disabled=!1},disableButtons:function(){this._domCache.confirmButton.disabled=!0,this._domCache.cancelButton.disabled=!0},enableConfirmButton:function(){this._domCache.confirmButton.disabled=!1},disableConfirmButton:function(){this._domCache.confirmButton.disabled=!0},enableInput:function(){var e=this.getInput();if(!e)return!1;if("radio"===e.type)for(var t=e.parentNode.parentNode.querySelectorAll("input"),n=0;n<t.length;n++)t[n].disabled=!1;else e.disabled=!1},disableInput:function(){var e=this.getInput();if(!e)return!1;if(e&&"radio"===e.type)for(var t=e.parentNode.parentNode.querySelectorAll("input"),n=0;n<t.length;n++)t[n].disabled=!0;else e.disabled=!0},showValidationError:function(e){var t=this._domCache;t.validationError.innerHTML=e;var n=window.getComputedStyle(t.popup);t.validationError.style.marginLeft="-"+n.getPropertyValue("padding-left"),t.validationError.style.marginRight="-"+n.getPropertyValue("padding-right"),S(t.validationError);var o=this.getInput();o&&(o.setAttribute("aria-invalid",!0),o.setAttribute("aria-describedBy",g.validationerror),x(o),B(o,g.inputerror))},resetValidationError:function(){var e=this._domCache;e.validationError&&P(e.validationError);var t=this.getInput();t&&(t.removeAttribute("aria-invalid"),t.removeAttribute("aria-describedBy"),A(t,g.inputerror))},_main:function(t){var n=this;ie(t);var i=this.params=o({},re.popupParams,t);!function(e){e.inputValidator||Object.keys(de).forEach(function(t){e.input===t&&(e.inputValidator=e.expectRejections?de[t]:he.adaptInputValidator(de[t]))}),(!e.target||"string"==typeof e.target&&!document.querySelector(e.target)||"string"!=typeof e.target&&!e.target.appendChild)&&(c('Target parameter is not valid, defaulting to "body"'),e.target="body");var t=void 0,n=V(),o="string"==typeof e.target?document.querySelector(e.target):e.target;t=n&&o&&n.parentNode!==o.parentNode?$(e):n||$(e),e.width&&(t.style.width="number"==typeof e.width?e.width+"px":e.width),e.padding&&(t.style.padding="number"==typeof e.padding?e.padding+"px":e.padding),e.background&&(t.style.background=e.background);for(var i=window.getComputedStyle(t).getPropertyValue("background-color"),r=t.querySelectorAll("[class^=swal2-success-circular-line], .swal2-success-fix"),a=0;a<r.length;a++)r[a].style.backgroundColor=i;var s=j(),u=I(),l=R().querySelector("#"+g.content),p=z(),f=M(),m=W(),h=K(),v=U();if(e.titleText?u.innerText=e.titleText:e.title&&(u.innerHTML=e.title.split("\n").join("<br />")),"string"==typeof e.backdrop?j().style.background=e.backdrop:e.backdrop||B([document.documentElement,document.body],g["no-backdrop"]),e.html?J(e.html,l):e.text?(l.textContent=e.text,S(l)):P(l),e.position in g?B(s,g[e.position]):(c('The "position" parameter is not valid, defaulting to "center"'),B(s,g.center)),e.grow&&"string"==typeof e.grow){var b="grow-"+e.grow;b in g&&B(s,g[b])}"function"==typeof e.animation&&(e.animation=e.animation.call()),e.showCloseButton?(h.setAttribute("aria-label",e.closeButtonAriaLabel),S(h)):P(h),t.className=g.popup,e.toast?(B([document.documentElement,document.body],g["toast-shown"]),B(t,g.toast)):B(t,g.modal),e.customClass&&B(t,e.customClass);var w=N(),C=parseInt(null===e.currentProgressStep?he.getQueueStep():e.currentProgressStep,10);e.progressSteps&&e.progressSteps.length?(S(w),O(w),C>=e.progressSteps.length&&c("Invalid currentProgressStep parameter, it should be less than progressSteps.length (currentProgressStep like JS arrays starts from 0)"),e.progressSteps.forEach(function(t,n){var o=document.createElement("li");if(B(o,g.progresscircle),o.innerHTML=t,n===C&&B(o,g.activeprogressstep),w.appendChild(o),n!==e.progressSteps.length-1){var i=document.createElement("li");B(i,g.progressline),e.progressStepsDistance&&(i.style.width=e.progressStepsDistance),w.appendChild(i)}})):P(w);for(var x=q(),k=0;k<x.length;k++)P(x[k]);if(e.type){var E=!1;for(var L in y)if(e.type===L){E=!0;break}if(!E)return d("Unknown alert type: "+e.type),!1;var _=t.querySelector("."+g.icon+"."+y[e.type]);S(_),e.animation&&B(_,"swal2-animate-"+e.type+"-icon")}var H=D();if(e.imageUrl?(H.setAttribute("src",e.imageUrl),H.setAttribute("alt",e.imageAlt),S(H),e.imageWidth?H.setAttribute("width",e.imageWidth):H.removeAttribute("width"),e.imageHeight?H.setAttribute("height",e.imageHeight):H.removeAttribute("height"),H.className=g.image,e.imageClass&&B(H,e.imageClass)):P(H),e.showCancelButton?m.style.display="inline-block":P(m),e.showConfirmButton?T(f,"display"):P(f),e.showConfirmButton||e.showCancelButton?S(p):P(p),f.innerHTML=e.confirmButtonText,m.innerHTML=e.cancelButtonText,f.setAttribute("aria-label",e.confirmButtonAriaLabel),m.setAttribute("aria-label",e.cancelButtonAriaLabel),f.className=g.confirm,B(f,e.confirmButtonClass),m.className=g.cancel,B(m,e.cancelButtonClass),e.buttonsStyling){B([f,m],g.styled),e.confirmButtonColor&&(f.style.backgroundColor=e.confirmButtonColor),e.cancelButtonColor&&(m.style.backgroundColor=e.cancelButtonColor);var F=window.getComputedStyle(f).getPropertyValue("background-color");f.style.borderLeftColor=F,f.style.borderRightColor=F}else A([f,m],g.styled),f.style.backgroundColor=f.style.borderLeftColor=f.style.borderRightColor="",m.style.backgroundColor=m.style.borderLeftColor=m.style.borderRightColor="";J(e.footer,v),!0===e.animation?A(t,g.noanimation):B(t,g.noanimation),e.showLoaderOnConfirm&&!e.preConfirm&&c("showLoaderOnConfirm is set to true, but preConfirm is not defined.\nshowLoaderOnConfirm should be used together with preConfirm, see usage example:\nhttps://sweetalert2.github.io/#ajax-request")}(i),Object.freeze(i);var r=this._domCache={popup:V(),container:j(),content:R(),actions:z(),confirmButton:M(),cancelButton:W(),closeButton:K(),validationError:H(),progressSteps:N()},a=this.constructor;return new Promise(function(t,o){var u=function(e){a.closePopup(i.onClose,i.onAfterClose),i.useRejections?t(e):t({value:e})},c=function(e){a.closePopup(i.onClose,i.onAfterClose),i.useRejections?o(e):t({dismiss:e})},p=function(e){a.closePopup(i.onClose,i.onAfterClose),o(e)};i.timer&&(r.popup.timeout=setTimeout(function(){return c("timer")},i.timer)),i.input&&setTimeout(function(){var e=n.getInput();e&&x(e)},0);for(var f=function(e){if(i.showLoaderOnConfirm&&a.showLoading(),i.preConfirm){n.resetValidationError();var t=Promise.resolve().then(function(){return i.preConfirm(e,i.extraParams)});i.expectRejections?t.then(function(t){return u(t||e)},function(e){n.hideLoading(),e&&n.showValidationError(e)}):t.then(function(t){L(r.validationError)||!1===t?n.hideLoading():u(t||e)},function(e){return p(e)})}else u(e)},v=function(e){var t=e||window.event,o=t.target||t.srcElement,s=r.confirmButton,u=r.cancelButton,l=s&&(s===o||s.contains(o)),d=u&&(u===o||u.contains(o));switch(t.type){case"click":if(l&&a.isVisible())if(n.disableButtons(),i.input){var m=function(){var e=n.getInput();if(!e)return null;switch(i.input){case"checkbox":return e.checked?1:0;case"radio":return e.checked?e.value:null;case"file":return e.files.length?e.files[0]:null;default:return i.inputAutoTrim?e.value.trim():e.value}}();if(i.inputValidator){n.disableInput();var h=Promise.resolve().then(function(){return i.inputValidator(m,i.extraParams)});i.expectRejections?h.then(function(){n.enableButtons(),n.enableInput(),f(m)},function(e){n.enableButtons(),n.enableInput(),e&&n.showValidationError(e)}):h.then(function(e){n.enableButtons(),n.enableInput(),e?n.showValidationError(e):f(m)},function(e){return p(e)})}else f(m)}else f(!0);else d&&a.isVisible()&&(n.disableButtons(),c(a.DismissReason.cancel))}},b=r.popup.querySelectorAll("button"),y=0;y<b.length;y++)b[y].onclick=v,b[y].onmouseover=v,b[y].onmouseout=v,b[y].onmousedown=v;if(r.closeButton.onclick=function(){c(a.DismissReason.close)},i.toast)r.popup.onclick=function(e){i.showConfirmButton||i.showCancelButton||i.showCloseButton||i.input||(a.closePopup(i.onClose,i.onAfterClose),c(a.DismissReason.close))};else{var w=!1;r.popup.onmousedown=function(){r.container.onmouseup=function(e){r.container.onmouseup=void 0,e.target===r.container&&(w=!0)}},r.container.onmousedown=function(){r.popup.onmouseup=function(e){r.popup.onmouseup=void 0,(e.target===r.popup||r.popup.contains(e.target))&&(w=!0)}},r.container.onclick=function(e){w?w=!1:e.target===r.container&&m(i.allowOutsideClick)&&c(a.DismissReason.backdrop)}}i.reverseButtons?r.confirmButton.parentNode.insertBefore(r.cancelButton,r.confirmButton):r.confirmButton.parentNode.insertBefore(r.confirmButton,r.cancelButton);var C=function(e,t){for(var n=F(i.focusCancel),o=0;o<n.length;o++){(e+=t)===n.length?e=0:-1===e&&(e=n.length-1);var r=n[e];if(L(r))return r.focus()}};i.toast&&re.windowOnkeydownOverridden&&(window.onkeydown=re.previousWindowKeyDown,re.windowOnkeydownOverridden=!1),i.toast||re.windowOnkeydownOverridden||(re.previousWindowKeyDown=window.onkeydown,re.windowOnkeydownOverridden=!0,window.onkeydown=function(e){var t=e||window.event;if("Enter"!==t.key||t.isComposing)if("Tab"===t.key){for(var o=t.target||t.srcElement,s=F(i.focusCancel),u=-1,l=0;l<s.length;l++)if(o===s[l]){u=l;break}t.shiftKey?C(u,-1):C(u,1),t.stopPropagation(),t.preventDefault()}else-1!==["ArrowLeft","ArrowRight","ArrowUp","ArrowDown","Left","Right","Up","Down"].indexOf(t.key)?document.activeElement===r.confirmButton&&L(r.cancelButton)?r.cancelButton.focus():document.activeElement===r.cancelButton&&L(r.confirmButton)&&r.confirmButton.focus():"Escape"!==t.key&&"Esc"!==t.key||!0!==m(i.allowEscapeKey)||c(a.DismissReason.esc);else if(t.target===n.getInput()){if(-1!==["textarea","file"].indexOf(i.input))return;a.clickConfirm(),t.preventDefault()}}),n.enableButtons(),n.hideLoading(),n.resetValidationError(),i.input&&B(document.body,g["has-input"]);for(var k=["input","file","range","select","radio","checkbox","textarea"],A=void 0,O=0;O<k.length;O++){var T=g[k[O]],j=E(r.content,T);if(A=n.getInput(k[O])){for(var _ in A.attributes)if(A.attributes.hasOwnProperty(_)){var V=A.attributes[_].name;"type"!==V&&"value"!==V&&A.removeAttribute(V)}for(var q in i.inputAttributes)A.setAttribute(q,i.inputAttributes[q])}j.className=T,i.inputClass&&B(j,i.inputClass),P(j)}var I=void 0;switch(i.input){case"text":case"email":case"password":case"number":case"tel":case"url":(A=E(r.content,g.input)).value=i.inputValue,A.placeholder=i.inputPlaceholder,A.type=i.input,S(A);break;case"file":(A=E(r.content,g.file)).placeholder=i.inputPlaceholder,A.type=i.input,S(A);break;case"range":var R=E(r.content,g.range),D=R.querySelector("input"),N=R.querySelector("output");D.value=i.inputValue,D.type=i.input,N.value=i.inputValue,S(R);break;case"select":var H=E(r.content,g.select);if(H.innerHTML="",i.inputPlaceholder){var M=document.createElement("option");M.innerHTML=i.inputPlaceholder,M.value="",M.disabled=!0,M.selected=!0,H.appendChild(M)}I=function(e){e.forEach(function(e){var t=s(e,2),n=t[0],o=t[1],r=document.createElement("option");r.value=n,r.innerHTML=o,i.inputValue.toString()===n.toString()&&(r.selected=!0),H.appendChild(r)}),S(H),H.focus()};break;case"radio":var W=E(r.content,g.radio);W.innerHTML="",I=function(e){e.forEach(function(e){var t=s(e,2),n=t[0],o=t[1],r=document.createElement("input"),a=document.createElement("label");r.type="radio",r.name=g.radio,r.value=n,i.inputValue.toString()===n.toString()&&(r.checked=!0),a.innerHTML=o,a.insertBefore(r,a.firstChild),W.appendChild(a)}),S(W);var t=W.querySelectorAll("input");t.length&&t[0].focus()};break;case"checkbox":var z=E(r.content,g.checkbox),U=n.getInput("checkbox");U.type="checkbox",U.value=1,U.id=g.checkbox,U.checked=Boolean(i.inputValue);var K=z.getElementsByTagName("span");K.length&&z.removeChild(K[0]),(K=document.createElement("span")).innerHTML=i.inputPlaceholder,z.appendChild(K),S(z);break;case"textarea":var Z=E(r.content,g.textarea);Z.value=i.inputValue,Z.placeholder=i.inputPlaceholder,S(Z);break;case null:break;default:d('Unexpected type of input! Expected "text", "email", "password", "number", "tel", "select", "radio", "checkbox", "textarea", "file" or "url", got "'+i.input+'"')}if("select"===i.input||"radio"===i.input){var Q=function(e){return I(l(e))};h(i.inputOptions)?(a.showLoading(),i.inputOptions.then(function(e){n.hideLoading(),Q(e)})):"object"===e(i.inputOptions)?Q(i.inputOptions):d("Unexpected type of inputOptions! Expected object, Map or Promise, got "+e(i.inputOptions))}else-1!==["text","email","number","tel","textarea"].indexOf(i.input)&&h(i.inputValue)&&(a.showLoading(),P(A),i.inputValue.then(function(e){A.value="number"===i.input?parseFloat(e)||0:e+"",S(A),n.hideLoading()}).catch(function(e){d("Error in inputValue promise: "+e),A.value="",S(A),n.hideLoading()}));pe(i.animation,i.onBeforeOpen,i.onOpen),i.toast||(m(i.allowEnterKey)?i.focusCancel&&L(r.cancelButton)?r.cancelButton.focus():i.focusConfirm&&L(r.confirmButton)?r.confirmButton.focus():C(-1,1):document.activeElement&&document.activeElement.blur()),r.container.scrollTop=0})}}),me=void 0;function he(){if("undefined"!=typeof window){"undefined"==typeof Promise&&d("This package requires a Promise library, please include a shim to enable it in this browser (See: https://github.com/sweetalert2/sweetalert2/wiki/Migration-from-SweetAlert-to-SweetAlert2#1-ie-support)");for(var e=arguments.length,t=Array(e),n=0;n<e;n++)t[n]=arguments[n];if(void 0===t[0])return d("SweetAlert2 expects at least 1 attribute!"),!1;if(!(this instanceof he))return new(Function.prototype.bind.apply(he,[null].concat(t)));me=this,this._promise=this._main(this.constructor.argsToParams(t))}}return he.prototype.then=function(e,t){return this._promise.then(e,t)},he.prototype.catch=function(e){return this._promise.catch(e)},he.prototype.finally=function(e){return this._promise.finally(e)},o(he.prototype,fe),o(he,le),Object.keys(fe).forEach(function(e){he[e]=function(){var t;if(me)return(t=me)[e].apply(t,arguments)}}),he.DismissReason=v,he.noop=function(){},he.version="7.18.0",he.default=he,"undefined"!=typeof window&&"object"===e(window._swalDefaults)&&he.setDefaults(window._swalDefaults),he}),"undefined"!=typeof window&&window.Sweetalert2&&(window.swal=window.sweetAlert=window.Swal=window.SweetAlert=window.Sweetalert2);
//Fixed top
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > 50) {
        $('.header-connect').addClass('header-connect-fixed');
        // console.log($('.header-connect'));
    } else {
        $('.header-connect').removeClass('header-connect-fixed');
        // console.log('remove');
    }
});


// $('i[class="fa fa-shopping-basket"]').hover(
// 	console.log('message');
// );
$( 'i[class="fa fa-shopping-basket"]' ).hover(
  function() {
    // $( this ).append( $( "<span> ***</span>" ) );
    var elment_ = $( this ).parent().parent();
    // console.log(elment_);

    $(elment_).addClass('open');

  }, function() {
    // $( this ).find( "span:last" ).remove();
  }
);

$( document ).ready(function() {
    validateDevies();
});

function validateDevies(){
    if(Modernizr.mq("(max-width: 768px)")){
        console.log('mobile');
        gmenu_logoutin_m();
        gmenu_historybuy_m();
        $('li[id="menu-basket"]').css('padding-right','0px');
        $('p[id="footer-line1"]').css("line-height","110%");
        $('p[id="footer-line2"]').css("line-height","110%");
        $('p[id="footer-line3"]').css("line-height","110%");
        $('p[id="footer-line4"]').css("line-height","110%");
    }else{
        console.log('desktop');
        gmenu_logoutin_d();
        gmenu_historybuy_d();
    }
}

function gmenu_logoutin_m(){
    var url = getUrlForAjaxUrl() + 'gmenu_logoutin_m';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('div[id="menu-loginoutchangepass-m"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_logoutin_m}');
        }
    }); // ) $.ajax
}
function gmenu_logoutin_d(){
    var url = getUrlForAjaxUrl() + 'gmenu_logoutin_d';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('li[id="menu-loginoutchangepass-d"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_logoutin_d}');
        }
    }); // ) $.ajax
}


function gmenu_historybuy_m(){
    var url = getUrlForAjaxUrl() + 'gmenu_historybuy_m';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('div[id="menu-historybuy-m"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_historybuy_m}');
        }
    }); // ) $.ajax
}
function gmenu_historybuy_d(){
    var url = getUrlForAjaxUrl() + 'gmenu_historybuy_d';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('li[id="menu-historybuy-d"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_historybuy_d}');
        }
    }); // ) $.ajax
}



function ValidateIPaddress(ipaddress)   
{  
    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/; 
    if(ipaddress.match(ipformat))  
     {  
        return true;  
     }else{  
        return false;
     } 
} 


//
var base_url = window.location.origin; //http://agrisale.dev/
var host = window.location.host; //agrisale.dev
var pathArray = window.location.pathname.split( '/' ); //array path

// sessionStorage.setItem("host", host);

// console.log('Base URL: ', base_url);
// console.log('Host: ', host);
// console.log('Path array: ', pathArray);

// var ss = getUrlForAjaxUrl();
// console.log('getUrlForAjaxUrl: ', ss);
function getUrlForAjaxUrl(){
    if(ValidateIPaddress(host)){
        console.log('It\'s IP: ', base_url + '/' + pathArray[1] + '/');
        return base_url + '/' + pathArray[1] + '/';
    }else{
        console.log('It\'s not IP: ', base_url + '/');
        return base_url + '/';
    }
}


function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
// var height_showproductdetila = $('aside[id="height-showproductdetail"]').height();

// console.log(height_showproductdetila);

//aside id="height-showproductdetail"

    // function getObjects(obj, key, val) {
    //     var objects = [];
    //     for (var i in obj) {
    //         if (!obj.hasOwnProperty(i)) continue;
    //         if (typeof obj[i] == 'object') {
    //             objects = objects.concat(getObjects(obj[i], key, val));
    //         } else if (i == key && obj[key] == val) {
    //             objects.push(obj);
    //         }
    //     }
    //     return objects;
    // }

    // function updateStorage(id, qty) {
    //     // body...'
    //     var oldarobj_basket = JSON.parse(sessionStorage.getItem('basket_obj'));

    //     sessionStorage.removeItem("basket_obj");
    //     // console.log(oldarobj_basket);
    //     // console.log('id', id);
    //     // console.log('qty', qty);
    //     var objects = [];
    //     // console.log('length', oldarobj_basket.length);
    //     if(oldarobj_basket.length>1){
    //         // console.log('1');
    //          for (var key in oldarobj_basket) {
    //             console.log('for id', oldarobj_basket[key].id);
    //             if(oldarobj_basket[key].id==id){
    //                 // console.log('message');
    //                 oldarobj_basket[key].qty = qty;
    //             }

    //             objects.push(oldarobj_basket[key]);
    //             // console.log(oldarobj_basket[key]);

    //             sessionStorage.setItem('basket_obj', JSON.stringify(objects));
    //          }
    //     }else{
    //         // console.log('2');

    //         oldarobj_basket['qty'] = qty;


    //         // console.log('else',oldarobj_basket);
    //         sessionStorage.setItem('basket_obj', JSON.stringify(oldarobj_basket));
    //     }

    // }


    // $('button[id="basket"]').click(function() {
    //     var qty_ofuser = $(this).parent().parent().find('input[id="qty"]');
    //     var qty = 1;
    //     // console.log(qty_ofuser[0]);
    //     // console.log(qty[0].value);


    //     // $(this).text('ดูตะกร้าสินค้า');

    //     if(typeof(qty_ofuser[0]) !== "undefined"){
    //         qty = qty_ofuser[0].value;
    //     }



    //     var retrievedBasketObject = sessionStorage.getItem('basket_obj');

    //     // console.log('retrievedBasketObject', retrievedBasketObject);

    //     if(!retrievedBasketObject){
    //         var varobj_basket = { 'id': this.value, 'qty': qty};

    //         sessionStorage.setItem('basket_obj', JSON.stringify(varobj_basket));

    //     }else{

    //         var matchingobject = getObjects(JSON.parse(sessionStorage.getItem('basket_obj')), 'id', this.value);

    //         if(typeof(matchingobject[0]) !== "undefined"){
    //             //if exist value
    //             console.log('exist value', matchingobject[0].id);

    //             matchingobject[0].qty = matchingobject[0].qty + 1;


    //             // localStorage.setItem('basket_obj', JSON.stringify(matchingobject) );
    //             updateStorage(matchingobject[0].id, matchingobject[0].qty);
    //             // console.log(matchingobject);


    //         }else{
    //             //if not exist value
    //             // console.log('not exist value', this.value);

    //             // // matchingobject.id = this.value;
    //             // // matchingobject.qty = qty;

    //             // var newvarobj_basket = { 'id': this.value, 'qty': qty};

    //             // var oldarobj_basket =[];
    //             // oldarobj_basket = JSON.parse(sessionStorage.getItem('basket_obj'));

    //             // // console.log(oldarobj_basket);
    //             // oldarobj_basket.push(newvarobj_basket);



    //             // sessionStorage.setItem('basket_obj', JSON.stringify(oldarobj_basket));




    //             // var oldarobj_basket = JSON.parse(sessionStorage.getItem('basket_obj'));
    //             // sessionStorage.removeItem("basket_obj");
    //             // var objects = [];
    //             // if(oldarobj_basket.length>1){
    //             //      for (var key in oldarobj_basket) {
    //             //         console.log('for id', oldarobj_basket[key].id);
    //             //         if(oldarobj_basket[key].id==id){
    //             //             // console.log('message');
    //             //             oldarobj_basket[key].qty = qty;
    //             //         }

    //             //         objects.push(oldarobj_basket[key]);
    //             //         // console.log(oldarobj_basket[key]);

    //             //         sessionStorage.setItem('basket_obj', JSON.stringify(objects));
    //             //      }

    //             // }else{
    //             //     // console.log('esl oooo');
    //             //     // var newvarobj_basket = { 'id': this.value, 'qty': qty};
    //             //     // // oldarobj_basket['qty'] = qty;
    //             //     // // console.log(newvarobj_basket);
    //             //     // objects.push(newvarobj_basket);
    //             //     // console.log(objects);

    //             //     // sessionStorage.setItem('basket_obj', JSON.stringify(oldarobj_basket));
    //             //     // sessionStorage.setItem('basket_obj', JSON.stringify(objects));

    //             // }
    //             // updateStorage(this.value, qty);
    //         }
    //         // console.log('message', matchingobject[0]);
    //     }


    //     console.log('varobj_basket', sessionStorage.getItem('basket_obj'));
        
    // });

// function removeSessionStorageBasketKey(){
//     localStorage.removeItem("basketKey");
// }


    if (typeof(Storage) !== "undefined") {

        // Code for localStorage/sessionStorage.
        // console.log('sessionStorage');

        // sessionStorage.setItem("basketKey", "value");
        var basketKey = sessionStorage.getItem("basketKey");
        console.log(basketKey);
        var all_basket = JSON.parse(getBasket(basketKey));
        // console.log(all_basket);
        updateBadgeQtyBasket(all_basket);
        updateBadgeDetailBasket(all_basket);
        updateCartDetailBasket(all_basket);


        // console.log(basketKey);
        if(!basketKey){
            console.log('get new basket key');


          // switch (checkBrowser())
          // {
          //   case "Opera":
          //   case "Safari":
          //   case "Chrome":
          //   case "Blink":
          //       console.log('Chrome Safari');
          //       break;
          //   case "Firefox":
          //   case "IE":
          //   case "Edge":
          //       console.log('IE');
          //       break;
          //   case "Error":
          //       console.log('Error switch {checkBrowser}');
          //     break;
          //} //.) switch







            //get new key 
            $.ajax({
                url: getUrlForAjaxUrl() + 'basket/create',
                type: 'get',
                data:'',
                cache: false,
                success: function(data){
                    // console.log(data);
                    sessionStorage.setItem("basketKey", data);
                },
                error: function(xhr, status, error) {
                    //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                    console.log('Status: ' + status + ' Error: ' + error + '{get new basket key}');
                }
            }); // ) $.ajax
        } //if(!basketKey)
    } else {
        // Sorry! No Web Storage support..
        alert('ขออภัยเว็บไซต์ไม่รองรับเว็บบราวเซอร์ กรุณาเปลี่ยนเวอร์ชั่นเป็นปัจจุบัน\nคำแนะนำ\nchrome4.0+\nIE8.0+\nfirefox3.5+\nsafari4.0+\nopera11.5+');
    }


	function urlExists(url, type){
        // console.log('check url exists:');
		var isValid = false;
		  $.ajax({
		    type: type,
		    url: url,
		    async: false,
		    success: function(){
		      // callback(true);
              console.log('check url result: exist');
		      isValid =  true;
		    },
		    error: function() {
		      // callback(false);
              console.log('check url result: don\'t exist');
		      isValid = false;
		    }
		  });
		 //  console.log(jqXHR.responseText);
		 // return jqXHR.responseText;
		  return isValid;
	} //end of function


    function getBasket(basketKey) {
    	console.log('getBasket', basketKey);
        
    	var url = getUrlForAjaxUrl() + 'basket/getBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		// // console.log(url_exist);

		// if(url_exist==false){
		// 	url = '../basket/getBasket'
		// }

// console.log(url);
        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{getBasket}');
            }
        }); // ) $.ajax

        return jqXHR.responseText;
    }

    function checkExistBasket(basketKey, productID) {
    	// console.log(basketKey, productID);
    	var url = getUrlForAjaxUrl() + 'basket/getexistbasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		// // console.log(url_exist);

		// if(url_exist==false){
		// 	url = '../basket/getexistbasket'
		// }

    	//if return true is mean exist 
    	//if return false is mean no exist product
        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{checkExistBasket}');
            }
        }); // ) $.ajax

        // console.log(jqXHR.responseText.length);
        if(jqXHR.responseText=='0'){
        	return false;
        }else{
        	return true;
        }
        // return jqXHR.responseText;
    } //end of function


    function addBasket(basketKey, productID, qty) {
    	// console.log('basketKey', basketKey);
    	// console.log('productID', productID);
    	// console.log('qty', qty);

    	var url = getUrlForAjaxUrl() + 'basket/addBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		
  //       // console.log('result url exist: ', url_exist);
		// if(url_exist==false){
		// 	url = '../basket/addBasket'
		// }

        // console.log(url);
        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID, qty: qty},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{addBasket}');
            }
        }); // ) $.ajax
    } //end of function


    function updateBasket(basketKey, productID, qty) {
    	// console.log('basketKey', basketKey);
    	// console.log('productID', productID);
    	// console.log('qty', qty);

    	var url = getUrlForAjaxUrl() + 'basket/updateBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		// // console.log(url_exist);
  //       // console.log('result url exist: ', url_exist);

		// if(url_exist==false){
		// 	url = '../basket/updateBasket'
		// }


        // console.log(url);

        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID, qty: qty},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{updateBasket}');
            }
        }); // ) $.ajax
    } //end of function

    function updateBasketQtyForCartPage(basketKey, productID, qty) {
    	var url = getUrlForAjaxUrl() + 'basket/updateBasketQtyForCartPage';
        var type = 'get';

		// var url_exist = urlExists(url, type);


		// if(url_exist==false){
		// 	url = '../basket/updateBasketQtyForCartPage'
		// }

        // console.log(url_exist);

        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID, qty: qty},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{updateBasketQtyForCartPage}');            
            }
        }); // ) $.ajax
    }

    function updateBadgeQtyBasket(arr) {
    	var qty = 0;
    	 for (var key in arr) {
    	 	qty = (parseInt(qty)) + (parseInt(arr[key].qty));
    	 	// console.log(arr[key].qty);
    	 }

    	 $('span[class="badge"]').text(qty);

    	 // console.log(qty);
    } //end of function

    function updateBadgeDetailBasket(arr) {
         var _eml = $('ul[id="showdetailproduct-topmenu"]').find('div[id="content"]');
         var _emlprice = $('ul[id="showdetailproduct-topmenu"]').find('label[id="price"]');

    	// console.log(_eml);

    	// console.log(arr);
    	var x = "";
    	var total = 0;

for (var r = 0; r < arr.length; r++) { 
    // console.log('fsdfdsds', arr[r]);

            total = total + (parseFloat(arr[r].prime) * parseFloat(arr[r].qty));
            x += '<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[r].name + '" onclick="redirectToProductdetail('+ arr[r].product_id +');">' + 
                    '<div class="row">' + 
                        '<div class="col-md-6">' +
                            '<img src="' +  getUrlForAjaxUrl() + 'public/' + arr[r].photo_path +'">' + 
                        '</div>' +
                        '<div class="col-md-6">' +
                            '<h3>' + arr[r].name + '</h3>' +
                            '<p>' + arr[r].qty + ' x ' + arr[r].prime + '฿</p>' + 
                        '</div>' +                      
                    '</div>' +
                 '</a><div class="dot-hr"></div>';

}



        // var r = 0; 
		// for (i < arr.length; i++;) {
		//     console.log('sss', arr[i]);
		//     // console.log(parseFloat(arr[i].prime), parseFloat(arr[i].qty));

		//     //{{ route('basket.show', $product->id ) }}

		//     total = total + (parseFloat(arr[i].prime) * parseFloat(arr[i].qty));
		//     x += '<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[i].name + '" onclick="redirectToProductdetail('+ arr[i].product_id +');">' + 
		//     		'<div class="row">' + 
		//     			'<div class="col-md-6">' +
		//     				'<img src="../public/' + arr[i].photo_path +'">' + 
		//     			'</div>' +
		//     			'<div class="col-md-6">' +
		//     				'<h3>' + arr[i].name + '</h3>' +
		//     				'<p>' + arr[i].qty + ' x ' + arr[i].prime + '฿</p>' + 
		//     			'</div>' +		    			
		//     		'</div>' +
		//     	 '</a><div class="dot-hr"></div>';
		// }

		// console.log(_emlprice);
		$(_eml).html(x);
		$(_emlprice).text('ราคารวม: ' + numberWithCommas(total) + '฿');

    } //end of function updateBadgeDetailBasket


    function updateCartDetailBasket(arr) {
    	// console.log(arr);
         var _emlss = $('div[id="showdetailproduct-cart"]').find('div[id="cartContentBasket"]');
         var _emlpricess = $('div[id="showdetailproduct-cart"]').find('label[id="cartConentBasketprice"]');
         // console.log(_emlss);
         // console.log(_emlpricess);

    	var x = "";
    	var total = 0;
        var i = 0; 
    	// for (i < arr.length; i++;) {
        for (var i = 0; i < arr.length; i++) { 

		    total = total + (parseFloat(arr[i].prime) * parseFloat(arr[i].qty));
		    x += '<div class="row" style="margin-bottom: 10px;">' + 
		    		'<div class="col-md-1">' +
		    			'<a id="remove-cart" style="cursor: pointer; color: red;" title="นำ ' + arr[i].name + ' ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket('+ arr[i].product_id +');">' +
		    				'<i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>' +
		    			'</a>' +
		    		'</div>' +
		    		'<div class="col-md-5">' +
		    			'<div id="topic" style="color: #5cb85c;font-size: 18px;margin-bottom: -10px;">' + arr[i].name + '</div>' +
		    			'<div id="content">' + arr[i].qty + ' x ' + arr[i].prime + '฿</div>' +
		    		'</div>' +
		    		'<div class="col-md-5">' +
		    			//'<a href="#"><img src="http://agrisale.dev/public//produce/1510807949.jpg"></a>' +
		    			'<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[i].name + '" onclick="redirectToProductdetail('+ arr[i].product_id +');"><img src="' +  getUrlForAjaxUrl() + 'public/' + arr[i].photo_path +'"></a>' + 
		    		'</div>' +		    		
		    	 '</div>';
    	}


		$(_emlss).html(x);
		$(_emlpricess).text('มูลค่าสินค้า: ' + numberWithCommas(total) + '฿');
    }  //end of function updateCartDetailBasket
    function updateCartDetailBasketHTMLforShowswal(arr, qty){
        // console.log(arr);
        var html_forshow = '';
        var total = 0;
        var total_qty = 0;
        var i = 0;
        var margin_bottom5 = 'margin-bottom: 5px;border-bottom: 1px dotted #CECBC7;';

        html_forshow += '<div class="row form-inline" style="border-bottom: 1px dotted #CECBC7;">' + 
                             '<div class="col-md-6" style="border-right: 1px dotted #CECBC7;">' +
                                '<div class="row"><div class="col-md-12"><h4 class="text-success text-left">สินค้า ' + qty + ' ชิ้น ได้ถูกเพิ่มเข้าไปในตะกร้าสินค้าของคุณแล้ว</h4></div></div>';

        for (var i = 0; i < arr.length; i++) { 
            total = total + (parseFloat(arr[i].prime) * parseFloat(arr[i].qty));
            total_qty = total_qty + parseFloat(arr[i].qty);
            // console.log(i, arr.length);

            var i_check = i + 1;
            if(i_check == arr.length){
                margin_bottom5 =  'margin-bottom: 0px;'
            }
            html_forshow += '<div class="row" style="' + margin_bottom5 + '">' + 
                    '<div class="col-md-1">' +
                        '<a id="remove-cart" style="cursor: pointer; color: red;" title="นำ ' + arr[i].name + ' ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket('+ arr[i].product_id +');">' +
                            '<i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>' +
                        '</a>' +
                    '</div>' +
                    '<div class="col-md-6 text-left">' +
                        '<div id="topic" style="color: #5cb85c;font-size: 18px;">' + arr[i].name + '</div>' +
                        '<div id="content">' + arr[i].qty + ' x ' + arr[i].prime + '฿</div>' +
                    '</div>' +
                    '<div class="col-md-5">' +
                        //'<a href="#"><img src="http://agrisale.dev/public//produce/1510807949.jpg"></a>' +
                        '<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[i].name + '" onclick="redirectToProductdetail('+ arr[i].product_id +');"><img src="' +  getUrlForAjaxUrl() + 'public/' + arr[i].photo_path +'" style="margin-bottom: 5px;"></a>' + 
                    '</div>' +
                 '</div>';
        }


        html_forshow += '</div>' + 
                            '<div class="col-md-6">' +
                            '<div class="row">' + 
                                '<div class="col-md-12 text-left">' + 
                                    '<h4>ตะกร้าสินค้าของคุณ <small>(สินค้า จำนวน ' + total_qty + ' ชิ้น)</small></h4>' + 
                                '</div>' +
                            '</div>' +
                            '<div class="dot-hr"></div>' +
                            '<div class="col-md-6 text-left">' +
                                '<h4>มูลค่าสินค้ารวม: </h4>' +
                            '</div>' + 
                            '<div class="col-md-6 text-right">' +
                                '<h4>' + numberWithCommas(total) + ' บาท</h4>' +
                            '</div>' +
                            //<button type="button" class="swal2-close" aria-label="Close this dialog" style="display: block;">×</button>
                            //window.location = getUrlForAjaxUrl() + "basket/checkout?basketKey=" + sessionStorage.getItem("basketKey");
                            '<div class="col-md-6 text-left">' +
                                '<a onclick="swal.close();" style="cursor: pointer;font-size: 18px;font-weight: unset;color: #167ac1;">เลือกซื้อสินค้าต่อ</a>' +
                            '</div>' + 
                            '<div class="col-md-6 text-right">' +
                                '<a onclick="checkouts();" class="btn border-btn-checkout" >สั่งซื้อสินค้า</a>' +
                            '</div>' +                                                      
                    '</div>';

        return html_forshow;
    }
    function checkouts(){
        window.location = getUrlForAjaxUrl() + "basket/checkout?basketKey=" + sessionStorage.getItem("basketKey");
    }

    function redirectToProductdetail(productid) {
    	// console.log(productid); 

    	window.location = getUrlForAjaxUrl() + 'basket/' + productid;

    } //end of function redirectToProductdetail

    function removeProductFromBasket(productid, reload) {
    	// console.log(productid);
        if (!reload) reload = 0; //This code is ES5 for support IE


    	var basketKey = sessionStorage.getItem("basketKey");
    	var url = getUrlForAjaxUrl() + 'basket/removeProductFromBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);


		// // console.log(url_exist);

		// if(url_exist==false){
		// 	url = '../basket/removeProductFromBasket';
		// }

        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productid},
            cache: false,
            async: false,
            success: function(data){
                // console.log('result', data);
                // sessionStorage.setItem("basketKey", data);
                // return data
                if(data){
                	console.log('reload status: ',reload);
                	if(reload==0){
                		swal("นำสินค้าออกจากตะกร้าเรียบร้อย", "คุณสามารถดูสินค้าล่าสุดได้ที่เมนูขวามือ หรือเมนูด้านบน!", "success");
                	}else{
                		swal("นำสินค้าออกจากตะกร้าเรียบร้อย", "คุณสามารถดูสินค้าล่าสุดได้ที่เมนูขวามือ หรือเมนูด้านบน!", "success")
						.then(function() { //for support IE
						  window.location.reload();
						});
                        /*.then((value) => { //IE don't suport arrow function
                          window.location.reload();
                        });*/
                	}
                	

                	var all_basket = JSON.parse(getBasket(basketKey));	
			        updateBadgeQtyBasket(all_basket);
			 		updateBadgeDetailBasket(all_basket);
			 		updateCartDetailBasket(all_basket);
                }else{
                	swal("นำสินค้าออกจาตะกร้าไม่สำเร็จ", "กรุณาลองอีกครั้ง!", "error");
                }


            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{removeProductFromBasket}');
            }
        }); // ) $.ajax    	
    } //end of function removeProductFromBasket


	$('a[id="remove-cart"]').hover(
	  function() {
	  	var hover_symbol = $(this).find('i[id="remove-cart-symbol"]');
	    // console.log(hover_symbol);
	    // var elment_ = $( this ).parent().parent(); content: "\f146";
	    // console.log(elment_);

	    $(hover_symbol).removeClass('fa-minus-square-o');
	    $(hover_symbol).addClass('fa-minus-square');

	  }, function() {
	    // $( this ).find( "span:last" ).remove();
	    // console.log('message2');
	    var hover_symbol = $(this).find('i[id="remove-cart-symbol"]');

	    $(hover_symbol).removeClass('fa-minus-square');
	    $(hover_symbol).addClass('fa-minus-square-o');
	  }
	);


    function redirectToCart() {
    	// console.log(productid);
    	var basketKey = sessionStorage.getItem("basketKey");
    	window.location = getUrlForAjaxUrl() + 'cart/' + basketKey;

    } //end of function redirectToProductdetail

	$('button[id="basket"]').click(function() {
		var elment_loading = $(this).find('i');
		$(elment_loading).addClass('fa fa-spinner fa-spin fa-1x fa-fw');
		$(this).attr('disabled','disabled');

		var call_textbutton = $(this).text().trim();

		console.log('Text button:', call_textbutton);

		if(call_textbutton=='Loading...หยิบใส่ตะกร้า'){

			var qty_ofuser = $(this).parent().parent().find('input[id="qty"]');
			var qty = 1;
			var basketKey = sessionStorage.getItem("basketKey");
	        // console.log(qty_ofuser[0]);
	     	// console.log(qty[0].value);

	        if(typeof(qty_ofuser[0]) !== "undefined"){
	            qty = qty_ofuser[0].value;
	        }

	        // console.log(qty);
	        // console.log(this.value);
	        var exist_basket = checkExistBasket(basketKey, this.value);

	        // console.log(exist_basket);
	        if(exist_basket){
	        	//if exist value
	        	console.log('update basket');

	        	updateBasket(basketKey, this.value, qty);

	        }else{
	        	//if not exist value
	        	console.log('add basket');

	        	addBasket(basketKey, this.value, qty);
	        }


	        var all_basket = JSON.parse(getBasket(basketKey));
	 		
	 		// console.log(all_basket);
	        updateBadgeQtyBasket(all_basket);
	 		updateBadgeDetailBasket(all_basket);
	 		updateCartDetailBasket(all_basket);

            var html_forshow = updateCartDetailBasketHTMLforShowswal(all_basket, qty);


	 		// swal("เพิ่มสินค้าเรียบร้อย", "คุณสามารถดูสินค้าได้ที่เมนูขวามือ หรือเมนูด้านบน!", "success");
            swal({
                    position: 'top',
                    // title: '<i>HTML</i> <u>example</u>',
                    width: 800,
                    // type: 'success',
                    html: html_forshow,
                    showConfirmButton: false,
                    showCloseButton: true,
                });


	 		$(this).text('ดูตะกร้าสินค้า');
 		}else{
 			redirectToCart();
 			// console.log('dfsddd');
 		}


        $(elment_loading).removeClass();
        $(this).removeAttr('disabled');
	});



	$('input[id="cart-qty"]').change(function() {
  		// console.log(this.value);
  		var price = $(this).parent().parent().find('#cart-price').text();
  		// console.log(price);
  		var total = price * this.value;
  		// console.log(total);
  		$(this).parent().parent().find('#cart-total-qty').html(total.toFixed(2));


  		// $('span[id="sum-total-price"]').html();
  		// console.log(this);
  		sumTotalPrice(this);
	}); //end of $('input[id="cart-qty"]').change(function()

	function sumTotalPrice(elm) {
		// body...

		var elm_each_row = $(elm).parent().parent().parent().find('tr');

		// console.log(elm_each_row); //COntinue here calculate total
		var sum_total = 0;
		for (var i = 0; i < elm_each_row.length; i++) {
			// console.log($(elm_each_row[i]).find('#cart-total-qty').text().trim());
			var total = $(elm_each_row[i]).find('#cart-total-qty').text().trim();

			// console.log(Number(total));
			sum_total +=  Number(total);
		}

		console.log('Sum total: ', sum_total);
		$('span[id="sum-total-price"]').html(sum_total);
	} //end of function sumTotalPrice(elm)

	function updateCart(elm) {
		var basketKey = sessionStorage.getItem("basketKey");
		var elm_each_row = $(elm).parent().parent().parent().find('tbody > tr');
		// console.log(basketKey);

		// updateBasket(basketKey, this.value, qty);
		//separate update function----
		// console.log(elm_each_row);
		for (var i = 0; i < elm_each_row.length; i++) {
			// console.log($(elm_each_row[i]).find('input[id="cart-qty"]').val());
			var productid = $(elm_each_row[i]).find('input[id="hidden-product-id"]').val()
			var last_qty = $(elm_each_row[i]).find('input[id="cart-qty"]').val();
			// console.log(productid);

			updateBasketQtyForCartPage(basketKey, productid, last_qty);

		}
		// var all_basket = JSON.parse(getBasket(basketKey));		
		swal("ปรับปรุงตะกร้าสินค้าเรียบร้อย", "คุณสามารถกดปุ่ม สั่งซื้อสินค้า เพื่อสั่งสินค้า!", "success")
		.then(function() { //for support IE
          window.location.reload();
        });
        /*.then((value) => { //IE don't suport arrow function
          window.location.reload();
        });*/




  //       updateBadgeQtyBasket(all_basket);
 	// 	updateBadgeDetailBasket(all_basket);
	} //end of function sumTotalPrice(elm)


	function checkout(status_login) {
		// body...

		if(status_login){
			// console.log('ok');
   //          checkout_getfile() 

			window.location = getUrlForAjaxUrl() + "basket/checkout?basketKey=" + sessionStorage.getItem("basketKey");//here double curly bracket

            
		}else{
			window.location = getUrlForAjaxUrl() + 'basket/checkou';
		}

	} //end of function checkout

function checkout_getfile() {
            console.log('get file');

            var url = getUrlForAjaxUrl() + 'basket/checkout_getfile';
            var type = 'get';

            // var url_exist = urlExists(url, type);

            // // console.log(url_exist);

            // if(url_exist==false){
            //     url = '../basket/checkout_getfile'
            // }

            $.ajax({
                url: url,
                type: type,
                data: {basketKey: sessionStorage.getItem("basketKey")},
                cache: false,
                async: false,
                success: function(data){
                    console.log('photo path: ',data);
                    // sessionStorage.setItem("basketKey", data);
                    // console.log(message);
                    // console.log($('div[id="img-path-acc"] > img'));
                    var n = data.search(".pdf");
                    console.log('search: ', n);
                    if(n>1){
                        // $('div[id="img-path-acc"]').html('<img style="width:60%" class="img-thumbnail" src="' + getUrlForAjaxUrl() +'public/checkout_file/img_pdf.png" />');
                        $('div[id="img-path-acc"]').html(' <a href="' + getUrlForAjaxUrl() +'public/'+data+'" target="_blank">เปิดดู</a>');
                    }else{
                        $('div[id="img-path-acc"]').html('<img style="width:60%" class="img-thumbnail" src="' + getUrlForAjaxUrl() +'public/'+data+'" />');
                    }
                    
                },
                error: function(xhr, status, error) {
                    //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                    console.log('Status: ' + status + ' Error: ' + error + '{checkout_getfile}');
                }
            }); // ) $.ajax   

}

Dropzone.options.dropzonecheckout = {
    maxFiles: 1,
    maxFilesize: 5, // Mb
    acceptedFiles: "image/*,application/pdf",    
  init: function() {
    this.on("addedfile", function(file) { 
        // alert("Added file.");  /
        checkout_getfile() 
    });
  }
};
        

function openChangeAddress() {
    //set style that full width
    $('select').parent().find('span[dir="ltr"]').css({"width": "100%"});


    // $('select').parent().find('span[dir="ltr"]').css({"width": ""});


    $("#myModal").modal();
}


function tag_alert(strong_word, explain_word) {
    const tag_alert = '<div style="margin-bottom:1px;" class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>'+strong_word+'</strong> '+explain_word+'</div>';

    return tag_alert;
}

const element_onfummitcheckout = document.getElementById('frmSubmitCheckout');
element_onfummitcheckout.addEventListener('submit', event => {
    var elment_loading = $(event.path[0]).find('i[class="fa fa-spinner fa-spin fa-2x fa-fw"]');
    var elment_alert = $(event.path[0]).find('alert[id="checkout"]');
    var rdo_typepay = $(event.path[0]).find('input[name="optradio_pay"]');
    var typepay_bank = rdo_typepay[0].checked;
    var typepay_cash = rdo_typepay[1].checked;

    var rdo_recieveproduct = $(event.path[0]).find('input[name="optradio_poitrecieveproduct"]');
    var recieveproduct_1 = rdo_recieveproduct[0].checked;
    var recieveproduct_2 = rdo_recieveproduct[1].checked;
    var recieveproduct_3 = rdo_recieveproduct[2].checked;
    var recieveproduct_4 = rdo_recieveproduct[3].checked;

    // console.log('0000000', rdo_typepay[0].checked);
    // console.log('1111111', rdo_typepay[1].checked);
    // console.log('typepay_bank', typepay_bank);
    // console.log('typepay_cash', typepay_cash);
    // console.log();
    if(typepay_bank===false && typepay_cash===false){
        $(elment_alert).append(tag_alert('ยังไม่ได้เลือกรูปแบบการชำระเงิน', 'กรุณาเลือก.'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        event.preventDefault();
        return false;
    }

    if(recieveproduct_1===false && recieveproduct_2===false && recieveproduct_3===false && recieveproduct_4===false){
        $(elment_alert).append(tag_alert('ยังไม่ได้เลือกรูปแบบการรับสินค้า', 'กรุณาเลือก.'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        event.preventDefault();
        return false;
    }

  //   console.log(rdo_recieveproduct);

  // event.preventDefault();
  // actual logic, e.g. validate the form
  // console.log('Form submission cancelled.');
  sessionStorage.removeItem('basketKey');
});
function loadingWaitOrder1(elm){
    var elment_loading = $(elm).find('i');
    $(elment_loading).addClass('fa fa-spinner fa-spin fa-2x fa-fw');
    // sessionStorage.removeItem('basketKey');
}
const element_onfummitupdatemap = document.getElementById('frmSubmitUpdateMap');
element_onfummitupdatemap.addEventListener('submit', event => {
    console.log('frmSubmitUpdateMap');
    // event.preventDefault();

    var element_findlalong = $('form[id="frmSubmitUpdateMap"]');
    var lat = $(element_findlalong).find('input[name="latitude"]').val();
    var log = $(element_findlalong).find('input[name="longitude"]').val();
    // console.log(lat, log);
    if(lat=='' && log==''){
        // console.log('dont lat long');
        event.preventDefault();
        return false;
    }


    var data_more = $(element_findlalong).find('textarea[name="data_more"]').val();
    if(data_more==''){
        // console.log('dont data more');
        event.preventDefault();
        return false;
    }
});
function loadingWaitOrder2(elm){
    // console.log(elm);
    var elment_loading = $(elm).find('i');
    var element_findlalong = $(elm).parent().parent().parent();
    var alert = $(element_findlalong).find('alert[id="alert"]');
    var lat = $(element_findlalong).find('input[name="latitude"]').val();
    var log = $(element_findlalong).find('input[name="longitude"]').val();

    var data_more = $(element_findlalong).find('textarea[name="data_more"]').val();
    // console.log(elment_loading);
    $(elment_loading).addClass('fa fa-spinner fa-spin fa-2x fa-fw');
    // $(elm).attr('disabled','disabled');
    //var basketKey = sessionStorage.getItem("basketKey");

    // console.log(element_findlalong);
    // console.log('lat', lat);
    // console.log('long', log);
    // console.log('data more', data_more);


    // if(lat!=='' && log!==''){
        // sessionStorage.removeItem('basketKey');
    if(lat=='' && log==''){
        $(alert).append(tag_alert('ยังไม่กดเลือกแผนที่!', 'กรุณาค้นหาตำแหน่งที่ต้องการให้เราจัดส่ง.'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        return false;
    }


    if(data_more==''){
        $(alert).append(tag_alert('', 'กรุณาใส่ข้อมูลเพิ่มเติม'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        return false;
    }

    //change to save button | save to cutomer table
    //then change status for sent by map or address || customer tble
    //Continue for save 
}



$('img[id="expand_image"]').hover(
  function() {
    // $( this ).append( $( "<span> ***</span>" ) );
    // $(this).removeAttr('style');
    $(this).css({"position": "absolute", "z-index": "90", "width": "250px"});
  }, function() {
    // $( this ).find( "span:last" ).remove();
    $(this).css({"position": "", "z-index": "", "width": "50%"});
  }
);




$('input[name="optradio_pay"]').on('ifChecked', function(event){
  // alert(event.type + ' callback');
  // console.log('check:', this.value);
  if(this.value=='โอนเงินเข้า บัญชีธนาคาร'){
    // console.log('11111');
    var ss = $(this).closest('form').find('#poitrecieveproduct_post').show(100);
    $('#myModaluploadfilechechout').modal();
  }else{
    // console.log('22222');
    $(this).closest('form').find('#poitrecieveproduct_post').hide(500);

  }

});


function openChangeAddressByGoogle(){
    $('#myModal').modal('hide')

    $('select[id="_placeName"]').parent().find('span[role="combobox"]').css({"height": "44px", "border": "1px solid #DADADA"});
    $('select[id="_placeName"]').parent().find('span[class="select2-selection__arrow"]').css({"top": "8px"});

    $("#myModalByGoogle").modal();
}


$('#_placeName').on('select2:open', function (e) {
// Do something
    document.getElementById("map-geo-explain").innerHTML = "";
    document.getElementById("map-geo").innerHTML = "<br><span class='fa fa-spinner fa-spin fa-2x fa-fw text-warning'></span> <h3 class='text-warning'>ระบบกำลังทำงาน รอจนกว่าเมาส์กระพริบ...</h3>";

    $('input[name="latitude"]').val('');
    $('input[name="longitude"]').val('');

    $('select[id="_placeName"]').parent().find('input[class="select2-search__field"]').attr("for", "_placeName");

    getGeoLocationForFirstTime();

    // document.getElementById("map-geo").innerHTML = "กดปุ่ม \"ค้นหาตำแหน่งปัจจุบัน\" เพื่อแสดงแผนที่";
});

//     var ss = $('input[class="select2-search__field"]');
// console.log(ss);

// $('input[class="select2-search__field"]').on('keyup', function (e) {
//     console.log('keyupd dfsdfdss');
// });
$(document).on('keyup', '.select2-search__field', function (e) {
    // var ss = $(e.target).parent().parent().parent().parent().parent().parent().find('select[id="_placeName"]');
    var _input = $(e.target.attributes[11]);
    // console.log(_input.length);
    if (_input.length > 0){
        // console.log('keyup', _input[0].nodeValue);
         if (_input[0].nodeValue=='_placeName') { 
            // console.log('in fo cus', e);
            var select_open = $('span[class="select2-container select2-container--default select2-container--open"]').find('li');
            // console.log(select_open);
            if(select_open.length==1){
                if(select_open[0].innerText=='No results found'){
                    // console.log(select_open[0].innerText); //--continue here for get data from google api
                    searchBoxGeoLocation(e.target.value);

                }
            }
        }       
    }
}); //end of function


$('#_placeName').on('select2:select', function (e) {
    // console.log('selected', e.params.data);

   $('select[id="_placeName"]').parent().find('span[class="select2-selection__rendered"]').css({"line-height": "44px", "color": "#838383", "text-align": "left"});

    getLatLong(e.params.data);
});