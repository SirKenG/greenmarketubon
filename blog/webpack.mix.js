let mix = require('laravel-mix');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');






    //     <link href="{{asset('css/fontsGoogleapis.css')}}" rel='stylesheet' type='text/css'>

    //     <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    //     <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    //     <link rel="icon" href="favicon.ico" type="image/x-icon">

    //     <link rel="stylesheet" href="{{asset('garo/assets/css/normalize.css')}}"> 
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/font-awesome.min.css')}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/fontello.css')}}">
    //     <link href="{{asset('garo/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet">
    //     <link href="{{asset('garo/assets/fonts/icon-7-stroke/css/helper.css')}}" rel="stylesheet">
    //     <link href="{{asset('garo/assets/css/animate.css')}}" rel="stylesheet" media="screen">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/bootstrap-select.min.css')}}"> 
    //     <link rel="stylesheet" href="{{asset('garo/bootstrap/css/bootstrap.min.css')}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/icheck.min_all.css')}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/price-range.css')}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/owl.carousel.css')}}">  
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/owl.theme.css')}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/owl.transitions.css')}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/style.css?v=').time()}}">
    //     <link rel="stylesheet" href="{{asset('garo/assets/css/responsive.css')}}">

    // <!-- Select2 -->
    // {!! Html::style('adminlte/plugins/select2/select2.min.css') !!}

mix.styles([
    'resources/assets/css/fontsGoogleapis.css',
    'resources/assets/garo/assets/css/normalize.css',
    'resources/assets/css/dropzone.css',
    'node_modules/sweetalert2/dist/sweetalert2.min.css',
    // 'resources/assets/garo/assets/css/font-awesome.min.css',
    // 'resources/assets/garo/assets/css/fontello.css',
    // 'resources/assets/garo/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css',
    // 'resources/assets/garo/assets/fonts/icon-7-stroke/css/helper.css',
    // 'resources/assets/garo/assets/css/animate.css',
    // 'resources/assets/garo/assets/css/bootstrap-select.min.css',
    // 'resources/assets/garo/bootstrap/css/bootstrap.min.css',
    // 'resources/assets/garo/assets/css/icheck.min_all.css',
    // 'resources/assets/garo/assets/css/price-range.css',
    // 'resources/assets/garo/assets/css/owl.carousel.css',
    // 'resources/assets/garo/assets/css/owl.theme.css',
    // 'resources/assets/garo/assets/css/owl.transitions.css',
    // 'resources/assets/garo/assets/css/style.css',
    // 'resources/assets/garo/assets/css/responsive.css',
    // 'resources/assets/select2/select2.min.css',

], '../public/css/libs.css');


    //     <script src="{{asset('garo/assets/js/modernizr-2.6.2.min.js')}}"></script>
    // <!-- jQuery 3.1.1 -->
    // {{-- {!! Html::script('adminlte/plugins/jQuery/jquery-3.1.1.min.js') !!} --}}
    //     <script src="{{asset('garo/assets/js/jquery-1.10.2.min.js')}}"></script> 
    //     <script src="{{asset('garo/bootstrap/js/bootstrap.min.js')}}"></script>
    //     <script src="{{asset('garo/assets/js/bootstrap-select.min.js')}}"></script>
    //     <script src="{{asset('garo/assets/js/bootstrap-hover-dropdown.js')}}"></script>

    //     <script src="{{asset('garo/assets/js/easypiechart.min.js')}}"></script>
    //     <script src="{{asset('garo/assets/js/jquery.easypiechart.min.js')}}"></script>

    //     <script src="{{asset('garo/assets/js/owl.carousel.min.js')}}"></script>
    //     <script src="{{asset('garo/assets/js/wow.js')}}"></script>

    //     <script src="{{asset('garo/assets/js/icheck.min.js')}}"></script>
    //     <script src="{{asset('garo/assets/js/price-range.js')}}"></script>

    //     <script src="{{asset('garo/assets/js/main.js')}}"></script>

    //     <!-- Select2 -->
    //     {!! Html::script('adminlte/plugins/select2/select2.full.min.js') !!}
//<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
/*const { detect } = require('detect-browser');


const browser = detect();*/


mix.scripts([
	// 'resources/assets/garo/assets/js/modernizr-2.6.2.min.js',
	// 'resources/assets/jQuery/jquery-3.1.1.min.js',
	// 'resources/assets/garo/bootstrap/js/bootstrap.min.js',
	// 'resources/assets/garo/assets/js/bootstrap-select.min.js',
	// 'resources/assets/garo/assets/js/bootstrap-hover-dropdown.js',
	// 'resources/assets/garo/assets/js/easypiechart.min.js',
	// 'resources/assets/garo/assets/js/jquery.easypiechart.min.js',
	// 'resources/assets/garo/assets/js/owl.carousel.min.js',
	// 'resources/assets/garo/assets/js/wow.js',
	// 'resources/assets/garo/assets/js/icheck.min.js',
	// 'resources/assets/garo/assets/js/price-range.js',
	// 'resources/assets/garo/assets/js/main.js',
	// 'resources/assets/select2/select2.full.min.js',

        //https://www.npmjs.com/package/detect-browser
        // 'node_modules/detect-browser/index.js',
        //http://www.dropzonejs.com/#installation
        'resources/assets/js/dropzone.js',
        //https://sweetalert.js.org/guides/
        //https://sweetalert2.github.io/
        'node_modules/sweetalert2/dist/sweetalert2.min.js',
        'resources/assets/js/custom.js',
	
	], '../public/js/libs.js');







