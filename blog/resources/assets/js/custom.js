//Fixed top
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > 50) {
        $('.header-connect').addClass('header-connect-fixed');
        // console.log($('.header-connect'));
    } else {
        $('.header-connect').removeClass('header-connect-fixed');
        // console.log('remove');
    }
});


// $('i[class="fa fa-shopping-basket"]').hover(
// 	console.log('message');
// );
$( 'i[class="fa fa-shopping-basket"]' ).hover(
  function() {
    // $( this ).append( $( "<span> ***</span>" ) );
    var elment_ = $( this ).parent().parent();
    // console.log(elment_);

    $(elment_).addClass('open');

  }, function() {
    // $( this ).find( "span:last" ).remove();
  }
);

$( document ).ready(function() {
    validateDevies();
});

function validateDevies(){
    if(Modernizr.mq("(max-width: 768px)")){
        console.log('mobile');
        gmenu_logoutin_m();
        gmenu_historybuy_m();
        $('li[id="menu-basket"]').css('padding-right','0px');
        $('p[id="footer-line1"]').css("line-height","110%");
        $('p[id="footer-line2"]').css("line-height","110%");
        $('p[id="footer-line3"]').css("line-height","110%");
        $('p[id="footer-line4"]').css("line-height","110%");
    }else{
        console.log('desktop');
        gmenu_logoutin_d();
        gmenu_historybuy_d();
    }
}

function gmenu_logoutin_m(){
    var url = getUrlForAjaxUrl() + 'gmenu_logoutin_m';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('div[id="menu-loginoutchangepass-m"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_logoutin_m}');
        }
    }); // ) $.ajax
}
function gmenu_logoutin_d(){
    var url = getUrlForAjaxUrl() + 'gmenu_logoutin_d';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('li[id="menu-loginoutchangepass-d"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_logoutin_d}');
        }
    }); // ) $.ajax
}


function gmenu_historybuy_m(){
    var url = getUrlForAjaxUrl() + 'gmenu_historybuy_m';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('div[id="menu-historybuy-m"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_historybuy_m}');
        }
    }); // ) $.ajax
}
function gmenu_historybuy_d(){
    var url = getUrlForAjaxUrl() + 'gmenu_historybuy_d';
    var type = 'get';

    $.ajax({
        url: url,
        type: type,
        // data:{},
        cache: false,
        async: false,
        success: function(data){
            $('li[id="menu-historybuy-d"]').append(data);
        },
        error: function(xhr, status, error) {
            //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
            console.log('Status: ' + status + ' Error: ' + error + '{gmenu_historybuy_d}');
        }
    }); // ) $.ajax
}



function ValidateIPaddress(ipaddress)   
{  
    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/; 
    if(ipaddress.match(ipformat))  
     {  
        return true;  
     }else{  
        return false;
     } 
} 


//
var base_url = window.location.origin; //http://agrisale.dev/
var host = window.location.host; //agrisale.dev
var pathArray = window.location.pathname.split( '/' ); //array path

// sessionStorage.setItem("host", host);

// console.log('Base URL: ', base_url);
// console.log('Host: ', host);
// console.log('Path array: ', pathArray);

// var ss = getUrlForAjaxUrl();
// console.log('getUrlForAjaxUrl: ', ss);
function getUrlForAjaxUrl(){
    if(ValidateIPaddress(host)){
        console.log('It\'s IP: ', base_url + '/' + pathArray[1] + '/');
        return base_url + '/' + pathArray[1] + '/';
    }else{
        console.log('It\'s not IP: ', base_url + '/');
        return base_url + '/';
    }
}


function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
// var height_showproductdetila = $('aside[id="height-showproductdetail"]').height();

// console.log(height_showproductdetila);

//aside id="height-showproductdetail"

    // function getObjects(obj, key, val) {
    //     var objects = [];
    //     for (var i in obj) {
    //         if (!obj.hasOwnProperty(i)) continue;
    //         if (typeof obj[i] == 'object') {
    //             objects = objects.concat(getObjects(obj[i], key, val));
    //         } else if (i == key && obj[key] == val) {
    //             objects.push(obj);
    //         }
    //     }
    //     return objects;
    // }

    // function updateStorage(id, qty) {
    //     // body...'
    //     var oldarobj_basket = JSON.parse(sessionStorage.getItem('basket_obj'));

    //     sessionStorage.removeItem("basket_obj");
    //     // console.log(oldarobj_basket);
    //     // console.log('id', id);
    //     // console.log('qty', qty);
    //     var objects = [];
    //     // console.log('length', oldarobj_basket.length);
    //     if(oldarobj_basket.length>1){
    //         // console.log('1');
    //          for (var key in oldarobj_basket) {
    //             console.log('for id', oldarobj_basket[key].id);
    //             if(oldarobj_basket[key].id==id){
    //                 // console.log('message');
    //                 oldarobj_basket[key].qty = qty;
    //             }

    //             objects.push(oldarobj_basket[key]);
    //             // console.log(oldarobj_basket[key]);

    //             sessionStorage.setItem('basket_obj', JSON.stringify(objects));
    //          }
    //     }else{
    //         // console.log('2');

    //         oldarobj_basket['qty'] = qty;


    //         // console.log('else',oldarobj_basket);
    //         sessionStorage.setItem('basket_obj', JSON.stringify(oldarobj_basket));
    //     }

    // }


    // $('button[id="basket"]').click(function() {
    //     var qty_ofuser = $(this).parent().parent().find('input[id="qty"]');
    //     var qty = 1;
    //     // console.log(qty_ofuser[0]);
    //     // console.log(qty[0].value);


    //     // $(this).text('ดูตะกร้าสินค้า');

    //     if(typeof(qty_ofuser[0]) !== "undefined"){
    //         qty = qty_ofuser[0].value;
    //     }



    //     var retrievedBasketObject = sessionStorage.getItem('basket_obj');

    //     // console.log('retrievedBasketObject', retrievedBasketObject);

    //     if(!retrievedBasketObject){
    //         var varobj_basket = { 'id': this.value, 'qty': qty};

    //         sessionStorage.setItem('basket_obj', JSON.stringify(varobj_basket));

    //     }else{

    //         var matchingobject = getObjects(JSON.parse(sessionStorage.getItem('basket_obj')), 'id', this.value);

    //         if(typeof(matchingobject[0]) !== "undefined"){
    //             //if exist value
    //             console.log('exist value', matchingobject[0].id);

    //             matchingobject[0].qty = matchingobject[0].qty + 1;


    //             // localStorage.setItem('basket_obj', JSON.stringify(matchingobject) );
    //             updateStorage(matchingobject[0].id, matchingobject[0].qty);
    //             // console.log(matchingobject);


    //         }else{
    //             //if not exist value
    //             // console.log('not exist value', this.value);

    //             // // matchingobject.id = this.value;
    //             // // matchingobject.qty = qty;

    //             // var newvarobj_basket = { 'id': this.value, 'qty': qty};

    //             // var oldarobj_basket =[];
    //             // oldarobj_basket = JSON.parse(sessionStorage.getItem('basket_obj'));

    //             // // console.log(oldarobj_basket);
    //             // oldarobj_basket.push(newvarobj_basket);



    //             // sessionStorage.setItem('basket_obj', JSON.stringify(oldarobj_basket));




    //             // var oldarobj_basket = JSON.parse(sessionStorage.getItem('basket_obj'));
    //             // sessionStorage.removeItem("basket_obj");
    //             // var objects = [];
    //             // if(oldarobj_basket.length>1){
    //             //      for (var key in oldarobj_basket) {
    //             //         console.log('for id', oldarobj_basket[key].id);
    //             //         if(oldarobj_basket[key].id==id){
    //             //             // console.log('message');
    //             //             oldarobj_basket[key].qty = qty;
    //             //         }

    //             //         objects.push(oldarobj_basket[key]);
    //             //         // console.log(oldarobj_basket[key]);

    //             //         sessionStorage.setItem('basket_obj', JSON.stringify(objects));
    //             //      }

    //             // }else{
    //             //     // console.log('esl oooo');
    //             //     // var newvarobj_basket = { 'id': this.value, 'qty': qty};
    //             //     // // oldarobj_basket['qty'] = qty;
    //             //     // // console.log(newvarobj_basket);
    //             //     // objects.push(newvarobj_basket);
    //             //     // console.log(objects);

    //             //     // sessionStorage.setItem('basket_obj', JSON.stringify(oldarobj_basket));
    //             //     // sessionStorage.setItem('basket_obj', JSON.stringify(objects));

    //             // }
    //             // updateStorage(this.value, qty);
    //         }
    //         // console.log('message', matchingobject[0]);
    //     }


    //     console.log('varobj_basket', sessionStorage.getItem('basket_obj'));
        
    // });

// function removeSessionStorageBasketKey(){
//     localStorage.removeItem("basketKey");
// }


    if (typeof(Storage) !== "undefined") {

        // Code for localStorage/sessionStorage.
        // console.log('sessionStorage');

        // sessionStorage.setItem("basketKey", "value");
        var basketKey = sessionStorage.getItem("basketKey");
        console.log(basketKey);
        var all_basket = JSON.parse(getBasket(basketKey));
        // console.log(all_basket);
        updateBadgeQtyBasket(all_basket);
        updateBadgeDetailBasket(all_basket);
        updateCartDetailBasket(all_basket);


        // console.log(basketKey);
        if(!basketKey){
            console.log('get new basket key');


          // switch (checkBrowser())
          // {
          //   case "Opera":
          //   case "Safari":
          //   case "Chrome":
          //   case "Blink":
          //       console.log('Chrome Safari');
          //       break;
          //   case "Firefox":
          //   case "IE":
          //   case "Edge":
          //       console.log('IE');
          //       break;
          //   case "Error":
          //       console.log('Error switch {checkBrowser}');
          //     break;
          //} //.) switch







            //get new key 
            $.ajax({
                url: getUrlForAjaxUrl() + 'basket/create',
                type: 'get',
                data:'',
                cache: false,
                success: function(data){
                    // console.log(data);
                    sessionStorage.setItem("basketKey", data);
                },
                error: function(xhr, status, error) {
                    //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                    console.log('Status: ' + status + ' Error: ' + error + '{get new basket key}');
                }
            }); // ) $.ajax
        } //if(!basketKey)
    } else {
        // Sorry! No Web Storage support..
        alert('ขออภัยเว็บไซต์ไม่รองรับเว็บบราวเซอร์ กรุณาเปลี่ยนเวอร์ชั่นเป็นปัจจุบัน\nคำแนะนำ\nchrome4.0+\nIE8.0+\nfirefox3.5+\nsafari4.0+\nopera11.5+');
    }


	function urlExists(url, type){
        // console.log('check url exists:');
		var isValid = false;
		  $.ajax({
		    type: type,
		    url: url,
		    async: false,
		    success: function(){
		      // callback(true);
              console.log('check url result: exist');
		      isValid =  true;
		    },
		    error: function() {
		      // callback(false);
              console.log('check url result: don\'t exist');
		      isValid = false;
		    }
		  });
		 //  console.log(jqXHR.responseText);
		 // return jqXHR.responseText;
		  return isValid;
	} //end of function


    function getBasket(basketKey) {
    	console.log('getBasket', basketKey);
        
    	var url = getUrlForAjaxUrl() + 'basket/getBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		// // console.log(url_exist);

		// if(url_exist==false){
		// 	url = '../basket/getBasket'
		// }

// console.log(url);
        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{getBasket}');
            }
        }); // ) $.ajax

        return jqXHR.responseText;
    }

    function checkExistBasket(basketKey, productID) {
    	// console.log(basketKey, productID);
    	var url = getUrlForAjaxUrl() + 'basket/getexistbasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		// // console.log(url_exist);

		// if(url_exist==false){
		// 	url = '../basket/getexistbasket'
		// }

    	//if return true is mean exist 
    	//if return false is mean no exist product
        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{checkExistBasket}');
            }
        }); // ) $.ajax

        // console.log(jqXHR.responseText.length);
        if(jqXHR.responseText=='0'){
        	return false;
        }else{
        	return true;
        }
        // return jqXHR.responseText;
    } //end of function


    function addBasket(basketKey, productID, qty) {
    	// console.log('basketKey', basketKey);
    	// console.log('productID', productID);
    	// console.log('qty', qty);

    	var url = getUrlForAjaxUrl() + 'basket/addBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		
  //       // console.log('result url exist: ', url_exist);
		// if(url_exist==false){
		// 	url = '../basket/addBasket'
		// }

        // console.log(url);
        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID, qty: qty},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{addBasket}');
            }
        }); // ) $.ajax
    } //end of function


    function updateBasket(basketKey, productID, qty) {
    	// console.log('basketKey', basketKey);
    	// console.log('productID', productID);
    	// console.log('qty', qty);

    	var url = getUrlForAjaxUrl() + 'basket/updateBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);

		// // console.log(url_exist);
  //       // console.log('result url exist: ', url_exist);

		// if(url_exist==false){
		// 	url = '../basket/updateBasket'
		// }


        // console.log(url);

        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID, qty: qty},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{updateBasket}');
            }
        }); // ) $.ajax
    } //end of function

    function updateBasketQtyForCartPage(basketKey, productID, qty) {
    	var url = getUrlForAjaxUrl() + 'basket/updateBasketQtyForCartPage';
        var type = 'get';

		// var url_exist = urlExists(url, type);


		// if(url_exist==false){
		// 	url = '../basket/updateBasketQtyForCartPage'
		// }

        // console.log(url_exist);

        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productID, qty: qty},
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{updateBasketQtyForCartPage}');            
            }
        }); // ) $.ajax
    }

    function updateBadgeQtyBasket(arr) {
    	var qty = 0;
    	 for (var key in arr) {
    	 	qty = (parseInt(qty)) + (parseInt(arr[key].qty));
    	 	// console.log(arr[key].qty);
    	 }

    	 $('span[class="badge"]').text(qty);

    	 // console.log(qty);
    } //end of function

    function updateBadgeDetailBasket(arr) {
         var _eml = $('ul[id="showdetailproduct-topmenu"]').find('div[id="content"]');
         var _emlprice = $('ul[id="showdetailproduct-topmenu"]').find('label[id="price"]');

    	// console.log(_eml);

    	// console.log(arr);
    	var x = "";
    	var total = 0;

for (var r = 0; r < arr.length; r++) { 
    // console.log('fsdfdsds', arr[r]);

            total = total + (parseFloat(arr[r].prime) * parseFloat(arr[r].qty));
            x += '<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[r].name + '" onclick="redirectToProductdetail('+ arr[r].product_id +');">' + 
                    '<div class="row">' + 
                        '<div class="col-md-6">' +
                            '<img src="' +  getUrlForAjaxUrl() + 'public/' + arr[r].photo_path +'">' + 
                        '</div>' +
                        '<div class="col-md-6">' +
                            '<h3>' + arr[r].name + '</h3>' +
                            '<p>' + arr[r].qty + ' x ' + arr[r].prime + '฿</p>' + 
                        '</div>' +                      
                    '</div>' +
                 '</a><div class="dot-hr"></div>';

}



        // var r = 0; 
		// for (i < arr.length; i++;) {
		//     console.log('sss', arr[i]);
		//     // console.log(parseFloat(arr[i].prime), parseFloat(arr[i].qty));

		//     //{{ route('basket.show', $product->id ) }}

		//     total = total + (parseFloat(arr[i].prime) * parseFloat(arr[i].qty));
		//     x += '<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[i].name + '" onclick="redirectToProductdetail('+ arr[i].product_id +');">' + 
		//     		'<div class="row">' + 
		//     			'<div class="col-md-6">' +
		//     				'<img src="../public/' + arr[i].photo_path +'">' + 
		//     			'</div>' +
		//     			'<div class="col-md-6">' +
		//     				'<h3>' + arr[i].name + '</h3>' +
		//     				'<p>' + arr[i].qty + ' x ' + arr[i].prime + '฿</p>' + 
		//     			'</div>' +		    			
		//     		'</div>' +
		//     	 '</a><div class="dot-hr"></div>';
		// }

		// console.log(_emlprice);
		$(_eml).html(x);
		$(_emlprice).text('ราคารวม: ' + numberWithCommas(total) + '฿');

    } //end of function updateBadgeDetailBasket


    function updateCartDetailBasket(arr) {
    	// console.log(arr);
         var _emlss = $('div[id="showdetailproduct-cart"]').find('div[id="cartContentBasket"]');
         var _emlpricess = $('div[id="showdetailproduct-cart"]').find('label[id="cartConentBasketprice"]');
         // console.log(_emlss);
         // console.log(_emlpricess);

    	var x = "";
    	var total = 0;
        var i = 0; 
    	// for (i < arr.length; i++;) {
        for (var i = 0; i < arr.length; i++) { 

		    total = total + (parseFloat(arr[i].prime) * parseFloat(arr[i].qty));
		    x += '<div class="row" style="margin-bottom: 10px;">' + 
		    		'<div class="col-md-1">' +
		    			'<a id="remove-cart" style="cursor: pointer; color: red;" title="นำ ' + arr[i].name + ' ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket('+ arr[i].product_id +');">' +
		    				'<i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>' +
		    			'</a>' +
		    		'</div>' +
		    		'<div class="col-md-5">' +
		    			'<div id="topic" style="color: #5cb85c;font-size: 18px;margin-bottom: -10px;">' + arr[i].name + '</div>' +
		    			'<div id="content">' + arr[i].qty + ' x ' + arr[i].prime + '฿</div>' +
		    		'</div>' +
		    		'<div class="col-md-5">' +
		    			//'<a href="#"><img src="http://agrisale.dev/public//produce/1510807949.jpg"></a>' +
		    			'<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[i].name + '" onclick="redirectToProductdetail('+ arr[i].product_id +');"><img src="' +  getUrlForAjaxUrl() + 'public/' + arr[i].photo_path +'"></a>' + 
		    		'</div>' +		    		
		    	 '</div>';
    	}


		$(_emlss).html(x);
		$(_emlpricess).text('มูลค่าสินค้า: ' + numberWithCommas(total) + '฿');
    }  //end of function updateCartDetailBasket
    function updateCartDetailBasketHTMLforShowswal(arr, qty){
        // console.log(arr);
        var html_forshow = '';
        var total = 0;
        var total_qty = 0;
        var i = 0;
        var margin_bottom5 = 'margin-bottom: 5px;border-bottom: 1px dotted #CECBC7;';

        html_forshow += '<div class="row form-inline" style="border-bottom: 1px dotted #CECBC7;">' + 
                             '<div class="col-md-6" style="border-right: 1px dotted #CECBC7;">' +
                                '<div class="row"><div class="col-md-12"><h4 class="text-success text-left">สินค้า ' + qty + ' ชิ้น ได้ถูกเพิ่มเข้าไปในตะกร้าสินค้าของคุณแล้ว</h4></div></div>';

        for (var i = 0; i < arr.length; i++) { 
            total = total + (parseFloat(arr[i].prime) * parseFloat(arr[i].qty));
            total_qty = total_qty + parseFloat(arr[i].qty);
            // console.log(i, arr.length);

            var i_check = i + 1;
            if(i_check == arr.length){
                margin_bottom5 =  'margin-bottom: 0px;'
            }
            html_forshow += '<div class="row" style="' + margin_bottom5 + '">' + 
                    '<div class="col-md-1">' +
                        '<a id="remove-cart" style="cursor: pointer; color: red;" title="นำ ' + arr[i].name + ' ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket('+ arr[i].product_id +');">' +
                            '<i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>' +
                        '</a>' +
                    '</div>' +
                    '<div class="col-md-6 text-left">' +
                        '<div id="topic" style="color: #5cb85c;font-size: 18px;">' + arr[i].name + '</div>' +
                        '<div id="content">' + arr[i].qty + ' x ' + arr[i].prime + '฿</div>' +
                    '</div>' +
                    '<div class="col-md-5">' +
                        //'<a href="#"><img src="http://agrisale.dev/public//produce/1510807949.jpg"></a>' +
                        '<a style="cursor: pointer;" title="ดูรายละเอียดของ ' + arr[i].name + '" onclick="redirectToProductdetail('+ arr[i].product_id +');"><img src="' +  getUrlForAjaxUrl() + 'public/' + arr[i].photo_path +'" style="margin-bottom: 5px;"></a>' + 
                    '</div>' +
                 '</div>';
        }


        html_forshow += '</div>' + 
                            '<div class="col-md-6">' +
                            '<div class="row">' + 
                                '<div class="col-md-12 text-left">' + 
                                    '<h4>ตะกร้าสินค้าของคุณ <small>(สินค้า จำนวน ' + total_qty + ' ชิ้น)</small></h4>' + 
                                '</div>' +
                            '</div>' +
                            '<div class="dot-hr"></div>' +
                            '<div class="col-md-6 text-left">' +
                                '<h4>มูลค่าสินค้ารวม: </h4>' +
                            '</div>' + 
                            '<div class="col-md-6 text-right">' +
                                '<h4>' + numberWithCommas(total) + ' บาท</h4>' +
                            '</div>' +
                            //<button type="button" class="swal2-close" aria-label="Close this dialog" style="display: block;">×</button>
                            //window.location = getUrlForAjaxUrl() + "basket/checkout?basketKey=" + sessionStorage.getItem("basketKey");
                            '<div class="col-md-6 text-left">' +
                                '<a onclick="swal.close();" style="cursor: pointer;font-size: 18px;font-weight: unset;color: #167ac1;">เลือกซื้อสินค้าต่อ</a>' +
                            '</div>' + 
                            '<div class="col-md-6 text-right">' +
                                '<a onclick="checkouts();" class="btn border-btn-checkout" >สั่งซื้อสินค้า</a>' +
                            '</div>' +                                                      
                    '</div>';

        return html_forshow;
    }
    function checkouts(){
        window.location = getUrlForAjaxUrl() + "basket/checkout?basketKey=" + sessionStorage.getItem("basketKey");
    }

    function redirectToProductdetail(productid) {
    	// console.log(productid); 

    	window.location = getUrlForAjaxUrl() + 'basket/' + productid;

    } //end of function redirectToProductdetail

    function removeProductFromBasket(productid, reload) {
    	// console.log(productid);
        if (!reload) reload = 0; //This code is ES5 for support IE


    	var basketKey = sessionStorage.getItem("basketKey");
    	var url = getUrlForAjaxUrl() + 'basket/removeProductFromBasket';
        var type = 'get';

		// var url_exist = urlExists(url, type);


		// // console.log(url_exist);

		// if(url_exist==false){
		// 	url = '../basket/removeProductFromBasket';
		// }

        var jqXHR = $.ajax({
            url: url,
            type: type,
            data:{basketKey: basketKey, productID: productid},
            cache: false,
            async: false,
            success: function(data){
                // console.log('result', data);
                // sessionStorage.setItem("basketKey", data);
                // return data
                if(data){
                	console.log('reload status: ',reload);
                	if(reload==0){
                		swal("นำสินค้าออกจากตะกร้าเรียบร้อย", "คุณสามารถดูสินค้าล่าสุดได้ที่เมนูขวามือ หรือเมนูด้านบน!", "success");
                	}else{
                		swal("นำสินค้าออกจากตะกร้าเรียบร้อย", "คุณสามารถดูสินค้าล่าสุดได้ที่เมนูขวามือ หรือเมนูด้านบน!", "success")
						.then(function() { //for support IE
						  window.location.reload();
						});
                        /*.then((value) => { //IE don't suport arrow function
                          window.location.reload();
                        });*/
                	}
                	

                	var all_basket = JSON.parse(getBasket(basketKey));	
			        updateBadgeQtyBasket(all_basket);
			 		updateBadgeDetailBasket(all_basket);
			 		updateCartDetailBasket(all_basket);
                }else{
                	swal("นำสินค้าออกจาตะกร้าไม่สำเร็จ", "กรุณาลองอีกครั้ง!", "error");
                }


            },
            error: function(xhr, status, error) {
                //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                console.log('Status: ' + status + ' Error: ' + error + '{removeProductFromBasket}');
            }
        }); // ) $.ajax    	
    } //end of function removeProductFromBasket


	$('a[id="remove-cart"]').hover(
	  function() {
	  	var hover_symbol = $(this).find('i[id="remove-cart-symbol"]');
	    // console.log(hover_symbol);
	    // var elment_ = $( this ).parent().parent(); content: "\f146";
	    // console.log(elment_);

	    $(hover_symbol).removeClass('fa-minus-square-o');
	    $(hover_symbol).addClass('fa-minus-square');

	  }, function() {
	    // $( this ).find( "span:last" ).remove();
	    // console.log('message2');
	    var hover_symbol = $(this).find('i[id="remove-cart-symbol"]');

	    $(hover_symbol).removeClass('fa-minus-square');
	    $(hover_symbol).addClass('fa-minus-square-o');
	  }
	);


    function redirectToCart() {
    	// console.log(productid);
    	var basketKey = sessionStorage.getItem("basketKey");
    	window.location = getUrlForAjaxUrl() + 'cart/' + basketKey;

    } //end of function redirectToProductdetail

	$('button[id="basket"]').click(function() {
		var elment_loading = $(this).find('i');
		$(elment_loading).addClass('fa fa-spinner fa-spin fa-1x fa-fw');
		$(this).attr('disabled','disabled');

		var call_textbutton = $(this).text().trim();

		console.log('Text button:', call_textbutton);

		if(call_textbutton=='Loading...หยิบใส่ตะกร้า'){

			var qty_ofuser = $(this).parent().parent().find('input[id="qty"]');
			var qty = 1;
			var basketKey = sessionStorage.getItem("basketKey");
	        // console.log(qty_ofuser[0]);
	     	// console.log(qty[0].value);

	        if(typeof(qty_ofuser[0]) !== "undefined"){
	            qty = qty_ofuser[0].value;
	        }

	        // console.log(qty);
	        // console.log(this.value);
	        var exist_basket = checkExistBasket(basketKey, this.value);

	        // console.log(exist_basket);
	        if(exist_basket){
	        	//if exist value
	        	console.log('update basket');

	        	updateBasket(basketKey, this.value, qty);

	        }else{
	        	//if not exist value
	        	console.log('add basket');

	        	addBasket(basketKey, this.value, qty);
	        }


	        var all_basket = JSON.parse(getBasket(basketKey));
	 		
	 		// console.log(all_basket);
	        updateBadgeQtyBasket(all_basket);
	 		updateBadgeDetailBasket(all_basket);
	 		updateCartDetailBasket(all_basket);

            var html_forshow = updateCartDetailBasketHTMLforShowswal(all_basket, qty);


	 		// swal("เพิ่มสินค้าเรียบร้อย", "คุณสามารถดูสินค้าได้ที่เมนูขวามือ หรือเมนูด้านบน!", "success");
            swal({
                    position: 'top',
                    // title: '<i>HTML</i> <u>example</u>',
                    width: 800,
                    // type: 'success',
                    html: html_forshow,
                    showConfirmButton: false,
                    showCloseButton: true,
                });


	 		$(this).text('ดูตะกร้าสินค้า');
 		}else{
 			redirectToCart();
 			// console.log('dfsddd');
 		}


        $(elment_loading).removeClass();
        $(this).removeAttr('disabled');
	});



	$('input[id="cart-qty"]').change(function() {
  		// console.log(this.value);
  		var price = $(this).parent().parent().find('#cart-price').text();
  		// console.log(price);
  		var total = price * this.value;
  		// console.log(total);
  		$(this).parent().parent().find('#cart-total-qty').html(total.toFixed(2));


  		// $('span[id="sum-total-price"]').html();
  		// console.log(this);
  		sumTotalPrice(this);
	}); //end of $('input[id="cart-qty"]').change(function()

	function sumTotalPrice(elm) {
		// body...

		var elm_each_row = $(elm).parent().parent().parent().find('tr');

		// console.log(elm_each_row); //COntinue here calculate total
		var sum_total = 0;
		for (var i = 0; i < elm_each_row.length; i++) {
			// console.log($(elm_each_row[i]).find('#cart-total-qty').text().trim());
			var total = $(elm_each_row[i]).find('#cart-total-qty').text().trim();

			// console.log(Number(total));
			sum_total +=  Number(total);
		}

		console.log('Sum total: ', sum_total);
		$('span[id="sum-total-price"]').html(sum_total);
	} //end of function sumTotalPrice(elm)

	function updateCart(elm) {
		var basketKey = sessionStorage.getItem("basketKey");
		var elm_each_row = $(elm).parent().parent().parent().find('tbody > tr');
		// console.log(basketKey);

		// updateBasket(basketKey, this.value, qty);
		//separate update function----
		// console.log(elm_each_row);
		for (var i = 0; i < elm_each_row.length; i++) {
			// console.log($(elm_each_row[i]).find('input[id="cart-qty"]').val());
			var productid = $(elm_each_row[i]).find('input[id="hidden-product-id"]').val()
			var last_qty = $(elm_each_row[i]).find('input[id="cart-qty"]').val();
			// console.log(productid);

			updateBasketQtyForCartPage(basketKey, productid, last_qty);

		}
		// var all_basket = JSON.parse(getBasket(basketKey));		
		swal("ปรับปรุงตะกร้าสินค้าเรียบร้อย", "คุณสามารถกดปุ่ม สั่งซื้อสินค้า เพื่อสั่งสินค้า!", "success")
		.then(function() { //for support IE
          window.location.reload();
        });
        /*.then((value) => { //IE don't suport arrow function
          window.location.reload();
        });*/




  //       updateBadgeQtyBasket(all_basket);
 	// 	updateBadgeDetailBasket(all_basket);
	} //end of function sumTotalPrice(elm)


	function checkout(status_login) {
		// body...

		if(status_login){
			// console.log('ok');
   //          checkout_getfile() 

			window.location = getUrlForAjaxUrl() + "basket/checkout?basketKey=" + sessionStorage.getItem("basketKey");//here double curly bracket

            
		}else{
			window.location = getUrlForAjaxUrl() + 'basket/checkou';
		}

	} //end of function checkout

function checkout_getfile() {
            console.log('get file');

            var url = getUrlForAjaxUrl() + 'basket/checkout_getfile';
            var type = 'get';

            // var url_exist = urlExists(url, type);

            // // console.log(url_exist);

            // if(url_exist==false){
            //     url = '../basket/checkout_getfile'
            // }

            $.ajax({
                url: url,
                type: type,
                data: {basketKey: sessionStorage.getItem("basketKey")},
                cache: false,
                async: false,
                success: function(data){
                    console.log('photo path: ',data);
                    // sessionStorage.setItem("basketKey", data);
                    // console.log(message);
                    // console.log($('div[id="img-path-acc"] > img'));
                    var n = data.search(".pdf");
                    console.log('search: ', n);
                    if(n>1){
                        // $('div[id="img-path-acc"]').html('<img style="width:60%" class="img-thumbnail" src="' + getUrlForAjaxUrl() +'public/checkout_file/img_pdf.png" />');
                        $('div[id="img-path-acc"]').html(' <a href="' + getUrlForAjaxUrl() +'public/'+data+'" target="_blank">เปิดดู</a>');
                    }else{
                        $('div[id="img-path-acc"]').html('<img style="width:60%" class="img-thumbnail" src="' + getUrlForAjaxUrl() +'public/'+data+'" />');
                    }
                    
                },
                error: function(xhr, status, error) {
                    //console.log('<b class="text-danger">xhr :</b> ' + xhr.responseText + '<br><b class="text-danger">status :</b> ' + status + '<br><b class="text-danger">error :</b> ' + error);
                    console.log('Status: ' + status + ' Error: ' + error + '{checkout_getfile}');
                }
            }); // ) $.ajax   

}

Dropzone.options.dropzonecheckout = {
    maxFiles: 1,
    maxFilesize: 5, // Mb
    acceptedFiles: "image/*,application/pdf",    
  init: function() {
    this.on("addedfile", function(file) { 
        // alert("Added file.");  /
        checkout_getfile() 
    });
  }
};
        

function openChangeAddress() {
    //set style that full width
    $('select').parent().find('span[dir="ltr"]').css({"width": "100%"});


    // $('select').parent().find('span[dir="ltr"]').css({"width": ""});


    $("#myModal").modal();
}


function tag_alert(strong_word, explain_word) {
    const tag_alert = '<div style="margin-bottom:1px;" class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>'+strong_word+'</strong> '+explain_word+'</div>';

    return tag_alert;
}

const element_onfummitcheckout = document.getElementById('frmSubmitCheckout');
element_onfummitcheckout.addEventListener('submit', event => {
    var elment_loading = $(event.path[0]).find('i[class="fa fa-spinner fa-spin fa-2x fa-fw"]');
    var elment_alert = $(event.path[0]).find('alert[id="checkout"]');
    var rdo_typepay = $(event.path[0]).find('input[name="optradio_pay"]');
    var typepay_bank = rdo_typepay[0].checked;
    var typepay_cash = rdo_typepay[1].checked;

    var rdo_recieveproduct = $(event.path[0]).find('input[name="optradio_poitrecieveproduct"]');
    var recieveproduct_1 = rdo_recieveproduct[0].checked;
    var recieveproduct_2 = rdo_recieveproduct[1].checked;
    var recieveproduct_3 = rdo_recieveproduct[2].checked;
    var recieveproduct_4 = rdo_recieveproduct[3].checked;

    // console.log('0000000', rdo_typepay[0].checked);
    // console.log('1111111', rdo_typepay[1].checked);
    // console.log('typepay_bank', typepay_bank);
    // console.log('typepay_cash', typepay_cash);
    // console.log();
    if(typepay_bank===false && typepay_cash===false){
        $(elment_alert).append(tag_alert('ยังไม่ได้เลือกรูปแบบการชำระเงิน', 'กรุณาเลือก.'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        event.preventDefault();
        return false;
    }

    if(recieveproduct_1===false && recieveproduct_2===false && recieveproduct_3===false && recieveproduct_4===false){
        $(elment_alert).append(tag_alert('ยังไม่ได้เลือกรูปแบบการรับสินค้า', 'กรุณาเลือก.'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        event.preventDefault();
        return false;
    }

  //   console.log(rdo_recieveproduct);

  // event.preventDefault();
  // actual logic, e.g. validate the form
  // console.log('Form submission cancelled.');
  sessionStorage.removeItem('basketKey');
});
function loadingWaitOrder1(elm){
    var elment_loading = $(elm).find('i');
    $(elment_loading).addClass('fa fa-spinner fa-spin fa-2x fa-fw');
    // sessionStorage.removeItem('basketKey');
}
const element_onfummitupdatemap = document.getElementById('frmSubmitUpdateMap');
element_onfummitupdatemap.addEventListener('submit', event => {
    console.log('frmSubmitUpdateMap');
    // event.preventDefault();

    var element_findlalong = $('form[id="frmSubmitUpdateMap"]');
    var lat = $(element_findlalong).find('input[name="latitude"]').val();
    var log = $(element_findlalong).find('input[name="longitude"]').val();
    // console.log(lat, log);
    if(lat=='' && log==''){
        // console.log('dont lat long');
        event.preventDefault();
        return false;
    }


    var data_more = $(element_findlalong).find('textarea[name="data_more"]').val();
    if(data_more==''){
        // console.log('dont data more');
        event.preventDefault();
        return false;
    }
});
function loadingWaitOrder2(elm){
    // console.log(elm);
    var elment_loading = $(elm).find('i');
    var element_findlalong = $(elm).parent().parent().parent();
    var alert = $(element_findlalong).find('alert[id="alert"]');
    var lat = $(element_findlalong).find('input[name="latitude"]').val();
    var log = $(element_findlalong).find('input[name="longitude"]').val();

    var data_more = $(element_findlalong).find('textarea[name="data_more"]').val();
    // console.log(elment_loading);
    $(elment_loading).addClass('fa fa-spinner fa-spin fa-2x fa-fw');
    // $(elm).attr('disabled','disabled');
    //var basketKey = sessionStorage.getItem("basketKey");

    // console.log(element_findlalong);
    // console.log('lat', lat);
    // console.log('long', log);
    // console.log('data more', data_more);


    // if(lat!=='' && log!==''){
        // sessionStorage.removeItem('basketKey');
    if(lat=='' && log==''){
        $(alert).append(tag_alert('ยังไม่กดเลือกแผนที่!', 'กรุณาค้นหาตำแหน่งที่ต้องการให้เราจัดส่ง.'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        return false;
    }


    if(data_more==''){
        $(alert).append(tag_alert('', 'กรุณาใส่ข้อมูลเพิ่มเติม'))
        $(elment_loading).removeClass('fa fa-spinner fa-spin fa-2x fa-fw');
        return false;
    }

    //change to save button | save to cutomer table
    //then change status for sent by map or address || customer tble
    //Continue for save 
}



$('img[id="expand_image"]').hover(
  function() {
    // $( this ).append( $( "<span> ***</span>" ) );
    // $(this).removeAttr('style');
    $(this).css({"position": "absolute", "z-index": "90", "width": "250px"});
  }, function() {
    // $( this ).find( "span:last" ).remove();
    $(this).css({"position": "", "z-index": "", "width": "50%"});
  }
);




$('input[name="optradio_pay"]').on('ifChecked', function(event){
  // alert(event.type + ' callback');
  // console.log('check:', this.value);
  if(this.value=='โอนเงินเข้า บัญชีธนาคาร'){
    // console.log('11111');
    var ss = $(this).closest('form').find('#poitrecieveproduct_post').show(100);
    $('#myModaluploadfilechechout').modal();
  }else{
    // console.log('22222');
    $(this).closest('form').find('#poitrecieveproduct_post').hide(500);

  }

});


function openChangeAddressByGoogle(){
    $('#myModal').modal('hide')

    $('select[id="_placeName"]').parent().find('span[role="combobox"]').css({"height": "44px", "border": "1px solid #DADADA"});
    $('select[id="_placeName"]').parent().find('span[class="select2-selection__arrow"]').css({"top": "8px"});

    $("#myModalByGoogle").modal();
}


$('#_placeName').on('select2:open', function (e) {
// Do something
    document.getElementById("map-geo-explain").innerHTML = "";
    document.getElementById("map-geo").innerHTML = "<br><span class='fa fa-spinner fa-spin fa-2x fa-fw text-warning'></span> <h3 class='text-warning'>ระบบกำลังทำงาน รอจนกว่าเมาส์กระพริบ...</h3>";

    $('input[name="latitude"]').val('');
    $('input[name="longitude"]').val('');

    $('select[id="_placeName"]').parent().find('input[class="select2-search__field"]').attr("for", "_placeName");

    getGeoLocationForFirstTime();

    // document.getElementById("map-geo").innerHTML = "กดปุ่ม \"ค้นหาตำแหน่งปัจจุบัน\" เพื่อแสดงแผนที่";
});

//     var ss = $('input[class="select2-search__field"]');
// console.log(ss);

// $('input[class="select2-search__field"]').on('keyup', function (e) {
//     console.log('keyupd dfsdfdss');
// });
$(document).on('keyup', '.select2-search__field', function (e) {
    // var ss = $(e.target).parent().parent().parent().parent().parent().parent().find('select[id="_placeName"]');
    var _input = $(e.target.attributes[11]);
    // console.log(_input.length);
    if (_input.length > 0){
        // console.log('keyup', _input[0].nodeValue);
         if (_input[0].nodeValue=='_placeName') { 
            // console.log('in fo cus', e);
            var select_open = $('span[class="select2-container select2-container--default select2-container--open"]').find('li');
            // console.log(select_open);
            if(select_open.length==1){
                if(select_open[0].innerText=='No results found'){
                    // console.log(select_open[0].innerText); //--continue here for get data from google api
                    searchBoxGeoLocation(e.target.value);

                }
            }
        }       
    }
}); //end of function


$('#_placeName').on('select2:select', function (e) {
    // console.log('selected', e.params.data);

   $('select[id="_placeName"]').parent().find('span[class="select2-selection__rendered"]').css({"line-height": "44px", "color": "#838383", "text-align": "left"});

    getLatLong(e.params.data);
});