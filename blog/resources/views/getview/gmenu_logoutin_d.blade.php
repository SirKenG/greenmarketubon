@if(session('member_login')==true)
    <a class="nav-button" style="background-color: #fff;color:#000;border: 1px solid #dcc313;margin-right: 4px;" href="{{route('member.edit', session('member_code'))}}" data-wow-delay="0.45s">
        เปลี่ยนรหัสผ่าน
    </a>                                
    <a class="nav-button  logout" href="{{route('member.create')}}" data-wow-delay="0.50s">
        ออกจากระบบ
    </a>
@else
    <a class="nav-button  login" href="{{route('member.index')}}" data-wow-delay="0.45s">
        เข้าระบบ
    </a>
    <a class="nav-button  login" href="{{route('customer.register')}}" data-wow-delay="0.50s" style="margin-left: 5px;">
        สมัครสมาชิก
    </a>   
@endif