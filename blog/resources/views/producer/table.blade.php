<table class="table table-responsive table-bordered" id="produces-table">
    <thead>
        <tr class="back-tr">
            <th>#</th>
            <th>หัวข้อ</th>
            {{-- <th>ขื่อสินค้า</th> --}}
            <th class="col-md-2">รูปภาพ</th>
            <th class="actions">Action</th>
        <tr>
    </thead>
    <tbody>
    @foreach($producers as $k => $producer)
        <tr>
            <td>{!! ($k+1) !!}</td>
            <td>{!! $producer->title !!}</td>
            <td class="col-md-2"><img src="{{asset($producer->photo_path)}}" style="width: 80%"></td>
            <td>
                {!! Form::open(['route' => ['producer.destroy', $producer->id], 'method' => 'delete']) !!}
                <!-- <div class='btn-group'> -->
                    <a href="{!! route('producer.showa', [$producer->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('producer.edit', [$producer->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                <!-- </div> -->
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
