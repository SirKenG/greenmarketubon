@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">จัดการกลุ่มผลิตอินทรีย์</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">จัดการกลุ่มผลิตอินทรีย์</h1>
                <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('producer.create') !!}">เขียนบล็อกกลุ่มผลิตอินทรีย์</a>
            </div>
            <div class="box-body">
                    @include('producer.table')
            </div>
        </div>
    </div>
@endsection
