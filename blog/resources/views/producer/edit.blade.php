@extends('layouts.app')

@section('content')
  @include('includes.tinyeditor')


  
    <section class="content-header">
        <h1>
            จัดการกลุ่มผลิตอินทรีย์
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">แก้ไขบล็อกกลุ่มผลิตอินทรีย์</h1>
            </div>
           <div class="box-body">
               <div class="row">
                <div class="col-sm-12" >
                   {!! Form::model($producer, ['route' => ['producer.update', $producer->id], 'method' => 'patch','type'=>'file','enctype'=>'multipart/form-data']) !!}

                        {{-- @include('produces.fields') --}}
                               <div class="form-group">
                                    {!! Form::label('title', 'หัวข้อ:') !!}
                                    {!! Form::text('title', null, ['class'=>'form-control'])!!}
                                </div>            

                                <div class="form-group col-md-12">
                                    {!! Form::label('photo_path', 'รูปภาพ:') !!}
                                    {!! Form::file('photo_path') !!}
                                </div>
                        
                                <div class="form-group">
                                    {!! Form::label('body', 'เนื้อหา:') !!}
                                    {!! Form::textarea('body', null, ['class'=>'form-control'])!!}
                                </div>
                        
                                <div class="form-group">
                                    {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                                </div>


                   {!! Form::close() !!}
                 </div>
               </div>
           </div>
       </div>
   </div>
@endsection
