<table class="table table-responsive" id="subDistricts-table">
    <thead>
        <th>Code</th>
        <th>Name</th>
        <th>Name Eng</th>
        <th>Region Id</th>
        <th>Province Id</th>
        <th>District Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($subDistricts as $subDistrict)
        <tr>
            <td>{!! $subDistrict->code !!}</td>
            <td>{!! $subDistrict->name !!}</td>
            <td>{!! $subDistrict->name_eng !!}</td>
            <td>{!! $subDistrict->region_id !!}</td>
            <td>{!! $subDistrict->province_id !!}</td>
            <td>{!! $subDistrict->district_id !!}</td>
            <td>
                {!! Form::open(['route' => ['subDistricts.destroy', $subDistrict->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('subDistricts.show', [$subDistrict->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('subDistricts.edit', [$subDistrict->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>