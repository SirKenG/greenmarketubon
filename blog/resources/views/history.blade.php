@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ประวัติคำสั่งซื้อ
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">ประวัติคำสั่งซื้อ</h1>
            </div>
            <div class="box-body">
               @if(Session::has('home_meassage'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('home_meassage')}}
                </div>
               @endif

	          <div class="table-responsive" style="margin-top: 20px;">          
		          <table class="table table-hover">
		            <thead>
		              <tr>
		                <th class="col-md-2 text-center">รหัสอ้างอิงการสั่งซื้อ</th>
		                <th class="col-md-2 text-center">ลูกค้า</th>
		                <th class="col-md-2 text-center">หลักฐานการโอนเงิน</th>
		                <th>รูปแบบการจ่ายเงิน </th>
		                <th>สถานที่นัดรับ</th>
		                <th>สั่งซื้อเมื่อ</th>
		                <th class="col-md-1 text-center">ดูสินค้า</th>
		                {{-- <th>จ่ายสินค้า</th> --}}
		              </tr>
		            </thead>
		            <tbody>
		             
		              @foreach($baskets_buying as $basket_buying)
		                <tr>
		                    <td  class="col-md-2 text-center">
		                      {{$basket_buying->basket_key}}
		                      {{-- {{$basket_buying->basket_id}} --}}
		                    </td>
		                    <td class="col-md-2 text-center">
		                    	<a title="ดูข้อมูลลูกค้า" href="{{route('customers.show', [$basket_buying->customer_id])}}">{{$basket_buying->first_name}} {{$basket_buying->last_name}}</a>
		                    </td>
		                    <td class="col-md-2 text-center">
		                      {{-- <img id="expand_image" src="{{asset($basket_buying->photo_path)}}" class="img-thumbnail" style="width: 50%;"> --}}

                  			@if($basket_buying->photo_path)
		                        <a href="{{asset($basket_buying->photo_path)}}" target="_blank">เปิดดู</a>
		                      @else
		                     	 <a href="#" >ไม่มีการอัพโหลด</a>
		                      @endif		                      
		                    </td>
		                    <td>
		                    	{{$basket_buying->pay}}
		                    </td>
		                    <td class="">
		                      {{$basket_buying->receive_place}}
		                    </td>
		                    <td class="">
		                      {{$basket_buying->updated_at_basket}}
		                    </td>
		                    <td class="col-md-1 text-center">
		                      <a href="{{route('home.seemore_products', $basket_buying->basket_key)}}" class="btn btn-info">เรียกดูสินค้า</a>
		                    </td>
		                  {{--   <td>
		                    	 <a href="{{route('home.update_status_basket', $basket_buying->basket_key)}}" class="btn btn-success">จ่ายสินค้า</a>
		                    </td> --}}
		                </tr>


		              @endforeach

		            </tbody>
		          </table>
	          </div>


            </div>
            <div class="box-footer text-right">
                {{ $baskets_buying->links() }}
            </div>
        </div>
    </div>
@endsection
