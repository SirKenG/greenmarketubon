@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ข้อมูลลูกค้า
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">รายละเอียดข้อมูลลูกค้า</h1>
            </div>
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    {{-- @include('customers.show_fields') --}}
                    @include('customers.show_field2')

                </div>
            </div>
            <div class="box-footer">
                <a href="{{ url()->previous() }}" class="btn btn-primary pull-right">Back</a>
            </div>
        </div>
    </div>
@endsection
