<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'ชื่อ :') !!}
    @if(session('signData.firstName'))
      {!! Form::text('first_name', session('signData.firstName') , ['class' => 'form-control']) !!}
    @else()
      {!! Form::text('first_name', null ,['class' => 'form-control']) !!}
    @endif()
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'นามสกุล :') !!}
    @if(session('signData.lastName'))
      {!! Form::text('last_name', session('signData.lastName') , ['class' => 'form-control']) !!}
    @else()
      {!! Form::text('last_name', null ,['class' => 'form-control']) !!}
    @endif()
</div>

<!-- Nick Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nick_name', 'ชื่อเล่น :') !!}
    {!! Form::text('nick_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Job Work Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job_work', 'อาชีพ :') !!}
    {!! Form::text('job_work', null, ['class' => 'form-control']) !!}
</div>

<!-- Sex Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sex', 'เพศ :') !!}
    {!! Form::select('sex',['ชาย'=>'ชาย','หญิง'=>'หญิง'], null, ['class' => 'form-control select2']) !!}
</div>

<!-- Age Field -->
<div class="form-group col-sm-6">
    {!! Form::label('age', 'อายุ :') !!}
    {!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<!-- Address No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_no', 'บ้านเลขที่ :') !!}
    {!! Form::text('address_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Moo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_moo', 'หมู่ :') !!}
    {!! Form::text('address_moo', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Soi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_soi', 'ซอย :') !!}
    {!! Form::text('address_soi', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Road Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_road', 'ถนน :') !!}
    {!! Form::text('address_road', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Id Field -->
<div class="form-group col-sm-6">
    {{-- {!! $provice !!} --}}
    {!! Form::label('province_id', 'จังหวัด :') !!}
    {!! Form::select('province_id', ['0'=>'---โปรดเลือก---'] + $provice, null, ['class' => 'form-control select2','id'=>'provinceID']) !!}
</div>

<!-- District Id Field -->
<div class="form-group col-sm-6">
    {{-- {!! $district !!} --}}
    {!! Form::label('district_id', 'อำเภอ :') !!}
    {!! Form::select('district_id',['0'=>'---โปรดเลือก---'] + $district, null, ['class' => 'form-control select2']) !!}
</div>

<!-- Sub District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_district_id', 'ตำบล :') !!}
    {!! Form::select('sub_district_id',['0'=>'---โปรดเลือก---'] + $sub_district, null, ['class' => 'form-control select2']) !!}
</div>

<!-- Zipcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zipcode', 'รหัสโปรษณีย์ :') !!}
    {!! Form::text('zipcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tel', 'เบอร์โทรศัพท์ที่สะดวกในการติดต่อ :') !!}
    {!! Form::text('tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email :') !!}
    @if(session('signData.email'))
      {!! Form::text('email', session('signData.email') , ['class' => 'form-control']) !!}
    @else()
      {!! Form::text('email', null ,['class' => 'form-control']) !!}
    @endif()
</div>

<!-- Facebook Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook', 'Facebook :') !!}
    @if(session('signData.profile'))
      {!! Form::text('facebook', session('signData.profile') , ['class' => 'form-control']) !!}
    @else()
      {!! Form::text('facebook', null ,['class' => 'form-control']) !!}
    @endif()
</div>

<!-- Line Field -->
<div class="form-group col-sm-6">
    {!! Form::label('line', 'Line :') !!}
    {!! Form::text('line', null, ['class' => 'form-control']) !!}
</div>

<!-- Send Produce Transfer Field -->
<div class="form-group col-sm-6 invisible style-mr-bt-60">
    {!! Form::label('send_produce_transfer', 'ที่อยู่ในการจัดส่ง เป็นที่เดียวกันกับข้อมูลข้างต้น ใช่หรือไม่:') !!}
    {!! Form::select('send_produce_transfer',['ใช่'=>'ใช่','ไม่ใช่'=>'ไม่ใช่'], null, ['class' => 'form-control select2']) !!}
</div>
