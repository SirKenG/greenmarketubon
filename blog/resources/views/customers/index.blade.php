@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">ข้อมูลลูกค้า</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">ข้อมูลลูกค้า</h1>
                {{-- <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('customers.create') !!}">เพิ่มข้อมูลลูกค้า</a> --}}
            </div>
            <div class="box-body">
                    @include('customers.table')
            </div>
        </div>
    </div>
@endsection
