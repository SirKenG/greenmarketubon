@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ข้อมูลลูกค้า
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
          <div class="box-header">
              <h1 class="text-center">เพิ่มข้อมูลลูกค้า</h1>
          </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'customers.store']) !!}

                        @include('customers.fields')
                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
                            <a href="{!! route('customers.index') !!}" class="btn btn-primary pull-right">Cancel</a>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
