<table class="table table-responsive table-bordered" id="customers-table">
    <thead>
      <tr class="back-tr">
        <th>รหัสลูกค้า</th>
        <th>ชื่อ</th>
        <th>นามสกุล</th>
        <th>อีเมล</th>
        <th>เบอร์โทรศัพท์</th>
        <th class="action">Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>{!! $customer->code !!}</td>
            <td>{!! $customer->first_name !!}</td>
            <td>{!! $customer->last_name !!}</td>
            <td>{!! $customer->email !!}</td>
            <td>{!! $customer->tel !!}</td>
            <td>
                {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                <!-- <div class='btn-group'> -->
                    <a href="{!! route('customers.show', [$customer->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('customers.edit', [$customer->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                <!-- </div> -->
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
