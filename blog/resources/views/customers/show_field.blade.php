<table class="table table-striped table-bordered detail-view">
    <tr>
        <th style="width:30%"><?php echo __('รหัสลูกค้า'); ?></th>
        <td>
            {!! $customer->code !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ชื่อ'); ?></th>
        <td>
            {!! $customer->first_name !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('นามสกุล'); ?></th>
        <td>
            {!! $customer->last_name !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ชื่อเล่น'); ?></th>
        <td>
            {!! $customer->nick_name !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('อาชีพ'); ?></th>
        <td>
            {!! $customer->job_work !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('เพศ'); ?></th>
        <td>
            {!! $customer->sex !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('อายุ'); ?></th>
        <td>
            {!! $customer->age !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ที่อยู่ บ้านเลขที่'); ?></th>
        <td>
            {!! $customer->address_no !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('หมู่'); ?></th>
        <td>
            {!! $customer->address_moo !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ซอย'); ?></th>
        <td>
            {!! $customer->address_soi !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ถนน'); ?></th>
        <td>
            {!! $customer->address_road !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('จังหวัด'); ?></th>
        <td>
            {!! $controller->showProvince($customer->province_id) !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('อำเภอ'); ?></th>
        <td>
            {!! $controller->showDistrict($customer->district_id) !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ตำบล'); ?></th>
        <td>
            {!! $controller->showSubDistrict($customer->sub_district_id) !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('รหัสไปรษณีย์'); ?></th>
        <td>
            {!! $customer->zipcode !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('เบอร์โทรศัพท์ที่สะดวกในการติดต่อ'); ?></th>
        <td>
            {!! $customer->tel !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('Email'); ?></th>
        <td>
            {!! $customer->email !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('Facebook'); ?></th>
        <td>
            {!! $customer->facebook !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('Line'); ?></th>
        <td>
            {!! $customer->line !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ที่อยู่จัดส่งตามรายละเอียดข้างต้น'); ?></th>
        <td>
            {!! $customer->send_produce_transfer !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('วันที่สมัคร'); ?></th>
        <td>
            {!! $customer->created_at !!}
        </td>
    </tr>

</table>
