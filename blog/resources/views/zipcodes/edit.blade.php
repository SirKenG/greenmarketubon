@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Zipcode
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($zipcode, ['route' => ['zipcodes.update', $zipcode->id], 'method' => 'patch']) !!}

                        @include('zipcodes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection