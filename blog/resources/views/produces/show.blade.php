@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            สินค้า
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">รายละเอียดข้อมูลสินค้า</h1>
            </div>
            <div class="box-body">
            <div class="row">
                <div class="col col-md-4">
                    @if( ! is_null($produce->photo_path))
                        {{ Html::image($produce->photo_path, 'a User', array('class' => 'user-show img-circle')) }}
                    @else
                        {{ Html::image('imgUser/notproduce.png', 'a User', array('class' => 'user-show img-circle')) }}
                    @endif
                </div>
                <div class="col col-md-8">
                    @include('produces.show_fields')
                </div>
            </div>
            </div>
            <div class="box-footer">
                <a href="{!! route('produces.index') !!}" class="btn btn-primary pull-right">Back</a>
            </div>
        </div>
    </div>
@endsection
<style>
    .user-show{
        width : 80%;
        height : 40%;
        align : middle;
         border-radius = 50%;
        text-align : center;
        padding : 20px;
    }

</style>
