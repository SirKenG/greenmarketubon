@extends('layouts.app')

@section('content')
  @include('includes.tinyeditor')


  
    <section class="content-header">
        <h1>
            ตะกร้าสินค้า
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">แก้ไขข้อมูลสินค้า</h1>
            </div>
           <div class="box-body">
               <div class="row">
                   {!! Form::model($produce, ['route' => ['produces.update', $produce->id], 'method' => 'patch','type'=>'file','enctype'=>'multipart/form-data']) !!}

                        @include('produces.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
