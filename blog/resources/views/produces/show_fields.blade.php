    <table class="table table-striped table-bordered detail-view">
        <tr>
            <th><?php echo __('ประเภทสินค้า'); ?></th>
            <td>
                {!! App\Produce::showProduceType($produce->produce_type_id) !!}
            </td>
        </tr>

        <tr>
            <th><?php echo __('ชื่อสินค้า'); ?></th>
            <td>
                {!! $produce->name !!}
            </td>
        </tr>

        <tr>
            <th><?php echo __('รายละเอียดสินค้า'); ?></th>
            <td>
                {!! $produce->detail !!}
            </td>
        </tr>

        <tr>
            <th><?php echo __('ราคา'); ?></th>
            <td>
                {!! $produce->prime !!}
            </td>
        </tr>

 <!--       <tr>
            <th><?php echo __('หน่วยราคาสินค้า'); ?></th>
            <td>
              @if(!empty($produce->unit) || !is_null($produce->unit))
                {!! $produce->unit !!}
              @else
                {!! __('-') !!}
              @endif

            </td>
        </tr>-->

        <tr>
            <th><?php echo __('วันที่เพิ่ม :'); ?></th>
            <td>
                {!! $produce->created_at !!}
            </td>
        </tr>

        <tr>
            <th><?php echo __('แก้ไขล่าสุด :'); ?></th>
            <td>
                @if( ! is_null($produce->updated_at))
                  {!! $produce->updated_at !!}
                @else
                  {!! __('-') !!}
                @endif
            </td>
        </tr>
    </table>
