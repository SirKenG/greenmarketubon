<!-- Produce Type Id Field -->
<div class="form-group col-md-12">
    {!! Form::label('produce_type_id', 'ประเภทสินค้า:') !!}
     {!! Form::select('produce_type_id',App\ProduceType::getList(), null, ['class' => 'form-control select2']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-md-12">
    {!! Form::label('name', 'ชื่อสินค้า:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Detail Field -->
<div class="form-group col-md-12">
    {!! Form::label('detail', 'รายละเอียดสินค้า:') !!}
    {!! Form::text('detail', null, ['class' => 'form-control']) !!}
</div>





<div class="form-group col-md-12">
    {!! Form::label('body', 'รายละเอียดผู้ผลิตสินค้า:') !!}
    {!! Form::textarea('body', null, ['class'=>'form-control'])!!}
</div>





<!-- Prime Field -->
<div class="form-group col-md-12">
    {!! Form::label('prime', 'ราคา:') !!}
    {!! Form::text('prime', null, ['class' => 'form-control number']) !!}
    &nbsp;<span id="errmsg" style="color : red; "></span>
</div>

{{-- <div class="form-group col-md-6">
    {!! Form::label('unit', 'หน่วยราคาสินค้า:') !!}
    {!! Form::text('unit', null, ['class' => 'form-control']) !!}
</div> --}}

<div class="form-group col-md-12">
    {!! Form::label('photo_path', 'รูปภาพ:') !!}
    {!! Form::file('photo_path') !!}
</div>







<!-- Submit Field -->
<div class="form-group col-md-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('produces.index') !!}" class="btn btn-primary pull-right">Cancel</a>
</div>
