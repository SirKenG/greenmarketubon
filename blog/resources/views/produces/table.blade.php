<div class="table-responsive">
<table class="table table-bordered" id="produces-table">
    <thead>
        <tr class="back-tr">
            <th>#</th>
            <th>ประเภทสินค้า</th>
            <th>ขื่อสินค้า</th>
            <th>รายละเอียดสินค้า</th>
            <th class="actions">Action</th>
        <tr>
    </thead>
    <tbody>
    @foreach($produces as $k => $produce)
        <tr>
            <td>{!! ($k+1) !!}</td>
            <td>{!! App\Produce::showProduceType($produce->produce_type_id) !!}</td>
            <td>{!! $produce->name !!}</td>
            <td>{!! $produce->detail !!}</td>
            <td>
                {!! Form::open(['route' => ['produces.destroy', $produce->id], 'method' => 'delete']) !!}
                <!-- <div class='btn-group'> -->
                    <a href="{!! route('produces.show', [$produce->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('produces.edit', [$produce->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                <!-- </div> -->
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
