@extends('layouts.app')

@section('content')
<style type="text/css">
/*! normalize.css v5.0.0 | MIT License | github.com/necolas/normalize.css */select.FormField:focus,textarea.FormField:focus{border-color:#3b99fc;outline:0}.ViewSelector,.ViewSelector2{display:block}.ViewSelector2-item,.ViewSelector table{display:block;margin-bottom:1em;width:100%}.ViewSelector2-item>label,.ViewSelector td:first-child{font-weight:700;margin:0 .25em .25em 0;display:block}.ViewSelector2-item>select{width:100%}.ViewSelector table,.ViewSelector tbody,.ViewSelector td,.ViewSelector tr{display:block}.ViewSelector table{height:auto!important}.ViewSelector table,.ViewSelector td{width:auto!important}.ViewSelector td:last-child *{display:block;text-align:left}.ViewSelector td:last-child>div{font-weight:400;margin:0}@media (min-width:570px){.ViewSelector,.ViewSelector2{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;margin:0 0 -1em -1em;width:-webkit-calc(100% + 1em);width:calc(100% + 1em)}.ViewSelector2-item,.ViewSelector table{-webkit-box-flex:1;-webkit-flex:1 1 -webkit-calc(100%/3 - 1em);-ms-flex:1 1 calc(100%/3 - 1em);flex:1 1 calc(100%/3 - 1em);margin-left:1em}}.ViewSelector2--stacked,.ViewSelector--stacked{display:block;margin:0;width:auto}.ViewSelector2--stacked .ViewSelector2-item,.ViewSelector--stacked table{margin-left:0}.u-visuallyHidden{border:0!important;clip:rect(1px,1px,1px,1px)!important;height:1px!important;overflow:hidden!important;padding:0!important;position:absolute!important;width:1px!important}.u-hidden{display:none}.u-block{display:block}@media (min-width:420px){.u-sm-hidden{display:none}.u-sm-block{display:block}}@media (min-width:570px){.u-md-hidden{display:none}.u-md-block{display:block}}@media (min-width:1024px){.u-lg-hidden{display:none}.u-lg-block{display:block}}.u-spaceDouble{margin:0 0 3em}
    .Titles-main {
        font-size: 1.4em;
    }

    .Titles-sub {
        opacity: .6;
        margin-top: .2em;
    }

    .Titles-main, .Titles-sub {
        color: inherit;
        /*font: inherit;*/
        margin: 0;
    }
</style>
    <section class="content-header">
        <h1 class="pull-left">รายงาน</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>


        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">รายงานสถิติการเยี่ยมชมเว็บไซต์ (google analytics)</h1>
            </div>
{{-- Step 1: Create the containing elements. --}}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="text-danger">*จำเป็นต้องล็อกอินด้วยบัญชีของ google ก่อนเท่านั้น!!! จึงสามารถดูรายงานได้</h5>
                        <h5 id="embed-api-auth-container" class="text-primary"></h2>
                    </div>
                    <div class="col-md-2">
                        <div id="active-users-container"></div>
                    </div>
                    <div class="col-md-4 text-center">
                        <a class="btn btn-info" href="https://analytics.google.com/analytics/web/?hl=th&pli=1#embed/report-home/a111634092w166496185p166885345/" target="_blank"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> ดูรายงานเพิ่มเติมจาก google analytics</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 id="loadding-gapi-analytics" class="text-center text-warning">กำลังโหลด กรุณารอสักครู่...</h1>
                        <div id="view-selector-container"></div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="Chartjs">
                          <h3 id="topic-1" class="Titles-main"></h3>
                          <h5 id="sub-1" class="Titles-sub"></h5>
                          <figure class="Chartjs-figure" id="chart-1-container"></figure>
                          <ol class="Chartjs-legend" id="legend-1-container"></ol>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="Chartjs">
                          <h3 id="topic-2" class="Titles-main"></h3>
                          <h5 id="sub-2" class="Titles-sub"></h5>
                          <figure class="Chartjs-figure" id="chart-2-container"></figure>
                          <ol class="Chartjs-legend" id="legend-2-container"></ol>
                        </div>
                    </div>    
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="Chartjs">
                          <h3 id="topic-3" class="Titles-main"></h3>
                          <h5 id="sub-3" class="Titles-sub"></h5>
                          <figure class="Chartjs-figure" id="chart-3-container"></figure>
                          <ol class="Chartjs-legend" id="legend-3-container"></ol>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="Chartjs">
                          <h3 id="topic-4" class="Titles-main"></h3>
                          <h5 id="sub-4" class="Titles-sub"></h5>
                          <figure class="Chartjs-figure" id="chart-4-container"></figure>
                          <ol class="Chartjs-legend" id="legend-4-container"></ol>
                        </div>
                    </div>    
                </div>
                <hr>



            </div>
        </div>
    </div>
{{-- Step 2: Load the library. --}}
<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>
<!-- This demo uses the Chart.js graphing library and Moment.js to do date
     formatting and manipulation. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

<!-- Include the ViewSelector2 component script. -->
<script src="https://ga-dev-tools.appspot.com/public/javascript/embed-api/components/view-selector2.js"></script>

<!-- Include the DateRangeSelector component script. -->
<script src="https://ga-dev-tools.appspot.com/public/javascript/embed-api/components/date-range-selector.js"></script>

<!-- Include the ActiveUsers component script. -->
<script src="https://ga-dev-tools.appspot.com/public/javascript/embed-api/components/active-users.js"></script>

{{-- Include the CSS that styles the charts. --}}
<link rel="stylesheet" href="https://ga-dev-tools.appspot.com/public/css/chartjs-visualizations.css">


<script>
function setupPropertyAfterloaded() {
    document.getElementById("loadding-gapi-analytics").style.display = "none";

    document.getElementById("topic-1").innerHTML = "This Week vs Last Week";
    document.getElementById("sub-1").innerHTML = "By sessions";

    document.getElementById("topic-2").innerHTML = "This Year vs Last Year";
    document.getElementById("sub-2").innerHTML = "By users";

    document.getElementById("topic-3").innerHTML = "Top Browsers";
    document.getElementById("sub-3").innerHTML = "By pageview";

    document.getElementById("topic-4").innerHTML = "Top City";
    document.getElementById("sub-4").innerHTML = "By IP addresses or Geographical";
}

gapi.analytics.ready(function() {
//-- Step 3: Authorize the user. --//
    var CLIENT_ID = '360625805897-btge1voc1qcinrj7mfv17rqateqk195p.apps.googleusercontent.com';
    /**
     * Authorize the user immediately if the user has already granted access.
     * If no access has been created, render an authorize button inside the
     * element with the ID "embed-api-auth-container".
    */
    gapi.analytics.auth.authorize({
        container: 'embed-api-auth-container',
        clientid: CLIENT_ID,
    });


//-- Step 4: Create the view selector. --//
  /**
   * Create a new ActiveUsers instance to be rendered inside of an
   * element with the id "active-users-container" and poll for changes every
   * five seconds.
   */
  var activeUsers = new gapi.analytics.ext.ActiveUsers({
    container: 'active-users-container',
    pollingInterval: 5
  });


    /**
     * Add CSS animation to visually show the when users come and go.
    */
    activeUsers.once('success', function() {
      var element = this.container.firstChild;
      var timeout;

      this.on('change', function(data) {
        var element = this.container.firstChild;
        var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
        element.className += (' ' + animationClass);

        clearTimeout(timeout);
        timeout = setTimeout(function() {
          element.className =
              element.className.replace(/ is-(increasing|decreasing)/g, '');
         }, 3000);
       });
    });


    /**
     * Create a new ViewSelector2 instance to be rendered inside of an
     * element with the id "view-selector-container".
    */
    var viewSelector2 = new gapi.analytics.ext.ViewSelector2({
      container: 'view-selector-container',
    });


//-- Step 5: Create the timeline chart. --//
  // var timeline = new gapi.analytics.googleCharts.DataChart({
  //   reportType: 'ga',
  //   query: {
  //     'dimensions': 'ga:date',
  //     'metrics': 'ga:sessions',
  //     'start-date': '30daysAgo',
  //     'end-date': 'yesterday',
  //   },
  //   chart: {
  //     type: 'LINE',
  //     container: 'timeline'
  //   }
  // });




//-- Step 6: Hook up the components to work together. --//
  gapi.analytics.auth.on('success', function(response) {
    console.log('auth:success');
    setupPropertyAfterloaded();

    viewSelector2.execute();
  });



  /**
   * Update the activeUsers component, the Chartjs charts, and the dashboard
   * title whenever the user changes the view.
   */
  viewSelector2.on('viewChange', function(data) {
    // var title = document.getElementById('view-name');
    // title.textContent = data.property.name + ' (' + data.view.name + ')';

    // Start tracking active users for this view.
    console.log('viewSelector2:viewChange', data);
    activeUsers.set(data).execute();

    console.log('data:ids',data.ids);
    let ids = data.ids;
    // var newIds = {
    //   query: {
    //     ids: data.ids
    //   }
    // }
    // timeline.set(newIds).execute();
    // Render all the of charts for this view.
    renderWeekOverWeekChart(data.ids);
    renderYearOverYearChart(data.ids);
    renderTopBrowsersChart(data.ids);
    renderTopCountriesChart(data.ids);
  });

 /**
   * Draw the a chart.js line chart with data from the specified view that
   * overlays session data for the current week over session data for the
   * previous week.
   */
  function renderWeekOverWeekChart(ids) {

    // Adjust `now` to experiment with different days, for testing only...
    var now = moment(); // .subtract(3, 'day');

    var thisWeek = query({
      'ids': ids,
      'dimensions': 'ga:date,ga:nthDay',
      'metrics': 'ga:sessions',
      'start-date': moment(now).subtract(1, 'day').day(0).format('YYYY-MM-DD'),
      'end-date': moment(now).format('YYYY-MM-DD')
    });

    var lastWeek = query({
      'ids': ids,
      'dimensions': 'ga:date,ga:nthDay',
      'metrics': 'ga:sessions',
      'start-date': moment(now).subtract(1, 'day').day(0).subtract(1, 'week')
          .format('YYYY-MM-DD'),
      'end-date': moment(now).subtract(1, 'day').day(6).subtract(1, 'week')
          .format('YYYY-MM-DD')
    });

    Promise.all([thisWeek, lastWeek]).then(function(results) {

      var data1 = results[0].rows.map(function(row) { return +row[2]; });
      var data2 = results[1].rows.map(function(row) { return +row[2]; });
      var labels = results[1].rows.map(function(row) { return +row[0]; });

      labels = labels.map(function(label) {
        return moment(label, 'YYYYMMDD').format('ddd');
      });

      var data = {
        labels : labels,
        datasets : [
          {
            label: 'Last Week',
            fillColor : 'rgba(220,220,220,0.5)',
            strokeColor : 'rgba(220,220,220,1)',
            pointColor : 'rgba(220,220,220,1)',
            pointStrokeColor : '#fff',
            data : data2
          },
          {
            label: 'This Week',
            fillColor : 'rgba(151,187,205,0.5)',
            strokeColor : 'rgba(151,187,205,1)',
            pointColor : 'rgba(151,187,205,1)',
            pointStrokeColor : '#fff',
            data : data1
          }
        ]
      };

      new Chart(makeCanvas('chart-1-container')).Line(data);
      generateLegend('legend-1-container', data.datasets);
    });
  }


  /**
   * Draw the a chart.js bar chart with data from the specified view that
   * overlays session data for the current year over session data for the
   * previous year, grouped by month.
   */
  function renderYearOverYearChart(ids) {

    // Adjust `now` to experiment with different days, for testing only...
    var now = moment(); // .subtract(3, 'day');

    var thisYear = query({
      'ids': ids,
      'dimensions': 'ga:month,ga:nthMonth',
      'metrics': 'ga:users',
      'start-date': moment(now).date(1).month(0).format('YYYY-MM-DD'),
      'end-date': moment(now).format('YYYY-MM-DD')
    });

    var lastYear = query({
      'ids': ids,
      'dimensions': 'ga:month,ga:nthMonth',
      'metrics': 'ga:users',
      'start-date': moment(now).subtract(1, 'year').date(1).month(0)
          .format('YYYY-MM-DD'),
      'end-date': moment(now).date(1).month(0).subtract(1, 'day')
          .format('YYYY-MM-DD')
    });

    Promise.all([thisYear, lastYear]).then(function(results) {
      var data1 = results[0].rows.map(function(row) { return +row[2]; });
      var data2 = results[1].rows.map(function(row) { return +row[2]; });
      var labels = ['Jan','Feb','Mar','Apr','May','Jun',
                    'Jul','Aug','Sep','Oct','Nov','Dec'];

      // Ensure the data arrays are at least as long as the labels array.
      // Chart.js bar charts don't (yet) accept sparse datasets.
      for (var i = 0, len = labels.length; i < len; i++) {
        if (data1[i] === undefined) data1[i] = null;
        if (data2[i] === undefined) data2[i] = null;
      }

      var data = {
        labels : labels,
        datasets : [
          {
            label: 'Last Year',
            fillColor : 'rgba(220,220,220,0.5)',
            strokeColor : 'rgba(220,220,220,1)',
            data : data2
          },
          {
            label: 'This Year',
            fillColor : 'rgba(151,187,205,0.5)',
            strokeColor : 'rgba(151,187,205,1)',
            data : data1
          }
        ]
      };

      new Chart(makeCanvas('chart-2-container')).Bar(data);
      generateLegend('legend-2-container', data.datasets);
    })
    .catch(function(err) {
      console.error(err.stack);
    });
  }


  /**
   * Draw the a chart.js doughnut chart with data from the specified view that
   * show the top 5 browsers over the past seven days.
   */
  function renderTopBrowsersChart(ids) {

    query({
      'ids': ids,
      'dimensions': 'ga:browser',
      'metrics': 'ga:pageviews',
      'sort': '-ga:pageviews',
      'max-results': 5
    })
    .then(function(response) {

      var data = [];
      var colors = ['#4D5360','#949FB1','#D4CCC5','#E2EAE9','#F7464A'];

      response.rows.forEach(function(row, i) {
        data.push({ value: +row[1], color: colors[i], label: row[0] });
      });

      new Chart(makeCanvas('chart-3-container')).Doughnut(data);
      generateLegend('legend-3-container', data);
    });
  }


  /**
   * Draw the a chart.js doughnut chart with data from the specified view that
   * compares sessions from mobile, desktop, and tablet over the past seven
   * days.
   */
  function renderTopCountriesChart(ids) {
    query({
      'ids': ids,
      'dimensions': 'ga:city', //'ga:country',
      'metrics': 'ga:sessions',
      'sort': '-ga:sessions',
      'max-results': 5
    })
    .then(function(response) {

      var data = [];
      var colors = ['#4D5360','#949FB1','#D4CCC5','#E2EAE9','#F7464A'];

      response.rows.forEach(function(row, i) {
        data.push({
          label: row[0],
          value: +row[1],
          color: colors[i]
        });
      });

      new Chart(makeCanvas('chart-4-container')).Doughnut(data);
      generateLegend('legend-4-container', data);
    });
  }


  /**
   * Extend the Embed APIs `gapi.analytics.report.Data` component to
   * return a promise the is fulfilled with the value returned by the API.
   * @param {Object} params The request parameters.
   * @return {Promise} A promise.
   */
  function query(params) {
    return new Promise(function(resolve, reject) {
      var data = new gapi.analytics.report.Data({query: params});
      data.once('success', function(response) { resolve(response); })
          .once('error', function(response) { reject(response); })
          .execute();
    });
  }


  /**
   * Create a new canvas inside the specified element. Set it to be the width
   * and height of its container.
   * @param {string} id The id attribute of the element to host the canvas.
   * @return {RenderingContext} The 2D canvas context.
   */
  function makeCanvas(id) {
    var container = document.getElementById(id);
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    container.innerHTML = '';
    canvas.width = container.offsetWidth;
    canvas.height = container.offsetHeight;
    container.appendChild(canvas);

    return ctx;
  }


  /**
   * Create a visual legend inside the specified element based off of a
   * Chart.js dataset.
   * @param {string} id The id attribute of the element to host the legend.
   * @param {Array.<Object>} items A list of labels and colors for the legend.
   */
  function generateLegend(id, items) {
    var legend = document.getElementById(id);
    legend.innerHTML = items.map(function(item) {
      var color = item.color || item.fillColor;
      var label = item.label;
      return '<li><i style="background:' + color + '"></i>' +
          escapeHtml(label) + '</li>';
    }).join('');
  }


  // Set some global Chart.js defaults.
  Chart.defaults.global.animationSteps = 60;
  Chart.defaults.global.animationEasing = 'easeInOutQuart';
  Chart.defaults.global.responsive = true;
  Chart.defaults.global.maintainAspectRatio = false;


  /**
   * Escapes a potentially unsafe HTML string.
   * @param {string} str An string that may contain HTML entities.
   * @return {string} The HTML-escaped string.
   */
  function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
  }




});
</script>
@endsection
