@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">

<style type="text/css">
  .label-search {
    font-weight: normal;
    white-space: nowrap;
    text-align: left;
  }
  .input-search {
    margin-left: 0.5em;
    display: inline-block;
    width: auto;
  }
</style>
  
  <section class="content-header">
      <h1 class="pull-left">รายงานสินค้าขายดี</h1>
  </section>
  <div class="content">
      <div class="clearfix"></div>

      <div class="box box-primary">
          <div class="box-header">
              <h2 class="text-center">เลือกเงื่อนไขเพื่อแสดงผลรายงานสินค้าขายดี</h2>
          </div>

          <div class="box-body">
{{--             <pre>
              {{$collapsed_saler_products}}
            </pre> --}}
            <div class="row" style="text-align: right;">
                <div class="col-md-6" style="text-align: left;">
                  <button class="btn btn-info" onclick="calculateSumonClik();">คำนวนผลรวม</button>
                </div>
                <div class="col-md-3">
                  <label class="label-search">
                    ค้นหาวันที่เริ่มต้น: 
                    <input type="text" id="datepicker_from" class="form-control form-control-sm input-search" placeholder="" aria-controls="example">
                  </label>
                </div>
                <div class="col-md-3">
                  <label class="label-search">
                    วันที่สิ้นสุด: 
                    <input type="text" id="datepicker_to" class="form-control form-control-sm input-search" placeholder="" aria-controls="example">
                  </label>
                </div>
            </div>
            <br>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>ขื่อสินค้า</th>
                      <th>สั่งซื้อเมื่อ</th>
                      <th>รหัสอ้างอิงการสั่งซื้อ</th>
                      <th>ลูกค้า</th>
                      <th>ราคา/หน่วย</th>
                      <th>จำนวน</th>
                      <th>ราคารวม</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $sumprice = 0;
                    $sumqty = 0;
                  ?>
                  @foreach($collapsed_saler_products as $key => $value)
                    <?php 
                      $sumqty += $value->qty;
                      $sumprice += $value->prime;

                      $total_price = 0;
                      $total_price = $value->qty * $value->prime;
                    ?>
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{date('Y-m-d', strtotime($value->updated_at_basket))}}</td>
                        <td>{{$value->basket_key}}</td>
                        <td>{{$value->first_name}} {{$value->last_name}}</td>
                        <td>{{$value->prime}}</td>
                        <td>{{$value->qty}}</td>
                        <td>
                        <label>฿{{number_format($total_price,2)}}</label>
                        </td>
                    </tr>

                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="6">รวม</th>
                        <th id="qtyResult">จำนวน</th>
                        <th id="priceResult">ราคารวม</th>
                    </tr>
                </tfoot>
            </table>



          </div>
      </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{--script that run in layouts.script--}}
<script>
  const body__for_collapse = $('body');
  body__for_collapse.addClass('sidebar-collapse');



  // $(function() {
  //   $(".datepicker_from").datepicker({
  //     dateFormat: "yy-mm-dd"
  //   });
  // });



  // $(function() {
  //   $("#datepicker_to").datepicker();
  // });
</script>



{{--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}

{{--   <script>
    $("#datepicker_from").datepicker({
      dateFormat: 'yy-mm-dd',
      "onSelect": function(date) {
        dateFrom = new Date(date).getTime();
        console.log(date, dateFrom);

      }
    });
    $("#datepicker_to").datepicker({
      dateFormat: 'yy-mm-dd'
    });
  </script> --}}

@endsection
