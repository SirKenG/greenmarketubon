@extends('layouts.app')

@section('content')
<style type="text/css">
    .Titles-main {
        font-size: 1.4em;
    }

    .Titles-sub {
        opacity: .6;
        margin-top: .2em;
    }

    .Titles-main, .Titles-sub {
        color: inherit;
        /*font: inherit;*/
        margin: 0;
    }
</style>
    <section class="content-header">
        <h1 class="pull-left">รายงาน</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">รายงานสรุปการสั่งซื้อสินค้า/ข้อมูลลูกค้า</h1>
            </div>

            <div class="box-body">
              <div class="row">
                  <div class="col-md-12 text-center">
                      <h2 class="text-info">ข้อมูลลูกค้า</h2>
                  </div>
              </div>
              {{-- <hr> --}}
              <div class="row">
                  <div class="col-md-6">
                      <h3 id="topic-1" class="Titles-main">เพศ</h3>
                      <h5 id="sub-1" class="Titles-sub">สรุปเพศของลูกค้า</h5>
                      <canvas id="chartjs-ages" class="chartjs" width="undefined" height="undefined"></canvas>
                  </div>
                  <div class="col-md-6">
                      <h3 id="topic-1" class="Titles-main">อายุ</h3>
                      <h5 id="sub-1" class="Titles-sub">สรุปอายุของลูกค้า</h5>
                      <canvas id="chartjs-1" class="chartjs" width="undefined" height="undefined"></canvas>
                  </div>
              </div>
              <hr>
              <div class="row">
                  <div class="col-md-6">
                      <h3 id="topic-1" class="Titles-main">อาชีพ</h3>
                      <h5 id="sub-1" class="Titles-sub">สรุปอาชีพของลูกค้า</h5>
                      <canvas id="chartjs-job" class="chartjs" width="undefined" height="undefined"></canvas>
                  </div>
                  <div class="col-md-6">
                      <h3 id="topic-1" class="Titles-main">สินค้าขายดี 5 อันดับแรก</h3>
                      <h5 id="sub-1" class="Titles-sub">คลิก <a target="_blank" href="{{ route('reports.bestsaler')}}">ที่นี้</a> เพื่อดูรายงานการขายสินค้าทั้งหมด</h5>
                      <canvas id="chartjs-sale-products" class="chartjs" width="770" height="385" style="display: block; width: 770px; height: 385px;"></canvas>
                  </div>
              </div>



            </div>
        </div>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

<script>
  new Chart(document.getElementById("chartjs-ages"),{"type":"doughnut","data":{"labels":["ชาย","หญิง","ไม่ระบุ"],"datasets":[{"label":"Gender","data":[{{$sumsexman}},{{$sumsexwoman}},{{$sumsexnonespecify}}],"backgroundColor":["rgb(54, 162, 235)","rgb(255, 99, 132)","rgb(255, 205, 86)"]}]}});

const labels_ = "{{$labels_chartjs1}}";
const data_ = "{{$data_chartjs1}}";
// console.log(data_);
const arrData = labels_.split(",");
const arrLabels = data_.split(",");
// console.log(arrLabels);
new Chart(document.getElementById("chartjs-1"),{"type":"bar","data":{"labels":arrLabels,"datasets":[{"label":"จำนวน","data":arrData,"fill":false,"backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],"borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]},"options":{"scales":{"yAxes":[{"ticks":{"beginAtZero":true}}]}}});

const job_work_ = "{{$job_work}}";
const Countjob_work_ = "{{$Countjob_work}}";

const arrJob_work_ = job_work_.split(",");
const arrCountjobWork = Countjob_work_.split(",");

new Chart(document.getElementById("chartjs-job"),{"type":"doughnut","data":{"labels":arrJob_work_,"datasets":[{"label":"Gender","data":arrCountjobWork,"backgroundColor":["rgb(0, 51, 102)","rgb(0, 204, 153)","rgb(153, 102, 51)","rgb(75, 192, 192)","rgba(54, 162, 235, 0.2)","rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)"]}]}});


const productName_ = "{{$labels_productName}}";
const qtyProductName_ = "{{$qty_productName}}";

const arrproductName__ = productName_.split(",");
const arrqtyProductName__ = qtyProductName_.split(",");


new Chart(document.getElementById("chartjs-sale-products"),{"type":"horizontalBar","data":{"labels":arrproductName__,"datasets":[{"label":"จำนวน","data":arrqtyProductName__,"fill":false,"backgroundColor":["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],"borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":1}]},"options":{"scales":{"xAxes":[{"ticks":{"beginAtZero":true}}]}}});
</script>
@endsection
