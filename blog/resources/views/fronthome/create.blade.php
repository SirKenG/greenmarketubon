@extends('layouts.app')

@section('content')
    @include('includes.tinyeditor')


    <style type="text/css">
      .thumbnail{
          position: relative;
          height: 130px;
          overflow: hidden;
          margin: 0;
      }

      .thumbnail img{
          width: 100%;
          display: block;
          margin: -18% 0;
      }

    </style>
    <section class="content-header">
        <h1>
            จัดการหน้าแรก
        </h1>
    </section>
    <div class="content">
       @if(Session::has('fronthome_update_meassage'))
        <div class="alert alert-success" role="alert">
            {{Session::get('fronthome_update_meassage')}}
        </div>
       @endif
        <div class="box box-primary">
          <div class="box-header">
              <h1 class="text-center">แก้ไขข้อมูล</h1>
          </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::model($fronthomes[0], ['method'=>'PATCH', 'action'=> ['FrontHomeController@update', $fronthomes[0]->id], 'files'=>false]) !!}
                            {!! Form::hidden('type', null, ['class'=>'form-control'])!!}
                            <div class="form-group">
                                {!! Form::label('title', 'ข้อความบนสไลด์:') !!}
                                {!! Form::text('message', null, ['class'=>'form-control'])!!}
                            </div>   
                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>                       
                        {!! Form::close() !!}
                    </div>
                </div>
                <hr>
                <div class="row">
                    <h4 class="text-center">ภาพสไลด์จะแสดงผลดีที่สุด แนะนำให้อัพโหลดภาพขนาด กว้าง 960 px สูง 640 px{{--1,740 x 1,000--}}
                      <br><a href="{{ url('/') }}" target="_blank">ดูตัวอย่างสไลด์</a></h4>
                    <br>
                    <div class="col-sm-4">
                        {!! Form::model($fronthomes[1], ['method'=>'PATCH', 'action'=> ['FrontHomeController@update', $fronthomes[1]->id], 'files'=>true]) !!}
                             {!! Form::hidden('type', null, ['class'=>'form-control'])!!}
                             <div class="thumbnail">
                                <img src="{{asset($fronthomes[1]->photo_path)}}" alt="...">
                             </div>
                             <div class="form-group">
                                {!! Form::label('title', 'ภาพสไลด์ ลำลับที่ 1:') !!}
                                {!! Form::file('photo_path') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>   
                        {!! Form::close() !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::model($fronthomes[2], ['method'=>'PATCH', 'action'=> ['FrontHomeController@update', $fronthomes[2]->id], 'files'=>true]) !!}
                             {!! Form::hidden('type', null, ['class'=>'form-control'])!!}
                             <div class="thumbnail">
                                <img src="{{asset($fronthomes[2]->photo_path)}}" alt="...">
                             </div>
                             <div class="form-group">
                                {!! Form::label('title', 'ภาพสไลด์ ลำลับที่ 2:') !!}
                                {!! Form::file('photo_path') !!}
                            </div>
                             <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>                              
                        {!! Form::close() !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::model($fronthomes[3], ['method'=>'PATCH', 'action'=> ['FrontHomeController@update', $fronthomes[3]->id], 'files'=>true]) !!}
                             {!! Form::hidden('type', null, ['class'=>'form-control'])!!}
                             <div class="thumbnail">
                                <img src="{{asset($fronthomes[3]->photo_path)}}" alt="...">
                             </div>
                             <div class="form-group">
                                {!! Form::label('title', 'ภาพสไลด์ ลำลับที่ 3:') !!}
                                {!! Form::file('photo_path') !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>                               
                        {!! Form::close() !!}
                    </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-12" >
                    {!! Form::model($fronthomes[4], ['method'=>'PATCH', 'action'=> ['FrontHomeController@update', $fronthomes[4]->id], 'files'=>false]) !!}
                      {!! Form::hidden('type', null, ['class'=>'form-control'])!!}
                            <div class="form-group">
                                {!! Form::label('message', 'หัวข้อ:') !!}
                                {!! Form::text('message', null, ['class'=>'form-control'])!!}
                            </div>            
                    
                            <div class="form-group">
                                {!! Form::label('message2', 'เนื้อหา:') !!}
                                {!! Form::textarea('message2', null, ['class'=>'form-control'])!!}
                            </div>
                    
                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>
                    {!! Form::close() !!}
                  </div>
                </div>

            </div>
        </div>
    </div>
@endsection
