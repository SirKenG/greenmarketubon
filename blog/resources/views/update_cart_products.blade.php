@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ปรับเปลี่ยนคำสั่งซื้อสินค้า
        </h1>
    </section>
    <div class="content">
      @include('flash::message')
        <div class="box box-primary">
            <div class="box-header">
                <h2 class="page-title">รหัสอ้างอิงคำสั่งซื้อ: {{$products[0]->basket_key}}</h2>
                <h3>นัดรับที่: {{$products[0]->receive_place}}</h3>    
            </div>
            <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <form method="get" action="{{route('basket.addBasketByAdmin')}}">
                    <div class="input-group">
                      <input type="hidden" name="basketKey" value="{{$products[0]->basket_key}}">
                     {!! Form::select('productID', ['0'=>'---เลือกสินค้าที่ต้องการ---'] + $Produce, null, ['class' => 'form-control select2','id'=>'productID']) !!}
                      <span class="input-group-btn">
                        <button class="btn btn-primary btn-xl" type="submit">เพิ่มสินค้าเข้าคำสั่งซื้อ</button>
                      </span>
                    </div> {{-- /input-group --}}
                  </form>
                </div>
            </div>
        <div class="row">
          <div class="table-responsive" style="margin-top: 20px;">          
          <table class="table table-hover">
            <thead>
              <tr>
                {{-- <th class="col-md-1 text-center">ลบ</th> --}}
                <th>รูปภาพ</th>
                <th>ชื่อสินค้า</th>
                <th>ราคา</th>
                <th>จำนวน</th>
                <th>ราคารวม</th>
                <th class="col-md-1 text-center">ลบ</th>
              </tr>
            </thead>
            <tbody>

              <?php $sum_total_price = 0;?>
              @foreach($products as $product)
                <tr>
{{--                   <td class="col-md-1 text-center">
                    <a id="remove-cart" style="cursor: pointer; color: red;" title="นำ {{$product->name}} ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket({{$product->id}}, 1);">
                      <i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>
                    </a>
                  </td> --}}
                  <td class="col-md-1">
                    <a style="cursor: pointer;" title="ดูรายละเอียดของ {{$product->name}}"><img src="{{asset($product->photo_path)}}" style="width: 100%;"></a>  
                  </td>
                  <td style="color: #5cb85c;">
                    <a style="cursor: pointer;" title="ดูรายละเอียดของ {{$product->name}}">
                      {{$product->name}}
                    </a>
                  </td>
                  <td>
                    <span id="cart-price">{{$product->prime}}</span>฿
                  </td>
                  <td class="col-md-2">
                    <form method="get" action="{{route('basket.updateBasketQtyForCartPageByAdmin')}}">
                      <div class="input-group">
                        <input type="hidden" name="productID" value="{{$product->id}}">
                        <input type="hidden" name="basketKey" value="{{$products[0]->basket_key}}">
                        <input type="number" name="qty" class="form-control" placeholder="จำนวน" min="1" value="{{$product->qty}}">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="submit">ปรับปรุง</button>
                        </span>
                      </div>
                    </form>
                  </td>
                  <td>
                    <label id="cart-total-qty">
                      <?php 
                        $total_price = 0;
                        $total_price = $product->qty * $product->prime; 

                        $sum_total_price += $total_price;
                      ?>
                      {{number_format($total_price,2)}}
                    </label>฿
                  </td>
                  <td class="col-md-1 text-center">
 {{--                    <a id="remove-cart" style="cursor: pointer; color: red;" title="นำ {{$product->name}} ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket({{$product->id}}, 1);">
                      <i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>
                    </a> --}}
                    <form method="get" action="{{route('basket.removeProductFromBasketByAdmin')}}">
                      <input type="hidden" name="basketKey" value="{{$products[0]->basket_key}}">
                      <input type="hidden" name="productID" value="{{$product->id}}">
                      <button type="submit" class="btn btn-danger" title="นำ {{$product->name}} ออกจากคำสั่งซื้อ">
                        <i id="remove-cart-symbol" class="fa fa-minus-square-o"" aria-hidden="true"></i>
                      </button>
                      {{-- <a style="cursor: pointer; color: red;" type="submit" title="นำ {{$product->name}} ออกจากตะกร้าสินค้า">
                        <i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>
                      </a> --}}
                    </form>


                  </td>
                </tr>
              @endforeach

            </tbody>
          </table>
          </div>
        </div>



       {{--  <br> --}}
        <div class="row" style="margin-top: -20px">
            <div class="col-md-4">
                
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                {{-- <a onclick="updateCart(this);" class="btn border-btn-green" style="padding: 15px; width: 100%">ปรับปรุงตะกร้าสินค้า</a> --}}
                <div style="margin-bottom: 10px;"></div>
                {{-- (กรณีที่เปลี่ยนจำนวน แล้วราคารวมไม่เปลี่ยนแปลง ให้กดอัพเดท) --}}
                <div class="thumbnail">
                  <h4>ราคารวมทั้งหมด:  <span id="sum-total-price">{{$sum_total_price}}</span>฿</h4>
                </div>

               {{--  <a href="{{ route('home.index') }}" class="btn btn-success pull-right" style="padding: 15px; width: 100%"><i class="fa fa-arrow-left" aria-hidden="true"></i> กลับและ <i class="fa fa-envelope" aria-hidden="true"></i> ส่งอีเมลแจ้งลูกค้า</a> --}}
                <form method="get" action="{{route('basket.checkout_buyByAdmin')}}">
                    <input type="hidden" name="basketKey" value="{{$products[0]->basket_key}}">
                    <button type="submit" class="btn btn-success pull-right" style="padding: 15px; width: 100%"><i class="fa fa-arrow-left" aria-hidden="true"></i> กลับและ <i class="fa fa-envelope" aria-hidden="true"></i> ส่งอีเมลแจ้งลูกค้า</button>
                </form>
            </div>    
        </div>

            </div>
            <div class="box-footer">
                
            </div>
        </div>
    </div>
@endsection
