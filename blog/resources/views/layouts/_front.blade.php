<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>ตลาดนัดสีเขียวกินสบายใจ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font-awesome-->
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" >
    <!-- Custom CSS -->
    <link href="{{asset('css/custom.front.css')}}" rel="stylesheet">
    <!-- Select2 -->
    {!! Html::style('adminlte/plugins/select2/select2.min.css') !!}
  </head>
  <body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-green">
      <a class="navbar-brand" href="#">ตลาดนัดสีเขียว กินสบายใจ</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item {{((Request::is('/')) ? 'active':'')}}">
            <a class="nav-link" href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true"></i> หน้าหลัก</a>
          </li>
          <li class="nav-item {{((Request::is('objective')) ? 'active':'')}}">
            <a class="nav-link" href="{{route('objective.index')}}"><i class="fa fa-globe" aria-hidden="true"></i> แนวคิดโครงการ</a>
          </li>
          <li class="nav-item {{((Request::is('information')) ? 'active':'')}}">
            <a class="nav-link" href="{{route('information.index')}}"><i class="fa fa-database" aria-hidden="true"></i> ข้อมูลโครงการ</a>
          </li>
          <li class="nav-item {{((Request::is('productinformation')) ? 'active':'')}}">
            <a class="nav-link" href="{{route('productinformation.index')}}"><i class="fa fa-pagelines" aria-hidden="true"></i> ข้อมูลผลิตภัณฑ์</a>
          </li>
          <li class="nav-item {{((Request::is('productvalue')) ? 'active':'')}}">
            <a class="nav-link" href="{{route('productvalue.index')}}"><i class="fa fa-bar-chart" aria-hidden="true"></i> คุณค่าของสินค้า</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-leaf" aria-hidden="true"></i> กลุ่มผลิตอินทรีย์</a>
          </li>
          <li class="nav-item dropdown {{((Request::is('payment')) ||  Request::is('logistic') ? 'active':'')}}">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-barcode" aria-hidden="true"></i> การซื้อสินค้า
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="{{route('basket.index')}}"><i class="fa fa-shopping-basket" aria-hidden="true"></i> ตะกร้าสินค้า</a>
              <a class="dropdown-item" href="{{route('payment.index')}}"><i class="fa fa-btc" aria-hidden="true"></i> การชำระเงิน</a>
              <a class="dropdown-item" href="{{route('logistic.index')}}"><i class="fa fa-plane" aria-hidden="true"></i> การจัดส่งสินค้า</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{route('customer.register')}}"><i class="fa fa-pencil" aria-hidden="true"></i> สมัครสมาชิก</a>
            </div>
          </li>
          <li class="nav-item {{((Request::is('contactus')) ? 'active':'')}}">
            <a class="nav-link" href="{{route('contactus.index')}}"><i class="fa fa-envelope" aria-hidden="true"></i> ติดต่อเรา</a>
          </li>
        </ul>
      </div>
    </nav>

   {{--  <div class="container"> --}}
      @yield('content')
    {{-- </div> --}}


    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <!-- jQuery 3.1.1 -->
    {!! Html::script('adminlte/plugins/jQuery/jquery-3.1.1.min.js') !!}
    <script src="{{asset('vendor/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- Select2 -->
    {!! Html::script('adminlte/plugins/select2/select2.full.min.js') !!}
    @include('layouts.script')
    @yield('scripts')
  </body>
</html>
