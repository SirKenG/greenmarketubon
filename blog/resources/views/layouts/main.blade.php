<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>ตลาดนัดสีเขียวกินสบายใจ</title>

    <!-- Bootstrap 3.3.6 -->
    {!! Html::style('adminlte/bootstrap/css/bootstrap.min.css') !!}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Date Picker -->
    {!! Html::style('adminlte/plugins/datepicker/datepicker3.css') !!}

    <!-- Select2 -->
    {!! Html::style('adminlte/plugins/select2/select2.min.css') !!}

    @yield('css')
  </head>
  <body>
      <div class="wrapper">
          <!-- Main Header -->
          <header class="main-header">
              <!-- Header Navbar -->
              <nav class="navbar navbar-static-top" role="navigation">

              </nav>
          </header>
          <div class="content-wrapper">
              @yield('content')
          </div>
      </div>
    <!-- jQuery 3.1.1 -->
    {!! Html::script('adminlte/plugins/jQuery/jquery-3.1.1.min.js') !!}

    <!-- Bootstrap 3.3.6 -->
    {!! Html::script('adminlte/bootstrap/js/bootstrap.min.js') !!}

    <!-- datepicker -->
    {!! Html::script('adminlte/plugins/datepicker/bootstrap-datepicker.js') !!}

    <!-- Select2 -->
    {!! Html::script('adminlte/plugins/select2/select2.full.min.js') !!}

    @include('layouts.script')
    @yield('scripts')
  </body>
</html>
