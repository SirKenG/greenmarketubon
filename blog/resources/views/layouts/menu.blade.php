
<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home.index') !!}"><i class="fa fa-edit"></i><span>คำสั่งซื้อ</span></a>
</li>

<li class="{{ Request::is('history') ? 'active' : '' }}">
    <a href="{!! route('home.history') !!}"><i class="fa fa-edit"></i><span>ประวัติคำสั่งซื้อ</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>ผู้ดูแลระบบ</span></a>
</li>

<li class="{{ Request::is('produceTypes*') ? 'active' : '' }}">
    <a href="{!! route('produceTypes.index') !!}"><i class="fa fa-edit"></i><span>ประเภทตะกร้าสินค้า</span></a>
</li>

<li class="{{ Request::is('produces*') ? 'active' : '' }}">
    <a href="{!! route('produces.index') !!}"><i class="fa fa-edit"></i><span>สินค้า</span></a>
</li>

<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{!! route('customers.index') !!}"><i class="fa fa-edit"></i><span>จัดการข้อมูลลูกค้า</span></a>
</li>



<li class="{{ Request::is('frontHome*') ? 'active' : '' }}">
    <a href="{!! route('frontHome.create') !!}"><i class="fa fa-edit"></i><span>จัดการหน้าแรก</span></a>
</li>

<li class="{{ Request::is('objective*') ? 'active' : '' }}">
    <a href="{!! route('objective.create') !!}"><i class="fa fa-edit"></i><span>จัดการแนวคิดโครงการ</span></a>
</li>

<li class="{{ Request::is('information*') ? 'active' : '' }}">
    <a href="{!! route('information.create') !!}"><i class="fa fa-edit"></i><span>จัดการข้อโครงการ</span></a>
</li>

<li class="{{ Request::is('producer*') ? 'active' : '' }}">
    <a href="{!! route('producer.index') !!}"><i class="fa fa-edit"></i><span>จัดการกลุ่มผลิตอินทรีย์</span></a>
</li>



<li class="{{ Request::is('productinformation*') ? 'active' : '' }}">
    <a href="{!! route('productinformation.create') !!}"><i class="fa fa-edit"></i><span>จัดการข้อมูลผลิตภัณฑ์</span></a>
</li>

<li class="{{ Request::is('productvalue*') ? 'active' : '' }}">
    <a href="{!! route('productvalue.create') !!}"><i class="fa fa-edit"></i><span>จัดการข้อมูลคุณค่าของสินค้า</span></a>
</li>

<li class="{{ Request::is('payment*') ? 'active' : '' }}">
    <a href="{!! route('payment.create') !!}"><i class="fa fa-edit"></i><span>จัดการข้อมูลการชำระเงิน</span></a>
</li>

<li class="{{ Request::is('logistic*') ? 'active' : '' }}">
    <a href="{!! route('logistic.create') !!}"><i class="fa fa-edit"></i><span>จัดการข้อมูลการจัดส่ง</span></a>
</li>

<li class="{{ Request::is('contactus*') ? 'active' : '' }}">
    <a href="{!! route('contactus.create') !!}"><i class="fa fa-edit"></i><span>จัดการข้อมูลติดต่อเรา</span></a>
</li>


<li class="{{ Request::is('basket*') ? 'active' : '' }}">
    <a href="{!! route('basket.edit_acc') !!}"><i class="fa fa-edit"></i><span>จัดการหน้าสั่งซื้อสินค้า</span></a>
</li>


<li class="{{ Request::is('reports.index') ? 'active' : '' }}">
    <a href="{!! route('reports.index') !!}"><i class="fa fa-edit"></i><span>รายงานสรุปสินค้า/ลูกค้า</span></a>
</li>

<li class="{{ Request::is('reports.google') ? 'active' : '' }}">
    <a href="{!! route('reports.google') !!}"><i class="fa fa-edit"></i><span>รายงานสถิติ (google analytics)</span></a>
</li>