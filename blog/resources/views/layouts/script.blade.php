<script>
    $(function () {
        $('.box-header').addClass('with-border');

        $(".select2").select2();

        $('#provinceID').change(function ()
        {
            var province = $(this).val();
            // console.log(province);
            if (province != 0) {
                $.ajax({
                    url: getUrlForAjaxUrl() + 'customer/district/' + province,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="district_id"]').find('option').remove();
                        $('select[name="district_id"]').append('<option value="0">---โปรดเลือก---</option>');
                        $('select[name="sub_district_id"]').find('option').remove();
                        $('select[name="sub_district_id"]').append('<option value="0">---โปรดเลือก---</option>');
                        $.each(data, function (key, value) {
                            $('select[name="district_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="district_id"]').find('option').remove();
                $('select[name="district_id"]').append('<option value="0">---โปรดเลือก---</option>');
                $('select[name="sub_district_id"]').find('option').remove().end();
                $('select[name="sub_district_id"]').append('<option value="0">---โปรดเลือก---</option>');
            }
        });

        $('#district_id').change(function ()
        {
            var district = $(this).val();
            if (district != 0) {
                $.ajax({
                    url: getUrlForAjaxUrl() + 'customer/subdistrict/' + district,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="sub_district_id"]').find('option').remove();
                        $('select[name="sub_district_id"]').append('<option value="0">---โปรดเลือก---</option>');
                        $.each(data, function (key, value) {
                            $('select[name="sub_district_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });

                    }
                });
            } else {
                $('select[name="sub_district_id"]').find('option').remove().end();
                $('select[name="sub_district_id"]').append('<option value="0">---โปรดเลือก---</option>');
            }
        });

        $('.number').keypress(function(e){
          //if the letter is not digit then display error and don't type anything
           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
              //display error message
              $("#errmsg").html("เฉพาะตัวเลขเท่านั้น").show().fadeOut("slow");
                     return false;
          }
        });

    });
</script>



<!--Data Table-->
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"  src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  {{-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --}}
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
//=========datatable ||| /reports/bestsaler
$(function() {
    var oTable = $('#example').DataTable({
        "language": {
            "search": "ค้นหาด้วยคำ:"
        }
    });


// console.log(oTable);
    $("#datepicker_from").datepicker({
      dateFormat: 'yy-mm-dd',
      "onSelect": function(date) {
        dateFrom = new Date(date).getTime();
        qtyResult = 0;
        priceResult = 0;
        // console.log('dddddd', dateFrom);
        oTable.draw();
        // console.log('oTable', oTable.data());
        // console.log(oTable.columns());
      }
    }).keyup(function() {
        dateFrom = new Date(this.value).getTime();
        qtyResult = 0;
        priceResult = 0;
        oTable.draw();
    }); 
    $("#datepicker_to").datepicker({
      dateFormat: 'yy-mm-dd',
      "onSelect": function(date) {
        dateTo = new Date(date).getTime();
        qtyResult = 0;
        priceResult = 0;
        oTable.draw();
      }
    }).keyup(function() {
        dateFrom = new Date(this.value).getTime();
        qtyResult = 0;
        priceResult = 0;
        oTable.draw();
    });


    oTable.on( 'search.dt', function () {
        // $('#filterInfo').html( 'Currently applied global search: '+table.search() );
        // console.log(oTable.row( ':last').data());
        // var value = $('.dataTables_filter input').val();
        // console.log(oTable.columns(6).data().flatten().filter( function ( value, index ) {return true;})); // <-- the value
        // $('th[id="qtyResult"]').html('')
        // $('th[id="priceResult"]').html('')
        console.log('SearchButton');
    } );

} );



// Date range filter
qtyResult = 0;
priceResult = 0;
dateFrom = "";
dateTo = "";


$.fn.dataTableExt.afnFiltering.push(
    function(oSettings, aData, iDataIndex) {
        console.log('dataTableExt', aData[2], iDataIndex);

        if (typeof aData._date == 'undefined') {
          aData._date = new Date(aData[2]).getTime();
        }

        // console.log('aData._date', aData._date);
        if (dateFrom && !isNaN(dateFrom)) {
            if (aData._date < dateFrom) {
                return false;
            }
        }


        if (dateTo && !isNaN(dateTo)) {
            if (aData._date > dateTo) {
                return false;
            }
        }

        $('th[id="qtyResult"]').html('')
        $('th[id="priceResult"]').html('')
        calculateSum(aData);
        return true;
    }
);

function calculateSum(data) {
    let qty = data[6];
    let price = data[7].replace('฿','');
    qtyResult +=  parseInt(qty);
    priceResult += parseFloat(price);

    $('th[id="qtyResult"]').html(qtyResult)
    $('th[id="priceResult"]').html('฿' + parseFloat(priceResult).toFixed(2))
    console.log(qtyResult, priceResult);
}

function calculateSumonClik() {
    let qtyResult = 0;
    let priceResult = 0;
    let calTable = $('table[id="example"] tbody')
    let childrenData = calTable.children();
    let lengthChildren = calTable.children().length;

    for (let i = 0; i < lengthChildren; i++) {
        let value_qty = $(childrenData[i]).children()[6];
        let qty = $(value_qty).text()

        let value_price = $(childrenData[i]).children()[7];
        let space_price = $(value_price).text().replace('฿','');
        let price = space_price.replace(/\s/g, '');

        qtyResult +=  parseInt(qty);
        priceResult += parseFloat(price);

        console.log(qtyResult, priceResult);
        $('th[id="qtyResult"]').html(qtyResult)
        $('th[id="priceResult"]').html('฿' + parseFloat(priceResult).toFixed(2))
    }
    // console.log(calTable.children().children(), lengthChildren);
}

</script>
