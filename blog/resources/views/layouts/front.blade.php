<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta htt
        p-equiv="X-UA-Compatible" content="IE=edge">
        <title>ตลาดนัดสีเขียวกินสบายใจ</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keyword" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{-- <link href="{{asset('css/fontsGoogleapis.css')}}" rel='stylesheet' type='text/css'> --}}

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
{{--         <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon"> --}}

        {{-- <link rel="stylesheet" href="{{asset('garo/assets/css/normalize.css')}}">  --}}
        <link rel="stylesheet" href="{{asset('garo/assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/fontello.css')}}">
        <link href="{{asset('garo/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css')}}" rel="stylesheet">
        <link href="{{asset('garo/assets/fonts/icon-7-stroke/css/helper.css')}}" rel="stylesheet">
        <link href="{{asset('garo/assets/css/animate.css')}}" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="{{asset('garo/assets/css/bootstrap-select.min.css')}}"> 
        <link rel="stylesheet" href="{{asset('garo/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/icheck.min_all.css')}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/price-range.css')}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/owl.carousel.css')}}">  
        <link rel="stylesheet" href="{{asset('garo/assets/css/owl.theme.css')}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/owl.transitions.css')}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/style.css?v=').time()}}">
        <link rel="stylesheet" href="{{asset('garo/assets/css/responsive.css?v=').time()}}">



        <!-- App stylesheet -->
        <link rel="stylesheet" href="{{ asset('css/libs.css') }}" >

     <!-- Select2 -->
    {!! Html::style('adminlte/plugins/select2/select2.min.css') !!}  

    {{-- Global site tag (gtag.js) - Google Analytics --}}
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111634092-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-111634092-1');
    </script>

    </head>
    <body>
        <!-- Pre loader -->
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>

        <div class="header-connect">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 pull-left">
                        <div class="header-half header-call">
                            {{-- <p> --}}
                         {{--        @if(session('member_login')==true)
                                    <span><i class="pe-7s-call"></i>  {{session('member_tel')}}</span>
                                    <span><i class="pe-7s-mail"></i>  {{session('member_email')}}</span>
                                @else --}}
                                    {{-- <span><i class="pe-7s-call"></i>  0-4535-3826</span>
                                    <span><i class="pe-7s-mail"></i>  rboondao@hotmail.com</span> --}}
                                    <span><a href="https://www.facebook.com/ubongreenmarket/" target="_blank" title="แฟนเพจตลาดนัดสีเขียวอุบลราชธานี"><img src="{{asset('img/logo-greenmarket.jpg')}}" height="40" width="40" alt="green market"></a></span>
                                     <span><a href="http://www.ubu.ac.th/" target="_blank" title="มหาวิทยาลัยอุบลราชธานี"><img src="{{asset('img/logo-ubon-university.png')}}" height="40" width="40" alt="Ubon Ratchathani University"></a></span>
                                     <span><a href="http://www.bus.ubu.ac.th/" target="_blank" title="คณะบริหารศาสตร์ มหาวิทยาลัยอุบลราชธานี"><img src="{{asset('img/logo-bus.png')}}" height="40" width="40" alt="Faculty of Management Science"></a></span>
                                     <span><a href="https://www.trf.or.th/" target="_blank" title="สำนักงานกองทุนสนับสนุนการวิจัย"><img src="{{asset('img/logo-trf.png')}}" height="40" width="40" alt="The Thailand Research Fund"></a></span>
                                     @if(session('member_login')==true)
                                    <span><i class="pe-7s-call"></i>  {{session('member_tel')}}</span>
                                    <span><i class="pe-7s-mail"></i>  {{session('member_email')}}</span>

                                @endif
                            {{-- </p> --}}
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="header-half">
                            <ul class="list-inline">

                                <li class="dropdown ymm-sw" data-wow-delay="0.1s" id="menu-historybuy-d"></li>

                                <li class="dropdown ymm-sw" data-wow-delay="0.2s" id="menu-basket">
                                    <a class="nav-button basket dropdown-toggle" href="{{route('member.create')}}" data-wow-delay="0.50s" data-toggle="dropdown" data-hover="dropdown">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span class="badge"></span>
                                    </a>

                                    <ul id="showdetailproduct-topmenu" class="dropdown-menu navbar-nav dropdown-menu-basketdetail">
                                            <div style="margin-bottom: 10px;"></div>


                                            <div id="content"></div>
                                            <hr>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a onclick="checkout({{session('member_login')}});" class="btn border-btn-checkout pull-right" >สั่งซื้อสินค้า</a>
                                                </div>
                                                <div class="col-md-6">
                                                    {{-- <label>ราคารวม: 30.00฿</label> --}}
                                                    <label id="price">ราคารวม: 0฿</label>
                                                </div>
                                            </div>

                                    </ul>
                                </li>

                                <li class="dropdown ymm-sw" data-wow-delay="0.4s" id="menu-loginoutchangepass-d"></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End top header -->


@if(Session::has('checkout_meassage'))
    <div class="alert alert-danger" role="alert" style="margin-bottom: 0px;">
        {{Session::get('checkout_meassage')}}
    </div>
@endif


        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}" style="margin-top: 12px;">ตลาดนัดสีเขียว กินสบายใจ</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse yamm" id="navigation">
                    <ul class="main-nav nav navbar-nav navbar-right">
                        <li class="wow fadeInDown" data-wow-delay="0.1s">
                            <div id="menu-historybuy-m" style="text-align: center;"></div>
                        </li>
                        <li class="wow fadeInDown" data-wow-delay="0.2s">
                            <div id="menu-loginoutchangepass-m" style="text-align: center;margin-top: 10px;"></div>
                        </li>
                        <li class="wow fadeInDown" data-wow-delay="0.3s">
                            <a href="{{ url('/') }}" class="{{((Request::is('/')) ? 'active':'')}}">หน้าแรก </a>
                        </li>

                        <li class="dropdown ymm-sw " data-wow-delay="0.4s">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ตะกร้าสินค้า <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                @foreach($product_types as $product_type)
                                    <li>
                                        <a href="{{route('baskettype.showbaseontype', $product_type->id)}}">{{$product_type->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>


                        <li class="dropdown ymm-sw " data-wow-delay="0.5s">
                            <a href="#" class="dropdown-toggle {{((Request::is('payment')) || (Request::is('logistic')) || (Request::is('customer')) ? 'active':'')}}" data-toggle="dropdown" data-hover="dropdown" data-delay="300">การซื้อสินค้า <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="{{route('payment.index')}}" class="" >การชำระเงิน </a>
                                </li>
                                <li>
                                    <a href="{{route('logistic.index')}}" class="" >การจัดส่งสินค้า </a>
                                </li>
                                <li>
                                    <a href="{{route('customer.register')}}" class="" >สมัครสมาชิก </a>
                                </li>                                
                            </ul>
                        </li>


                        <li class="dropdown ymm-sw " data-wow-delay="0.6s">
                            <a href="#" class="dropdown-toggle {{((Request::is('objective')) || (Request::is('information'))? 'active':'')}}" data-toggle="dropdown" data-hover="dropdown" data-delay="400">เกี่ยวกับโครงการ <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="{{route('objective.index')}}" class="" >แนวคิดโครงการ </a>
                                </li>
                                <li>
                                    <a href="{{route('information.index')}}" class="" >ข้อมูลโครงการ </a>
                                </li>
                                <li>
                                    <a href="{{route('producer.showall')}}" class="" >กลุ่มผลิตอินทรีย์ </a>
                                </li>                                
                            </ul>
                        </li>


                        <li class="dropdown ymm-sw " data-wow-delay="0.7s">
                            <a href="#" class="dropdown-toggle {{((Request::is('productinformation')) || (Request::is('productvalue'))? 'active':'')}}" data-toggle="dropdown" data-hover="dropdown" data-delay="500">เกี่ยวกับผลิตภัณฑ์ <b class="caret"></b></a>
                            <ul class="dropdown-menu navbar-nav">
                                <li>
                                    <a href="{{route('productinformation.index')}}" class="" >ข้อมูลผลิตภัณฑ์ </a>
                                </li>
                                <li>
                                    <a href="{{route('productvalue.index')}}" class="" >คุณค่าของสินค้า </a>
                                </li>
                            </ul>
                        </li>



                        <li class="wow fadeInDown" data-wow-delay="0.8s">
                            <a href="{{route('contactus.index')}}" class="{{(Request::is('contactus'))}}" >ติดต่อเรา </a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- End of nav bar -->





        {{--Start Content--}}
        @yield('content')
        {{--End Content--}}





        <!-- Footer area-->
        <div class="footer-area">

            <div class=" footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">
                                <h4>About us </h4>
                                <div class="footer-title-line"></div>
                                <p id="footer-line1">ระบบจัดการธุรกิจออนไลน์ของกลุ่มวิสาหกิจชุมชนตลาดนัดสีเขียวกินสบายใจ จังหวัดอุบลราชธานี</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 wow fadeInRight animated">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-sm-6 wow fadeInRight animated">
                            <div class="single-footer">

                                <p><strong>ทีมวิจัย ประกอบด้วย</strong>
                                </p>
                                <p id="footer-line2">รองศาสตราจารย์ ดร. รุ่งรัศมี  บุญดาว คณะบริหารศาสตร์ มหาวิทยาลัยอุบลราชธานี</p>
                                <p id="footer-line3">ดร. จักริน วชิรเมธิน คณะบริหารศาสตร์ มหาวิทยาลัยอุบลราชธานี</p>
                                <p id="footer-line4">นายสุชัย  เจริญมุขยนันท์  กรรมการและเลขาธิการมูลนิธิสื่อสร้างสุข จังหวัดอุบลราชธานี</p>
                                <p>นางสาวคนึงนุช วงศ์เย็น มูลนิธิสื่อสร้างสุข จังหวัดอุบลราชธานี</p>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 wow fadeInRight animated">
                            <ul class="footer-adress">
                                <li><i class="pe-7s-map-marker strong"> </i> ห้องพักอาจารย์ ms 409</li>
                                <li><i class="pe-7s-mail strong"> </i>  rboondao@hotmail.com</li>
                                <li><i class="pe-7s-call strong"> </i> 0-4535-3826</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-copy text-center">
                <div class="container">
                    <div class="row">
                        <div class="pull-left">
                            <span> &copy; <a href="http://greenmarketubon.com/">ตลาดนัดสีเขียว กินสบายใจ </a> , All rights reserved {{date('Y')}}  </span> 
                        </div> 
                        <div class="bottom-menu pull-right"> 
                            <ul> 
{{--                                 <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.2s">Home</a></li>
                                <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.3s">Property</a></li>
                                <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.4s">Faq</a></li>
                                <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.6s">Contact</a></li> --}}
                            </ul> 
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script src="{{asset('garo/assets/js/modernizr-2.6.2.min.js')}}"></script>
    <!-- jQuery 3.1.1 -->
    {{-- {!! Html::script('adminlte/plugins/jQuery/jquery-3.1.1.min.js') !!} --}}


        <script src="{{asset('garo/assets/js/jquery-1.10.2.min.js')}}"></script> 
        <script src="{{asset('garo/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('garo/assets/js/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('garo/assets/js/bootstrap-hover-dropdown.js')}}"></script>

        <script src="{{asset('garo/assets/js/easypiechart.min.js')}}"></script>
        <script src="{{asset('garo/assets/js/jquery.easypiechart.min.js')}}"></script>

        <script src="{{asset('garo/assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('garo/assets/js/wow.js')}}"></script>

        <script src="{{asset('garo/assets/js/icheck.min.js')}}"></script>
        <script src="{{asset('garo/assets/js/price-range.js')}}"></script>

        <script src="{{asset('garo/assets/js/main.js')}}"></script>

        <!-- Select2 -->
        {!! Html::script('adminlte/plugins/select2/select2.full.min.js') !!}


        <script src="{{ asset('js/libs.js?v=').time() }}"></script>

    @include('layouts.script')
    @yield('scripts')
    </body>
</html>