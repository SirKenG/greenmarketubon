@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">ประเภทตะกร้าสินค้า</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">ข้อมูลประเภทตะกร้าสินค้า</h1>
                <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('produceTypes.create') !!}">เพิ่มประเภทตะกร้าสินค้า</a>
            </div>
            <div class="box-body">
                    @include('produce_types.table')
            </div>
        </div>
    </div>
@endsection
