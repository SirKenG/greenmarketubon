<table class="table table-striped table-bordered detail-view">
    <tr>
        <th><?php echo __('Id'); ?></th>
        <td>
            {!! $produceType->id !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ชื่อประเภทตะกร้าสินค้า'); ?></th>
        <td>
            {!! $produceType->name !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('วันที่เพิ่ม :'); ?></th>
        <td>
            {!! $produceType->created_at !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('แก้ไขล่าสุด :'); ?></th>
        <td>
            {!! $produceType->updated_at !!}
        </td>
    </tr>
</table>
