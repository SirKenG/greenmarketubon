@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ประเภทตะกร้าสินค้า
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">เพิ่มประเภทตะกร้าสินค้า</h1>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'produceTypes.store']) !!}

                        @include('produce_types.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
