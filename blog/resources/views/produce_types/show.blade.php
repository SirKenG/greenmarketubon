@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ประเภทตะกร้าสินค้า
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">รายละเอียดประเภทตะกร้าสินค้า</h1>
            </div>
            <div class="box-body">
                    @include('produce_types.show_fields')

            </div>
            <div class="box-footer">
                <a href="{!! route('produceTypes.index') !!}" class="btn btn-primary pull-right">Back</a>
            </div>
        </div>
    </div>
@endsection
