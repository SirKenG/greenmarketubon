<table class="table table-responsive table-bordered" id="produceTypes-table">
    <thead>
        <tr class="back-tr">
            <th>#</th>
            <th>ประเภทตะกร้าสินค้า</th>
            <th class="actions">Action</th>
        <tr>
    </thead>
    <tbody>
    @foreach($produceTypes as $k => $produceType)
        <tr>
            <td>{!! ($k+1) !!}</td>
            <td>{!! $produceType->name !!}</td>
            <td>
                {!! Form::open(['route' => ['produceTypes.destroy', $produceType->id], 'method' => 'delete']) !!}
                <!-- <div class='btn-group pull-right'> -->
                    <a href="{!! route('produceTypes.show', [$produceType->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('produceTypes.edit', [$produceType->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                <!-- </div> -->
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
