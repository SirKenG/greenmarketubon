<!-- Name Field -->
<div class="form-group col-md-12">
    {!! Form::label('name', 'ประเภทตะกร้าสินค้า:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('produceTypes.index') !!}" class="btn btn-primary pull-right">Cancel</a>
</div>
