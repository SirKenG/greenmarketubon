@extends('layouts.app')

@section('content')
    @include('includes.tinyeditor')



    <section class="content-header">
        <h1>
            ข้อมูลผลิตภัณฑ์
        </h1>
    </section>
    <div class="content">
       @if(Session::has('productinformation_update_meassage'))
        <div class="alert alert-success" role="alert">
            {{Session::get('productinformation_update_meassage')}}
        </div>
       @endif
        <div class="box box-primary">
          <div class="box-header">
              <h1 class="text-center">แก้ไขข้อมูล</h1>
          </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12" >
                        {!! Form::model($productinformation[0], ['method'=>'PATCH', 'action'=> ['ProductInformationController@update', $productinformation[0]->id], 'files'=>false]) !!}
                            <div class="form-group">
                                {!! Form::label('title', 'หัวข้อ:') !!}
                                {!! Form::text('title', null, ['class'=>'form-control'])!!}
                            </div>            
                    
                            <div class="form-group">
                                {!! Form::label('body', 'เนื้อหา:') !!}
                                {!! Form::textarea('body', null, ['class'=>'form-control'])!!}
                            </div>
                    
                            <div class="form-group">
                                {!! Form::submit('Save', ['class'=>'btn btn-success']) !!}
                            </div>
                    
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
