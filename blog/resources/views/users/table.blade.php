<table class="table table-responsive table-bordered" id="users-table">
    <thead>
        <tr class="back-tr">
            <th>#</th>
            <th>ชื่อ-สกุล</th>
            <th>Email</th>
            <th class="actions">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $k => $user)
        <tr>
            <td>{!! ($k+1) !!}</td>
            <td>{!! $user->name !!}</td>
            <td>{!! $user->email !!}</td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <!-- <div class='btn-group pull-right'> -->
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::user()->id==$user->id || Auth::user()->email=='webmaster@sirkeng.me' || Auth::user()->email=='rboondao@hotmail.com')
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                <!-- </div> -->
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>