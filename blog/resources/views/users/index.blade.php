@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">ผู้ดูแลระบบ</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">ข้อมูลผู้ดูแลระบบ</h1>
                <a class="btn btn-primary" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">เพิ่มผู้ดูแลระบบ</a>
            </div>
            <div class="box-body">
                    @include('users.table')
            </div>
        </div>
    </div>
@endsection
