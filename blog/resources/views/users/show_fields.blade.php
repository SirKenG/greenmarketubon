<table class="table table-striped table-bordered detail-view">
    <tr>
        <th><?php echo __('Id'); ?></th>
        <td>
            {!! $user->id !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('ชื่อ-สกุล'); ?></th>
        <td>
            {!! $user->name !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('Email'); ?></th>
        <td>
            {!! $user->email !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('วันที่สร้าง:'); ?></th>
        <td>
            {!! $user->created_at !!}
        </td>
    </tr>
    <tr>
        <th><?php echo __('แก้ไขล่าสุด:'); ?></th>
        <td>
            {!! $user->updated_at !!}
        </td>
    </tr>
</table>
