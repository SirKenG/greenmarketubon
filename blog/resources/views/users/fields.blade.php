
<div class="form-group col-md-12">
    {!! Form::label('name', 'ชื่อ-สกุล:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'name']) !!}

    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'email']) !!}

    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password',['class' => 'form-control','placeholder'=>'password']) !!}

    {!! Form::label('photo_file', 'รูปโปรไฟล์:') !!}
    {!! Form::file('photo_file') !!}

</div>
@if(isset($user->id))
<div class="form-group col-md-12">
    <a href="{!! route('users.deleteimg', $user->id) !!}" class="btn btn-danger">ลบรูปโปรไฟล์</a>
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-md-12">
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-primary pull-right">Cancel</a>
</div>
