@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            ผู้ดูแลระบบ
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">ข้อมูลผู้ดูแลระบบ</h1>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col col-md-4">
                    @if(count($user->photo_file) > 0)
                        {{ Html::image($user->photo_file, 'a User', array('class' => 'user-show img-circle')) }}
                    @else
                        {{ Html::image('imgUser/User.png', 'a User Image', array('class' => 'user-image')) }}
                    @endif
                    </div>
                    <div class="col col-md-8">
                        @include('users.show_fields')
                    </div>
                </div>
                    
            </div>
            <div class="box-footer">
                <a href="{!! route('users.index') !!}" class="btn btn-primary pull-right">ย้อนกลับ</a>
            </div>
        </div>
    </div>
@endsection
<style>
    .user-show{
        /* width : 40%; */
        height : 40%;
        align : middle;
        border-radius = 50%;
        text-align : center;
        padding : 20px;
    }

</style>
