@extends('layouts.app')

@section('content')
    <div class="content">
        @include('adminlte-templates::common.errors')
        @include('flash::message')
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">เพิ่มผู้ดูแลระบบ</h1>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'users.store','type'=>'file','enctype'=>'multipart/form-data']) !!}

                        @include('users.fields')
                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
