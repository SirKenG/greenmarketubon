@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            คำสั่งซื้อ
        </h1>
    </section>
    <div class="content">
    	@include('flash::message')
        <div class="box box-primary">
            <div class="box-header">
                <h1 class="text-center">คำสั่งซื้อ</h1>
            </div>
            <div class="box-body">
               @if(Session::has('home_meassage'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('home_meassage')}}
                </div>
               @endif

	          <div class="table-responsive" style="margin-top: 20px;">          
		          <table class="table table-hover">
		            <thead>
		              <tr>
		                <th class="col-md-1 text-center">รหัสอ้างอิงการสั่งซื้อ</th>
		                <th class="col-md-2 text-center">ลูกค้า</th>
		                <th class="col-md-2 text-center">หลักฐานการโอนเงิน</th>
		                <th>รูปแบบการจ่ายเงิน </th>
		                <th>สถานที่นัดรับ</th>
		                <th>สั่งซื้อเมื่อ</th>
		                <th class="col-md-1 text-center">ดูสินค้า</th>
		                <th class="col-md-1 text-center">ปรับเปลี่ยนคำสั่งซื้อ</th>
		                <th>จ่ายสินค้า</th>
		              </tr>
		            </thead>
		            <tbody>
		             
		              @foreach($baskets_buying as $basket_buying)
		                <tr>
		                    <td  class="col-md-1 text-center">
		                      {{$basket_buying->basket_key}}
		                      {{-- {{$basket_buying->basket_id}} --}}
		                    </td>
		                    <td class="col-md-2 text-center">
		                    	<a title="ดูข้อมูลลูกค้า" href="{{route('customers.show', [$basket_buying->customer_id])}}">{{$basket_buying->first_name}} {{$basket_buying->last_name}}</a>
		                    </td>
		                    <td class="col-md-2 text-center">

		                      {{-- <img id="expand_image" src="{{asset($basket_buying->photo_path)}}" class="img-thumbnail" style="width: 50%;"> --}}
		                      {{-- @if(strpos($basket_buying->photo_path, '.pdf') !== false) --}}
		                      @if($basket_buying->photo_path)
		                        <a href="{{asset($basket_buying->photo_path)}}" target="_blank">เปิดดู</a>
		                  {{--     @else

		                        <img id="expand_imagea" src="{{asset($basket_buying->photo_path)}}" class="img-thumbnail" style="width: 50%;">
	
		                      @endif --}}
		                      @else
		                     	 <a href="#" >ยังไม่มีการอัพโหลด</a>
		                      @endif
		                    </td>
		                    <td>
		                    	{{$basket_buying->pay}}
		                    </td>
		                    <td class="">
		                    	@if($basket_buying->receive_place!='จัดส่งตามแผนที่')
		                      		{{$basket_buying->receive_place}}
		                      	@else
		                      		<?php
		                      			$d_ = json_decode($basket_buying->lat_long, true);
		                      			// echo "$d_";
		                      		?>
		                      		<a target="_blank" href="http://maps.google.com/?q={{$d_['lat']}},{{$d_['long']}}">คลิ๊กดูแผนที</a>
		                      		<hr>
		                      		<u>ข้อมูลเพิ่มเติม:</u> {{$d_['data_more']}}
		                      	@endif
		                    </td>
		                    <td class="">
		                      {{$basket_buying->basket_updated_at}}
		                    </td>
		                    <td class="col-md-1 text-center">
		                      <a href="{{route('home.seemore_products', $basket_buying->basket_key)}}" class="btn btn-info">เรียกดูสินค้า</a>
		                    </td>
		                    <td class="col-md-1 text-center">
		                    	<a href="{{route('home.update_cart_products', $basket_buying->basket_key)}}" class="btn btn-primary">ปรับเปลี่ยน</a>
		                    </td>
		                    <td>
		                    	 <a href="{{route('home.update_status_basket', $basket_buying->basket_key)}}" class="btn btn-success">จ่ายสินค้า</a>
		                    </td>
		                </tr>


		              @endforeach

		            </tbody>
		          </table>
	          </div>


            </div>
            <div class="box-footer text-right">
                {{ $baskets_buying->links() }}
            </div>
        </div>
    </div>
@endsection
