@extends('layouts.front')



@section('content')

{{--   <div class="productvalue" style="
    background-image:url({{ asset('images/background-productvalue.jpg')}});
    background-repeat: no-repeat;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;">
    <div class="container-fluid">
      <div class="row">
	    <div class="col-md-6">

	    </div>
	    <div class="col-md-6 content">
        <h2 class="text-center">{{$productvalue[0]->title}}</h2>
        <p>{!! $productvalue[0]->body !!}</p>
	    </div>		
      </div>
    </div> <!--/.container-->
  </div> <!--/.productvalue--> --}}


<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">{{$productvalue[0]->title}}</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- content area -->
<div class="content-area recent-property padding-top-40" style="background-color: #FFF;">
    <div class="container">  
        <div class="row">
            <div class="col-md-8 col-md-offset-2"> 
                <div class="" id="contact1">                        
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                              {!! $productvalue[0]->body !!}
                            </p>
                        </div>
                        <!-- /.col-sm-4 -->
                    </div>
                    <!-- /.row -->

                </div>
            </div>    
        </div>
    </div>
</div>
<!-- End content area -->


@stop