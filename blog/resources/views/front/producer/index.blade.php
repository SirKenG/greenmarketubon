@extends('layouts.front')



@section('content')


<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">กลุ่มผลิตอินทรีย์</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- product area -->
<div class="content-area home-area-1 recent-property" style="background-color: #F3F3F3; padding-bottom: 55px;">
    <div class="container">   
        <div class="row">
            <div class="proerty-th">
               {{-- {!! $producers !!} --}}
                @foreach($producers as $producer)
                  <div class="col-md-4 ">
                    <div class="box-two proerty-item">
                      <div class="item-thumb">
                        <a href="{{ route('producer.show', $producer->id ) }}""><img src="{{asset($producer->photo_path)}}"></a>
                      </div>
                      <div class="item-entry overflow text-center">
                        {{$producer->title}}
                      </div>
                    </div>
                  </div>
                @endforeach

            </div>
        </div>




    </div>
</div>



@stop