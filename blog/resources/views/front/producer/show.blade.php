@extends('layouts.front')



@section('content')

<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title text-center">{{$producer->title }}</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- content area -->
        <div class="content-area single-property" style="background-color: #ffffff;margin-top: -50px;">&nbsp;
            <div class="container">   

                <div class="clearfix">

                    <div class="col-md-12 p0">
                        <aside id="height-showproductdetail" class="sidebar sidebar-property blog-asside-right property-style2">
                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header" style="margin-bottom: -20px;">                      
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10 text-center">
                                                <img src="{{asset($producer->photo_path)}}" style="width: 50%" />
                                            </div>
                                            <div class="col-md-1"></div>

               
                                            <div class="row" style="color: #6d6d6d;">
                                                <div class="col-md-12">
                                                    {!! $producer->body !!}
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="disqus_thread"></div>
                                                        <script>

                                                        /**
                                                        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                                        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                                        /*
                                                        var disqus_config = function () {
                                                        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                                        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                                        };
                                                        */
                                                        (function() { // DON'T EDIT BELOW THIS LINE
                                                        var d = document, s = d.createElement('script');
                                                        s.src = 'https://greenmarketubon.disqus.com/embed.js';
                                                        s.setAttribute('data-timestamp', +new Date());
                                                        (d.head || d.body).appendChild(s);
                                                        })();
                                                        </script>
                                                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                            
                                                        <script id="dsq-count-scr" src="//greenmarketubon.disqus.com/count.js" async></script>

                                                    
                                                </div>
                                            </div>




                                        </div>





                                    </div>
                                </div>
                            </div>







                        </aside>
                    </div>











                </div>

{{--                 <div class="clearfix">
                   <div class="col-md-12 p0" id="showdetailproduct-cart">
                        <aside class="sidebar sidebar-property blog-asside-right property-style2" ">

                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header" >                                          
                                            <h1 class="property-title" style="color: #5cb85c;">บล็อกอื่นๆ</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                <div class="panel-body recent-property-widget">
                                    




                                </div>
                            </div>

                        </aside>
                    </div>



                </div> --}}




            </div>
        </div>
<!-- End content area -->

@stop