
<h2>เข้าสู่ระบบ : </h2> 

<form method="POST" action="{{route('member.store')}}">
  @include('adminlte-templates::common.errors')
  @include('flash::message')

  <div class="form-row">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label for="email">Username</label>
        <input type="text" class="form-control" name="username" id="username">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" id="password">
    </div>

    <div class="form-group col-sm-6 text-left" style="padding-top: 6px;">
      <br>
      <button type="submit" class="btn btn-success">เข้าสู่ระบบ</button>
      <a href="{{route('member.forgotpassword')}}">ลืมรหัสผ่าน</a>
    </div>
  </div>
</form>

 