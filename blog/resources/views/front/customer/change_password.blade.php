@extends('layouts.front')



@section('content')


<!-- register-area -->
<div class="register-area" style="background-color: rgb(249, 249, 249);">
    <div class="container">

        <div class="col-md-12">
            <div class="box-for overflow">
                <div class="col-md-12 col-xs-12 register-blocks">
                  
                  <h2>เปลี่ยนรหัสผ่าน : </h2> 

                  {!! Form::model($customer, ['route' => ['member.update', $customer->code], 'method' => 'patch']) !!}
                    @include('adminlte-templates::common.errors')
                    @include('flash::message')

                    <div class="form-group">
                    <label for="old_password">รหัสผ่านเดิม</label>
                        <input type="text" class="form-control" name="old_password" id="old_password">
                    </div>
                    <div class="form-group">
                        <label for="new_password">รหัสผ่านใหม่</label>
                        <input type="password" class="form-control" name="new_password" id="new_password">
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">ยืนยันรหัสผ่านใหม่อีกครั้ง</label>
                        <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                    </div>

                    <div class="form-group">
                        {!! Form::submit('เปลี่ยนรหัสผ่าน', ['class' => 'btn btn-success']) !!}
                    </div>

                  {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End register-area -->


@stop
