@extends('layouts.main')

@section('content')
    <section class="content-header">
        <h1>
            Customer
        </h1>
    </section>
    <div class="content">
      @include('adminlte-templates::common.errors')
      <div class="box box-primary">
          <div class="box-body">
                  {!! Form::open(['url' => 'customer/addRegister','id'=>'FormCustomer']) !!}

                      @include('customers.fields')
                      Submit Field 
                      <div class="form-group col-sm-12">
                          {!! Form::button('Save', ['class' => 'btn btn-success','id'=>'btnSave']) !!}
                          <a class="btn btn-primary" href="{{ url('customer/facebook') }}"><i class="fa fa-facebook-square" aria-hidden="true"></i><?php echo __(' sign with facebook') ?></a>
                          <a class="btn btn-danger" href="{{ url('customer/google') }}"><i class="fa fa-google-plus" aria-hidden="true"></i><?php echo __(' sign with google') ?></a>
                          <a href="{{ url('/') }}" class="btn btn-primary pull-right">Cancel</a>
                      </div>
                  {!! Form::close() !!}
          </div>
      </div>
    </div>
@endsection
@section('scripts')
  <script>
      $(function(){
          $('#btnSave').click(function(){
              $('#FormCustomer').attr('url','customers/addRegister').submit();
          });
      });
  </script>
@endsection
{{--       <a class="btn " href="{{ url('customer/facebook') }}" style="background-color: #2862e4;color: #fff;border-color: #2466fb;">
        <i class="fa fa-facebook-square" aria-hidden="true"></i><?php echo (' sign with facebook') ?>
      </a>
      <a class="btn " href="{{ url('customer/google') }}" style="    background-color: #efefef;color: #020202;border-color: #e8e8e8;">
        <i class="fa fa-google-plus" aria-hidden="true"></i><?php echo (' sign with google') ?>
      </a> --}}
