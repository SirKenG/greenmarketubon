@extends('layouts.front')



@section('content')


<!-- register-area -->
<div class="register-area" style="background-color: rgb(249, 249, 249);">
    <div class="container">

        <div class="col-md-12">
            <div class="box-for overflow">
                <div class="col-md-12 col-xs-12 register-blocks">
                  @if(session('member_login')==true)
                    <h2>กำลังอยู่ในระบบ... </h2> 
                    @include('flash::message')
                  @else
                  @include('front.customer.register_field')
{{--                   
                  <h2>ลงทะเบียนใหม่ : </h2> 
                  <form method="POST" action="{{route('customer/addRegister')}}" id="ssss">
                    @include('adminlte-templates::common.errors')
                    @include('flash::message')

                    <div class="form-row">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      @include('front.customer.fields')

                      <div class="form-group col-sm-6 text-left" style="padding-top: 6px;">
                        <br>
                        <button type="submit" class="btn btn-success">บันทึก</button>

                      </div>
                    </div>
                  </form> --}}
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End register-area -->


@stop
