@extends('layouts.front')



@section('content')


<!-- register-area -->
<div class="register-area" style="background-color: rgb(249, 249, 249);">
    <div class="container">

        <div class="col-md-12">
            <div class="box-for overflow">
                <div class="col-md-12 col-xs-12 register-blocks">
                  
                  <h2>ลืมรหัสผ่าน : </h2> 

                  {{-- {!! Form::model(['route' => ['member.sentemailforgotpassword'], 'method' => 'get']) !!} --}}
                <form method="POST" action="{{route('member.sentemailforgotpassword')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                    <label for="email">ใส่อีเมล์ เพื่อให้ระบบส่งรหัสผ่านใหม่เข้าอีเมล์ของคุณ</label>
                        <input type="text" class="form-control" name="email">
                    </div>

                    
                    @include('adminlte-templates::common.errors')
                    @include('flash::message')

                    <div class="form-group">
                        {!! Form::submit('รีเซ็ตรหัสผ่าน', ['class' => 'btn btn-success']) !!}
                    </div>
                </form>
                  {{-- {!! Form::close() !!} --}}

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End register-area -->


@stop
