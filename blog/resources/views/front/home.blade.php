@extends('layouts.front')



@section('content')


<!-- slider area -->
{{-- <div class="slider-area">
    <div class="slider">
        <div id="bg-slider" class="owl-carousel owl-theme">

            <div class="item"><img src="{{asset($fronthomes[1]->photo_path)}}" alt=""></div>
            <div class="item"><img src="{{asset($fronthomes[2]->photo_path)}}" alt=""></div>
            <div class="item"><img src="{{asset($fronthomes[3]->photo_path)}}" alt=""></div>

        </div>
    </div>
    <div class="slider-content">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <h2>{{$fronthomes[0]->message}}</h2>
                <p></p>
            </div>
        </div>
    </div>
</div> --}}
  <div id="myCarousel" class="carousel slide" data-ride="carousel"">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="{{asset($fronthomes[1]->photo_path)}}" alt="">
        <div class="carousel-caption">
          {{-- <h1>{{$fronthomes[0]->message}}</h1> --}}
          {{-- <p>LA is always so much fun!</p> --}}
        </div>
      </div>

      <div class="item">
        <img src="{{asset($fronthomes[2]->photo_path)}}" alt="" >
        <div class="carousel-caption">
          {{-- <h1>{{$fronthomes[0]->message}}</h1> --}}
          {{-- <p>Thank you, Chicago!</p> --}}
        </div>
      </div>
    
      <div class="item">
        <img src="{{asset($fronthomes[3]->photo_path)}}" alt="">
        <div class="carousel-caption">
          {{-- <h1>{{$fronthomes[0]->message}}</h1> --}}
          {{-- <p>We love the Big Apple!</p> --}}
        </div>
      </div>
          <div class="carousel-caption">
          <h1>{{$fronthomes[0]->message}}</h1>
          {{-- <p>LA is always so much fun!</p> --}}
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<!-- End slider area -->


<!-- welcome area -->
<div class="content-area home-area-1 recent-property" style="background-color: #FCFCFC; padding-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center page-title">
                <h2>{!! $fronthomes[4]->message !!}</h2>
                {!! $fronthomes[4]->message2 !!}
            </div>
        </div>
    </div>
</div>
<!-- End welcome area -->

<!-- producers area -->
<div class="content-area home-area-1 recent-property" style="background-color: #F3F3F3; padding-bottom: 0px;">
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center page-title" style="padding-top: 0px;margin-bottom: 20px;">
                <h2>ข้อมูลข่าวสาร</h2>
            </div>
        </div>
        <div class="row">
            <div class="proerty-th">
               {{-- {!! $producers !!} --}}
                @foreach($producers as $producer)
                  <div class="col-md-4 ">
                    <div class="box-two proerty-item">
                      <div class="item-thumb">
                        <a href="{{ route('producer.show', $producer->id ) }}""><img src="{{asset($producer->photo_path)}}"></a>
                      </div>
                      <div class="item-entry overflow text-center">
                        {{$producer->title}}
                      </div>
                    </div>
                  </div>
                @endforeach

            </div>
        </div>


    </div>
</div>
<!-- End producers area -->


{{-- <hr> --}}
<!-- product area -->
<div class="content-area home-area-1 recent-property" style="background-color: #FCFCFC; padding-bottom: 55px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center page-title" style="padding-top: 0px;margin-bottom: 20px;">
                <h2>สินค้ามาใหม่</h2>
            </div>
        </div>        



        <div class="row">
            <div class="proerty-th">

                @foreach($products as $product)
                    <div class="col-md-4 ">
                        <div class="box-two proerty-item">
                            <div class="item-thumb">
                                <a href="{{ route('basket.show', $product->id ) }}" ><img src="{{asset($product->photo_path)}}"></a>
                            </div>
                            <div class="item-entry overflow">
                                <h2 style="margin-top: 0px;margin-bottom: 0.5em;">
                                    <a href="{{ route('basket.show', $product->id ) }}">{{str_limit($product->name, 15)}}</a>
                                </h2>
                                <div style="width: 40%;float: right;">
                                    <input type="number" id="qty" class="form-control input-xs" placeholder="จำนวน" min="1" value="1" style="    margin-top: -40px;">
                                </div>
                                <div class="dot-hr"></div>
                                {{-- <span class="pull-left"><b>Area :</b> 120m </span> --}}
                                <span class="proerty-price pull-left">{{$product->prime}}฿<br><small>{{str_limit($product->detail, 25)}}</small></span>
                                {{-- Do not change space text in button id="basket" --}}
                                <button class="btn border-btn-green pull-right" id="basket" value="{{$product->id}}"><i></i><span class="sr-only">Loading...</span>หยิบใส่ตะกร้า</button>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-md-12 text-center" style="margin-top: 20px;">
                    <div class="more-entry overflow">
                        {{-- <h5><a href="property-1.html" >CAN'T DECIDE ? </a></h5>
                        <h5 class="tree-sub-ttl">Show all properties</h5> --}}
                        <a class="btn border-btn more-black" href="{{route('basket.index')}}">สินค้าทั้งหมด</a>
                    </div>
                </div>
            </div>
        </div>




    </div>
</div>
<!-- End product area -->




@stop