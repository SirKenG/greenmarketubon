@extends('layouts.front')



@section('content')

<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">สินค้าทั้งหมด</h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- product area -->
<div class="content-area home-area-1 recent-property" style="background-color: #F3F3F3; padding-bottom: 55px;">
    <div class="container">   
@if(!$products)     
        <div class="row">
            <div class="proerty-th">
                @foreach($products as $product)
                    <div class="col-md-4 ">
                        <div class="box-two proerty-item">
                            <div class="item-thumb">
                                <a href="{{ route('basket.show', $product->id ) }}" ><img src="{{asset($product->photo_path)}}"></a>
                            </div>
                            <div class="item-entry overflow">
                                <h2 style="margin-top: 0px;margin-bottom: 0.5em;">
                                    <a href="{{ route('basket.show', $product->id ) }}">{{str_limit($product->name, 15)}}</a>
                                </h2>
                                <div style="width: 40%;float: right;">
                                    <input type="number" id="qty" class="form-control input-xs" placeholder="จำนวน" min="1" value="1" style="    margin-top: -40px;">
                                </div>
                                <div class="dot-hr"></div>
                                {{-- <span class="pull-left"><b>Area :</b> 120m </span> --}}
                                <span class="proerty-price pull-left">{{$product->prime}}฿<br><small>{{str_limit($product->detail, 25)}}</small></span>
                                {{-- Do not change space text in button id="basket" --}}
                                <button class="btn border-btn-green pull-right" id="basket" value="{{$product->id}}"><i></i><span class="sr-only">Loading...</span>หยิบใส่ตะกร้า</button>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="box-footer text-right">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
@endif


    </div>
</div>


@stop