@extends('layouts.front')



@section('content')

<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">สั่งซื้อสินค้า</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

@if(session('member_login')==true)
  

  @if($baskets[0]->status_buy==0)
    <!-- product area -->
    <div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
        <div class="container">   
            <div class="row">
                <div class="col-md-12" style="margin-top: 10px;">
                    @include('flash::message')



                    <h2 class="text-center">{{$basket_acc->title}}</h2>


                   @if(Session::has('checkout_buy_meassage'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
                        {{Session::get('checkout_buy_meassage')}}
                    </div>
                   @endif 

                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    {!! $basket_acc->body !!}
                </div>
            </div>



            <form method="POST"action="{{route('basket.checkout_buy')}}" id="frmSubmitCheckout" > <!-- -->
              {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="margin-left: 28px;">เลือกรูปแบบการชำระเงิน</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                      <div class="radio">
                        <label><input type="radio" name="optradio_pay" value="โอนเงินเข้า บัญชีธนาคาร"> โอนเงินเข้า บัญชีธนาคาร</label>
                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="radio">
                        <label><input type="radio" name="optradio_pay" value="ชำระเป็นเงินสด เมื่อมารับสินค้า"> ชำระเป็นเงินสด เมื่อมารับสินค้า</label>
                      </div>


                    </div>
                </div>






                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="margin-left: 28px;">เลือกรูปแบบการรับสินค้า</h3>
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-12">
                       @if(Session::has('update_customer_meassage'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('update_customer_meassage')}}
                        </div>
                       @endif
                        
                          

                          <div class="radio" id="poitrecieveproduct_post">
                            <label><input type="radio" name="optradio_poitrecieveproduct" value="จัดส่งไปรษณีย์"> จัดส่งไปรษณีย์</label>
                            <span> ตามที่อยู่ | คลิ๊ก <a style="cursor: pointer;" onclick="openChangeAddress();">ที่นี้</a> เพื่อเปลี่ยนที่อยู่</span>
                          </div>
                          
                          <div class="radio">
                            <label><input type="radio" name="optradio_poitrecieveproduct" value="ตลาดนัดมหาวิทยาลัยอุบลราชธานี - รับได้เฉพาะวันศุกร์"> ตลาดนัดมหาวิทยาลัยอุบลราชธานี - รับได้เฉพาะวันศุกร์</label>
                          </div>
                          <div class="radio">
                            <label><input type="radio" name="optradio_poitrecieveproduct"  value="ตลาดนักสีเขียว(ตึกสุนีย์) - รับได้เฉพาะวันเสาร์"> ตลาดนักสีเขียว(ตึกสุนีย์) - รับได้เฉพาะวันเสาร์</label>
                          </div>
                          <div class="radio">
                            <label><input type="radio" name="optradio_poitrecieveproduct"  value="ตลาดนัดโรงพยาบาลสรรพสิทธิประสงค์"> ตลาดนัดโรงพยาบาลสรรพสิทธิประสงค์</label>
                          </div> 

                          <alert id="checkout"></alert>
                          <button onclick="loadingWaitOrder1(this);" type="submit" class="btn border-btn-green" style="padding: 15px; width: 100%"><i></i>สั่งซื้อ</button>
                        
                   </div>
               </div>       



           </form>



        </div>
    </div>


    <!-- Modal -->
    <div id="myModaluploadfilechechout" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-danger text-center">*โอนเงินเข้า บัญชีธนาคาร จำเป็นต้องมีหลักฐานแนบ</h4>
          </div>
          <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                  <form method="POST" action="{{route('basket.checkout_uploadfile')}}" class="dropzone" id="dropzonecheckout" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="dz-message" data-dz-message><span>วางหรืออัพโหลดหลักฐานการโอนที่นี้</span></div>
                    <div class="fallback">
                      <input name="photo_path" type="file" />
                    </div>
                  </form>

                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="button" onclick="checkout_getfile();" class="btn btn-success" >ตรวจสอบหลักฐานการโอนเงิน</button>
                  <div id="img-path-acc"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" onclick="location.reload();" class="btn btn-warning" >อัพโหลดใหม่</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">เสร็จสิ้น</button>
          </div>
        </div>

      </div>
    </div>








    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            {{-- <h4 class="modal-title">แก้ไขข้อมูลการจัดส่ง</h4> --}}
            <h2 class="text-danger text-center">กรอกข้อมูลด้านล่าง หรือ คลิ๊ก <a style="cursor: pointer;" onclick="openChangeAddressByGoogle();">ที่นี้</a> เพื่อค้นหาตำแหน่งปัจจุบัน</h2>
          </div>
          <div class="modal-body">
            <div class="row">
              {!! Form::model($customer, ['route' => ['basket.updateCustomer', $customer->id], 'method' => 'patch']) !!}

                @include('customers.fields')
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit('บันทึกและจัดส่งตามที่อยู่ข้างต้น', ['class' => 'btn btn-success']) !!}
                </div>

              {!! Form::close() !!}
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


    <style type="text/css">
      #map-geo {
        height: 300px;
        margin: 0;
      }

      #map-geo .centerMarker {
        position: absolute;
        /*url of the marker*/
        background: url({{asset('img/marker.png')}}) no-repeat;
        /*background: url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;*/
        /*center the marker*/
     /*   top: 50%;
        left: 50%;*/
        top: 35%;
        left: 48.3%;
        z-index: 1;
        /*fix offset when needed*/
        margin-left: -10px;
        margin-top: -34px;
        /*size of the image*/
      /*  height: 34px; 
        width: 20px;*/
        height: 75px; 
        width: 50px;
        cursor: pointer;
      }

    </style>


    <div id="myModalByGoogle" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            {{-- <h4 class="modal-title">แก้ไขข้อมูลการจัดส่ง</h4> --}}
          </div>
          <div class="modal-body">
            {!! Form::model($customer, ['route' => ['basket.updateCustomerMap', $customer->id], 'method' => 'patch', 'id' => 'frmSubmitUpdateMap']) !!}

            

            <div class="row">
              <div class="col-md-3 text-center">
                <button type="button" class="btn btn-info" onclick="getCurrentLocation()" style="width:100%;">
                  <i class="fa fa-location-arrow"></i>
                  ค้นหาตำแหน่งปัจจุบัน
                </button>
                <input type="hidden" name="latitude">
                <input type="hidden" name="longitude">
              </div>
              <div class="col-md-1 text-center" style="margin-top: 10px;">
                หรือ
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <select class="form-control" id="sel1"> <!--- onchange="searchBoxGeoLocation()"-->
                    <option value="restaurant">ชื่อร้านอาหาร</option>
                    <option value="grocery_or_supermarket">ชื่อร้านขายของชำ/ชุปเปอร์มาเก็ต</option>
                    <option value="establishment">ชื่อสถานประกอบการ/โรงเรียน</option>
                    <option value="lodging">ชื่อโรงแรม/รีสอร์ท</option>
                    <option value="hospital">ชื่อโรงพยาบาล</option>
                    <option value="point_of_interest">ชื่อจุดสังเกตุ</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4 text-center">
{{--                 <div class="form-group input_autocomplete_place">
                  <input type="text" onfocus="getGeoLocationForFirstTime()"  onkeyup="searchBoxGeoLocation()" id="_placeName" class="form-control" placeholder="พิมพ์ชื่อค้นหาพื้นที่">
                  <ul id="autocomplete_place_list"></ul> <!-onblur="setGeoLocationForOut()"->
                </div> --}}
                <div class="form-group"> <!--onfocus="searchBoxGeoLocation()"-->
                  <select class="form-control select2" id="_placeName" multiple>
                     <option value=""></option>
                   </select>
                </div>
              </div>
            </div>
            {{-- <p></p> --}}
            <div class="row">
                <div class="col-md-12">
                    <alert id="alert"></alert>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 text-center">
                    <textarea class="form-control" name="data_more" placeholder="ใส่ข้อมูลเพิ่มเติมเพื่อให้เราเจอคุณง่ายขึ้น เช่น จุดสังเกตุป้าย อื่นๆ"></textarea>
                </div>
                <div class="col-md-4">
                  <button onclick="loadingWaitOrder2(this);" type="submit" class="btn border-btn-green" style="padding: 10px; width: 100%; margin-top: 4px"><i></i>บันทึกและจัดส่งทางแผนที่</button>
                </div>
                <div class="col-md-12 text-center">
                  <div id="map-geo-explain"></div>
                  <div id="map-geo">กดปุ่ม "ค้นหาตำแหน่งปัจจุบัน" หรือ "พิมพ์ชื่อค้นหา" เพื่อแสดงแผนที่</div>
                </div>
            </div>

            {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
     var mapf = document.getElementById("map-geo");
      var mape = document.getElementById("map-geo-explain");
      mapf.innerHTML = "กดปุ่ม \"ค้นหาตำแหน่งปัจจุบัน\" เพื่อแสดงแผนที่";
      mape.innerHTML = "";
      function getCurrentLocation(){
          // console.log('onclick="getLocation()"');
          // $('div[id="map-geo"]').css({"height": "300px"});
          mapf.innerHTML = "<span class='fa fa-spinner fa-spin fa-2x fa-fw text-warning'></span> <h3 class='text-warning'>กำลังค้นหา กรุณารอสักครู่...</h3>";
          mape.innerHTML = "";


          // Try HTML5 geolocation.
          if (navigator.geolocation) {
            // mapf.innerHTML = "แสดงแผนที่";
            navigator.geolocation.getCurrentPosition(success, error, options);
          } else {
            // $('div[id="map-geo"]').css({"height": ""});
            mapf.innerHTML = "<div class=text-danger>บราวเซอร์ไม่สนับสนุนระบบค้นหาตำแหน่ง กรุณาเปลี่ยนบราวเซอร์ที่เป็นปัจจุบัน!<br>***ระวัง ถ้าเปลี่ยนบราวเซอร์ คุณจะต้องเลือกสินค้าใหม่***</div>";
          }
      }



      var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };

      function success(pos) {
        console.log(pos);
        var crd = pos.coords;
        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);

        updateLatLng(crd.latitude, crd.longitude);

        //===========v1
        // var map, infoWindow;
        // map = new google.maps.Map(document.getElementById('map-geo'), {
        //       center: {lat: 15.2287, lng: 104.8564}, //ubon ratchathani province
        //       zoom: 16
        //     });
        // infoWindow = new google.maps.InfoWindow;

        // var pos = {
        //       lat: pos.coords.latitude,
        //       lng: pos.coords.longitude
        //     };

        // infoWindow.setPosition(pos);
        // infoWindow.setContent('<img src="'+marker+'" alt="You are here" height="42" width="42">');
        // infoWindow.open(map);
        // map.setCenter(pos);



          //===========v2
          // var myLatLng = {lat: crd.latitude, lng: crd.longitude};

          // var map = new google.maps.Map(document.getElementById('map-geo'), {
          //   zoom: 4,
          //   center: myLatLng
          // });

          // var marker = new google.maps.Marker({
          //   position: myLatLng,
          //   map: map,
          //   title: 'Hello World!'
          // });


          //===========v3
          // var marker;
          // var map = new google.maps.Map(document.getElementById('map-geo'), {
          //   zoom: 16,
          //   center: {lat: crd.latitude, lng: crd.longitude}
          // });

          //https://maps.googleapis.com/maps/api/place/radarsearch/json?location=15.2286861,104.85642170000006&radius=5000&type=hospital&key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs
          // marker = new google.maps.Marker({
          //   map: map,
          //   draggable: true,
          //   animation: google.maps.Animation.DROP,
          //   position: {lat: crd.latitude, lng: crd.longitude},
          //   title: "เราจะจัดส่งให้คุณที่นี้"
          // });
          // marker.addListener('click', toggleBounce);





 
          // google.maps.event.addListener(marker, "position_changed", function() {
          //   var lat = marker.getPosition().lat();
          //   var lng = marker.getPosition().lng();
          //   updateLatLng(lat, lng);
          // });


          //===========v4
          var mapOptions = {
            zoom: 16,
            center: new google.maps.LatLng(crd.latitude, crd.longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          map = new google.maps.Map(document.getElementById('map-geo'),
            mapOptions);
          google.maps.event.addListener(map,'center_changed', function() {
            mape.innerHTML = "<h3 class='text-warning'>กำลังตรววจจับตำแหน่ง กรุณารอสักครู่...</h3>";
            updateLatLng(map.getCenter().lat(), map.getCenter().lng());
            mape.innerHTML = "<h3 class='text-info'>ลากแผนที่ ไปตำแหน่งที่ท่านต้องการให้จัดส่ง</h3>";
          });
          $('<div/>').addClass('centerMarker').appendTo(map.getDiv())
            //do something onclick
            .click(function() {
              var that = $(this);
              if (!that.data('win')) {
                that.data('win', new google.maps.InfoWindow({
                  content: 'เราจะจัดส่ง &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; ให้คุณที่นี้'
                }));
                that.data('win').bindTo('position', map, 'center');
              }
              that.data('win').open(map);
            });


            mape.innerHTML = "<h3 class='text-info'>ลากแผนที่ ไปตำแหน่งที่ท่านต้องการให้จัดส่ง</h3>";
      }; //end of function success()

      function updateLatLng(latitude, longitude){
        $('input[name="latitude"]').val(latitude);
        $('input[name="longitude"]').val(longitude);
      }

      function error(err) {
        // $('div[id="map-geo"]').css({"height": ""});
        console.warn(`ERROR(${err.code}): ${err.message}`);
        mapf.innerHTML = "<div class=text-danger>ผิดพลาด อาจจะเกิดจากด้านความปลอดภัยจากผู้ให้บริการ <br> แนะนำให้ลองใหม่อีกครั้ง<br>ERROR("+err.code+"): "+err.message+"</div>";
      };

      function toggleBounce() {
        if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }
      }


      function getMapForShow(pos){

        console.log(pos);
        var crd = pos.coords;
        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);

        // updateLatLng(crd.latitude, crd.longitude);





      }

      function getLatLong(props){
        // console.log('getLatLong', props.id);

        var jqXHR = getDataFromJSONFile();
        
        if(jqXHR.responseText!=""){
          var geo_json = JSON.parse(jqXHR.responseText);
          console.log('data from json file in getLatLong', geo_json);
          // console.log('count:', geo_json.length);
          var props_latlong = geo_json.filter(checkLatLong.bind(geo_json, props.id, geo_json.length));

          console.log('message', props_latlong);

          var position_Coordinates = {latitude: props_latlong[0].geometry.location.lat, longitude: props_latlong[0].geometry.location.lng, accuracy: ''};
          var position_api = {
            coords: position_Coordinates,
            timestamp: ''
          };

          // var position_big = ['Position', position_api];
          // getMapForShow(position_api);
          success(position_api);
          //--Continue here for get map
          // console.log(position_api.coords);
        }else{
          mapf.innerHTML = "<div class=text-danger>ผิดพลาด ไม่สามารถนำข้อมูลมาเปรีบเทียบกันได้ <br> แนะนำให้ลองใหม่อีกครั้ง</div>";
        }
      } //end of function

      function checkLatLong(id, props_length, props){
        // var arr_count = $(props);
        // console.log('checkLatLong: '+id, props.id);
        if(id==props.id){
          return props;
        }
      } // end of function

      function getDataFromJSONFile(){
        var urljs = '{{asset('geo/data.json')}}';

        var jqXHR = $.ajax({
            url: urljs,
            // type: type,
            cache: false,
            async: false,
            success: function(data){
                // console.log(data);
                // sessionStorage.setItem("basketKey", data);
                // return data
            },
            error: function(xhr, status, error) {
                console.log('Status: ' + status + ' Error: ' + error + '{getGeoLocationForFirstTimeF}');
                // $('#autocomplete_place_list').html('ผิดพลาด แนะนำให้โหลดหน้า...');

            }
        }); // ) $.ajax

        return jqXHR
      }

      function checkType(type){
        var type_option = $('select[id="sel1"] option:selected').val();
        var var_type = type.types;
        // console.log('checkType:', var_type);
        var i;
        for (i = 0; i < var_type.length; i++) {
          // console.log('for: '+i,var_type[i]);
          // console.log('type_option:',type_option);
          if(var_type[i]==type_option){
            return type;
          }
        }
      } //end of function

      //get data all of type from google api to to client 
      function getGeoLocationForFirstTime(){
        //1. validate exist data
        //   if exist that get 20 first data to show
        //   if don't exist that get data from google api then bring insert to /public/geo/data.json by same format from google api
        // $('#autocomplete_place_list').show();
        // $('#autocomplete_place_list').html('กำลังโหลด กรุณารอสักครู่...');

        // console.log('fdfdsfdsds');
        var jqXHR = getDataFromJSONFile();
        // console.log('jqXHR:', jqXHR.responseText);
        if(jqXHR.responseText!=""){
          //if exist that get 20 first data to show of type 
          //  if has type that show first 20 data
          //  if don't have that get new data from google map api
          var type = $('select[id="sel1"] option:selected').val();
          var geo_json = JSON.parse(jqXHR.responseText);
          console.log('all data from json file:' + type, geo_json);
          var geo_forShow = geo_json.filter(checkType);
          
          if(geo_forShow.length!=0){
            // console.log('have data in json file - get from json file:', geo_forShow);
            // console.log('geo_forShow:', geo_forShow);

            getDataAutocomplete(geo_forShow);

          }else{
            console.log('dont\' have in json file');
            getDataGoogleMapAPI();
          }
          // console.log('message');
        }else{
          //if don't exist
          getDataGoogleMapAPI();
        }
      } //end of function

      function getDataGoogleMapAPI(){
          var type = $('select[id="sel1"] option:selected').val();
          // console.log('type:', type);
           $.ajax({
            url: '/basket/get_data_googlemap_api',
            type: 'get',
            data:{type: type},
            cache: false,
            async: false,
            success: function(data){
              console.log('success: - get data from google api:', data);
              getGeoLocationForFirstTime();
              // addGeoDataToJson(data);
            },
            error: function(xhr, status, error) {
                console.log('Status: ' + status + ' Error: ' + error + '{getDataGoogleMapAPI}');
                // $('#autocomplete_place_list').show();
                // $('#autocomplete_place_list').html('<b class="text-danger">ผิดพลาด แนะนำให้โหลดหน้า...</b>');
            }
        }); // ) $.ajax        
      } //end of functoin


      function getDataAutocomplete(items){
        console.log('getDataAutocomplete', items);

        $('select[id="_placeName"]')
          .find('option')
          .remove()
          .end();

        $.each(items, function (i, item) {
            $('select[id="_placeName"]').append($('<option>', { 
                value: item.id,
                text : item.name 
            }));
        });
      } //end of function

      // function set_item_place(){
      //   console.log('set_item_place');
      //   $('#autocomplete_place_list').hide();
      //   $('#autocomplete_place_list').html('');
      // }


      function  searchBoxGeoLocation(keyword){
        //1. validate that /public/geo/data.json 
        //   if exist that get and show (select type and name from json)
        //   if don't exist that get data from google api then bring insert to /public/geo/data.json by same format from google api
        // mapf.innerHTML = "<span class='fa fa-spinner fa-spin fa-2x fa-fw text-warning'></span> <h3 class='text-warning'>กำลังค้นหา กรุณารอสักครู่...</h3>";
        // mape.innerHTML = "";

        // console.log('Value: ', elm.value);

        // var keyword = $('input[id="_placeName"]').val();
        var type = $('select[id="sel1"] option:selected').val();
        console.log('get type: ',type);

        // var min_length = 0; // min caracters to display the autocomplete

        // console.log('place name:', keyword);
        // console.log('type:', type);

        // if (keyword.length > min_length) {
        // console.log('get value: ', keyword);
        var map;
        var service;
        var infowindow;

        // var ubon = new google.maps.LatLng(15.2286861,104.85642170000006);

        // // map = new google.maps.Map(document.getElementById('map-geo'), {
        // //     center: ubon,
        // //     zoom: 16
        // //   });
        

        // //   var request = {
        // //     location: ubon,
        // //     radius: '5000',
        // //     type: [type], //[type, 'establishment']
        // //   };
        // //   service = new google.maps.places.PlacesService(map);
        // //   service.nearbySearch(request, callback);
          $.ajax({
            url: '/basket/googlemap_api',
            type: 'get',
            data:{type: type, keyword: keyword},
            cache: false,
            async: false,
            success: function(data){
              // autoCompletePlace(data, keyword);
              console.log('data: ', data);
              getGeoLocationForFirstTime();
              // $('#autocomplete_place_list').show();
              // $('#autocomplete_place_list').html(data[1]);
            },
            error: function(xhr, status, error) {
                mapf.innerHTML = "<div class=text-danger>ผิดพลาด อาจจะเกิดจากด้านความปลอดภัยจากผู้ให้บริการ <br> แนะนำให้ลองใหม่อีกครั้ง<br>ERROR"+error+"</div>";
            }
        }); // ) $.ajax  
        // } else {
        //   // $('#autocomplete_place_list').hide();
        // }

          // console.log(service);
        // console.log(request);
        // var places = searchBox.getPlaces();
        
      } //end of function searchBoxGeoLocation(elm)

      // function callback(results, status) {
      //   if (status == google.maps.places.PlacesServiceStatus.OK) {

      //     console.log(results);
      //     // for (var i = 0; i < results.length; i++) {
      //     //   var place = results[i];
      //     //   // createMarker(results[i]);
      //     //   console.log('place: '+ i +'', place);
      //     // }
      //   }
      // }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs&libraries=places">
    </script>
  @else

<div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
    <div class="container">   
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px;">
                {{-- @include('flash::message') --}}
                <div class="alert alert-success" role="alert">
                    <h2 class="text-center">สั่งซื้อสินค้าเรียบร้อย</h2>
                    <p class="text-center">โปรดรับสินค้าในวันเวลาที่กำหนด - ระบบได้ส่งยืนยันคำสั่งซื้อไปยังอีเมล์ของคุณแล้ว, คุณสามารถดูสถานะการสั่งซื้อได้ที่ปุ่มเมนูสถานะการสั่งซื้อสินค้า</p>
                </div>
                
            </div>
        </div>



    </div>
</div>

{{-- <script type="text/javascript">
  // removeSessionStorageBasketKey();
  localStorage.removeItem("basketKey");
</script> --}}

  @endif
  





@else

<script type="text/javascript">
    window.location = "{{route('basket.checkou')}}"; //here double curly basket
</script>

@endif

@stop