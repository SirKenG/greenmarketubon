@extends('layouts.front')



@section('content')

{{--   <div class="basket" style="

    background-color: #e6e6e6;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;">

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 basket-topic-order">
            <div class="basket-topic-order-text text-right">
              <label>ตะกร้า 0 ชิ้น ราคารวม ฿ 0</label>
              <br>
              <label>คลิ๊ก <a href="#">ที่นี้</a> เพื่อสั่งสินค้า</label>
          </div>
        </div>  
      </div>
    </div>


    <div class="container-fluid">
      <div class="row">    

          @foreach($baskets as $basket)

            <div class="col-md-3">

                  <div class="card card-basket">
                    <div class="card-body">
                      <h4 class="card-title">Card title</h4>
                      <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      <a href="#" class="card-link">Card link</a>
                      <a href="#" class="card-link">Another link</a>
                    </div>
                  </div>

            </div>
          @endforeach

      </div>
    </div> <!--/.container-->
  </div> <!--/.contactus--> --}}


<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">{{$productstype->name}}</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- product area -->
<div class="content-area home-area-1 recent-property" style="background-color: #F3F3F3; padding-bottom: 55px;">
    <div class="container">   
        <div class="row">
            <div class="proerty-th">

                @foreach($products as $product)
                    <div class="col-md-4 ">
                        <div class="box-two proerty-item">
                            <div class="item-thumb">
                                <a href="{{ route('basket.show', $product->id ) }}" ><img src="{{asset($product->photo_path)}}"></a>
                            </div>
                            <div class="item-entry overflow">
                                <h2 style="margin-top: 0px;margin-bottom: 0.5em;">
                                    <a href="{{ route('basket.show', $product->id ) }}">{{str_limit($product->name, 15)}}</a>
                                </h2>
                                <div style="width: 40%;float: right;">
                                    <input type="number" id="qty" class="form-control input-xs" placeholder="จำนวน" min="1" value="1" style="    margin-top: -40px;">
                                </div>
                                <div class="dot-hr"></div>
                                {{-- <span class="pull-left"><b>Area :</b> 120m </span> --}}
                                <span class="proerty-price pull-left">{{$product->prime}}฿<br><small>{{str_limit($product->detail, 25)}}</small></span>
                                {{-- Do not change space text in button id="basket" --}}
                                <button class="btn border-btn-green pull-right" id="basket" value="{{$product->id}}"><i></i><span class="sr-only">Loading...</span>หยิบใส่ตะกร้า</button>
                            </div>
                        </div>
                    </div>
                @endforeach
                
            </div>
        </div>




    </div>
</div>



@stop