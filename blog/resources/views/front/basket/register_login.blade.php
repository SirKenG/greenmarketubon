@extends('layouts.front')



@section('content')

<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">สั่งซื้อสินค้า</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- product area -->
@if(session('member_login')==true)
    {{-- {{route('basket.checkout')}} --}}

<script type="text/javascript">
    window.location = "{{route('basket.checkout')}}?basketKey=" + sessionStorage.getItem("basketKey") + "&status_login=" + session('member_login');//here double curly bracket


    // window.location = "{{route('basket.checkout')}}?basketKey=" + sessionStorage.getItem("basketKey");
</script>
{{-- <div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
    <div class="container">   
        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>




    </div>
</div>
 --}}

@else


{{-- <div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
    <div class="container">   
        <div class="row">
          <div class="alert alert-danger" style="margin-top: 10px;">
            <strong>ยังไม่ได้เข้าระบบ!</strong> ถ้าคุณเป็นลูกค้าเก่า กรุณาเข้าระบบด้วยอีเมล์และเบอร์โทรศัพท์ หรือเป็นลูกค้าใหม่กรุณากรอกข้อมูลก่อนสั่งซื้อ.
          </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include('front.customer.login_field') 
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                @include('front.customer.register_field')
            </div>
        </div>


    </div>
</div> --}}

<div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
    <div class="container">   
        <div class="row">
            <div class="col-md-12">
              <div class="alert alert-success" style="margin-top: 10px;">
                <strong>กรอกข้อมูลเพียงเล็กน้อย เพื่อสั่งสินค้า!</strong> คุณสามารถกรอกข้อมูลตามด้านล่างเพื่อสั่งสินค้าได้เลย หรือ คลิก <a href="{{route('customer.register')}}">ที่นี้</a> เพื่อสมัครสมาชิก
              </div>

            @if(Session::has('addlitleregister_message'))
                <div class="alert alert-danger" role="alert">
                    {{Session::get('addlitleregister_message')}}
                </div>
           @endif 
            </div>
        </div>
        

{{--         <div class="row">
            <div class="col-md-12">
                @include('front.customer.login_field') 
            </div>
        </div> --}}

        {{-- <hr> --}}
        <div class="row">
            {{-- <div class="col-md-12"> --}}
                {{-- @include('front.customer.register_field') --}}
                <form method="POST" action="{{route('customer.addLitleRegister')}}">
                  <div class="form-row">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!-- First Name Field -->
                        <div class="form-group col-md-4">
                            {!! Form::label('first_name', 'ชื่อ :') !!} <span class="text-danger">*</span>
                              {!! Form::text('first_name', null,['class' => 'form-control']) !!}
                        </div>

                        <!-- Tel Field -->
                        <div class="form-group col-sm-3">
                            {!! Form::label('tel', 'เบอร์โทรศัพท์ที่สะดวกในการติดต่อ :') !!} <span class="text-danger">*</span>
                            {!! Form::text('tel', null, ['class' => 'form-control']) !!}
                        </div>

                    <!-- Email Field -->
                    <div class="form-group col-sm-3">
                        {!! Form::label('email', 'Email :') !!}
                        @if(session('signData.email'))
                          {!! Form::text('email', session('signData.email') , ['class' => 'form-control']) !!}
                        @else()
                          {!! Form::text('email', null ,['class' => 'form-control']) !!}
                        @endif()
                    </div>

                    <div class="form-group col-sm-2 text-left" style="padding-top: 34px;">
                      <button type="submit" class="btn btn-success">สั่งซื้อสินค้า</button>
                    </div>

                  </div>
                </form>
            {{-- </div> --}}
        </div>


    </div>
</div>

@endif

@stop