@extends('layouts.front')



@section('content')

<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Cart</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- product area -->
<div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
    <div class="container">   
        <div class="row">
          <div class="table-responsive" style="margin-top: 20px;">          
          <table class="table table-hover">
            <thead>
              <tr>
                <th class="col-md-1 text-center">ลบ</th>
                <th>รูปภาพ</th>
                <th>ชื่อสินค้า</th>
                <th>ราคา</th>
                <th>จำนวน</th>
                <th>ราคารวม</th>
              </tr>
            </thead>
            <tbody>

              <?php $sum_total_price = 0;?>
              @foreach($baskets as $basket)
                <tr>
                  <td class="col-md-1 text-center">
                    <a id="remove-cart" style="cursor: pointer; color: red;" title="นำ {{$basket->name}} ออกจากตะกร้าสินค้า" onclick="removeProductFromBasket({{$basket->id}}, 1);">
                      <i id="remove-cart-symbol" class="fa fa-minus-square-o fa-2x" style="margin-top: 15px;" aria-hidden="true"></i>
                    </a>
                  </td>
                  <td class="col-md-1">
                    <a style="cursor: pointer;" title="ดูรายละเอียดของ {{$basket->name}}" onclick="redirectToProductdetail({{$basket->id}});"><img src="{{asset($basket->photo_path)}}" style="width: 100%;"></a>  
                  </td>
                  <td style="color: #5cb85c;">
                    <a style="cursor: pointer;" title="ดูรายละเอียดของ {{$basket->name}}" onclick="redirectToProductdetail({{$basket->id}});">
                      {{$basket->name}}
                    </a>
                  </td>
                  <td>
                    <span id="cart-price">{{$basket->prime}}</span>฿
                  </td>
                  <td class="col-md-2">
                    <input type="hidden" id="hidden-product-id" value="{{$basket->id}}">
                    <input type="number" id="cart-qty" class="form-control" placeholder="จำนวน" min="1" value="{{$basket->qty}}">
                  </td>
                  <td>
                    <label id="cart-total-qty">
                      <?php 
                        $total_price = 0;
                        $total_price = $basket->qty * $basket->prime; 

                        $sum_total_price += $total_price;
                      ?>
                      {{number_format($total_price,2)}}
                    </label>฿
                  </td>
                </tr>
              @endforeach

            </tbody>
          </table>
          </div>
        </div>

       {{--  <br> --}}
        <div class="row" style="margin-top: -20px">
            <div class="col-md-4">
                
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <a onclick="updateCart(this);" class="btn border-btn-green" style="padding: 15px; width: 100%">ปรับปรุงตะกร้าสินค้า</a>
                <div style="margin-bottom: 10px;"></div>
                {{-- (กรณีที่เปลี่ยนจำนวน แล้วราคารวมไม่เปลี่ยนแปลง ให้กดอัพเดท) --}}
                <div class="thumbnail">
                  <h4>ราคารวมทั้งหมด:  <span id="sum-total-price">{{number_format($sum_total_price,2)}}</span>฿</h4>
                </div>
                <a onclick="checkout({{session('member_login')}});" class="btn border-btn-checkout pull-right" style="padding: 15px; width: 100%">สั่งซื้อสินค้า</a>
            </div>    
        </div>


        </div>
    </div>
</div>



@stop