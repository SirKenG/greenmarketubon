@extends('layouts.front')



@section('content')
@if(session('member_login')==true)
<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">สถานะการสั่งซื้อสินค้า</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- product area -->
<div class="content-area home-area-1 recent-property" style="background-color: #ffffff; padding-bottom: 50px;">
    <div class="container">   
        <div class="row">
          <h2>สินค้าที่กำลังสั่งซื้อ</h2>
          <div class="table-responsive" style="margin-top: 20px;">          
          <table class="table table-hover">
            <thead>
              <tr>
                <th class="col-md-2 text-center">รหัสอ้างอิงการสั่งซื้อ</th>
                <th class="col-md-2 text-center">หลักฐานการโอนเงิน</th>
                <th>รูปแบบการจ่ายเงิน </th>
                <th>สถานที่นัดรับ</th>
                <th>สั่งซื้อเมื่อ</th>
                <th class="col-md-1 text-center">ดูสินค้า</th>
              </tr>
            </thead>
            <tbody>
             
              @foreach($baskets_buying as $basket_buying)
                <tr>
                    <td  class="col-md-2 text-center">
                      {{$basket_buying->basket_key}}
                    </td>
                    <td class="col-md-2 text-center">
{{--                       @if(strpos($basket_buying->photo_path, '.pdf') !== false)
                        <a href="{{asset($basket_buying->photo_path)}}" target="_blank">เปิดดู</a>
                      @else
                        <img id="expand_image" src="{{asset($basket_buying->photo_path)}}" class="img-thumbnail" style="width: 50%;">
                      @endif --}}

                        @if($basket_buying->photo_path)
                            <a href="{{asset($basket_buying->photo_path)}}" target="_blank">เปิดดู</a>
                          @else
                           {{-- <a href="#" >ยังไม่มีการอัพโหลด</a> --}}


               <form method="POST" action="{{route('basket.checkout_uploadfile_again')}}" class="dropzone" id="dropzonecheckout" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="basketkey" value="{{$basket_buying->basket_key}}">
                <div class="dz-message" data-dz-message><span>วางหรืออัพโหลดหลักฐานการโอนที่นี้</span></div>
                <div class="fallback">
                  <input name="photo_path" type="file" />
                </div>
              </form> 
              

                          @endif
                    </td>
                    <td>
                      {{$basket_buying->pay}}
                    </td>
                    <td class="">
                      {{$basket_buying->receive_place}}
                    </td>
                    <td class="">
                      {{$basket_buying->updated_at}}
                    </td>
                    <td class="col-md-1 text-center">
                      <a href="{{route('basket.seemore_products', $basket_buying->basket_key)}}" class="btn btn-info">เรียกดูสินค้า</a>
                    </td>
                </tr>


              @endforeach

            </tbody>
          </table>
          </div>
        </div>




        <div class="row">
          <h2>สินค้าที่เคยสั่งซื้อ</h2>

          <div class="table-responsive" style="margin-top: 20px;">          
          <table class="table table-hover">
            <thead>
              <tr>
                <th class="col-md-2 text-center">รหัสอ้างอิงการสั่งซื้อ</th>
                <th class="col-md-2 text-center">หลักฐานการโอนเงิน</th>
                <th>รูปแบบการจ่ายเงิน </th>
                <th>สถานที่นัดรับ</th>
                <th>สั่งซื้อเมื่อ</th>
                <th class="col-md-1 text-center">ดูสินค้า</th>
              </tr>
            </thead>
            <tbody>

              @foreach($baskets_buy as $basket_buy)
                <tr>
                    <td  class="col-md-1 text-center">
                      {{$basket_buy->basket_key}}
                    </td>
                    <td class="col-md-2 text-center">
{{--                       @if(strpos($basket_buying->photo_path, '.pdf') !== false)
                        <a href="{{asset($basket_buy->photo_path)}}" target="_blank">เปิดดู</a>
                      @else
                        <img id="expand_image" src="{{asset($basket_buy->photo_path)}}" class="img-thumbnail" style="width: 50%;">
                      @endif --}}

                        @if($basket_buy->photo_path)
                            <a href="{{asset($basket_buy->photo_path)}}" target="_blank">เปิดดู</a>
                          @else
                           <a href="#" >ไม่มีการอัพโหลด</a>
                          @endif


                    </td>
                    <td>
                      {{$basket_buy->pay}}
                    </td>
                    <td class="">
                      {{$basket_buy->receive_place}}
                    </td>
                    <td class="">
                      {{$basket_buy->updated_at}}
                    </td>
                    <td class="col-md-1 text-center">
                      <a href="{{route('basket.seemore_products', $basket_buy->basket_key)}}" class="btn btn-info">เรียกดูสินค้า</a>
                    </td>
                </tr>


              @endforeach

            </tbody>
          </table>
          </div>
        </div>




        </div>
    </div>
</div>




@else

<script type="text/javascript">
    window.location = "{{route('frontHome.index')}}";//here double curly bracket
</script>

@endif





@stop