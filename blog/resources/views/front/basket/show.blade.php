@extends('layouts.front')



@section('content')

{{--   <div class="payment" style="
    background-image:url({{ asset('images/background-payment.jpg')}});
    background-repeat: no-repeat;
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;">
    <div class="container-fluid">
      <div class="row">
      <div class="col-md-6 ">

      </div>
      <div class="col-md-6 content">
        <h2 class="text-center">{{$payment[0]->title}}</h2>
        <p>{!! $payment[0]->body !!}</p>
      </div>    
      </div>
    </div> <!--/.container-->
  </div> <!--/.payment--> --}}

<!-- page header -->
<div class="page-head"> 
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">{{$product->name }}</h1>               
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- content area -->
        <div class="content-area single-property" style="background-color: #ffffff;">&nbsp;
            <div class="container">   

                <div class="clearfix">

{{--                     <div class="col-md-4 single-property-content prp-style-2" style="height: 400px;">
                        <div class="row">
                            <div class="light-slide-item" style="margin-top: -15px;">            
                                <div class="clearfix">

                                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                        <li > 
                                            <img src="{{asset($product->photo_path)}}" />
                                        </li>                                    
                                    </ul>

                                </div>
                            </div>
                        </div>

                    </div> --}}

                    <div class="col-md-8 p0">
                        <aside id="height-showproductdetail" class="sidebar sidebar-property blog-asside-right property-style2">
                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header" style="margin-bottom: -20px;">                      

                                            <div class="col-md-6">
                                                <img src="{{asset($product->photo_path)}}" />
                                            </div>
                                            <div class="col-md-6">
                                                <h1 class="property-title" style="margin-bottom: 25px;">{{$product->name}}</h1>
                                                <span class="property-price">{{$product->prime}}฿</span>  

                                                <div class="property-info-label" style="color: #797979;font-size: 20px;">
                                                    {{$product->detail}}
                                                </div> 


                                                <div>
                                                    <div style="width: 50%;float: left;">
                                                      <input type="number" id="qty" class="form-control" placeholder="จำนวน" min="1" value="1">
                                                    </div>
                                                    <div style="width: 49%;float:right;margin-top: -11px;margin-left: 2px;">
                                                    {{-- Do not change space text in button id="basket" --}}
                                                      <button class="btn border-btn-green" id="basket" value="{{$product->id}}" style="padding: 11px 25px;"><i></i><span class="sr-only">Loading...</span>หยิบใส่ตะกร้า</button>

                                                    </div>
                                                </div>
                                       

                                              <p class="property-info-label" style="color: #797979;font-size: 16px;margin-top: 100px;border-top: 1px solid #797979;padding-top: 15px;">
                                                ประเภทสินค้า: <a href="{{route('baskettype.showbaseontype', $product->produce_type_id)}}">{{$product->producttype_name}}</a>
                                              </p>


                                            </div>        

                                            <div class="row">
                                                <div class="col-md-12" style="color: #797979; border-top:1px solid #f5f5f5">
                                                    {!! $product->body !!}
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="disqus_thread"></div>
                                                        <script>

                                                        /**
                                                        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                                        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                                        /*
                                                        var disqus_config = function () {
                                                        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                                        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                                        };
                                                        */
                                                        (function() { // DON'T EDIT BELOW THIS LINE
                                                        var d = document, s = d.createElement('script');
                                                        s.src = 'https://greenmarketubon.disqus.com/embed.js';
                                                        s.setAttribute('data-timestamp', +new Date());
                                                        (d.head || d.body).appendChild(s);
                                                        })();
                                                        </script>
                                                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                            
                                                        <script id="dsq-count-scr" src="//greenmarketubon.disqus.com/count.js" async></script>

                                                    
                                                </div>
                                            </div>




                                        </div>





                                    </div>
                                </div>
                            </div>







                        </aside>
                    </div>








                    <div class="col-md-4 p0" id="showdetailproduct-cart"> {{--796 add 358 = 1154||| 570 add 100 = 700--}}
                        <aside class="sidebar sidebar-property blog-asside-right property-style2">

                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header" style="border-bottom: 2px solid #5cb85c;padding-bottom: 0px;">                                          
                                            <h1 class="property-title">CART</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                <div class="panel-body recent-property-widget" style="margin-top: -40px;">

                                    <div id="cartContentBasket"></div>
                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label id="cartConentBasketprice">มูลค่าสินค้า: 0฿</label>
                                        </div>
                                    </div>



                                   @if(Session::has('gotocart_meassage'))
                                    <div class="alert alert-danger" role="alert">
                                        {{Session::get('gotocart_meassage')}}
                                    </div>
                                   @endif
                                   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a onclick="redirectToCart();" class="btn border-btn-checkout pull-right" >ดูตะกร้าสินค้า</a>
                                        </div>
                                    </div>


                                    
                                </div>
                            </div> {{--./panel panel-default sidebar-menu wow fadeInRight animated--}}

                        </aside>
                    </div>


                </div>

                <div class="clearfix">
                   <div class="col-md-12 p0" id="showdetailproduct-cart">
                        <aside class="sidebar sidebar-property blog-asside-right property-style2" ">

                            <div class="dealer-widget">
                                <div class="dealer-content">
                                    <div class="inner-wrapper">
                                        <div class="single-property-header" >                                          
                                            <h1 class="property-title" style="color: #5cb85c;">YOU MAY ALSO LIKE</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel-default sidebar-menu wow fadeInRight animated">
                                <div class="panel-body recent-property-widget">
                                    

                                @foreach($products_randoms as $products_random)
                                    <div class="col-md-4 ">
                                        <div class="box-two proerty-item">
                                            <div class="item-thumb">
                                                <a href="{{ route('basket.show', $products_random->id ) }}" ><img src="{{asset($products_random->photo_path)}}" style="min-height: 265px;max-height: 265px;"></a>
                                            </div>
                                            <div class="item-entry overflow">
                                                <h2 style="margin-top: 5px;margin-bottom: 0.5em;margin-left: 0.4em;"><a href="#" >{{str_limit($products_random->name, 15)}}</a></h2>
                                                <div class="dot-hr"></div>
                                                {{-- <span class="pull-left"><b>Area :</b> 120m </span> --}}
                                                <span class="proerty-price pull-left" style="margin-top: 5px;margin-bottom: 0.5em;margin-left: 0.4em;">{{$products_random->prime}}฿</span>
                                                {{-- Do not change space text in button id="basket" --}}
                                                <button style="margin: 0.4em;" class="btn border-btn-green pull-right" id="basket" value="{{$products_random->id}}"><i></i><span class="sr-only">Loading...</span>หยิบใส่ตะกร้า</button>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach


                                </div>
                            </div>

                        </aside>
                    </div>



                </div>




            </div>
        </div>
<!-- End content area -->



@stop