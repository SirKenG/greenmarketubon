<table class="table table-responsive" id="districts-table">
    <thead>
        <th>Code</th>
        <th>Name</th>
        <th>Name Eng</th>
        <th>Region Id</th>
        <th>Province Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($districts as $district)
        <tr>
            <td>{!! $district->code !!}</td>
            <td>{!! $district->name !!}</td>
            <td>{!! $district->name_eng !!}</td>
            <td>{!! $district->region_id !!}</td>
            <td>{!! $district->province_id !!}</td>
            <td>
                {!! Form::open(['route' => ['districts.destroy', $district->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('districts.show', [$district->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('districts.edit', [$district->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>