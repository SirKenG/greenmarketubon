<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'The first name field is required.' => 'กรุณากรอกชื่อ.',
    'The last name field is required.' => 'กรุณากรอกนามสกุล.',
    'The age field is required.' => 'กรุณากรอกอายุ.',
    'The zipcode field is required.' => 'กรุณากรอกรหัสไปรษณีย์.',
    'The tel field is required.' => 'กรุณากรอกเบอร์โทร.',
    'The email field is required.' => 'กรุณากรอก email.',

];
