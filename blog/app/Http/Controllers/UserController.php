<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        
        

        $sizeimge = 0;
        if($image = $request->file('photo_file')){
            $sizeimge = $request->file('photo_file')->getClientSize();
            if($sizeimge>1500000){
                Flash::error('ภาพที่คุณอัพโหลดมีขนาด '.($sizeimge/1000000).' MB ระบบไม่รองรับ!, แนะนำให้อัพโหลดภาพไม่เกิน 1.5 MB');

                return redirect(route('users.create'));
            }
            $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();

            // $destinationPath = public_path('\images');
            $destinationPath = base_path('../public/images');

            $image->move($destinationPath, $nameFile['imagename']);

            $input['photo_file'] = '/images/'. $nameFile['imagename'];
        }




        $user = $this->userRepository->create($input);


        Flash::success('บันทึกผู้ดูแลระบบสำเร็จ.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('ไม่เจอผู้ดูแลระบบนี้');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('ไม่เจอผู้ดูแลระบบนี้');

            return redirect(route('users.index'));
        }
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        $sizeimge = 0;
        if($image = $request->file('photo_file')){

            $sizeimge = $request->file('photo_file')->getClientSize();
            if($sizeimge>1500000){
                Flash::error('ภาพที่คุณอัพโหลดมีขนาด '.($sizeimge/1000000).' MB ระบบไม่รองรับ!, แนะนำให้อัพโหลดภาพไม่เกิน 1.5 MB');

                return redirect(route('users.edit', $id));
            }
            $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();

            // $destinationPath = public_path('images');
            $destinationPath = base_path('../public/images');

            $image->move($destinationPath, $nameFile['imagename']);

            $data['photo_file'] = '/images/'. $nameFile['imagename'];
        }



        $user = $this->userRepository->update($data, $id);

        Flash::success('แก้ไขผู้ดูแลระบบสำเร็จ.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }

    public function deleteimg($id)
    {
        // return 'id: '.$id;
        $user = $this->userRepository->findWithoutFail($id);

        $data = $user->toArray();
        if (empty($user)) {
            Flash::error('ไม่เจอผู้ดูแลระบบนี้');

            return redirect(route('users.index'));
        }

        if(file_exists('public'.$user->photo_file)){
            @unlink('public'.$user->photo_file);
        }
        $data['photo_file'] = '/images/User.png';
        $user = $this->userRepository->update($data, $id);

        Flash::success('ลบรูปโปรไฟล์เรียบร้อย, แนะนำให้อัพรูปภาพประจำตัวเพื่อบ่งบอกถึงตัวคุณ!.');
        return redirect(route('users.edit', $id));
        // dd($user);

    }
}
