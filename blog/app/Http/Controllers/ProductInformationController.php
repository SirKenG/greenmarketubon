<?php

namespace App\Http\Controllers;

use App\ProductInformation;
use Illuminate\Http\Request;

class ProductInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productinformation = ProductInformation::all();

        return view('front.productinformation.index', compact('productinformation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productinformation = ProductInformation::all();

        return view('productinformation.create', compact('productinformation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductInformation  $productInformation
     * @return \Illuminate\Http\Response
     */
    public function show(ProductInformation $productInformation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductInformation  $productInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductInformation $productInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductInformation  $productInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productinformation = ProductInformation::findOrFail($id);
        $productinformation->update($request->all());

        $request->session()->flash('productinformation_update_meassage', 'แก้ไขข้อมูลสำเสร็จ');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductInformation  $productInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductInformation $productInformation)
    {
        //
    }
}
