<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Produce;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'It work!!!';

        // $baskets_buying = DB::table('basket')->where([
        //             ['reservation', '=', 1],
        //             ['status_buy', '=', 1]
        //         ])->get();

        $baskets_buying = DB::table('basket')
                ->join('customers', 'basket.customer_code', '=', 'customers.code')
                ->select('basket.*', 'basket.id as basket_id', 'basket.updated_at as basket_updated_at','customers.*', 'customers.updated_at as customer_updated_at', 'customers.id as customer_id')
                ->where([
                    ['reservation', '=', 1],
                    ['status_buy', '=', 1]
                ])->orderby('basket.updated_at', 'desc')->paginate(5);


        // $baskets_buy = DB::table('basket')
        //         ->join('customers', 'basket.customer_code', '=', 'customers.code')
        //         ->select('basket.*', 'customers.*', 'customers.id as customer_id')
        //         ->where([
        //             ['reservation', '=', 1],
        //             ['status_buy', '=', 2]
        //         ])->get();
        // dd($baskets_buying);

        $this->checksamevaluefordelete($baskets_buying->toArray());
        // dd($samevalue);
        return view('home', compact('baskets_buying'));
        // $vvv = json_decode($baskets_buying->items, true);
        // dd($baskets_buying) ;
        // return view('home')->with('baskets_buying', json_decode($baskets_buying, true));
    }
    private function checksamevaluefordelete($array = array()){
        $aa = count($array['data']);

        $arrays = array();
        for ($i = 0; $i < $aa; $i++) {
            // echo $array['data'][$i]->basket_key.'<br>';
            // $arrays[] =  (array) $object;
            $arrays[] = $array['data'][$i]->basket_key;
        }

        $vals = array_count_values($arrays);
        // echo 'No. of NON Duplicate Items: '.count($vals).'<br><br>';
        // print_r($vals);
        // echo "<br>";

        // print_r($arrays);

        foreach ($vals as $key => $value) {
            # code...
            if($value>=2){
                // echo "<br>";
                // echo "no delete: ".$key;
                // echo "<br>";
                $basket = DB::table('basket')->where('basket_key', $key)->first();
                // print_r($basket->id);
                DB::table('basket')->where('id', '=', $basket->id)->delete();
            }
            
        }

        // dd($aa);
    }


    public function history()
    {
        $baskets_buying = DB::table('basket')
                ->join('customers', 'basket.customer_code', '=', 'customers.code')
                ->select('basket.*', 'basket.id as basket_id', 'basket.updated_at as updated_at_basket', 'customers.*', 'customers.updated_at as updated_at_customer', 'customers.id as customer_id')
                ->where([
                    ['reservation', '=', 1],
                    ['pay', '!=', NULL],
                    ['status_buy', '=', 2],
                ])->orderby('basket.basket_key', 'desc')->paginate(5);

                // dd($baskets_buyings);
        // $baskets_buying = $baskets_buyings->groupBy('basket_key');
                // dd($baskets_buying);



        return view('history', compact('baskets_buying'));
    }




    public function seemore_products($id)
    {
    //     $products = DB::table('basket')
    //             ->join('produces', 'basket.product_id', '=', 'produces.id')
    //             ->select('basket.*', 'basket.photo_path as image_money', 'produces.*', 'produces.photo_path as image_product')
    //             ->where([
    //                 ['basket_key', '=', $id]
    //             ])->get();

        $products = DB::table('basket')
                ->join('produces', 'basket.product_id', '=', 'produces.id')
                ->select('basket.*', 'basket.photo_path as image_money', 'produces.*', 'produces.photo_path as image_product')
                ->where([
                    ['basket_key', '=', $id]
                ])->get();



        // dd($products); //seemore_products
        return view('seemore_products', compact('products'));
    }
    public function update_cart_products($id)
    {
        // return $id;
        $products = DB::table('basket')
                ->join('produces', 'basket.product_id', '=', 'produces.id')
                ->select('basket.*', 'basket.photo_path as image_money', 'produces.*', 'produces.photo_path as image_product')
                ->where([
                    ['basket_key', '=', $id]
                ])->get();


        $Produce = Produce::pluck('name','id')->all();


        return view('update_cart_products', compact('products', 'Produce'));
    }


    public function update_status_basket($id)
    {
        // return $id;

        $basket = DB::table('basket')
                    ->where('basket_key', $id)
                    ->update(['status_buy' => 2]);


        session()->flash('home_meassage', 'จ่ายสินค้าสำเร็จ คุณสามารถดูประวัติคำสั่งซื้อย้อนหลังได้ที่ เมนู ประวัติคำสั่งซื้อ');
        return redirect()->back(); 

    }


    public function checkout_buy()
    {

    }
}
