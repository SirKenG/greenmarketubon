<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProduceTypeRequest;
use App\Http\Requests\UpdateProduceTypeRequest;
use App\Repositories\ProduceTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ProduceTypeController extends AppBaseController
{
    /** @var  ProduceTypeRepository */
    private $produceTypeRepository;

    public function __construct(ProduceTypeRepository $produceTypeRepo)
    {
        $this->produceTypeRepository = $produceTypeRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the ProduceType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->produceTypeRepository->pushCriteria(new RequestCriteria($request));
        $produceTypes = $this->produceTypeRepository->all();

        return view('produce_types.index')
            ->with('produceTypes', $produceTypes);
    }

    /**
     * Show the form for creating a new ProduceType.
     *
     * @return Response
     */
    public function create()
    {
        return view('produce_types.create');
    }

    /**
     * Store a newly created ProduceType in storage.
     *
     * @param CreateProduceTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateProduceTypeRequest $request)
    {
        $input = $request->all();

        $produceType = $this->produceTypeRepository->create($input);

        Flash::success('บันทึกประเภทตะกร้าสินค้าเรียบร้อย.');

        return redirect(route('produceTypes.index'));
    }

    /**
     * Display the specified ProduceType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            Flash::error('ไม่พบประเภทตะกร้าสินค้า.');

            return redirect(route('produceTypes.index'));
        }

        return view('produce_types.show')->with('produceType', $produceType);
    }

    /**
     * Show the form for editing the specified ProduceType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            Flash::error('ไม่พบประเภทตะกร้าสินค้า.');

            return redirect(route('produceTypes.index'));
        }

        return view('produce_types.edit')->with('produceType', $produceType);
    }

    /**
     * Update the specified ProduceType in storage.
     *
     * @param  int              $id
     * @param UpdateProduceTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduceTypeRequest $request)
    {
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            Flash::error('ไม่พบประเภทตะกร้าสินค้า.');

            return redirect(route('produceTypes.index'));
        }

        $produceType = $this->produceTypeRepository->update($request->all(), $id);

        Flash::success('แก้ไขประเภทตะกร้าสินค้าเรียบร้อย.');

        return redirect(route('produceTypes.index'));
    }

    /**
     * Remove the specified ProduceType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            Flash::error('ไม่พบประเภทตะกร้าสินค้า.');

            return redirect(route('produceTypes.index'));
        }

        $this->produceTypeRepository->delete($id);

        Flash::success('ลบประเภทตะกร้าสินค้าเรียบร้อย.');

        return redirect(route('produceTypes.index'));
    }
}
