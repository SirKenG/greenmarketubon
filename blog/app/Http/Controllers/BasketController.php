<?php

namespace App\Http\Controllers;

use App\Basket;
use App\Produce;
use App\Province;
use App\District;
use App\SubDistrict;
use App\BasketAcc;
use Illuminate\Http\Request;

use App\Customer;

use Flash;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\DB;
use Session;

class BasketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $products = Produce::orderBy('updated_at', 'desc')->all();
        $products = DB::table('produces')
                        ->where('deleted_at', NULL)
                        ->orderBy('updated_at', 'desc')
                        ->paginate(45);;


        // dd($baskets);

        return view('front.basket.index', compact('products'));
    }


    public function showbaseontype($id)
    {
       $products = DB::table('produces')->where([
                    ['produce_type_id', '=', $id]
                ])->where('deleted_at', NULL)
                  ->orderBy('updated_at', 'desc')
                  ->get();


       $productstype = DB::table('produce_types')->where([
                    ['id', '=', $id]
                ])->first();

        // dd($products); 

        return view('front.basket.showbaseontype', compact('products', 'productstype'));
    }

    /**
     * Create Basket key for user
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $last_insert = Basket::latest()->first();

        // return $last_insert->basket_key  ;
        // // $reservation_id = 0;
        $reservation_id = $last_insert->basket_key + 1;

        // return $reservation_id;
        if($reservation_id){

            $data['basket_key'] = $reservation_id;
            $data['reservation'] = 1;
            $data['status_buy'] = 0;

            session([
                'member_basket_key' => $data['basket_key']
            ]);

            Basket::create($data);


        }

        return $reservation_id;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }


    public function getBasket(Request $request)
    {

       $baskets = DB::table('basket')
                ->join('produces', 'basket.product_id', '=', 'produces.id')
                ->select('basket.*', 'produces.*')
                ->where([
                    ['basket_key', '=', $request->basketKey],
                    ['reservation', '=', 0],
                    ['status_buy', '=', 0]
                ])->get();



       return $baskets;
    }

    public function getexistbasket(Request $request)
    {
        # code...
        $baskets = DB::table('basket')->where([
            ['basket_key', '=', $request->basketKey],
            ['product_id', '=', $request->productID],
        ])->get();

        // // return $baskets ;
        $result = '0';
        if(count($baskets)>=1){
            $result = '1';
        }

        return $result;
        
    }

    public function addBasket(Request $request)
    {
        $data['basket_key'] = $request->basketKey;
        $data['reservation'] = 0;
        $data['status_buy'] = 0;
        $data['product_id'] = $request->productID;
        $data['qty'] = $request->qty;

        $basket = Basket::create($data);

        return $basket;
    }
    public function addBasketByAdmin(Request $request)
    {
        // dd($request);
        $baskets = DB::table('basket')->where([
            ['basket_key', '=', $request->basketKey],
            ['product_id', '=', $request->productID],
        ])->first();        

        if(!$baskets){
            $basket = DB::table('basket')->where([
                ['basket_key', '=', $request->basketKey],
            ])->first();   


            $data['basket_key'] = $request->basketKey;
            $data['reservation'] = 0;
            $data['status_buy'] = 1;
            $data['product_id'] = $request->productID;
            $data['qty'] = 1;

            $data['pay'] = $basket->pay;
            $data['customer_code'] = $basket->customer_code;
            $data['receive_place'] = $basket->receive_place;


            $basket = Basket::create($data);

            Flash::success('เพิ่มสินค้าเรียบร้อย.');
        }else{
            Flash::error('มีสินค้านี้อยู่แล้ว!');
        }

        return redirect()->back(); 

    }


    public function updateBasket(Request $request)
    {
        $baskets = DB::table('basket')->where([
            ['basket_key', '=', $request->basketKey],
            ['product_id', '=', $request->productID],
        ])->first();

        // $data['basket_key'] = $request->basketKey;
        // $data['reservation'] = 0;
        // $data['product_id'] = $request->productID;
        $data['qty'] = $baskets->qty + $request->qty;

         //DB::update('update users set votes = 100 where name = ?', ['John']);
        $basket = DB::table('basket')
                    ->where('basket_key', $request->basketKey)
                    ->where('product_id', $request->productID)
                    ->update(['qty' => $data['qty']]);


        return $basket;
    }


    public function updateBasketQtyForCartPage(Request $request)
    {
        // $baskets = DB::table('basket')->where([
        //     ['basket_key', '=', $request->basketKey],
        //     ['product_id', '=', $request->productID],
        // ])->first();

        // $data['basket_key'] = $request->basketKey;
        // $data['reservation'] = 0;
        // $data['product_id'] = $request->productID;
        $data['qty'] = $request->qty;

         //DB::update('update users set votes = 100 where name = ?', ['John']);
        $basket = DB::table('basket')
                    ->where('basket_key', $request->basketKey)
                    ->where('product_id', $request->productID)
                    ->update(['qty' => $data['qty']]);


        return $basket;
    }
    public function updateBasketQtyForCartPageByAdmin(Request $request)
    {
        if(!is_numeric($request->qty)){
            Flash::error('กรุณาใส่จำนวนเป็นตัวเลข!');
        }

        if($request->basketKey && $request->productID){
            $data['qty'] = $request->qty;

            $basket = DB::table('basket')
                        ->where('basket_key', $request->basketKey)
                        ->where('product_id', $request->productID)
                        ->update(['qty' => $data['qty']]);


            Flash::success('ปรับปรุงจำนวนสินค้าเรียบร้อย.');
        }else{
            Flash::error('เกิดข้อผิดพลาดกรุณาลองใหม่ภายหลัง!');
        }


        return redirect()->back(); 
    }

    public function removeProductFromBasket(Request $request)
    {
        // return $request->productID; //basketKey
        $delete_baskets = DB::table('basket')->where([
                    ['basket_key', '=', $request->basketKey],
                    ['product_id', '=', $request->productID]
                ])->delete();

       return $delete_baskets;       
    }
    public function removeProductFromBasketByAdmin(Request $request)
    {
        // return 'productID: '.$request->productID.'  - basketKey: '.$request->basketKey;
        if($request->productID && $request->basketKey){
            
            $delete_baskets = DB::table('basket')->where([
                    ['basket_key', '=', $request->basketKey],
                    ['product_id', '=', $request->productID]
                ])->delete();

            Flash::success('นำสินค้าออกจากคำสั่งซื้อสินค้าเรียบร้อย.');

        }else{
            Flash::error('เกิดข้อผิดพลาดกรุณาลองใหม่ภายหลัง!');
        }
        return redirect()->back(); 

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $product = DB::table('produces')->where('id', $id)->first();
        // dd($products);


           $product =  DB::table('produces')
                            ->join('produce_types', 'produces.produce_type_id', '=', 'produce_types.id')
                            ->select('produces.*', 'produce_types.name as producttype_name')
                            ->where([
                                ['produces.id', '=', $id]
                            ])->first();



             $products_randoms = DB::table('produces')
                    ->where('deleted_at', NULL)
                    ->inRandomOrder()
                    ->limit(3)
                    ->get();                        

             // dd($products_random);
        return view('front.basket.show', compact('product', 'products_randoms'));
    }

    /**
     * Show result of all basket of product that user choose for buy
     *
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function cart($basketkey)
    {
        // return $basketkey;


        $baskets = DB::table('basket')
                ->join('produces', 'basket.product_id', '=', 'produces.id')
                ->select('basket.*', 'produces.*')
                ->where([
                    ['basket_key', '=', $basketkey],
                    ['reservation', '=', 0],
                    ['status_buy', '=', 0]
                ])->get();

        // dd($baskets->first()->customer_code);
        if($baskets->first()){
            if($baskets[0]->customer_code){

                if($baskets[0]->customer_code==session('member_code')){
                    return view('front.basket.cart', compact('baskets'));
                }else{
                    session()->flash('gotocart_meassage', 'ไม่สามารถดูตะกร้าสินค้าได้ กรุณาเข้าระบบ');
                    // Flash::success('คุณสามารถเข้าใช้ระบบโดย [Username: Email ที่ลงทะเบียน, Password: เบอร์โทรศัพท์ที่ลงทะเบียน].');
                    return redirect()->back();
                }
            }else{
                return view('front.basket.cart', compact('baskets'));
            }
        }else{
            return view('front.basket.cart', compact('baskets'));
        }


    }

    public function checkout(Request $request)
    {
        # code...
        // return $request->basketKey;
        // return session('member_code');
        // if(!$request->status_login){
        //     // return 'ok';
        // // }else{
        //     // return $this->checkou($request);//view('front.basket.register_login');
        //         // $provice = Province::pluck('name','id')->all();

        //     // return view('front.basket.register_login', compact('provice'));
        //     return redirect()->route('basket.checkou');
        // }





        // dd(session('member_basket_key'));


        if(session('member_login')==true){


            $baskets = DB::table('basket')
                    ->join('produces', 'basket.product_id', '=', 'produces.id')
                    ->select('basket.photo_path as path_acc', 'basket.customer_code', 'basket.status_buy', 'produces.*')
                    ->where([
                        ['basket_key', '=', $request->basketKey],
                        ['reservation', '=', 0]
                    ])->get();

            $basket_acc = BasketAcc::first();





            $customer = DB::table('customers')->where([
                        ['code', '=', session('member_code')]
                    ])->first();

            $provice = Province::pluck('name','id')->all();
            $district = District::where("province_id",$customer->province_id)
                        ->pluck("name","id")->all();

            $sub_district = SubDistrict::where("district_id",$customer->district_id)
                            ->pluck("name","id")->all();




            //dd($baskets);
            // $this->create();
            if(count($baskets)!=0){
                if($baskets[0]->customer_code){
                    if($baskets[0]->customer_code==session('member_code')){
                        Flash::success('คุณสามารถเข้าใช้ระบบโดย [Username: Email ที่ลงทะเบียน, Password: เบอร์โทรศัพท์ที่ลงทะเบียน].');


                        DB::table('basket') //member_code
                                ->where('basket_key', $request->basketKey)
                                ->update(['customer_code' => session('member_code')]);

                        if(!Session::has('member_basket_key')){
                            session([
                                'member_basket_key' => $request->basketKey
                            ]);
                        }
                        

                    }else{

                        session()->flash('checkout_meassage', 'ไม่สามารถสั่งซื้อสินค้าได้ เนื่องจากรหัสลูกค้าไม่ตรงกันกับรหัสลูกค้าของตะกร้าสินค้า แนะนำให้ปิดและเปิดบราวเซอร์ใหม่');

                        return redirect()->back(); 


                    }
                }else{
                        Flash::success('คุณสามารถเข้าใช้ระบบโดย [Username: Email ที่ลงทะเบียน, Password: เบอร์โทรศัพท์ที่ลงทะเบียน].');


                        DB::table('basket') //member_code
                                ->where('basket_key', $request->basketKey)
                                ->update(['customer_code' => session('member_code')]);

                        if(!Session::has('member_basket_key')){
                            session([
                                'member_basket_key' => $request->basketKey
                            ]);
                        }
                }
            }else{


                $products = DB::table('produces')
                                ->orderBy('updated_at', 'desc')
                                ->get();


                // dd($baskets);
                session()->flash('checkout_meassage', 'ยังไม่มีการเลือกสินค้า กรุณาเลือกสินค้าก่อนสั่งซื้อสินค้า');

                return view('front.basket.index', compact('products'));

            }

        }
        

        return view('front.basket.checkout', compact('basket_acc', 'baskets', 'customer', 'provice', 'district', 'sub_district'));
    }

    public function checkout_uploadfile(Request $request)
    {
        // $input = $request->all();

        $imageName = session('member_basket_key').'_'.time().'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('checkout_file'), $imageName);


        $photo_path = 'checkout_file/'. $imageName;

        DB::table('basket') //member_code
            ->where('basket_key', session('member_basket_key'))
            ->update(['photo_path' => $photo_path]);

        return response()->json(['success'=>$imageName]);

    }


    public function checkout_uploadfile_again(Request $request)
    {
        $imageName = $request->basketkey.'_'.time().'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('checkout_file'), $imageName);


        $photo_path = 'checkout_file/'. $imageName;

        DB::table('basket') //member_code
            ->where('basket_key', $request->basketkey)
            ->update(['photo_path' => $photo_path]); //basketkey

        return response()->json(['success'=>$imageName]);
    }

    public function checkout_getfile(Request $request)
    {
         // $baskets = DB::table('basket')
         //        ->join('produces', 'basket.product_id', '=', 'produces.id')
         //        ->select('basket.photo_path as path_acc', 'basket.customer_code', 'produces.*')
         //        ->where([
         //            ['basket_key', '=', $request->basketKey],
         //            ['reservation', '=', 1]
         //        ])->first();


        $baskets = DB::table('basket')->where([
                    ['basket_key', '=', $request->basketKey]
                ])->get();
        // $baskets =  Basket::findOrFail();

        return $baskets[0]->photo_path;

        // return $request->basketKey;

    }

    public function checkou(Request $request)
    {

        $provice = Province::pluck('name','id')->all();

        return view('front.basket.register_login', compact('provice'));
    }

    public function checkout_buy(Request $request)
    {
        // return 'I\'t work';
        if(!$request->optradio_pay){
            session()->flash('checkout_buy_meassage', 'ยังไม่ได้เลือกรูปแบบการชำระเงิน กรุณาเลือก');
            return redirect()->back(); 
        }


        if(!$request->optradio_poitrecieveproduct){
            session()->flash('checkout_buy_meassage', 'ยังไม่ได้เลือกรูปแบบการรับสินค้า กรุณาเลือก');
            return redirect()->back(); 
        }


        $_receive_place = $request->optradio_poitrecieveproduct;
        //if sent via post office to home must check address and address fully
        if($request->optradio_poitrecieveproduct=='จัดส่งไปรษณีย์'){

            $check_customeraddress = DB::table('customers')->where([
                        ['code', '=', session('member_code')]
                    ])->first();


            $customer_latlongDecode = json_decode($check_customeraddress->lat_long, true);
            //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=15.2286861,104.85642170000006&types=insurance_agency&rankby=distance&key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs

            if($customer_latlongDecode['sentbymap']==false) {
                if(empty($check_customeraddress->first_name) || empty($check_customeraddress->address_no) || empty($check_customeraddress->address_moo) || empty($check_customeraddress->address_road) || empty($check_customeraddress->province_id) || empty($check_customeraddress->province_id) || empty($check_customeraddress->sub_district_id) || empty($check_customeraddress->zipcode)){

                    session()->flash('checkout_buy_meassage', 'กรณีที่จัดส่งไปรษณีย์ ที่อยู่ต้องครบถ้วน โปรดตรวจสอบ ชื่อ ที่อยู่ ว่าครบถ้วนหรือไม่');
                    return redirect()->back()->withInput();    
                }
            }else{
                $_receive_place = 'จัดส่งตามแผนที่';
            }



             //dd($check_customeraddress);

        }



        //Insert 
        $basket = DB::table('basket')
                    ->where('basket_key',session('member_basket_key'))
                    ->where('customer_code', session('member_code'))
                    ->update([
                        'pay' => $request->optradio_pay,
                        'status_buy' => 1, 
                        'receive_place' => $_receive_place
                    ]);
         // dd($basket);
        // session()->flash('checkout_buy_meassage', session('member_code'));
        //sent email to customer
        if($basket){
            //sent email to customer
            $basket_get_buying = DB::table('basket')
                    ->join('customers', 'basket.customer_code', '=', 'customers.code')
                    ->select('basket.*', 'customers.*', 'customers.id as customer_id')
                    ->where([
                        ['basket_key', '=', session('member_basket_key')],
                        ['reservation', '=', 1]
                    ])->first();

                    // dd($basket_get_buying);
            $data = [
                'email'=> $basket_get_buying->email,
                'name'=> 'เรียนคุณ, '.$basket_get_buying->first_name.' ได้มีคำสั่งซื้อ',
                'title'=> 'รายละเอียดสินค้า รหัสอ้างอิงคำสั่งซื้อ: '.$basket_get_buying->basket_key,
                'pay'=> 'รูปแบบการจ่ายเงิน: '.$basket_get_buying->pay,
                'place'=> 'สถานที่นัดรับ: '.$basket_get_buying->receive_place,
                'content'=> 'สามารถดูสถานะการสั่งซื้อสินค้าทั้งหมดได้ที่: https://greenmarketubon.com/basket/status_checkout_buy',
                'thank'=> 'ขอบคุณที่อุดหนุน',
                'from'=> 'ระบบจัดการธุรกิจออนไลน์ของกลุ่มวิสาหกิจชุมชนตลาดนัดสีเขียวกินสบายใจ จังหวัดอุบลราชธานี'
            ];


            Mail::send('front.basket.email', $data, function($message) use ($data){
                // dd($data['email']);
                $message->to($data['email'])->subject('คำยืนยันการสั่งซื้อสินค้า');

            });


            //sent group LINE Notify to prducer
            $token = 'v26VgKhh8wFkaxqQtvcIlzxkDOYNcTPRACS4U2GSVxz'; //Token of group วิจัยตลาดกินสบายใจ
            $str = "[ข้อความอัตโนมัติ] คำสั่งซื้อรหัสอ้างอิง: $basket_get_buying->basket_key \nชื่อลูกค้า: $basket_get_buying->first_name $basket_get_buying->last_name \nเบอร์โทร: $basket_get_buying->tel \nรูปแบบการจ่ายเงิน: $basket_get_buying->pay \nสถานที่นัดรับ: $basket_get_buying->receive_place \nเข้าระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม: https://greenmarketubon.com/login"; //message max 1,000 character

            $this->line_notify($token, $str);


        }



        // $user = array();

        return redirect()->back(); 


    }
    public function checkout_buyByAdmin(Request $request)
    {
        // return $request->basketKey; //continue here for sent mail to customer and notify to line group
        $customer_basket = DB::table('basket')
                    ->join('customers', 'basket.customer_code', '=', 'customers.code')
                    ->select('basket.*', 'customers.*', 'customers.id as customer_id')
                    ->where([
                        ['basket_key', '=', $request->basketKey],
                        ['reservation', '=', 1]
                    ])->first();

        // dd($customer_basket);
        if($customer_basket){

            $data = [
                'email'=> $customer_basket->email,
                'name'=> 'เรียนคุณ, '.$customer_basket->first_name.' เปลี่ยนแปลงคำสั่งซื้อเรียบร้อย',
                'title'=> 'รายละเอียดสินค้า รหัสอ้างอิงคำสั่งซื้อ: '.$customer_basket->basket_key,
                'pay'=> 'รูปแบบการจ่ายเงิน: '.$customer_basket->pay,
                'place'=> 'สถานที่นัดรับ: '.$customer_basket->receive_place,
                'content'=> 'สามารถดูสถานะการสั่งซื้อสินค้าทั้งหมดได้ที่: https://greenmarketubon.com/basket/status_checkout_buy',
                'thank'=> 'ขอบคุณที่อุดหนุน',
                'from'=> 'ระบบจัดการธุรกิจออนไลน์ของกลุ่มวิสาหกิจชุมชนตลาดนัดสีเขียวกินสบายใจ จังหวัดอุบลราชธานี'
            ];


            Mail::send('front.basket.email_byadmin', $data, function($message) use ($data){
                // dd($data['email']);
                $message->to($data['email'])->subject('การสั่งซื้อสินค้าได้ถูกเปลี่ยนแปลงเรียบร้อย');

            });


            //sent group LINE Notify to prducer
            $token = 'v26VgKhh8wFkaxqQtvcIlzxkDOYNcTPRACS4U2GSVxz'; //Token of group วิจัยตลาดกินสบายใจ
            $str = "[ข้อความอัตโนมัติ] ได้มีการเปลี่ยนแปลงสินค้า คำสั่งซื้อรหัสอ้างอิง: $customer_basket->basket_key \nเข้าระบบเพื่อตรวจสอบข้อมูลล่าสุด: https://greenmarketubon.com/login"; //message max 1,000 character

            $this->line_notify($token, $str);


            Flash::success('ส่งอีเมลแจ้งลูกค้าเรียบร้อย.');
        }else{
            Flash::error('มีบางอย่างผิดพลาด ไม่สามารถส่งอีเมลแจ้งลูกค้าได้ กรุณาลองใหม่ภายหลัง!');
        }

       return redirect(route('home.index'));
     
            
    }



    private function line_notify($Token, $message)
    {
        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms =  trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        // SSL USE 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        //POST 
        curl_setopt( $chOne, CURLOPT_POST, 1); 
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms"); 
        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
            curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
        //Check error 
        if(curl_error($chOne)) 
        { 
               echo 'error:' . curl_error($chOne); 
        } 
        else { 
        $result_ = json_decode($result, true); 
           echo "status : ".$result_['status']; echo " message : ". $result_['message'];
            } 
        curl_close( $chOne );   
    }




    public function editCustomer(Request $request)
    {
        // $customer = $this->customerRepository->findWithoutFail($id);
        // $provice = Province::pluck('name','id')->all();
        // if (empty($customer)) {
        //     Flash::error('ไม่พบข้อมูลลูกค้านี้.');

        //     return redirect(route('customers.index'));
        // }

        // return view('customers.edit')->with('customer', $customer)->with(compact('provice'));
    }

    public function updateCustomer(Request $request)
    {
        // $customer = $this->customerRepository->findWithoutFail($id);
        $get_customerid = DB::table('customers')->where([
                    ['code', '=', session('member_code')]
                ])->first();

        $customer = Customer::findOrFail($get_customerid->id);

        // dd($customer);

        if (empty($customer)) {
            // Flash::error('ไม่พบข้อมูลลูกค้านี้.');
            $request->session()->flash('update_customer_meassage', 'ไม่พบข้อมูลลูกค้านี้.');

            // return redirect(route('customers.index'));
            return redirect()->back();
        }

        // $customer = $this->customerRepository->update($request->all(), $id);
        // DB::table('customers') //member_code
        //     ->where('code', session('member_code'))
        //     ->update(['photo_path' => $photo_path]);


          // $customer = Customer::update($request->all());

        //{"lat":34382423,"long":78.4328,"sentbymap":false}
        $customer_latlongDecode = json_decode($customer['lat_long'], true);



        $lat_long_field = array(
                        'lat' =>  $customer_latlongDecode['lat'],
                        'long' => $customer_latlongDecode['long'],
                        'data_more' => $customer_latlongDecode['data_more'],
                        'sentbymap' => false
                    );
        $request['lat_long'] = json_encode($lat_long_field);
        
        // get value from customer value
        // validate
        // if don't have that sent to empty value
        // if have value that sent value to field again
        // print_r($customer_latlongDecode);
        // return $request['lat_long'];

        $customer->update($request->all());

        // Flash::success('แก้ไขข้อมูลลูกค้าสำเร็จ.');
        $request->session()->flash('update_customer_meassage', 'แก้ไขข้อมูลลูกค้าสำเร็ จ เราจะจัดส่งไปตามที่อยู่');

        return redirect()->back();
    }
    public function updateCustomerMap(Request $request)
    {
        $get_customerid = DB::table('customers')->where([
                    ['code', '=', session('member_code')]
                ])->first();

        $customer = Customer::findOrFail($get_customerid->id);

        if (empty($customer)) {
            // Flash::error('ไม่พบข้อมูลลูกค้านี้.');
            $request->session()->flash('update_customer_meassage', 'ไม่พบข้อมูลลูกค้านี้.');

            // return redirect(route('customers.index'));
            return redirect()->back();
        }

        // $customer_latlongDecode = json_decode($customer['lat_long'], true);



        $lat_long_field = array(
                        'lat' =>  $request['latitude'],
                        'long' => $request['longitude'],
                        'data_more' => $request['data_more'],
                        'sentbymap' => true
                    );

        // return $lat_long_field;

        $request['lat_long'] = json_encode($lat_long_field);



        $request->session()->flash('update_customer_meassage', 'แก้ไขข้อมูลลูกค้าสำเร็จ เราจะจัดส่งไปตามแผนที่');


        $customer->update($request->all());

        return redirect()->back();

    }

    public function status_checkout_buy(Request $request)
    {
        $baskets_buying = DB::table('basket')->where([
                    ['customer_code', '=', session('member_code')],
                    ['reservation', '=', 1],
                    ['status_buy', '=', 1]
                ])->orderby('basket.basket_key', 'desc')->get();

        $baskets_buy = DB::table('basket')->where([
                    ['customer_code', '=', session('member_code')],
                    ['reservation', '=', 1],
                    ['status_buy', '=', 2]
                ])->orderby('basket.basket_key', 'desc')->get();

        // $baskets = DB::table('basket')
        //         ->join('produces', 'basket.product_id', '=', 'produces.id')
        //         ->select('basket.*', 'basket.photo_path as image_money', 'produces.*', 'produces.photo_path as image_product')
        //         ->where([
        //             ['customer_code', '=', session('member_code')],
        //             // ['reservation', '=', 1],
        //             ['status_buy', '=', 1]
        //         ])->get();

        // dd($baskets); //status_checkout_buy


        return view('front.basket.status_checkout_buy', compact('baskets_buying', 'baskets_buy'));
        // return 'fddfds';

    }

    public function seemore_products($id)
    {
        $products = DB::table('basket')
                ->join('produces', 'basket.product_id', '=', 'produces.id')
                ->select('basket.*', 'basket.photo_path as image_money', 'produces.*', 'produces.photo_path as image_product')
                ->where([
                    ['basket_key', '=', $id],
                    ['customer_code', '=', session('member_code')]
                ])->get();


        // dd($products); //seemore_products
        return view('front.basket.seemore_products', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function edit(Basket $basket)
    {
        //
        return 'dfdsfd';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Basket $basket)
    {
        //


    }

    public function edit_acc()
    {
        $basket_acc = DB::table('basket_acc')
                        ->get();

        // dd($basket_acc);

        return view('basket.edit', compact('basket_acc'));
    }

    public function update_acc(Request $request, $id)
    {
        $basket_acc = BasketAcc::findOrFail($id);
        $basket_acc->update($request->all());

        $request->session()->flash('basketacc_update_meassage', 'แก้ไขข้อมูลสำเสร็จ');

        return redirect()->back();
    }

    private function getmore_data_googlemap_api1($pagetoken)
    {
        $url2 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?sensor=false&pagetoken={$pagetoken}&key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs";
        $resp_json2 = file_get_contents($url2);
        $resp2 = json_decode($resp_json2, true);

        return $url2;
    }
    private function appendNewData($results)
    {
        $jsonData = json_encode($results);
        $path = public_path('geo/data.json');

        //get data
        $curren_data = file_get_contents($path);
        $array_data = json_decode($curren_data, true);


        //add data
        if(count($array_data)=='0'){
        //     $array_data = $results;
            $parray_data = array();
            $parray_data = $results;

            $final_data = json_encode($parray_data);
            $ret = file_put_contents($path, $final_data);
        }else{
            
            // array_push($array_data, $results);

            foreach ($results as $key => $value) {
                # code...
                $extra = array(
                    'geometry' =>  array(
                        'location' =>  array(
                            'lat' => $value['geometry']['location']['lat'], 
                            'lng' => $value['geometry']['location']['lng']
                        ),
                        'viewport' => array(
                            'northeast' => array(
                                'lat' => $value['geometry']['viewport']['northeast']['lat'],
                                'lng' => $value['geometry']['viewport']['northeast']['lng']
                            ),
                            'southwest' => array(
                                'lat' => $value['geometry']['viewport']['southwest']['lat'],
                                'lng' => $value['geometry']['viewport']['southwest']['lng']
                            )
                        )
                    ),
                    'icon' => $value['icon'],
                    'id' => $value['id'],
                    'name' => $value['name'],
                    'place_id' => $value['place_id'],
                    'reference' => $value['reference'],
                    'scope' => $value['scope'],
                    'types' => $value['types'],
                    'vicinity' => $value['vicinity']
                );


                $array_data[] = $extra;

            }

            $final_data = json_encode($array_data);
            $ret = file_put_contents($path, $final_data);
            
        }
        
        // $final_data = json_encode($parray_data);

        // $ret = file_put_contents($path, $final_data, FILE_APPEND | LOCK_EX);


        return $array_data;
    }
    public function get_data_googlemap_api(Request $request)
    {
        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=15.2286861,104.85642170000006&rankby=distance&types={$request->type}&key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs";

        $resp_json = file_get_contents($url);
        $resp = json_decode($resp_json, true);



        // $data = $this->getmore_data_googlemap_api1($resp['next_page_token']);
        $data = $this->appendNewData($resp['results']);

        // $all_results = [];

        // $all_results = $resp['results'];

        // $my_array = array_merge($all_results, $resp['results']);

        return $data;
    }

    public function googlemap_api(Request $request)
    {
        $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=15.2286861,104.85642170000006&rankby=distance&types={$request->type}&keyword={$request->keyword}&key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs";
        
        $resp_json = file_get_contents($url);
        $resp = json_decode($resp_json, true);

        //$resp['next_page_token'] ----Continue here ค่อยๆเก็บทีละ array
        //ครั้งแรกเก็บ 20 อันดับแรก ครั้งแต่ไปให้กดค้นหา แล้วเก็บใส่ เครื่อง client
        ///
        //CsQDuwEAAFw-juRgAP0Xauq8xm0x8C-9lSc1B9OmjixKIz07hk0UQQy46PCUUnxffI5hvO-qcf1vyBBeo_myoXef0BORwijJiqvEueec8AHEs0wfOHKn_uC4VTl_fUuNRJiFSsvDpTBnp5R8eAVXsBX8htTn7nI-gSWJH6RoCIKEh3fBTcI8SGXoLIDAmZeKfc9TD8foKPjwlytA1mBMBHvUu7vcFwMVsshOW8iE61EWmoHseMOgThv4QI3uHJPjE2yRPy2oWcSbWFL0V0QMbW9YRu966Eec_pH3sZ2mPLF59Lo0zt4vBOoknC71sQJ8zdPWyv2DBjbvvn0wnDnnjr9Vz0eQM67Uuz2yHpFyEc3_TPj2pDVqINmeIIPiAUSKuU_56MyvyXhvPRWBIgVfQaFzM3UgMvlL7PYBqd-RKTc7h3O9tKsoZBr4Ndyyye5QHsqwBG9HhaaKWax0ynJVOOg9gXT5EBR8fJgajD8MkxhlucO_Z_xa_hoLA2jBy2DC5SPXNwxQAMueFNIcDk3lbrSaxuTh2KdUScjfzmy77BcaBY3Q43zcVmPC-e6p0_jAqbgKwzPcARw_xKEReBHzhQU6F0g4k7cSEFCMiN2vbWfsrI3gU7m8r9waFAc2W6IQm3UMrKHeouyUUR0p2GBA

        // $url2 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=CsQDuwEAAFw-juRgAP0Xauq8xm0x8C-9lSc1B9OmjixKIz07hk0UQQy46PCUUnxffI5hvO-qcf1vyBBeo_myoXef0BORwijJiqvEueec8AHEs0wfOHKn_uC4VTl_fUuNRJiFSsvDpTBnp5R8eAVXsBX8htTn7nI-gSWJH6RoCIKEh3fBTcI8SGXoLIDAmZeKfc9TD8foKPjwlytA1mBMBHvUu7vcFwMVsshOW8iE61EWmoHseMOgThv4QI3uHJPjE2yRPy2oWcSbWFL0V0QMbW9YRu966Eec_pH3sZ2mPLF59Lo0zt4vBOoknC71sQJ8zdPWyv2DBjbvvn0wnDnnjr9Vz0eQM67Uuz2yHpFyEc3_TPj2pDVqINmeIIPiAUSKuU_56MyvyXhvPRWBIgVfQaFzM3UgMvlL7PYBqd-RKTc7h3O9tKsoZBr4Ndyyye5QHsqwBG9HhaaKWax0ynJVOOg9gXT5EBR8fJgajD8MkxhlucO_Z_xa_hoLA2jBy2DC5SPXNwxQAMueFNIcDk3lbrSaxuTh2KdUScjfzmy77BcaBY3Q43zcVmPC-e6p0_jAqbgKwzPcARw_xKEReBHzhQU6F0g4k7cSEFCMiN2vbWfsrI3gU7m8r9waFAc2W6IQm3UMrKHeouyUUR0p2GBA&key=AIzaSyAV1En9d4jDK1pZRU91Ul_OOQTr9mZ2wSs";
        // $resp_json2 = file_get_contents($url2);
        // $resp2 = json_decode($resp_json2, true);


        $data = $this->appendNewData($resp['results']);


        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Basket  $basket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Basket $basket)
    {
        //
    }
}
