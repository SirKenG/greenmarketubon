<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProduceRequest;
use App\Http\Requests\UpdateProduceRequest;
use App\Repositories\ProduceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\ProduceType;

class ProduceController extends AppBaseController
{
    /** @var  ProduceRepository */
    private $produceRepository;

    public function __construct(ProduceRepository $produceRepo)
    {
        $this->produceRepository = $produceRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Produce.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->produceRepository->pushCriteria(new RequestCriteria($request));
        $produces = $this->produceRepository->paginate(10);

        return view('produces.index')
            ->with('produces', $produces);
    }

    /**
     * Show the form for creating a new Produce.
     *
     * @return Response
     */
    public function create()
    {
        return view('produces.create');
    }

    /**
     * Store a newly created Produce in storage.
     *
     * @param CreateProduceRequest $request
     *
     * @return Response
     */
    public function store(CreateProduceRequest $request)
    {
        $input = $request->all();

        $image = $request->file('photo_path');
        if(!empty($image)){
          $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();

          $destinationPath = public_path('produce');

          $image->move($destinationPath, $nameFile['imagename']);

          $input['photo_path'] = '/produce/'. $nameFile['imagename'];
        }
        $produce = $this->produceRepository->create($input);

        Flash::success('บันทึกข้อมูลสินค้าเรียบร้อย.');

        return redirect(route('produces.index'));
    }

    /**
     * Display the specified Produce.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $produce = $this->produceRepository->findWithoutFail($id);
        if (empty($produce)) {
            Flash::error('ไม่พบข้อมูลสินค้านี้.');

            return redirect(route('produces.index'));
        }
        return view('produces.show')->with('produce', $produce)->with('controller', $this);
    }

    /**
     * Show the form for editing the specified Produce.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $produce = $this->produceRepository->findWithoutFail($id);
        if (empty($produce)) {
            Flash::error('ไม่พบข้อมูลสินค้านี้.');

            return redirect(route('produces.index'));
        }

        return view('produces.edit')->with('produce', $produce);
    }

    /**
     * Update the specified Produce in storage.
     *
     * @param  int              $id
     * @param UpdateProduceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduceRequest $request)
    {
        $produce = $this->produceRepository->findWithoutFail($id);
        if (empty($produce)) {
            Flash::error('ไม่พบข้อมูลสินค้านี้.');

            return redirect(route('produces.index'));
        }
        $data = $request->all();
            $image = $request->file('photo_path');
            if(!empty($image)){
              $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();

              // return public_path('produce');
              $destinationPath = public_path('produce');

              $image->move($destinationPath, $nameFile['imagename']);

              $data['photo_path'] = '/produce/'. $nameFile['imagename'];
            }
        $produce = $this->produceRepository->update($data, $id);

        Flash::success('แก้ไขข้อมูลสินค้าเรียบร้อย.');

        return redirect(route('produces.index'));
    }

    /**
     * Remove the specified Produce from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $produce = $this->produceRepository->findWithoutFail($id);

        if (empty($produce)) {
            Flash::error('ไม่พบข้อมูลสินค้านี้.');

            return redirect(route('produces.index'));
        }

        $this->produceRepository->delete($id);

        Flash::success('ลบข้อมูลสินค้าเรียบร้อย.');

        return redirect(route('produces.index'));
    }


}
