<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Customer;
use Illuminate\Support\Facades\Mail;


class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return 'It work!';
        return view('front.customer.login');
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        session([
            'member_login' => false,
            'member_email' => '',
            'member_tel' => ''
        ]);
        return redirect()->back();
        // return redirect(route('frontHome.index'));
    }

    /**
     * Login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // return 'It work!';
    
        // dd($request->all());
        $input = $request->all();

        // $getcustomer = DB::table('customers')->where('email', $input['email'])->first();

        // $getcustomer = DB::table('customers')->where([
        //     ['email', '=', $input['email']],
        //     ['tel', '=', $input['password']],
        // ])->get();
        // $getcustomer = DB::table('customers')
        //             ->where('email',  $input['username'])
        //             ->orWhere('tel', $input['password'])
        //             ->first();
        
        //Here sam code in App\Http\Controllers\CustomerController@addRegister
        //If change this code must change above function       
        // dd(bcrypt($input['password']));
        // dd($input);
        $getcustomer = DB::table('customers')->where([
                    ['email', '=', $input['username']],
                    // ['password', '=', bcrypt($input['password'])],
                ])->first();
        // dd($getcustomer);
        if($getcustomer){
            if(Hash::check($input['password'], $getcustomer->password)){
                // $request->session()->keep(['email', 'email']);
                // Flash::error('ddssds');
                // Session::set('variableName', $getcustomer->email);
                // $request->session()->push('email_member', $getcustomer->email);
                session([
                    'member_login' => true,
                    'member_code' => $getcustomer->code,
                    'member_email' => $getcustomer->email,
                    'member_tel' => $getcustomer->tel
                ]);

            //Here sam code in App\Http\Controllers\CustomerController@addRegister
            //If change this code must change above function 
                // return redirect()->route('frontHome.index');
            }else{
                Flash::error('Username หรือ Password ไม่ถูกต้องกรุณาตรวจสอบ!');
            }
        }else{
            Flash::error('Username หรือ Password ไม่ถูกต้องกรุณาตรวจสอบ!');
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // return 'It work '.$id;
        if(session('member_login')==true){
            $customer = Customer::where("code",$id)->first();
            // dd($customer);
            return view('front.customer.change_password', compact('customer'));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return 'fsdfds'.$id;
        $customer = Customer::where("code", $id)->first();
        // dd($customer);
        if(!empty($request->old_password) && !empty($request->new_password) && !empty($request->confirm_password)){
            if(Hash::check($request->old_password, $customer->password)){
                if($request->new_password==$request->confirm_password){

                    $input = $request->all();
                    $input['password'] = bcrypt($request->confirm_password);
                    // dd($input);

                    $gcustomer = $customer->update($input);
                    if($gcustomer){
                        Flash::success('เปลี่ยนรหัสผ่านสำเร็จ');
                    }else{
                        Flash::error('เปลี่ยนรหัสไม่สำเร็จ กรุณาลองใหม่ภายหลัง !');
                    }
                }else{
                    Flash::error('รหัสผ่านใหม่ไม่ตรงกัน !');
                }
            }else{
                Flash::error('รหัสผ่านเดิมไม่ถูกต้อง !');
            }
        }else{
            Flash::error('กรุณาใส่ให้ครบทุกช่อง !');
        }
        return redirect()->back();

    }

    public function forgotpassword()
    {
        // return 'It\'work';
        return view('front.customer.forgotpassword');
    }

    public function sentemailforgotpassword(Request $request)
    {
        // return $request->email;
        if(empty($request->email)){
            Flash::error('กรุณาใส่อีเมล์ของคุณ !');
            return redirect()->back();
        }
        //validate email format
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
          Flash::error('รูปแบบอีเมล์ไม่ถูกต้องกรุณาตรวจสอบ !');
          return redirect()->back();
        }        

        $customer = Customer::where("email", $request->email)->first();

        if(!$customer){
          Flash::error('ไม่มีอีเมล์นี้ในระบบ !');
          return redirect()->back();            
        }
        
        // dd($customer);
        $input = $request->all();
        $input['id'] = $customer->id;
        $input['password'] = bcrypt($customer->tel);
        // dd($input);
        $gcustomer = $customer->update($input);
        if($gcustomer){

            //Sent email to customer
            $data = [
                'email'=> $request->email,
                'title'=> 'รหัสผ่านใหม่ของคุณคือ: '.$customer->tel,
                'content'=> 'สามารถเข้าระบบใหม่ได้ที่: https://greenmarketubon.com/member',
                'thank'=> 'ขอบคุณค่ะ',
                'from'=> 'ระบบจัดการธุรกิจออนไลน์ของกลุ่มวิสาหกิจชุมชนตลาดนัดสีเขียวกินสบายใจ จังหวัดอุบลราชธานี'
            ];


            Mail::send('front.customer.email', $data, function($message) use ($data){
                // dd($data['email']);
                $message->to($data['email'])->subject('รีเซ็ตรหัสผ่านใหม่');
            });



            Flash::success('ระบบได้ส่งรหัสผ่านใหม่เข้าอีเมล์ '.$request->email.' กรุณาตรวจสอบ');
        }else{
            Flash::error('มีบางอย่างผิดพลาด กรุณาลองใหม่ภายหลัง !');
        }

        return redirect()->back(); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
