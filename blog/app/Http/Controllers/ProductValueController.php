<?php

namespace App\Http\Controllers;

use App\ProductValue;
use Illuminate\Http\Request;

class ProductValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productvalue = ProductValue::all();

        return view('front.productvalue.index', compact('productvalue'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productvalue = ProductValue::all();

        return view('productvalue.create', compact('productvalue'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductValue  $productValue
     * @return \Illuminate\Http\Response
     */
    public function show(ProductValue $productValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductValue  $productValue
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductValue $productValue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductValue  $productValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productvalue = ProductValue::findOrFail($id);
        $productvalue->update($request->all());

        $request->session()->flash('productvalue_update_meassage', 'แก้ไขข้อมูลสำเสร็จ');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductValue  $productValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductValue $productValue)
    {
        //
    }
}
