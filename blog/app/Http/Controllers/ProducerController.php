<?php

namespace App\Http\Controllers;


use App\Producer;

use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProducerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $producers = DB::table('producer')
                        ->orderBy('updated_at', 'desc')
                        ->get();
        // dd($producers);
        // return 'It work';
        return view('producer.index', compact('producers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('producer.create');
    }

    // public function createa()
    // {
    //     return view('producer.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();

        $image = $request->file('photo_path');
        if(!empty($image)){
            $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('producers');

            $image->move($destinationPath, $nameFile['imagename']);

            $input['photo_path'] = '/producers/'. $nameFile['imagename'];

        }

        $producer = Producer::create($input);

        Flash::success('บันทึกสำเร็จ.');

        return redirect(route('producer.index'));

    }

    public function showall()
    {
        // return 'It work==================';
        $producers = DB::table('producer')
                        ->orderBy('updated_at', 'desc')
                        ->get();

        return view('front.producer.index', compact('producers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // //
        // // return 'It work===กดหก==============='.$id;
        // $producer = DB::table('producer')
        //                 ->where('id', $id)
        //                 ->first();
        // // dd($producer);

        // return view('front.producer.show', compact('producer'));

    }

    public function showa($id)
    {
        //
        // return 'It work===กดหก==============='.$id;

        if($id=='create'){
           return $this->create();
        }


        $producer = DB::table('producer')
                        ->where('id', $id)
                        ->first();
        // dd($producer);

        return view('front.producer.show', compact('producer'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producer = Producer::findOrFail($id);
        if (empty($producer)) {
            Flash::error('ไม่พบข้อมูลสินค้านี้.');

            return redirect(route('producer.index'));
        }

        return view('producer.edit', compact('producer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producer = Producer::findOrFail($id);

        if (empty($producer)) {
            Flash::error('ไม่พบข้อมูล.');

            return redirect(route('producer.index'));
        }

        $data = $request->all();
            $image = $request->file('photo_path');
            if(!empty($image)){
              $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();

              // return public_path('produce');
              $destinationPath = public_path('producers');

              $image->move($destinationPath, $nameFile['imagename']);

              $data['photo_path'] = '/producers/'. $nameFile['imagename'];
            }
        // $produce = $this->produceRepository->update($data, $id);
            $producer = $producer->update($data);

        Flash::success('แก้ไขข้อมูลเรียบร้อย.');

        return redirect(route('producer.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producer = Producer::findOrFail($id);

        if (empty($producer)) {
            Flash::error('ไม่พบข้อมูล.');

            return redirect(route('producer.index'));
        }

        $producer->delete($id);

        Flash::success('ลบข้อมูลเรียบร้อย.');

        return redirect(route('producer.index'));
    }
}
