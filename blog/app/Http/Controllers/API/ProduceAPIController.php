<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProduceAPIRequest;
use App\Http\Requests\API\UpdateProduceAPIRequest;
use App\Produce;
use App\Repositories\ProduceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProduceController
 * @package App\Http\Controllers\API
 */

class ProduceAPIController extends AppBaseController
{
    /** @var  ProduceRepository */
    private $produceRepository;

    public function __construct(ProduceRepository $produceRepo)
    {
        $this->produceRepository = $produceRepo;
    }

    /**
     * Display a listing of the Produce.
     * GET|HEAD /produces
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->produceRepository->pushCriteria(new RequestCriteria($request));
        $this->produceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $produces = $this->produceRepository->all();

        return $this->sendResponse($produces->toArray(), 'Produces retrieved successfully');
    }

    /**
     * Store a newly created Produce in storage.
     * POST /produces
     *
     * @param CreateProduceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProduceAPIRequest $request)
    {
        $input = $request->all();

        $produces = $this->produceRepository->create($input);

        return $this->sendResponse($produces->toArray(), 'Produce saved successfully');
    }

    /**
     * Display the specified Produce.
     * GET|HEAD /produces/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Produce $produce */
        $produce = $this->produceRepository->findWithoutFail($id);

        if (empty($produce)) {
            return $this->sendError('Produce not found');
        }

        return $this->sendResponse($produce->toArray(), 'Produce retrieved successfully');
    }

    /**
     * Update the specified Produce in storage.
     * PUT/PATCH /produces/{id}
     *
     * @param  int $id
     * @param UpdateProduceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Produce $produce */
        $produce = $this->produceRepository->findWithoutFail($id);

        if (empty($produce)) {
            return $this->sendError('Produce not found');
        }

        $produce = $this->produceRepository->update($input, $id);

        return $this->sendResponse($produce->toArray(), 'Produce updated successfully');
    }

    /**
     * Remove the specified Produce from storage.
     * DELETE /produces/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Produce $produce */
        $produce = $this->produceRepository->findWithoutFail($id);

        if (empty($produce)) {
            return $this->sendError('Produce not found');
        }

        $produce->delete();

        return $this->sendResponse($id, 'Produce deleted successfully');
    }
}
