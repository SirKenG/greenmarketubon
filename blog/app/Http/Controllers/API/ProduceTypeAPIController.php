<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProduceTypeAPIRequest;
use App\Http\Requests\API\UpdateProduceTypeAPIRequest;
use App\ProduceType;
use App\Repositories\ProduceTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ProduceTypeController
 * @package App\Http\Controllers\API
 */

class ProduceTypeAPIController extends AppBaseController
{
    /** @var  ProduceTypeRepository */
    private $produceTypeRepository;

    public function __construct(ProduceTypeRepository $produceTypeRepo)
    {
        $this->produceTypeRepository = $produceTypeRepo;
    }

    /**
     * Display a listing of the ProduceType.
     * GET|HEAD /produceTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->produceTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->produceTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $produceTypes = $this->produceTypeRepository->all();

        return $this->sendResponse($produceTypes->toArray(), 'Produce Types retrieved successfully');
    }

    /**
     * Store a newly created ProduceType in storage.
     * POST /produceTypes
     *
     * @param CreateProduceTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProduceTypeAPIRequest $request)
    {
        $input = $request->all();

        $produceTypes = $this->produceTypeRepository->create($input);

        return $this->sendResponse($produceTypes->toArray(), 'Produce Type saved successfully');
    }

    /**
     * Display the specified ProduceType.
     * GET|HEAD /produceTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ProduceType $produceType */
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            return $this->sendError('Produce Type not found');
        }

        return $this->sendResponse($produceType->toArray(), 'Produce Type retrieved successfully');
    }

    /**
     * Update the specified ProduceType in storage.
     * PUT/PATCH /produceTypes/{id}
     *
     * @param  int $id
     * @param UpdateProduceTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProduceTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProduceType $produceType */
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            return $this->sendError('Produce Type not found');
        }

        $produceType = $this->produceTypeRepository->update($input, $id);

        return $this->sendResponse($produceType->toArray(), 'ProduceType updated successfully');
    }

    /**
     * Remove the specified ProduceType from storage.
     * DELETE /produceTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ProduceType $produceType */
        $produceType = $this->produceTypeRepository->findWithoutFail($id);

        if (empty($produceType)) {
            return $this->sendError('Produce Type not found');
        }

        $produceType->delete();

        return $this->sendResponse($id, 'Produce Type deleted successfully');
    }
}
