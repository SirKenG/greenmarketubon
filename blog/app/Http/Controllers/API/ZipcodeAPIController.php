<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateZipcodeAPIRequest;
use App\Http\Requests\API\UpdateZipcodeAPIRequest;
use App\Zipcode;
use App\Repositories\ZipcodeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ZipcodeController
 * @package App\Http\Controllers\API
 */

class ZipcodeAPIController extends AppBaseController
{
    /** @var  ZipcodeRepository */
    private $zipcodeRepository;

    public function __construct(ZipcodeRepository $zipcodeRepo)
    {
        $this->zipcodeRepository = $zipcodeRepo;
    }

    /**
     * Display a listing of the Zipcode.
     * GET|HEAD /zipcodes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->zipcodeRepository->pushCriteria(new RequestCriteria($request));
        $this->zipcodeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $zipcodes = $this->zipcodeRepository->all();

        return $this->sendResponse($zipcodes->toArray(), 'Zipcodes retrieved successfully');
    }

    /**
     * Store a newly created Zipcode in storage.
     * POST /zipcodes
     *
     * @param CreateZipcodeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateZipcodeAPIRequest $request)
    {
        $input = $request->all();

        $zipcodes = $this->zipcodeRepository->create($input);

        return $this->sendResponse($zipcodes->toArray(), 'Zipcode saved successfully');
    }

    /**
     * Display the specified Zipcode.
     * GET|HEAD /zipcodes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Zipcode $zipcode */
        $zipcode = $this->zipcodeRepository->findWithoutFail($id);

        if (empty($zipcode)) {
            return $this->sendError('Zipcode not found');
        }

        return $this->sendResponse($zipcode->toArray(), 'Zipcode retrieved successfully');
    }

    /**
     * Update the specified Zipcode in storage.
     * PUT/PATCH /zipcodes/{id}
     *
     * @param  int $id
     * @param UpdateZipcodeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateZipcodeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Zipcode $zipcode */
        $zipcode = $this->zipcodeRepository->findWithoutFail($id);

        if (empty($zipcode)) {
            return $this->sendError('Zipcode not found');
        }

        $zipcode = $this->zipcodeRepository->update($input, $id);

        return $this->sendResponse($zipcode->toArray(), 'Zipcode updated successfully');
    }

    /**
     * Remove the specified Zipcode from storage.
     * DELETE /zipcodes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Zipcode $zipcode */
        $zipcode = $this->zipcodeRepository->findWithoutFail($id);

        if (empty($zipcode)) {
            return $this->sendError('Zipcode not found');
        }

        $zipcode->delete();

        return $this->sendResponse($id, 'Zipcode deleted successfully');
    }
}
