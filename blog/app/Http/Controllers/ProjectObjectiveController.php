<?php

namespace App\Http\Controllers;

use App\ProjectObjective;
use Illuminate\Http\Request;



class ProjectObjectiveController extends Controller
{
    // *
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
     
    // public function showdata()
    // {
    //     return view('front.objective.index');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objective = ProjectObjective::all();

        return view('front.objective.index', compact('objective'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $objective = ProjectObjective::all();

        //return $objective[0]->id;
        //dd($objective);

        return view('objective.create', compact('objective'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectObjective  $projectObjective
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectObjective $projectObjective)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectObjective  $projectObjective
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectObjective $projectObjective)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectObjective  $projectObjective
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($projectObjective);
        // $input = $request->all();
        $objective = ProjectObjective::findOrFail($id);
        $objective->update($request->all());

        $request->session()->flash('objective_update_meassage', 'แก้ไขข้อมูลสำเสร็จ');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectObjective  $projectObjective
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectObjective $projectObjective)
    {
        //
    }
}
