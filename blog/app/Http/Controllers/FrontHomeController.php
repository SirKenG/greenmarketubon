<?php

namespace App\Http\Controllers;

use App\FrontHome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'It work';
        $fronthomes = FrontHome::all();


        $products = DB::table('produces')
                ->where('deleted_at', NULL)
                ->latest()
                ->limit(3)
                ->get();


        $producers = DB::table('producer')
                    ->where('deleted_at', NULL)
                    ->latest()
                    ->limit(3)
                    ->get();
        // dd($products); 

        return view('front.home', compact('fronthomes', 'products', 'producers'));


        // //Test LINE Notify
        // $token = 'v26VgKhh8wFkaxqQtvcIlzxkDOYNcTPRACS4U2GSVxz'; //Token
        // $str = "[ข้อความอัตโนมัติ] - [คำสั่งซื้อรหัสอ้างอิง: 32 \n ลูกค้าชื่อคุณ: ปีเตอร์ กินแต่ผัก | เบอร์โทร: 093-09871134 | รูปแบบการจ่ายเงิน: ชำระเป็นเงินสด เมื่อมารับสินค้า | สถานที่นัดรับ: ตลาดนัดมหาวิทยาลัยอุบลราชธานี - รับได้เฉพาะวันศุกร์] - [เข้าระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม: https://greenmarketubon.com/login]"; //message max 1,000 character

        // $this->line_notify($token, $str);

    }

    // private function line_notify($Token, $message)
    // {
    //     $lineapi = $Token; // ใส่ token key ที่ได้มา
    //     $mms =  trim($message); // ข้อความที่ต้องการส่ง
    //     date_default_timezone_set("Asia/Bangkok");
    //     $chOne = curl_init(); 
    //     curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
    //     // SSL USE 
    //     curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
    //     curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
    //     //POST 
    //     curl_setopt( $chOne, CURLOPT_POST, 1); 
    //     curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms"); 
    //     curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
    //     $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
    //         curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
    //     curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
    //     $result = curl_exec( $chOne ); 
    //     //Check error 
    //     if(curl_error($chOne)) 
    //     { 
    //            echo 'error:' . curl_error($chOne); 
    //     } 
    //     else { 
    //     $result_ = json_decode($result, true); 
    //        echo "status : ".$result_['status']; echo " message : ". $result_['message'];
    //         } 
    //     curl_close( $chOne );   
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $fronthomes = FrontHome::all();

        // dd($fronthomes);

        return view('fronthome.create', compact('fronthomes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FrontHome  $frontHome
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $products = DB::table('users')->where('name', 'John')->first();
        // return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FrontHome  $frontHome
     * @return \Illuminate\Http\Response
     */
    public function edit(FrontHome $frontHome)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FrontHome  $frontHome
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fronthome = FrontHome::findOrFail($id);
        
        
        if($image = $request->file('photo_path')){
            $data = $request->all();

            $nameFile['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('garo/assets/img/slide1');

            $image->move($destinationPath, $nameFile['imagename']);

            $data['photo_path'] = '/garo/assets/img/slide1/'. $nameFile['imagename'];

            // return $data;
            $fronthome->update($data);
            // $fronthome->update($request->photo_path);
            $request->session()->flash('fronthome_update_meassage', 'แก้ไขรูปภาพสไลด์สำเสร็จ');

        }elseif($request->type=='slider_text'){
            $fronthome->update($request->all());
            $request->session()->flash('fronthome_update_meassage', 'แก้ไขข้อความบนสไลด์สำเสร็จ');
        }else{
            // dd($request->all());
            $fronthome->update($request->all());
            $request->session()->flash('fronthome_update_meassage', 'แก้ไขเนื้อหาใต้สไลด์สำเสร็จ');
        }

        

        

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FrontHome  $frontHome
     * @return \Illuminate\Http\Response
     */
    public function destroy(FrontHome $frontHome)
    {
        //
    }
}
