<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Province;
use App\District;
use App\SubDistrict;
use DB;
use Socialite;
use Illuminate\Support\Facades\Hash;
// use App\Customer;


class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
        $this->middleware('auth',['except'=>['register','handleProviderCallback','handleProviderCallbackGoogle','redirectToProvider','redirectToProviderGoogle','addRegister', 'addLitleRegister']]);
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customerRepository->pushCriteria(new RequestCriteria($request));
        $customers = $this->customerRepository->all();

        return view('customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        $provice = Province::pluck('name','id')->all();
        return view('customers.create')->with(compact('provice'));
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();
        $input['code'] = $this->getCode();
        $customer = $this->customerRepository->create($input);

        Flash::success('บันทึกข้อมูลลูกค้าสำเร็จ.');

        return redirect(route('customers.index'));
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);
        // dd($customer);

        if (empty($customer)) {
            Flash::error('ไม่พบข้อมูลลูกค้านี้.');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customer', $customer)->with('controller',$this);
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);
        $provice = Province::pluck('name','id')->all();

        // $district = Province::pluck('name','id')->all();
        $district = District::where("province_id",$customer->province_id)
                    ->pluck("name","id")->all();

        $sub_district = SubDistrict::where("district_id",$customer->district_id)
                        ->pluck("name","id")->all();

        // dd($provice);            
        // dd($district);
        if (empty($customer)) {
            Flash::error('ไม่พบข้อมูลลูกค้านี้.');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')->with('customer', $customer)->with(compact('provice', 'district', 'sub_district'));

        // return view('customers.edit', compact('customer', 'provice'));
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('ไม่พบข้อมูลลูกค้านี้.');

            return redirect(route('customers.index'));
        }

        $customer = $this->customerRepository->update($request->all(), $id);

        Flash::success('แก้ไขข้อมูลลูกค้าสำเร็จ.');

        return redirect(route('customers.index'));
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('ไม่พบข้อมูลลูกค้า');

            return redirect(route('customers.index'));
        }

        $this->customerRepository->delete($id);

        Flash::success('ลบข้อมูลลูกค้าสำเร็จ');

        return redirect(route('customers.index'));
    }

    public function register(){
        // $this->middleware('guest');
        $provice = Province::pluck('name','id')->all();
        return view('front.customer.register')->with(compact('provice'));
    }

    private function getCode(){
        $dataCode = DB::table('customers')->max('code');
        $dataTest = ($dataCode+1);
        $val = sprintf("%05d",$dataTest);
        return $val;
    }

    public function testFacebook(){
        return view('customers.test_facebook');
    }

    public function testPost(Request $request){
      $data = $request->all();

        print_r($data);exit;
    }

    public function redirectToProvider()
    {
        // return 'It\' works';
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $datas = Socialite::driver('facebook')->stateless()->user();
        print_r($datas);exit;
        $Name = explode(' ',$datas->name);
        $signData = array('firstName'=>$Name[0],'lastName'=>$Name[1],'id'=>$datas->id,'email'=>$datas->email,'picture'=>$datas->avatar_original,'profile'=>$datas->profileUrl);
        return redirect()->action('CustomerController@register')->with('signData',$signData);

        // $test = 'name :: '.$datas->name .'<br> ID:: '.$datas->id.'<br> Email:: '.$datas->email.'<br> picture::'.$datas->avatar_original;
        // $test .= '<br> gender ::'.$datas->user['gender'].'<br> Profile::'.$datas->profileUrl;
        // return $test;
        // $user->token;
    }

    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallbackGoogle()
    {
        $datas = Socialite::driver('google')->stateless()->user();
        // print_r($datas);exit;
        // $test = 'name :: '.$datas->name .'<br> ID:: '.$datas->id.'<br> Email:: '.$datas->email.'<br> picture::'.$datas->avatar_original;
        $Name = explode(' ',$datas->name);
        $signData = array('firstName'=>$Name[0],'lastName'=>$Name[1],'id'=>$datas->id,'email'=>$datas->email,'picture'=>$datas->avatar_original);
        return redirect()->action('CustomerController@register')->with('signData',$signData);
        // return redirect('customer/register')->with(compact('provice','signData'));
        // return view('customers.register')->with(compact('provice','signData'));
        // return $test;
        // $user->token;
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function addRegister(CreateCustomerRequest $request)
    {
      $input = $request->all();

      //change incase customer don't pass email will use tel instead 
      $email = $input['email'];
      $txtAleartUser = 'Email ที่ลงทะเบียน';

      if($email===null) {
        $email = $input['tel'];
        //pass tel email insteal for use login 
        $input['email'] = $input['tel'];

        $txtAleartUser = 'เบอร์โทรศัพท์ที่ลงทะเบียน';
      }

      $getcustomer = DB::table('customers')->where('email', $email)->first();
      

      // dd($input['email']);

      if(!$getcustomer){
        // dd($input['email']);
        $input['code'] = $this->getCode();
        // dd($input['tel']);
        $input['password'] = bcrypt($input['tel']); //$input['password'] = bcrypt($input['password']);
        $customer = $this->customerRepository->create($input);


        // return redirect()->route('member.store', ['username' => $customer->email, 'password' => $customer->tel,]);      

        //Here sam code in App\Http\Controllers\MemberController@store 
        //If change this code must change above function
        // $getcustomers = DB::table('customers')
        //             ->where('email',  $customer->email)
        //             ->orWhere('password', bcrypt($customer->tel))
        //             ->first();

        $getcustomers = DB::table('customers')->where([
                    ['email', '=', $customer->email],
                    // ['password', '=', bcrypt($customer->tel)],
                ])->first();
            
        if(Hash::check($customer->tel, $getcustomers->password)){
            session([
                'member_login' => true,
                'member_code' => $getcustomers->code,
                'member_email' => $getcustomers->email,
                'member_tel' => $getcustomers->tel
            ]);
        //Here same code in App\Http\Controllers\MemberController@store 
        //If change this code must change above function



            Flash::success('บันทึกข้อมูลลูกค้าสำเร็จ คุณสามารถเข้าใช้ระบบโดย [Username: '.$txtAleartUser.', Password: เบอร์โทรศัพท์ที่ลงทะเบียน].');            
        }else{
            Flash::error('เกิดความผิดพลาด กรุณาสมัครสมาชิกใหม่');
        }



      }else{
        Flash::error('ไม่สามารถลงทะเบียนได้เนื่องจาก email ซ้ำ');
      }
      

      // return redirect()->action('CustomerController@register');
      return redirect()->back()->withInput();
    }

    public function addLitleRegister(Request $request)
    {
        $input = $request->all();

        if(empty($input['first_name']) || empty($input['tel'])){
            // Flash::error('กรุณากรอกข้อมูล เพื่อให้เราติดต่อคุณได้!');
            session()->flash('addlitleregister_message', 'กรุณากรอกข้อมูล เพื่อให้เราติดต่อคุณได้!');
            return redirect()->back()->withInput();
        }

        if(!empty($input['email'])) {

         //validate email format
           if(!filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            // Flash::error('รูปแบบอีเมล์ไม่ถูกต้องกรุณาตรวจสอบ !');
            session()->flash('addlitleregister_message', 'รูปแบบอีเมล์ไม่ถูกต้องกรุณาตรวจสอบ !');
            return redirect()->back()->withInput();
          } 
        } else {
          $input['email'] = $input['tel'];
        }      



        //check duplicate email
        $customers = DB::table('customers')->where([
                    ['email', '=', $input['email']]
                    // ['password', '=', bcrypt($customer->tel)],
                ])->first();

        if($customers){
            //exist
            // dd($customers);
            session()->flash('addlitleregister_message', 'มีผู้ใช้งานกับอีเมล์นี้แล้ว. กรุณาเข้าระบบ !');
            return redirect()->back()->withInput();
        }else{
            //not exist
            // return 'don\'t exist';
            $input['code'] = $this->getCode();
            $input['password'] = bcrypt($input['tel']); //$input['password'] = bcrypt($input['password']);
            $customer = $this->customerRepository->create($input);


            $getcustomers = DB::table('customers')->where([
                        ['email', '=', $customer->email],
                        // ['password', '=', bcrypt($customer->tel)],
                    ])->first();
                
            if(Hash::check($customer->tel, $getcustomers->password)){
                session([
                    'member_login' => true,
                    'member_code' => $getcustomers->code,
                    'member_email' => $getcustomers->email,
                    'member_tel' => $getcustomers->tel
                ]);
            //Here same code in App\Http\Controllers\MemberController@store 
            //If change this code must change above function
            

                // Flash::success('บันทึกข้อมูลลูกค้าสำเร็จ คุณสามารถเข้าใช้ระบบโดย [Username: Email ที่ลงทะเบียน, Password: เบอร์โทรศัพท์ที่ลงทะเบียน].');   
                return redirect(route('basket.checkout', ['basketKey' => session('member_basket_key')]));     
                //return redirect(route('customers.index'));
            }else{
                // Flash::error('เกิดความผิดพลาด กรุณาสมัครสมาชิกใหม่');
                session()->flash('addlitleregister_message', 'เกิดความผิดพลาด กรุณาลองใหม่อีกครั้ง !');
                return redirect()->back()->withInput();
            }           

        }

        

    }


}
