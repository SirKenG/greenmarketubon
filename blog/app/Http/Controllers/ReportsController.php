<?php

namespace App\Http\Controllers;

use App\Reports;
use Illuminate\Http\Request;
use DB;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return 'it work!!';

        //Gender
        $sexmans = DB::table('customers')->where('sex', 'ชาย')->get();
        $sexwomans = DB::table('customers')->where('sex', 'หญิง')->get();
        $sexnonespecify = DB::table('customers')->where('sex', null)->get();

        $sumsexman = count($sexmans);
        $sumsexwoman = count($sexwomans);
        $sumsexnonespecify = count($sexnonespecify);



        //Age
        // $ages = DB::table('customers')->select('first_name', 'age')->get();
        // $ages = DB::table('customers')->havingRaw('COUNT(age) > 1')->get();
        $countAges = DB::table('customers')
                     ->select(DB::raw('count(*) as number_ages, age'))
                     // ->where('status', '<>', 1)
                     ->groupBy('age')
                     ->get();


        $labels_chartjs1 = "";
        $data_chartjs1 = "";
        foreach ($countAges as $key => $value) {
            # code...
            // echo $value->age;
            // if($value->age != $lastValue) {
            //     $lastValue = $value->age;
            //     echo '<br>';
            // }
            $labels_chartjs1 .= $value->number_ages.",";

            if($value->age==null) {
                $data_chartjs1 .= "ไม่ระบุ,";
            }else{
                $data_chartjs1 .= 'อายุ '.$value->age.",";
            }

        }



        $countJob = DB::table('customers')
                     ->select(DB::raw('count(*) as number_job, job_work'))
                     // ->where('status', '<>', 1)
                     ->groupBy('job_work')
                     ->get();
        // dd($countJob);
        $job_work = "";
        $Countjob_work = "";
        foreach ($countJob as $key => $value) {

            $Countjob_work .= $value->number_job.",";

            if($value->job_work==null) {
                $job_work .= "ไม่ระบุ,";
            }else{
                $job_work .= $value->job_work.",";
            }
        }

        // dd($countJob);
        // Get value group by product name
        $chart_sale_products = DB::table('basket')
             // ->select('product_id')
            ->join('produces', 'basket.product_id', '=', 'produces.id')
            ->select('basket.*', 'basket.id as basket_id', 'produces.*', 'produces.id as produces_id')
            // ->selectRaw('*')
            ->where([
                ['reservation', '=', 0],
                ['pay', '!=', NULL],
                ['status_buy', '=', 2],
            ])
            ->get()
            ->groupBy('product_id');

        // Receive collection for map
        // $collection = collect($chart_sale_products)->map(function ($id) {
        //     return ($id);
        // })
        // ->reject(function ($id) {
        //     return empty($id);
        // });

        // print_r($collection);
        // spread from conllection
        $collapsed = $chart_sale_products->collapse();

        // print_r($collapsed->all());
        // $collapsed_foreach = $collapsed->all();;
        

        $sum_collapsed = $collapsed->groupBy('name')->map(function ($row) {
            return $row->sum('qty');
        });

        // 
        $products_arr= $sum_collapsed->all();
        //sort descending array
        arsort($products_arr);
        // print_r($products_arr);
        // limit show top 5 qty
        $slice_product = array_slice($products_arr, 0, 5);


        // print_r($slice_product);
        // echo '<br>=======================================================================================<br>';
        $lables_productName = "";
        $lables_qtyProductName = "";
        foreach ($slice_product as $key => $value) {
            // echo 'key: '.$key.' value: '.$value.'<br>';
            $lables_productName .= $key.",";
            $lables_qtyProductName .= $value.",";
        }

        // echo '<br>=======================================================================================<br>';
        // dd($chart_sale_products);(`reservation` = 0 and `pay` is not null and `status_buy` = 2)



        $data_chartjs1 = rtrim($data_chartjs1, ',');
        $labels_chartjs1 = rtrim($labels_chartjs1, ',');


        $job_work = rtrim($job_work, ',');
        $Countjob_work = rtrim($Countjob_work, ',');


        $labels_productName = rtrim($lables_productName, ',');
        $qty_productName = rtrim($lables_qtyProductName, ',');

        // $labels_chartjs1 = 17;

        // print_r($arrAges);
        // echo
        // echo 'lable: '.$labels_chartjs1;
        // echo '<br>';
        // echo 'age: '.$data_chartjs1;
        // dd($countAges);
        // return count($sumsexnonespecify);
        // dd($sumsexman);
        return view('reports.index')->with(compact('sumsexman', 'sumsexwoman', 'sumsexnonespecify', 'labels_chartjs1', 'data_chartjs1', 'job_work', 'Countjob_work', 'labels_productName', 'qty_productName'));
    }

    public function bestsaler()
    {

        $collection_saler_products = DB::table('basket')
            ->join('produces', 'basket.product_id', '=', 'produces.id')
            ->join('customers', 'basket.customer_code', '=', 'customers.code')
            ->select('basket.*', 'basket.id as basket_id', 'basket.updated_at as updated_at_basket', 'produces.*', 'produces.updated_at as updated_at_produces', 'produces.id as produces_id', 'customers.first_name', 'customers.last_name', 'customers.nick_name')
            ->where([
                ['reservation', '=', 0],
                ['pay', '!=', NULL],
                ['status_buy', '=', 2],
            ])
            ->get()
            ->groupBy('product_id');

        // $map_saler_products = collect($collection_saler_products)->map(function ($id) {
        //     return ($id);
        // })
        // ->reject(function ($id) {
        //     return empty($id);
        // });

        $collapsed_saler_products = $collection_saler_products->collapse();

        // echo '<pre>';
        // print_r($map_saler_products);
        // echo '</pre>';
        // dd($collapsed_saler_products);



        return view('reports.bestsaler', compact('collapsed_saler_products'));
    }


    public function google()
    {
        return view('reports.google');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function show(Reports $reports)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function edit(Reports $reports)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reports $reports)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reports $reports)
    {
        //
    }
}
