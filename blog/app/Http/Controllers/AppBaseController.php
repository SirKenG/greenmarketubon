<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;
use Response;
use App\District;
use App\SubDistrict;
use App\Province;
// use DB;
/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function getDistrict($id)
    {
        $cities = District::where("province_id",$id)
                    ->pluck("name","id");
        return json_encode($cities);
    }

    public function getSubDistrict($id)
    {
        $subDistrict = SubDistrict::where("district_id",$id)
                    ->pluck("name","id");
        return json_encode($subDistrict);
    }

    public function showProvince($id){
      $data =  Province::select('name')->where('id',$id)->first();
      // return $data->name;
      // dd($data->name);
      if(!is_null($data)){
        return $data->name;
      }else{
        return '';
      }
      
    }

    public function showDistrict($id){
      $data =  District::select('name')->where('id',$id)->first();
      if(!is_null($data)){
        return $data->name;
      }else{
        return '';
      }
    }

    public function showSubDistrict($id){
      $data =  SubDistrict::select('name')->where('id',$id)->first();
      if(!is_null($data)){
        return $data->name;
      }else{
        return '';
      }
    }

}
