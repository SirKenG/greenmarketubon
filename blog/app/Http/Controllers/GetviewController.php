<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GetviewController extends Controller
{
    public function gmenu_logoutin_d()
    {
        return view('getview.gmenu_logoutin_d');
    }
    public function gmenu_logoutin_m()
    {
        return view('getview.gmenu_logoutin_m');
    }

    public function gmenu_historybuy_d()
    {
        return view('getview.gmenu_historybuy_d');

    }
    public function gmenu_historybuy_m()
    {
        return view('getview.gmenu_historybuy_m');
    }

    // public function gfooter_line1_d()
    // {

    // }
    // public function gfooter_line1_m()
    // {
        
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
