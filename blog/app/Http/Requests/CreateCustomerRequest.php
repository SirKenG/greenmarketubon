<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Customer;

class CreateCustomerRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Customer::$rules;
    }

    public function messages() {
        return [
          'first_name.required' => 'กรุณากรอกชื่อ.',
          // 'last_name.required' => 'กรุณากรอกนามสกุล.',
          // 'age.required' => 'กรุณากรอกอายุ.',
          // 'province_id.required' => 'กรุณาเลือกจังหวัด',
          // 'district_id.required' => 'กรุณาเลือกอำเภอ',
          // 'sub_district_id.required' => 'กรุณาเลือกอำเภอ',
          // 'zipcode.required' => 'กรุณากรอกรหัสไปรษณีย์',
          'tel.required' => 'กรุณากรอกเบอร์โทรศัพท์',
          'email.required' => 'กรุณากรอก Email',
        ];
    }
}
