<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectInformation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'information';
    

    protected $fillable = [
    	'title',
    	'body'

    ];
}
