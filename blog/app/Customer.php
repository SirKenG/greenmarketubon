<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App
 * @version July 28, 2017, 7:00 am UTC
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'first_name',
        'last_name',
        'nick_name',
        'job_work',
        'sex',
        'age',
        'address_no',
        'address_moo',
        'address_soi',
        'address_road',
        'province_id',
        'district_id',
        'sub_district_id',
        'zipcode',
        'tel',
        'email',
        'password',
        'facebook',
        'line',
        'send_produce_transfer',
        'lat_long'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'code' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'nick_name' => 'string',
        'job_work' => 'string',
        'sex' => 'string',
        'age' => 'string',
        'address_no' => 'string',
        'address_moo' => 'string',
        'address_soi' => 'string',
        'address_road' => 'string',
        'district_id' => 'integer',
        'sub_district_id' => 'integer',
        'zipcode' => 'string',
        'tel' => 'string',
        'email' => 'string',
        'password' => 'string',
        'facebook' => 'string',
        'line' => 'string',
        'send_produce_transfer' => 'string',
        'lat_long' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        // 'last_name' => 'required',
        // 'nick_name' => 'required',
        // 'job_work' => 'required',
        // 'sex' => 'required',
        // 'age' => 'required',
        // 'province_id' => 'required',
        // 'district_id' => 'required',
        // 'sub_district_id' => 'required',
        // 'zipcode' => 'required',
        'tel' => 'required',
        // 'email' => 'required',
        // 'facebook' => 'required',
        // 'line' => 'required',
        // 'send_produce_transfer' => 'required'
    ];

}
