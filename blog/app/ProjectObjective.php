<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectObjective extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'objective';
    

    protected $fillable = [
    	'title',
    	'body'

    ];


}
