<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'basket';
    

    protected $fillable = [
    	'basket_key',
    	'reservation',
    	'product_id',
    	'qty',
        'customer_code',
        'status_buy',

    ];

}
