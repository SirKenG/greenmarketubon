<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontHome extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'front_home';
    

    protected $fillable = [
    	'type',
    	'message',
        'message2',
    	'photo_path'

    ];
}
