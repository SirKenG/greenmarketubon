<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\ProduceType;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $product_types = ProduceType::all();
     // dd($product_type);
        view()->share('product_types', $product_types);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
