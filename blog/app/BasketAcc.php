<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasketAcc extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'basket_acc';
    

    protected $fillable = [
    	'title',
    	'body'

    ];
}
