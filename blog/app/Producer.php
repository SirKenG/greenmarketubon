<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'producer';
    

    protected $fillable = [
    	'title',
    	'body',
    	'photo_path'
    ];
}
