<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductInformation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'productinformation';
    

    protected $fillable = [
    	'title',
    	'body'

    ];
}
