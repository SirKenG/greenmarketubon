<?php

namespace App\Repositories;

use App\Customer;
use InfyOm\Generator\Common\BaseRepository;

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'first_name',
        'last_name',
        'nick_name',
        'job_work',
        'sex',
        'age',
        'address_no',
        'address_moo',
        'address_soi',
        'address_road',
        'province_id',
        'district_id',
        'sub_district_id',
        'zipcode',
        'tel',
        'email',
        'password',
        'facebook',
        'line',
        'send_produce_transfer'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
