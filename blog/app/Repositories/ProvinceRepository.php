<?php

namespace App\Repositories;

use App\Province;
use InfyOm\Generator\Common\BaseRepository;

class ProvinceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'name_eng',
        'region_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Province::class;
    }
}
