<?php

namespace App\Repositories;

use App\SubDistrict;
use InfyOm\Generator\Common\BaseRepository;

class SubDistrictRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'name_eng',
        'region_id',
        'province_id',
        'district_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubDistrict::class;
    }
}
