<?php

namespace App\Repositories;

use App\Zipcode;
use InfyOm\Generator\Common\BaseRepository;

class ZipcodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'district_id',
        'zipcode'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Zipcode::class;
    }
}
