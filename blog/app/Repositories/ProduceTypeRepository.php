<?php

namespace App\Repositories;

use App\ProduceType;
use InfyOm\Generator\Common\BaseRepository;

class ProduceTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProduceType::class;
    }
}
