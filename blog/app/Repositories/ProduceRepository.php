<?php

namespace App\Repositories;

use App\Produce;
use InfyOm\Generator\Common\BaseRepository;

class ProduceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'produce_type_id',
        'name',
        'detail'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Produce::class;
    }
}
