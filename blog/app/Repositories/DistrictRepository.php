<?php

namespace App\Repositories;

use App\District;
use InfyOm\Generator\Common\BaseRepository;

class DistrictRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'name',
        'name_eng',
        'region_id',
        'province_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return District::class;
    }
}
