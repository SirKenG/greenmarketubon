<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logistic extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'logistic';
    

    protected $fillable = [
    	'title',
    	'body'

    ];}
