<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SubDistrict
 * @package App
 * @version July 28, 2017, 6:46 am UTC
 */
class SubDistrict extends Model
{
    use SoftDeletes;

    public $table = 'sub_districts';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'name',
        'name_eng',
        'region_id',
        'province_id',
        'district_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_eng' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
