<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProduceType
 * @package App
 * @version July 23, 2017, 10:34 am UTC
 */
class ProduceType extends Model
{
    use SoftDeletes;

    public $table = 'produce_types';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public static function getList(){
        $data = ProduceType::pluck('name','id');
        return $data;
    }

}
