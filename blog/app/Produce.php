<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Produce
 * @package App
 * @version July 23, 2017, 11:23 am UTC
 */
class Produce extends Model
{
    use SoftDeletes;

    public $table = 'produces';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'produce_type_id',
        'name',
        'detail',
        'body',
        'photo_path',
        'prime',
        'unit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'produce_type_id' => 'integer',
        'name' => 'string',
        'detail' => 'string',
        'body' => 'string',
        'prime' => 'integer',
        'unit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'produce_type_id' => 'required',
        'name' => 'required'
    ];


        public static function showProduceType($id){
            $data = ProduceType::find($id);
            return $data->name;
        }


}
