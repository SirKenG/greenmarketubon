<?php

namespace App;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Zipcode
 * @package App
 * @version July 28, 2017, 6:47 am UTC
 */
class Zipcode extends Model
{
    use SoftDeletes;

    public $table = 'zipcodes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'district_id',
        'zipcode'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'zipcode' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
