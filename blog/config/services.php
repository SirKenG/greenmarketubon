<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_APP_ID', '1965172543750640'),
        'client_secret' => env('FACEBOOK_SECRET_KEY', 'bdee2ae404b0d27c8c212eac810020f7'),
        'redirect' => env('FACEBOOK_REDIRECT', 'http://localhost:8000/customer/facebook/callback'),
    ],
    /*    'facebook' => [
        'client_id' => env('FACEBOOK_APP_ID', '390943148020020'),
        'client_secret' => env('FACEBOOK_SECRET_KEY', '45829ef5a8f5ac3f7405a7b1da7ba305'),
        'redirect' => env('FACEBOOK_REDIRECT', 'https://greenmarketubon.com/customer/facebook/callback'),
    ],*/
    'google' => [
        'client_id' => env('GOOGLE_APP_ID','67598719540-qoq7solj78e503377se2cf12vtcp11q4.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_SECRET_KEY','0X6txIzxfCV1OIr8KHSrvw9I'),
        'redirect' => env('GOOGLE_REDIRECT','http://localhost:8000/customer/google/callback'),
    ],

];
