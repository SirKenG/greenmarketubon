<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProduceApiTest extends TestCase
{
    use MakeProduceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProduce()
    {
        $produce = $this->fakeProduceData();
        $this->json('POST', '/api/v1/produces', $produce);

        $this->assertApiResponse($produce);
    }

    /**
     * @test
     */
    public function testReadProduce()
    {
        $produce = $this->makeProduce();
        $this->json('GET', '/api/v1/produces/'.$produce->id);

        $this->assertApiResponse($produce->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProduce()
    {
        $produce = $this->makeProduce();
        $editedProduce = $this->fakeProduceData();

        $this->json('PUT', '/api/v1/produces/'.$produce->id, $editedProduce);

        $this->assertApiResponse($editedProduce);
    }

    /**
     * @test
     */
    public function testDeleteProduce()
    {
        $produce = $this->makeProduce();
        $this->json('DELETE', '/api/v1/produces/'.$produce->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/produces/'.$produce->id);

        $this->assertResponseStatus(404);
    }
}
