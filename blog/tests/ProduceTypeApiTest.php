<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProduceTypeApiTest extends TestCase
{
    use MakeProduceTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateProduceType()
    {
        $produceType = $this->fakeProduceTypeData();
        $this->json('POST', '/api/v1/produceTypes', $produceType);

        $this->assertApiResponse($produceType);
    }

    /**
     * @test
     */
    public function testReadProduceType()
    {
        $produceType = $this->makeProduceType();
        $this->json('GET', '/api/v1/produceTypes/'.$produceType->id);

        $this->assertApiResponse($produceType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateProduceType()
    {
        $produceType = $this->makeProduceType();
        $editedProduceType = $this->fakeProduceTypeData();

        $this->json('PUT', '/api/v1/produceTypes/'.$produceType->id, $editedProduceType);

        $this->assertApiResponse($editedProduceType);
    }

    /**
     * @test
     */
    public function testDeleteProduceType()
    {
        $produceType = $this->makeProduceType();
        $this->json('DELETE', '/api/v1/produceTypes/'.$produceType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/produceTypes/'.$produceType->id);

        $this->assertResponseStatus(404);
    }
}
