<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ZipcodeApiTest extends TestCase
{
    use MakeZipcodeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateZipcode()
    {
        $zipcode = $this->fakeZipcodeData();
        $this->json('POST', '/api/v1/zipcodes', $zipcode);

        $this->assertApiResponse($zipcode);
    }

    /**
     * @test
     */
    public function testReadZipcode()
    {
        $zipcode = $this->makeZipcode();
        $this->json('GET', '/api/v1/zipcodes/'.$zipcode->id);

        $this->assertApiResponse($zipcode->toArray());
    }

    /**
     * @test
     */
    public function testUpdateZipcode()
    {
        $zipcode = $this->makeZipcode();
        $editedZipcode = $this->fakeZipcodeData();

        $this->json('PUT', '/api/v1/zipcodes/'.$zipcode->id, $editedZipcode);

        $this->assertApiResponse($editedZipcode);
    }

    /**
     * @test
     */
    public function testDeleteZipcode()
    {
        $zipcode = $this->makeZipcode();
        $this->json('DELETE', '/api/v1/zipcodes/'.$zipcode->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/zipcodes/'.$zipcode->id);

        $this->assertResponseStatus(404);
    }
}
