<?php

use Faker\Factory as Faker;
use App\ProduceType;
use App\Repositories\ProduceTypeRepository;

trait MakeProduceTypeTrait
{
    /**
     * Create fake instance of ProduceType and save it in database
     *
     * @param array $produceTypeFields
     * @return ProduceType
     */
    public function makeProduceType($produceTypeFields = [])
    {
        /** @var ProduceTypeRepository $produceTypeRepo */
        $produceTypeRepo = App::make(ProduceTypeRepository::class);
        $theme = $this->fakeProduceTypeData($produceTypeFields);
        return $produceTypeRepo->create($theme);
    }

    /**
     * Get fake instance of ProduceType
     *
     * @param array $produceTypeFields
     * @return ProduceType
     */
    public function fakeProduceType($produceTypeFields = [])
    {
        return new ProduceType($this->fakeProduceTypeData($produceTypeFields));
    }

    /**
     * Get fake data of ProduceType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProduceTypeData($produceTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $produceTypeFields);
    }
}
