<?php

use Faker\Factory as Faker;
use App\Customer;
use App\Repositories\CustomerRepository;

trait MakeCustomerTrait
{
    /**
     * Create fake instance of Customer and save it in database
     *
     * @param array $customerFields
     * @return Customer
     */
    public function makeCustomer($customerFields = [])
    {
        /** @var CustomerRepository $customerRepo */
        $customerRepo = App::make(CustomerRepository::class);
        $theme = $this->fakeCustomerData($customerFields);
        return $customerRepo->create($theme);
    }

    /**
     * Get fake instance of Customer
     *
     * @param array $customerFields
     * @return Customer
     */
    public function fakeCustomer($customerFields = [])
    {
        return new Customer($this->fakeCustomerData($customerFields));
    }

    /**
     * Get fake data of Customer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCustomerData($customerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'first_name' => $fake->word,
            'last_name' => $fake->word,
            'nick_name' => $fake->word,
            'job_work' => $fake->word,
            'sex' => $fake->word,
            'age' => $fake->word,
            'address_no' => $fake->word,
            'address_moo' => $fake->word,
            'address_soi' => $fake->word,
            'address_road' => $fake->word,
            'province_id' => $fake->word,
            'district_id' => $fake->randomDigitNotNull,
            'sub_district_id' => $fake->randomDigitNotNull,
            'zipcode' => $fake->word,
            'tel' => $fake->word,
            'email' => $fake->word,
            'facebook' => $fake->word,
            'line' => $fake->word,
            'send_produce_transfer' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $customerFields);
    }
}
