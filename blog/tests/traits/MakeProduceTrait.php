<?php

use Faker\Factory as Faker;
use App\Produce;
use App\Repositories\ProduceRepository;

trait MakeProduceTrait
{
    /**
     * Create fake instance of Produce and save it in database
     *
     * @param array $produceFields
     * @return Produce
     */
    public function makeProduce($produceFields = [])
    {
        /** @var ProduceRepository $produceRepo */
        $produceRepo = App::make(ProduceRepository::class);
        $theme = $this->fakeProduceData($produceFields);
        return $produceRepo->create($theme);
    }

    /**
     * Get fake instance of Produce
     *
     * @param array $produceFields
     * @return Produce
     */
    public function fakeProduce($produceFields = [])
    {
        return new Produce($this->fakeProduceData($produceFields));
    }

    /**
     * Get fake data of Produce
     *
     * @param array $postFields
     * @return array
     */
    public function fakeProduceData($produceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'produce_type_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'detail' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $produceFields);
    }
}
