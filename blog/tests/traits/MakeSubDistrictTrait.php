<?php

use Faker\Factory as Faker;
use App\SubDistrict;
use App\Repositories\SubDistrictRepository;

trait MakeSubDistrictTrait
{
    /**
     * Create fake instance of SubDistrict and save it in database
     *
     * @param array $subDistrictFields
     * @return SubDistrict
     */
    public function makeSubDistrict($subDistrictFields = [])
    {
        /** @var SubDistrictRepository $subDistrictRepo */
        $subDistrictRepo = App::make(SubDistrictRepository::class);
        $theme = $this->fakeSubDistrictData($subDistrictFields);
        return $subDistrictRepo->create($theme);
    }

    /**
     * Get fake instance of SubDistrict
     *
     * @param array $subDistrictFields
     * @return SubDistrict
     */
    public function fakeSubDistrict($subDistrictFields = [])
    {
        return new SubDistrict($this->fakeSubDistrictData($subDistrictFields));
    }

    /**
     * Get fake data of SubDistrict
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSubDistrictData($subDistrictFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'name' => $fake->word,
            'name_eng' => $fake->word,
            'region_id' => $fake->word,
            'province_id' => $fake->word,
            'district_id' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $subDistrictFields);
    }
}
