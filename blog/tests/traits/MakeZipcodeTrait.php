<?php

use Faker\Factory as Faker;
use App\Zipcode;
use App\Repositories\ZipcodeRepository;

trait MakeZipcodeTrait
{
    /**
     * Create fake instance of Zipcode and save it in database
     *
     * @param array $zipcodeFields
     * @return Zipcode
     */
    public function makeZipcode($zipcodeFields = [])
    {
        /** @var ZipcodeRepository $zipcodeRepo */
        $zipcodeRepo = App::make(ZipcodeRepository::class);
        $theme = $this->fakeZipcodeData($zipcodeFields);
        return $zipcodeRepo->create($theme);
    }

    /**
     * Get fake instance of Zipcode
     *
     * @param array $zipcodeFields
     * @return Zipcode
     */
    public function fakeZipcode($zipcodeFields = [])
    {
        return new Zipcode($this->fakeZipcodeData($zipcodeFields));
    }

    /**
     * Get fake data of Zipcode
     *
     * @param array $postFields
     * @return array
     */
    public function fakeZipcodeData($zipcodeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'district_id' => $fake->word,
            'zipcode' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $zipcodeFields);
    }
}
