<?php

use App\Models\Produce;
use App\Repositories\ProduceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProduceRepositoryTest extends TestCase
{
    use MakeProduceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProduceRepository
     */
    protected $produceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->produceRepo = App::make(ProduceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProduce()
    {
        $produce = $this->fakeProduceData();
        $createdProduce = $this->produceRepo->create($produce);
        $createdProduce = $createdProduce->toArray();
        $this->assertArrayHasKey('id', $createdProduce);
        $this->assertNotNull($createdProduce['id'], 'Created Produce must have id specified');
        $this->assertNotNull(Produce::find($createdProduce['id']), 'Produce with given id must be in DB');
        $this->assertModelData($produce, $createdProduce);
    }

    /**
     * @test read
     */
    public function testReadProduce()
    {
        $produce = $this->makeProduce();
        $dbProduce = $this->produceRepo->find($produce->id);
        $dbProduce = $dbProduce->toArray();
        $this->assertModelData($produce->toArray(), $dbProduce);
    }

    /**
     * @test update
     */
    public function testUpdateProduce()
    {
        $produce = $this->makeProduce();
        $fakeProduce = $this->fakeProduceData();
        $updatedProduce = $this->produceRepo->update($fakeProduce, $produce->id);
        $this->assertModelData($fakeProduce, $updatedProduce->toArray());
        $dbProduce = $this->produceRepo->find($produce->id);
        $this->assertModelData($fakeProduce, $dbProduce->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProduce()
    {
        $produce = $this->makeProduce();
        $resp = $this->produceRepo->delete($produce->id);
        $this->assertTrue($resp);
        $this->assertNull(Produce::find($produce->id), 'Produce should not exist in DB');
    }
}
