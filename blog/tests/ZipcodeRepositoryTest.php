<?php

use App\Models\Zipcode;
use App\Repositories\ZipcodeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ZipcodeRepositoryTest extends TestCase
{
    use MakeZipcodeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ZipcodeRepository
     */
    protected $zipcodeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->zipcodeRepo = App::make(ZipcodeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateZipcode()
    {
        $zipcode = $this->fakeZipcodeData();
        $createdZipcode = $this->zipcodeRepo->create($zipcode);
        $createdZipcode = $createdZipcode->toArray();
        $this->assertArrayHasKey('id', $createdZipcode);
        $this->assertNotNull($createdZipcode['id'], 'Created Zipcode must have id specified');
        $this->assertNotNull(Zipcode::find($createdZipcode['id']), 'Zipcode with given id must be in DB');
        $this->assertModelData($zipcode, $createdZipcode);
    }

    /**
     * @test read
     */
    public function testReadZipcode()
    {
        $zipcode = $this->makeZipcode();
        $dbZipcode = $this->zipcodeRepo->find($zipcode->id);
        $dbZipcode = $dbZipcode->toArray();
        $this->assertModelData($zipcode->toArray(), $dbZipcode);
    }

    /**
     * @test update
     */
    public function testUpdateZipcode()
    {
        $zipcode = $this->makeZipcode();
        $fakeZipcode = $this->fakeZipcodeData();
        $updatedZipcode = $this->zipcodeRepo->update($fakeZipcode, $zipcode->id);
        $this->assertModelData($fakeZipcode, $updatedZipcode->toArray());
        $dbZipcode = $this->zipcodeRepo->find($zipcode->id);
        $this->assertModelData($fakeZipcode, $dbZipcode->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteZipcode()
    {
        $zipcode = $this->makeZipcode();
        $resp = $this->zipcodeRepo->delete($zipcode->id);
        $this->assertTrue($resp);
        $this->assertNull(Zipcode::find($zipcode->id), 'Zipcode should not exist in DB');
    }
}
