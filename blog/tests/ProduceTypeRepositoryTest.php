<?php

use App\Models\ProduceType;
use App\Repositories\ProduceTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProduceTypeRepositoryTest extends TestCase
{
    use MakeProduceTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProduceTypeRepository
     */
    protected $produceTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->produceTypeRepo = App::make(ProduceTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateProduceType()
    {
        $produceType = $this->fakeProduceTypeData();
        $createdProduceType = $this->produceTypeRepo->create($produceType);
        $createdProduceType = $createdProduceType->toArray();
        $this->assertArrayHasKey('id', $createdProduceType);
        $this->assertNotNull($createdProduceType['id'], 'Created ProduceType must have id specified');
        $this->assertNotNull(ProduceType::find($createdProduceType['id']), 'ProduceType with given id must be in DB');
        $this->assertModelData($produceType, $createdProduceType);
    }

    /**
     * @test read
     */
    public function testReadProduceType()
    {
        $produceType = $this->makeProduceType();
        $dbProduceType = $this->produceTypeRepo->find($produceType->id);
        $dbProduceType = $dbProduceType->toArray();
        $this->assertModelData($produceType->toArray(), $dbProduceType);
    }

    /**
     * @test update
     */
    public function testUpdateProduceType()
    {
        $produceType = $this->makeProduceType();
        $fakeProduceType = $this->fakeProduceTypeData();
        $updatedProduceType = $this->produceTypeRepo->update($fakeProduceType, $produceType->id);
        $this->assertModelData($fakeProduceType, $updatedProduceType->toArray());
        $dbProduceType = $this->produceTypeRepo->find($produceType->id);
        $this->assertModelData($fakeProduceType, $dbProduceType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteProduceType()
    {
        $produceType = $this->makeProduceType();
        $resp = $this->produceTypeRepo->delete($produceType->id);
        $this->assertTrue($resp);
        $this->assertNull(ProduceType::find($produceType->id), 'ProduceType should not exist in DB');
    }
}
