<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nick_name');
            $table->string('job_work');
            $table->string('sex');
            $table->string('age');
            $table->string('address_no');
            $table->string('address_moo');
            $table->string('address_soi');
            $table->string('address_road');
            $table->intreger('province_id');
            $table->integer('district_id');
            $table->integer('sub_district_id');
            $table->string('zipcode');
            $table->string('tel');
            $table->string('email');
            $table->string('facebook');
            $table->string('line');
            $table->string('send_produce_transfer');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
