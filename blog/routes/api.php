<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('users', 'UserAPIController');

Route::resource('produce_types', 'ProduceTypeAPIController');

Route::resource('produces', 'ProduceAPIController');

Route::resource('provinces', 'ProvinceAPIController');

Route::resource('districts', 'DistrictAPIController');

Route::resource('sub_districts', 'SubDistrictAPIController');

Route::resource('zipcodes', 'ZipcodeAPIController');

Route::resource('customers', 'CustomerAPIController');