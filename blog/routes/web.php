<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'frontHome.index', 'uses'=>'FrontHomeController@index']);

Auth::routes();



Route::get('producer/showall',array('as'=>'producer.showall','uses'=>'ProducerController@showall'));

Route::get('producer/{id}',array('as'=>'producer.showa','uses'=>'ProducerController@showa'));

/*
|
| Back-End
|
*/
Route::group(['middleware'=>'auth'], function(){
	Route::resource('frontHome', 'FrontHomeController');

	Route::resource('objective', 'ProjectObjectiveController');

	Route::resource('information', 'ProjectInformationController');

	Route::resource('productinformation', 'ProductInformationController');

	Route::resource('productvalue', 'ProductValueController');

	Route::resource('contactus', 'ContactUsController');

	Route::resource('payment', 'PaymentController');

	Route::resource('logistic', 'LogisticController');

	Route::get('basket/edit_acc', ['as'=>'basket.edit_acc', 'uses'=>'BasketController@edit_acc']);
	Route::patch('basket/update_acc/{id}', ['as'=>'basket.update_acc', 'uses'=>'BasketController@update_acc']); //



	// Route::get('producer/createa',array('as'=>'producer.createa','uses'=>'ProducerController@createa'));

	Route::resource('producer', 'ProducerController');



	Route::get('reports/google',array('as'=>'reports.google','uses'=>'ReportsController@google'));
	Route::get('reports/bestsaler',array('as'=>'reports.bestsaler','uses'=>'ReportsController@bestsaler'));
	Route::resource('reports', 'ReportsController');
});


// Route::get('/home', 'HomeController@index')->name('home');

Route::get('home/checkout_buy',array('as'=>'home.checkout_buy','uses'=>'HomeController@checkout_buy'));

Route::get('home/seemore_products/{id}',array('as'=>'home.seemore_products','uses'=>'HomeController@seemore_products')); //update_status_basket

Route::get('home/update_status_basket/{id}',array('as'=>'home.update_status_basket','uses'=>'HomeController@update_status_basket')); //history

Route::get('home/update_cart_products/{id}',array('as'=>'home.update_cart_products','uses'=>'HomeController@update_cart_products'));

Route::get('/history',array('as'=>'home.history','uses'=>'HomeController@history'));

Route::get('/home', array('as'=>'home.index','uses'=>'HomeController@index'));


Route::get('deleteimg/{id}', ['as'=>'users.deleteimg', 'uses'=>'UserController@deleteimg']); 
Route::resource('users', 'UserController');

Route::resource('produceTypes', 'ProduceTypeController');

Route::resource('produces', 'ProduceController');

// Route::resource('provinces', 'ProvinceController');

// Route::resource('districts', 'DistrictController');

// Route::resource('subDistricts', 'SubDistrictController');

// Route::resource('zipcodes', 'ZipcodeController');



Route::get('customer/district/{id}',array('as'=>'create.ajax','uses'=>'AppBaseController@getDistrict'));

Route::get('customer/subdistrict/{id}',array('as'=>'create.ajax','uses'=>'AppBaseController@getSubDistrict'));

Route::get('customer/testFacebook','CustomerController@testFacebook');

Route::post('customer/testPost',array('as'=>'testPost','uses'=>'CustomerController@testPost'));

Route::get('customer/facebook',array('as'=>'customer/facebook', 'uses'=>'CustomerController@redirectToProvider'));
Route::get('customer/facebook/callback',array('as'=>'customer/facebook/callback ','uses'=>'CustomerController@handleProviderCallback'));

Route::get('customer/google',array('as'=>'customer/google', 'uses'=>'CustomerController@redirectToProviderGoogle'));
Route::get('customer/google/callback',array('as'=>'customer/google/callback','uses'=>'CustomerController@handleProviderCallbackGoogle'));

Route::post('customer/addRegister',array('as'=>'customer/addRegister','uses'=>'CustomerController@addRegister'));

Route::post('customer/addLitleRegister',array('as'=>'customer.addLitleRegister','uses'=>'CustomerController@addLitleRegister'));

//Route::resource('producer', 'ProducerController');
Route::resource('customers', 'CustomerController');

/*
|
| Front-End
|
*/
// Route::get('/', function () {
//     // return view('welcome');
//     return view('front.home');
// });



// Route::get('/', function () {
// 		return 'It work!';
// 	});



Route::get('objective', ['as'=>'objective.index', 'uses'=>'ProjectObjectiveController@index']);

Route::get('information', ['as'=>'information.index', 'uses'=>'ProjectInformationController@index']);

Route::get('productinformation', ['as'=>'productinformation.index', 'uses'=>'ProductInformationController@index']);

Route::get('productvalue', ['as'=>'productvalue.index', 'uses'=>'ProductValueController@index']);

Route::get('contactus', ['as'=>'contactus.index', 'uses'=>'ContactUsController@index']);

Route::get('payment', ['as'=>'payment.index', 'uses'=>'PaymentController@index']);

Route::get('logistic', ['as'=>'logistic.index', 'uses'=>'LogisticController@index']);

// Route::get('customer/register', 'CustomerController@register');
Route::get('customer/register', ['as'=>'customer.register', 'uses'=>'CustomerController@register']);

// Route::get('customers/{id}/edit', ['as'=>'customers.edit', 'uses'=>'CustomerController@edit']);

Route::get('member/forgotpassword', ['as'=>'member.forgotpassword', 'uses'=>'MemberController@forgotpassword']);
Route::post('member/sentemailforgotpassword', ['as'=>'member.sentemailforgotpassword', 'uses'=>'MemberController@sentemailforgotpassword']);
Route::resource('member', 'MemberController');

//add a route to that method separately, before you register the resource:
//For Basket
Route::get('basket/getBasket', ['as'=>'basket.getBasket', 'uses'=>'BasketController@getBasket']);
Route::get('basket/getexistbasket', ['as'=>'basket.getexistbasket', 'uses'=>'BasketController@getexistbasket']);
Route::get('basket/addBasket', ['as'=>'basket.addBasket', 'uses'=>'BasketController@addBasket']);
Route::get('basket/updateBasket', ['as'=>'basket.updateBasket', 'uses'=>'BasketController@updateBasket']);
Route::get('basket/addBasketByAdmin', ['as'=>'basket.addBasketByAdmin', 'uses'=>'BasketController@addBasketByAdmin']);
Route::get('basket/updateBasketQtyForCartPage', ['as'=>'basket.updateBasketQtyForCartPage', 'uses'=>'BasketController@updateBasketQtyForCartPage']);
Route::get('basket/updateBasketQtyForCartPageByAdmin', ['as'=>'basket.updateBasketQtyForCartPageByAdmin', 'uses'=>'BasketController@updateBasketQtyForCartPageByAdmin']);

Route::get('baskettype/{id}', ['as'=>'baskettype.showbaseontype', 'uses'=>'BasketController@showbaseontype']); 

Route::get('basket/removeProductFromBasket', ['as'=>'basket.removeProductFromBasket', 'uses'=>'BasketController@removeProductFromBasket']);
Route::get('basket/removeProductFromBasketByAdmin', ['as'=>'basket.removeProductFromBasketByAdmin', 'uses'=>'BasketController@removeProductFromBasketByAdmin']);

Route::get('basket/editCustomer', ['as'=>'basket.editCustomer', 'uses'=>'BasketController@editCustomer']);
Route::patch('basket/updateCustomer', ['as'=>'basket.updateCustomer', 'uses'=>'BasketController@updateCustomer']);
Route::patch('basket/updateCustomerMap', ['as'=>'basket.updateCustomerMap', 'uses'=>'BasketController@updateCustomerMap']);

//For card and checkout
Route::get('cart/{basketkey}', ['as'=>'basket.cart', 'uses'=>'BasketController@cart']);

Route::get('basket/checkout', ['as'=>'basket.checkout', 'uses'=>'BasketController@checkout']);

Route::get('basket/get_data_googlemap_api', ['as'=>'basket.get_data_googlemap_api', 'uses'=>'BasketController@get_data_googlemap_api']); 
Route::get('basket/googlemap_api', ['as'=>'basket.googlemap_api', 'uses'=>'BasketController@googlemap_api']); 


Route::get('basket/checkou', ['as'=>'basket.checkou', 'uses'=>'BasketController@checkou']); 

Route::post('basket/checkout_uploadfile', ['as'=>'basket.checkout_uploadfile', 'uses'=>'BasketController@checkout_uploadfile']); //

Route::post('basket/checkout_uploadfile_again', ['as'=>'basket.checkout_uploadfile_again', 'uses'=>'BasketController@checkout_uploadfile_again']);

Route::get('basket/checkout_getfile', ['as'=>'basket.checkout_getfile', 'uses'=>'BasketController@checkout_getfile']); 


Route::post('basket/checkout_buy', ['as'=>'basket.checkout_buy', 'uses'=>'BasketController@checkout_buy']); //status_checkout_buy
Route::get('basket/checkout_buyByAdmin',array('as'=>'basket.checkout_buyByAdmin','uses'=>'BasketController@checkout_buyByAdmin'));

Route::get('basket/status_checkout_buy', ['as'=>'basket.status_checkout_buy', 'uses'=>'BasketController@status_checkout_buy']); //seemore_products

Route::get('seemore_products/{id}', ['as'=>'basket.seemore_products', 'uses'=>'BasketController@seemore_products']); 

Route::resource('basket', 'BasketController');




Route::get('gmenu_logoutin_d', ['as'=>'getview.gmenu_logoutin_d', 'uses'=>'GetviewController@gmenu_logoutin_d']);
Route::get('gmenu_logoutin_m', ['as'=>'getview.gmenu_logoutin_m', 'uses'=>'GetviewController@gmenu_logoutin_m']); 

Route::get('gmenu_historybuy_d', ['as'=>'getview.gmenu_historybuy_d', 'uses'=>'GetviewController@gmenu_historybuy_d']);
Route::get('gmenu_historybuy_m', ['as'=>'getview.gmenu_historybuy_m', 'uses'=>'GetviewController@gmenu_historybuy_m']);

// Route::get('gfooter_line1_d', ['as'=>'getview.gfooter_line1_d', 'uses'=>'GetviewController@gfooter_line1_d']);
// Route::get('gfooter_line1_m', ['as'=>'getview.gfooter_line1_m', 'uses'=>'GetviewController@gfooter_line1_m']);
Route::resource('getview', 'GetviewController');


// Route::get('member/login', ['as'=>'member.login', 'uses'=>'MemberController@index']); //Request $request
// Route::post('member/store', ['as'=>'member.store', 'uses'=>'MemberController@store']);



// Route::get('basket', ['as'=>'basket.index', 'uses'=>'BasketController@index']);






















